<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('m_payment','',TRUE);
		$this->load->library('email');
	}
	public $sharedKey = '9yaZ6kZAsqTh';
    public $mallId = '11523536';
	public function index()

	{
     $urlDoku = 'https://pay.doku.com/Suite/Receive';
      $chainMerchant = 'NA'; 
      $currency = '360'; 
      $purchaseCurrency = '360';
      $amount = $this->uri->segment(2);
      $basket = 'Order Oris Cake,'.$amount.',1,'.$amount.'';
      $purchaseAmount = $amount;
      $transIdMerchant = $this->uri->segment(1);
      $sha1 = $amount.$this->mallId.$this->sharedKey.$transIdMerchant;
      $words = sha1($sha1);
      $requestDate = date('YmdHis');
      $sessionId = $this->uri->segment(1);
      $paymentChannel = "['15','04','02','05']";
      $id_user = 0;
      $name = "oriscake customer";
      $email = "customer@gmail.com";
      $phone = "";
      if($this->uri->segment(3)!=""){
      	$id_user = $this->uri->segment(3);
      	$users = $this->m_payment->getUser($id_user);
      	$name = $users[0]->name;
      	$email = $users[0]->email;
      	if($users[0]->tlp != null){
      		$phone = $users[0]->tlp;
      	}
      }

      echo '<form style="visibility: hidden" name="OneCheckoutTester" method="post" action="https://pay.doku.com/Suite/Receive"><div class="wrapper-material">
	<div class="material-form">
	<div class="row">
		<div class="col-md-4">
			<input name="MALLID" id="MALLID" type="hidden" placeholder="Mall ID" maxlength="120" value="'.$this->mallId.'">
			<input name="Basket" id="Basket" type="text" maxlength="120" value="'.$basket.'" class="filled">
			<input name="SHAREDKEY" id="SHAREDKEY" type="hidden" placeholder="Shared Key" maxlength="120" value="'.$this->sharedKey.'">
			<input name="CHAINMERCHANT" id="chainmerchant" type="text" maxlength="120" value="NA" class="filled">
			<input name="SESSIONID" id="SESSIONID" type="text" value="'.$sessionId.'" class="filled">
			<input name="TRANSIDMERCHANT" id="TRANSIDMERCHANT" type="text" maxlength="120" value="'.$transIdMerchant.'" class="filled">	
			<input class="filled" id="WORDS" name="WORDS" type="text" value="'.$words.'">
			<input name="REQUESTDATETIME" id="REQUESTDATETIME" type="text" value="'.$requestDate.'" class="filled">
			<input name="SERVICEID" id="SERVICEID" type="text" value="">
			<input name="CURRENCY" id="CURRENCY" type="text" value="360" class="filled">
			<input name="PURCHASECURRENCY" id="PURCHASECURRENCY" type="text" value="360" class="filled">
			<input name="AMOUNT" id="AMOUNT" type="text" value="'.$amount.'" class="filled">
			<input name="PURCHASEAMOUNT" id="PURCHASEAMOUNT" type="text" maxlength="120" value="'.$amount.'" class="filled">
			<input name="NAME" id="NAME" type="text" value="'.$name.'" class="filled">
			<input name="EMAIL" id="EMAIL" type="text" value="'.$email.'" class="filled">
			<input name="MOBILEPHONE" id="MOBILEPHONE" type="text" class="form-control" value="'.$phone.'">
			<input name="submit" type="submit" class="bt_submit" id="submit" value="SUBMIT" />
			</div>
		</div>
	</div>
</form><script>document.getElementById("submit").click();</script>';
	}
	function notify(){
		$checkData = $this->m_payment->checkPayment($_POST["TRANSIDMERCHANT"]);
		if(sizeof($checkData) == 0){
			$data = array("id_order"=>$_POST["TRANSIDMERCHANT"],"PAYMENTDATETIME"=>$_POST["PAYMENTDATETIME"],"PURCHASECURRENCY"=>$_POST["PURCHASECURRENCY"],"LIABILITY"=>$_POST["LIABILITY"],"PAYMENTCHANNEL"=>$_POST["PAYMENTCHANNEL"],"AMOUNT"=>$_POST["AMOUNT"],"PAYMENTCODE"=>$_POST["PAYMENTCODE"],"MCN"=>$_POST["MCN"],"WORDS"=>$_POST["WORDS"],"RESULTMSG"=>$_POST["RESULTMSG"],"VERIFYID"=>$_POST["VERIFYID"],"TRANSIDMERCHANT"=>$_POST["TRANSIDMERCHANT"],"BANK"=>$_POST["BANK"],"STATUSTYPE"=>$_POST["STATUSTYPE"],"APPROVALCODE"=>$_POST["APPROVALCODE"],"EDUSTATUS"=>$_POST["EDUSTATUS"],"THREEDSECURESTATUS"=>$_POST["THREEDSECURESTATUS"],"VERIFYSCORE"=>$_POST["VERIFYSCORE"],"CURRENCY"=>$_POST["CURRENCY"],"RESPONSECODE"=>$_POST["RESPONSECODE"],"CHNAME"=>$_POST["CHNAME"],"BRAND"=>$_POST["BRAND"],"VERIFYSTATUS"=>$_POST["VERIFYSTATUS"],"SESSIONID"=>$_POST["SESSIONID"]);
			echo json_encode($this->m_payment->insertNotify($data));
			if($_POST["RESULTMSG"] == "SUCCESS"){
				$order = $this->m_payment->getUserOrder($_POST["TRANSIDMERCHANT"]);
				$this->m_payment->updateStatus($_POST["TRANSIDMERCHANT"]);
				$this->sendSuccess($order[0]->email);
			}
		}
		
	}
	function sendSuccess($email = ""){
		$time = strtotime($_POST["PAYMENTDATETIME"]);
		$date = date('d M Y, h:i',$time);
		$channel = $this->m_payment->getPaymentChannel($_POST["PAYMENTCHANNEL"]);
		$channelLabel = $channel[0]->description;
		$htmlmessage = '<!DOCTYPE html> 
						<head> 
					    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
						</head>
						<style>
							.content {
							    max-width: 500px;
							    margin: auto;
							}
							.title{
								width: 60%;
							}

						</style>
						<body> 
							<div class="content">
<div bgcolor="#f9f9f9" style="font-family:'."'Open Sans'".',sans-serif">
    <table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" style="background-color:#fff;margin:5% auto;width:100%;max-width:600px">
        
        <tbody><tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#643612" style="padding:10px 15px;font-size:14px">
                    <tbody><tr>
                        <td width="60%" align="left" style="padding:5px 0 0">
                            <img src="http://ganeshcomdemo.com/oriscake/assets/front/images/logo.png" alt="Logo" width="50" class="CToWUd">
                        </td>
                        <td width="40%" align="right" style="padding:5px 0 0">
                            <span style="font-size:18px;font-weight:300;color:#ffffff">
                                Payment
                            </span>
                        </td>
                    </tr>
                </tbody></table>
            </td>
        </tr>        
        <tr>
            <td style="padding:25px 15px 10px">
                <table width="100%">
                    <tbody><tr>
                        <td>
                            <h1 style="margin:0;font-size:16px;font-weight:bold;line-height:24px;color:rgba(0,0,0,0.70)">Halo Customer,</h1>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style="margin:0;font-size:16px;line-height:24px;color:rgba(0,0,0,0.70)">Selamat, transaksi Anda BERHASIL</p>
                        </td>
                    </tr>
                </tbody></table>
            </td>
        </tr>
        <tr>
            <td style="padding:0 15px">
                
                <table width="100%" style="margin:15px 0;color:rgba(0,0,0,0.70);font-size:14px;border-collapse:collapse">
                    <tbody>
                        <tr>
                            <td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12)" width="40%">No. Invoice</td>
                            <td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold;color:#f5a84c" width="60%">
                                '.$_POST["TRANSIDMERCHANT"].'
                            </td>
                        </tr>                                                                                               
                            <tr>
                                <td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Waktu</td>
                                <td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$date.'</td>
                            </tr>
                            <tr>
                                <td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Metode Pemabayaran</td>
                                <td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$channelLabel.'</td>
                            </tr>
                            <tr>
                                <td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Total</td>
                                <td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">Rp '.$_POST["AMOUNT"].'</td>
                            </tr>
                        
                    </tbody>
                </table>
                <div style="text-align:center">
                    <div style="text-align:left;font-size:13px;margin:30px 0 10px;color:#999999">
                        Email ini dibuat secara otomatis. Mohon tidak mengirimkan balasan ke email ini.
                    </div>
                </div>

            </td>
        </tr>
    </tbody></table>
<p>&nbsp;<br></p>
</div>

							</div>	
									
						</body>
</html>';
		$this->email->from('noreply@oriscake.com', 'Oris Cake');
		$this->email->to($email);
		$this->email->subject('Payment Result');
		$this->email->set_mailtype("html");
		$this->email->message($htmlmessage);

		$send = $this->email->send();
		if($send){
			echo "berhasil";
		}
	}

}