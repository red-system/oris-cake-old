<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class M_payment extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
		}

		function insertNotify($data){
			return $this->db->insert("tb_transaction",$data);
		}
		function getUser($id){
			$this->db->select('users.*,tb_detail_user.tlp');
			$this->db->from('users');
			$this->db->join('tb_detail_user','tb_detail_user.id_user = users.id','LEFT');
			$this->db->where("id",$id);
			return $this->db->get()->result();
		}
		function getPaymentChannel($code){
			$this->db->where('code',$code);
			return $this->db->get('tb_payment_channel')->result();
		}
		function getUserOrder($id_order){
			$this->db->select('tb_order.*,users.email');
			$this->db->from('tb_order');
			$this->db->join('users','tb_order.id_user = users.id');
			$this->db->where("id_order",$id_order);
			return $this->db->get()->result();
		}
		function updateStatus($id_order){
			$data = array("status_order"=>"terbayar");
			$this->db->where("id_order",$id_order);
			return $this->db->update("tb_order",$data);
		}
		function checkPayment($id_order){
			$this->db->where('id_order',$id_order);
			$this->db->where('RESULTMSG','SUCCESS');
			return $this->db->get('tb_transaction')->result();
		}
	}
?>