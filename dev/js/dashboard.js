$(document).ready(function(){
  // Random Color
  function getRandomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

  var url_daily_gross = '../admin/chart/summary_today';
  var Labels_daily_gross = new Array();
  var jumlah_daily_gross = new Array();
  $.get(url_daily_gross, function(response){
    response.label.forEach(function(label) {
      Labels_daily_gross.push(label);
    })
    response.order.forEach(function(order) {
      jumlah_daily_gross.push(order.total);
    })
    var daily_gross = document.getElementById("daily_gross").getContext('2d');
    var daily_gross_chart = new Chart(daily_gross, {
      type: 'bar',
      data: {
        labels: Labels_daily_gross,
        datasets: [{
          label: 'Penjualan',
          backgroundColor: getRandomColor(),
          borderColor: [],
          data: jumlah_daily_gross,
          borderWidth: 2
        }]
      },
      options: {
        maintainAspectRatio: false,
        scales: {
          xAxes: [{
            time: {
              unit: 'day'
            }
          }],
          yAxes: [{
            ticks: {
              beginAtZero:true
            }
          }]
        },
        legend: { display: true },
        title: {
          display: false,
          text: 'Daily Gross'
        }
      }
    });
  });

  var url_sales_history = '../admin/chart/order_history';
  var Labels_sales_history = new Array();
  var jumlah_sales_history = new Array();
  $.get(url_sales_history, function(response){
    response.label.forEach(function(label) {
      Labels_sales_history.push(label.tgl);
    })
    response.order.forEach(function(order) {
      jumlah_sales_history.push(order.total);
    })
    var sales_history = document.getElementById("sales-history").getContext('2d');
    var sales_history_chart = new Chart(sales_history, {
      type: 'bar',
      data: {
        labels: Labels_sales_history,
        datasets: [{
          label: 'Penjualan',
          backgroundColor: getRandomColor(),
          borderColor: [],
          data: jumlah_sales_history,
          borderWidth: 2
        }]
      },
      options: {
        maintainAspectRatio: false,
        scales: {
          xAxes: [{
            time: {
              unit: 'day'
            }
          }],
          yAxes: [{
            ticks: {
              beginAtZero:true
            }
          }]
        },
        legend: { display: true },
        title: {
          display: false,
          text: 'Daily Gross'
        }
      }
    });
  });

  $.ajax({
    url: "../admin/chart/top_product",
    method: "GET",
    success: function(data) {
      var category = [];
      var total = [];
      var coloR = [];

      for (var i in data) {
        category.push(data[i].C_name);
        total.push(data[i].jumlah);
        coloR.push(getRandomColor());
      }
      var chartData = {
        labels: category,
        datasets: [{
          label: 'Product',
          backgroundColor: coloR,
          borderColor: 'rgba(200, 200, 200, 0.75)',
          hoverBorderColor: 'rgba(200, 200, 200, 1)',
          data: total
        }]
      };

      var top_selling_product = $("#top-selling-product");
      var top_selling_product_chart = new Chart(top_selling_product, {
        type: 'pie',
        data: chartData,
        options: {
          maintainAspectRatio: false,
        }
      })
    },
    error: function(data) {
      // console.log(data);
    },
  });

  $.ajax({
    url: "../admin/chart/top_category",
    method: "GET",
    success: function(data) {
      var category = [];
      var total = [];
      var coloR = [];

      for (var i in data) {
        category.push(data[i].C_name);
        total.push(data[i].jumlah);
        coloR.push(getRandomColor());
      }
      var chartData = {
        labels: category,
        datasets: [{
          label: 'Category',
          backgroundColor: coloR,
          borderColor: 'rgba(200, 200, 200, 0.75)',
          hoverBorderColor: 'rgba(200, 200, 200, 1)',
          data: total
        }]
      };

      var top_category_selling = $("#top-category-selling");
      var top_category_selling_chart = new Chart(top_category_selling, {
        type: 'pie',
        data: chartData,
        options: {
          maintainAspectRatio: false,
        }
      })
    },
    error: function(data) {
      // console.log(data);
    },
  });

  $.ajax({
    url: "../admin/chart/top_customer",
    method: "GET",
    success: function(data) {
      var category = [];
      var total = [];
      var coloR = [];

      for (var i in data) {
        category.push(data[i].C_name);
        total.push(data[i].jumlah);
        coloR.push(getRandomColor());
      }
      var chartData = {
        labels: category,
        datasets: [{
          label: 'Category',
          backgroundColor: coloR,
          borderColor: 'rgba(200, 200, 200, 0.75)',
          hoverBorderColor: 'rgba(200, 200, 200, 1)',
          data: total
        }]
      };

      var top_customer = $("#top-customer");
      var top_customer_chart = new Chart(top_customer, {
        type: 'pie',
        data: chartData,
        options: {
          maintainAspectRatio: false,
        }
      })
    },
    error: function(data) {
      // console.log(data);
    },
  });
}); //END
