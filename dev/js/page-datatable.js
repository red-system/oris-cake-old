var TableDatatablesEditable = function () {
  var handleTable = function () {
    var table = $('#sample_editable_1');
    var oTable = table.dataTable({
      "lengthMenu": [
        [5, 10, 15, 20, -1],
        [5, 10, 15, 20, "All"] // change per page values here
      ],
      // set the initial value
      "pageLength": 10,
      "language": {
        "lengthMenu": " _MENU_ records"
      },
      "columnDefs": [{ // set default column settings
        'orderable': true,
        'targets': [0]
      }, {
        "searchable": true,
        "targets": [0]
      }],
      "order": [
        [0, "asc"]
      ] // set first column as a default sort by asc
    });
    var tableWrapper = $("#sample_editable_1_wrapper");
  }

  return {
    //main function to initiate the module
    init: function () {
      handleTable();
    }
  };
}();

jQuery(document).ready(function() {
  TableDatatablesEditable.init();
});
