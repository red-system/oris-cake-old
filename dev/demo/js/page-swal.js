$(document).ready(function () {
  $(document.body).on('click', '.js-submit-confirm', function (event) {
    event.preventDefault()
    var $form = $(this).closest('form')
    swal({
      title: "Are you sure?",
      text: "You can not undo this process!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: "Cancel",
      closeOnConfirm: true
    },
    function () {
      $form.submit()
    });
  })
})
