-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 15, 2020 at 06:32 PM
-- Server version: 10.0.38-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `oriscake_demo`
--

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE `article` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumb_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `short_description` text COLLATE utf8mb4_unicode_ci,
  `conten` text COLLATE utf8mb4_unicode_ci,
  `additional_conten` text COLLATE utf8mb4_unicode_ci,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keyword` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `position` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `published` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `more_config` enum('0','1') COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `admin_config` enum('0','1') COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `lang` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `equal_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `article`
--

INSERT INTO `article` (`id`, `title`, `thumb_image`, `short_description`, `conten`, `additional_conten`, `meta_title`, `meta_keyword`, `meta_description`, `position`, `published`, `link`, `slug`, `parent_id`, `longitude`, `latitude`, `more_config`, `admin_config`, `lang`, `equal_id`, `created_at`, `updated_at`) VALUES
(2, 'Home', NULL, 'Home', 'Home', NULL, 'Home', 'Home', 'Home', 'menu-utama', '1', '/', NULL, NULL, NULL, NULL, '1', '1', '', 'ART5b3b0d106081d0.75724110', NULL, '2018-07-03 12:52:48'),
(3, 'Cake', NULL, NULL, 'Cake Category', NULL, 'Cake Category', 'Cake Category', 'Cake', 'menu-utama', '1', 'cake', NULL, NULL, NULL, NULL, '0', '0', NULL, 'ART5b3b0d75c4e215.09808536', NULL, NULL),
(4, 'Cart', NULL, 'Cart', 'Cart', NULL, 'Cart', 'Cart', 'Cart', 'menu-utama', '1', 'cart', NULL, NULL, NULL, NULL, '1', '0', NULL, 'ART5b3b0d79ebf264.89790443', NULL, NULL),
(5, 'Testimonial', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'menu-utama', '1', 'testimonial', NULL, NULL, NULL, NULL, '1', '1', NULL, 'ART5b3b0d7b65ab87.97467387', NULL, NULL),
(6, 'FAQ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'menu-utama', '1', 'faq', NULL, NULL, NULL, NULL, '1', '1', NULL, 'ART5b619ae02ce2d5.46017369', NULL, NULL),
(7, 'Blog', NULL, 'Blog', 'Blog', NULL, 'Blog', 'Blog', 'Blog', 'menu-utama', '1', 'blog', NULL, NULL, NULL, NULL, '1', '1', NULL, 'ART5b3b0d7e9dd2d7.95104340', NULL, NULL),
(8, 'Contact Us', NULL, 'contact', '<p>contact</p>', NULL, 'contact', 'contact', 'contact', 'menu-utama', '1', 'contact', NULL, NULL, '115,223274', '-8,627242', '1', '1', NULL, 'ART5b3b0d8119fd03.83426629', NULL, '2018-08-01 19:45:10'),
(9, 'Checkout', NULL, 'Checkout', 'Checkout', NULL, NULL, NULL, NULL, 'cart', '1', 'checkout', NULL, 4, NULL, NULL, '0', '0', NULL, 'ART5b4c4bd6cbb622.76403670', NULL, NULL),
(10, 'Doni Agustina', 'doni-agustina-oriscake9f1.jpeg', NULL, '<p><em>Rasa kue nya sangat enak, seperti ice cream. Next pasti beli lagi.</em></p>', NULL, NULL, NULL, NULL, 'testimonial', '1', NULL, NULL, NULL, NULL, NULL, '0', '0', NULL, NULL, '2018-07-11 12:02:45', '2018-10-16 16:16:28'),
(11, 'Jhon Doe', 'jhon-doe-oriscake1VY.jpeg', NULL, '<p><em>doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo<br />\r\ninventore veritatis et quasi architecto beatae</em></p>', NULL, NULL, NULL, NULL, 'testimonial', '1', NULL, NULL, NULL, NULL, NULL, '0', '0', NULL, NULL, '2018-07-11 12:04:16', '2018-10-16 16:19:02'),
(12, 'Mauris pulvinar lorem', 'mauris-pulvinar-lorem-oriscakeEnj.jpeg', 'Offal yuccie meggings everyday carry skateboard direct trade, tumblr literally migas biodiesel vinyl banjo. Offal yuccie meggings everyday carry skateboard direct trade, tumblr', '<p>Offal yuccie meggings everyday carry skateboard direct trade, tumblr literally migas biodiesel vinyl banjo. Offal yuccie meggings everyday carry skateboard direct trade, tumblr</p>\r\n\r\n<p>Offal yuccie meggings everyday carry skateboard direct trade, tumblr literally migas biodiesel vinyl banjo. Offal yuccie meggings everyday carry skateboard direct trade, tumblr</p>', NULL, 'blog', 'blog', 'blog', 'blog', '1', 'blog', 'mauris-pulvinar-lorem', 7, NULL, NULL, '0', '0', NULL, NULL, '2018-07-13 15:14:47', '2018-07-13 15:14:47'),
(13, 'Mauris pulvinar', 'mauris-pulvinar-oriscake4oN.jpeg', 'Mauris pulvinar lorem\r\nWhen an unknown printer took a galley type scrambled it to make type Nam and sheets Mauris pulvinar tincidut lorem. It has only survived five centuries', '<p>Mauris pulvinar lorem<br />\r\nWhen an unknown printer took a galley type scrambled it to make type Nam and sheets Mauris pulvinar tincidut lorem. It has only survived five centuries Mauris pulvinar lorem<br />\r\nWhen an unknown printer took a galley type scrambled it to make type Nam and sheets Mauris pulvinar tincidut lorem. It has only survived five centuries</p>', NULL, 'blog', 'blog', 'blog', 'blog', '1', 'blog', 'mauris-pulvinar', 7, NULL, NULL, '0', '0', NULL, NULL, '2018-07-13 15:20:44', '2018-07-13 15:20:44'),
(14, 'Contact', NULL, NULL, '<p>\r\n                                </p>\r\n                                <p><span class=\"rounded-icon\"><i class=\"fa fa-map-marker\"></i></span>Jl. Nangka Utara, Kel. Tonja, Kec. Denpasar Utara<br>\r\n                                    Denpasar, Bali 80239\r\n                                </p>\r\n                                <p><span class=\"rounded-icon\"><i class=\"fa fa-phone\"></i></span>+123 456 7890 <br>\r\n                                    +123 456 7890\r\n                                </p>\r\n                                <p><span class=\"rounded-icon\"><i class=\"fa fa-envelope-o\"></i></span>+123 456 7890<br>\r\n                                    +123 456 7890\r\n                                </p>', NULL, NULL, NULL, NULL, 'footer-contact', '1', NULL, NULL, NULL, NULL, NULL, '0', '0', NULL, 'ART5a3d3753a68e67.76688128', NULL, '2019-03-09 18:11:15'),
(15, 'Address', NULL, NULL, 'Jl. Nangka Utara, Kelurahan Tonja, Kec. Denpasar Utara, Denpasar-Bali 80239', NULL, NULL, NULL, NULL, 'onpage-ctoffice', '1', NULL, NULL, 8, NULL, NULL, '0', '0', NULL, 'ART5b618f1c831849.89416643', NULL, '2019-03-09 18:08:41'),
(16, 'Call Us', NULL, NULL, '08123762499', NULL, NULL, NULL, NULL, 'onpage-ctcall', '1', NULL, NULL, 8, NULL, NULL, '0', '0', NULL, 'ART5b618f25112239.66361391', NULL, '2018-11-15 12:08:55'),
(17, 'Mail Us', NULL, NULL, '<p>admin@gmail.com</p>', NULL, NULL, NULL, NULL, 'onpage-ctmail', '1', NULL, NULL, 8, NULL, NULL, '0', '0', NULL, 'ART5b618f25836445.24086926\"', NULL, '2018-08-01 17:53:13'),
(18, 'Help', NULL, NULL, 'At vero eos et accusamus et iusto lits agnissmos ducimus. \r\nAt vero eos et accusamus et iusto lits agnissmos ducimus. \r\nAt vero eos et accusamus et iusto lits agnissmos ducimus. At vero eos et accusamus et iusto lits agnissmos ducimus. At vero eos et accusamus et iusto lits agnissmos ducimus.', NULL, NULL, NULL, NULL, 'onpage-home', '1', 'help', NULL, 6, NULL, NULL, '0', '0', NULL, 'ART5b619446bb57e4.85227298', NULL, '2018-08-01 18:40:51'),
(19, 'Information', NULL, NULL, '<p>\r\n                                    At vero eos et accusamus et iusto lits agnissmos ducimus.\r\n                                </p>\r\n                                <p><span class=\"rounded-icon\"><i class=\"fa fa-map-marker\"></i></span>PO Box 16122 Collins Street <br>\r\n                                    West Victoria 8007 Australia\r\n                                </p>\r\n                                <p><span class=\"rounded-icon\"><i class=\"fa fa-phone\"></i></span>+123 456 7890 <br>\r\n                                    +123 456 7890\r\n                                </p>\r\n                                <p><span class=\"rounded-icon\"><i class=\"fa fa-envelope-o\"></i></span>+123 456 7890<br>\r\n                                    +123 456 7890\r\n                                </p>', NULL, NULL, NULL, NULL, 'footer-information', '1', NULL, NULL, NULL, NULL, NULL, '0', '0', NULL, 'ART5b926b1e3b7490.96720056', NULL, '2018-08-01 17:25:18'),
(20, 'Bagaimana Cara Memesan ?', NULL, NULL, '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora iure fugit, temporibus explicabo, sequi vitae, aut tempore consequuntur aspernatur saepe placeat. Quis mollitia perspiciatis quae quidem consequuntur quia, quos eos. Ex aut, esse ipsam vero quasi, consectetur ab sint deserunt aperiam nemo mollitia minima iste repellendus voluptatum, tempore doloremque aliquid!</p>', NULL, NULL, NULL, NULL, 'faq', '1', 'faq', 'bagaimana-cara-memesan', 6, NULL, NULL, '0', '0', NULL, 'ART5bd189cf13e6b0.33636542', '2018-10-25 16:15:59', '2018-10-25 16:15:59'),
(21, 'Lorem ipsum dolor', NULL, NULL, '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora iure fugit, temporibus explicabo, sequi vitae, aut tempore consequuntur aspernatur saepe placeat. Quis mollitia perspiciatis quae quidem consequuntur quia, quos eos. Ex aut, esse ipsam vero quasi, consectetur ab sint deserunt aperiam nemo mollitia minima iste repellendus voluptatum, tempore doloremque aliquid!</p>', NULL, NULL, NULL, NULL, 'faq', '1', 'faq', 'lorem-ipsum-dolor', 6, NULL, NULL, '0', '0', NULL, 'ART5bd18a66855401.59408718', '2018-10-25 16:18:30', '2018-10-25 16:18:30');

-- --------------------------------------------------------

--
-- Table structure for table `article_category`
--

CREATE TABLE `article_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `article_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `article_category`
--

INSERT INTO `article_category` (`id`, `article_id`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 12, 2, '2018-07-13 15:14:47', '2018-07-13 15:14:47'),
(2, 13, 2, '2018-07-13 15:20:44', '2018-07-13 15:20:44');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(10) UNSIGNED NOT NULL,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'id',
  `equal_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `icon` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `category`, `type`, `slug`, `lang`, `equal_id`, `created_at`, `updated_at`, `icon`) VALUES
(2, 'blog category', 'blog', 'blog-category', 'id', '', '2018-07-10 16:00:37', '2018-07-10 16:00:37', '');

-- --------------------------------------------------------

--
-- Table structure for table `contact_messages`
--

CREATE TABLE `contact_messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'waiting-confirmation',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contact_messages`
--

INSERT INTO `contact_messages` (`id`, `name`, `email`, `phone`, `subject`, `message`, `status`, `created_at`, `updated_at`) VALUES
(1, 'gung', 'nachahppc@gmail.com', '0987654', 'subject', 'message', 'confirmed', NULL, '2018-08-30 18:17:25'),
(2, 'nacha', 'nachahppc@gmail.com', '987654345678', 'subject', 'message', 'confirmed', '2018-08-30 18:19:19', '2018-09-07 17:05:17'),
(3, 'EW', 'doniagustina27@gmail.com', 'EW', 'EW', 'EW', 'confirmed', '2018-10-15 21:07:53', '2018-11-15 13:13:10'),
(5, 'gung', 'nachahppc@gmail.com', '082341297863', 'sub', 'mes', 'confirmed', '2018-10-25 16:31:29', '2018-11-15 14:04:03'),
(6, 'DanielMumma', 'yourmail@gmail.com', '88481699459', 'Test, just a test', 'Hello. And Bye.', 'waiting-confirmation', '2018-12-22 14:05:50', '2018-12-22 14:05:50'),
(7, 'Randy', 'randy@talkwithlead.com', '416-385-3200', 'Concerning oriscake.com', 'Hi,\r\n\r\nMy name is Randy and I was looking at a few different sites online and came across your site oriscake.com.  I must say - your website is very impressive.  I found your website on the first page of the Search Engine.\r\n\r\nHave you noticed that 70 percent of visitors who leave your website will never return?  In most cases, this means that 95 percent to 98 percent of your marketing efforts are going to waste, not to mention that you are losing more money in customer acquisition costs than you need to.\r\nAs a business person, the time and money you put into your marketing efforts is extremely valuable.  So why let it go to waste?  Our users have seen staggering improvements in conversions with insane growths of 150 percent going upwards of 785 percent. Are you ready to unlock the highest conversion revenue from each of your website visitors?  \r\n\r\nTalkWithLead is a widget which captures a website visitor’s Name, Email address and Phone Number and then calls you immediately, so that you can talk to the Lead exactly when they are live on your website — while they\'re hot! Best feature of all, we offer FREE International Long Distance Calling!\r\n  \r\nTry the TalkWithLead Live Demo now to see exactly how it works.  Visit: http://www.talkwithcustomer.com\r\n\r\nWhen targeting leads, speed is essential - there is a 100x decrease in Leads when a Lead is contacted within 30 minutes vs being contacted within 5 minutes.\r\n\r\nIf you would like to talk to me about this service, please give me a call.  We do offer a 14 days free trial.  \r\n\r\nThanks and Best Regards,\r\nRandy\r\n\r\nIf you\'d like to unsubscribe click here. http://liveserveronline.com/talkwithcustomer.aspx?d=oriscake.com', 'waiting-confirmation', '2018-12-23 16:00:28', '2018-12-23 16:00:28'),
(8, 'Thomaswaf', 'bumpsie15@hotmail.com', '87295284966', 'How To Make Extra Money From Home - $3000 Per Day Easy', 'Paid Surveys: Earn $3,000 Or More Per Week: http://yourls.site/bestinvestsystem87993', 'waiting-confirmation', '2019-01-02 08:53:37', '2019-01-02 08:53:37'),
(9, 'Thomaswaf', 'cdkroen@hotmail.com', '88779617194', 'Hei?e Frauen fur guten Sex jeden Tag', 'Hei?e Frauen fur guten Sex jeden Tag: https://aaa.moda/bestsexygirlsinyourcity65937', 'waiting-confirmation', '2019-01-03 22:48:25', '2019-01-03 22:48:25'),
(10, 'Thomaswaf', 'ccmarlar@hotmail.com', '87532433541', 'Finde eine hei?e Frau fur guten Sex in deiner Stadt', 'Hei?e Frauen fur guten Sex jeden Tag: http://awalsh.com/bestsexygirlsinyourcity28682', 'waiting-confirmation', '2019-01-04 21:31:18', '2019-01-04 21:31:18'),
(11, 'Thomaswaf', 'qelle@msn.com', '86684445367', 'Finde eine hei?e Frau fur guten Sex in deiner Stadt', 'Hei?e Frauen fur guten Sex jeden Tag: https://atho.me/3WSg', 'waiting-confirmation', '2019-01-05 22:26:04', '2019-01-05 22:26:04'),
(12, 'Thomaswaf', 'maasoo@hotmail.com', '88645956635', 'Hei?e Frauen fur guten Sex jeden Tag', 'Finde eine hei?e Frau fur guten Sex in deiner Stadt: http://www.short4free.us/bestsexygirlsinyourcity21778', 'waiting-confirmation', '2019-01-07 02:52:37', '2019-01-07 02:52:37'),
(13, 'Thomaswaf', 'zpeaches@hotmail.com', '86937461476', 'How to Make $3000 FAST | Fast Money | The Busy Budgeter', 'What\'s the easiest way to earn $3000 a month: https://atho.me/3Xmb', 'waiting-confirmation', '2019-01-08 01:17:45', '2019-01-08 01:17:45'),
(14, 'Thomaswaf', 'carrieyazzie@hotmail.com', '85233418632', 'How would you use $3,000 to make more money', 'I\'m 23. I have $3000. How can I best use it to make more money: http://corta.co/bestinvestment142416', 'waiting-confirmation', '2019-01-08 21:43:58', '2019-01-08 21:43:58'),
(15, 'Thomaswaf', 'am_gonzalez83@yahoo.com', '83249369913', 'Make $200 per hour doing this', '[OMG -  PROFIT in under 60 seconds: http://go.fireontherim.com/bestinvest300021552', 'waiting-confirmation', '2019-01-09 22:44:10', '2019-01-09 22:44:10'),
(16, 'MichaelKat', 's_stebner@hotmail.com', '87464926213', 'How to make $450 per hour', 'LAZY way for $200 in 20 mins: http://www.vkvi.net/bestinvest300064356', 'waiting-confirmation', '2019-01-10 23:42:17', '2019-01-10 23:42:17'),
(17, 'JosephHop', 'derek4u_08@yahoo.com', '81288289732', 'Get $100 – $600 A Day', 'Make $200 per hour doing this: http://wntdco.mx/bestinvest300037118', 'waiting-confirmation', '2019-01-12 04:04:12', '2019-01-12 04:04:12'),
(18, 'Phillippaf', 'dlodwick@hotmail.com', '87112712277', 'Where to invest $ 3000 once and receive every month from $ 55000', 'Where to invest $ 3000 once and receive every month from $ 55000: http://webhop.se/howtoinvest300046173', 'waiting-confirmation', '2019-01-13 08:24:59', '2019-01-13 08:24:59'),
(19, 'LesterRenty', 'tehern@msn.com', '84837348722', 'How To Make Extra Money From Home - $3000 Per Day Easy', 'Make $200 per hour doing this: http://www.vkvi.net/bestinvestsystems44140', 'waiting-confirmation', '2019-01-14 02:13:53', '2019-01-14 02:13:53'),
(20, 'LesterRenty', 'gderkel@aol.com', '81971455377', 'Finde eine hei?e Frau fur guten Sex in deiner Stadt', 'Hei?e und saftige Frauen in Ihrer Stadt wollen Sex: http://corta.co/bestadultdating2837459040', 'waiting-confirmation', '2019-01-14 22:44:21', '2019-01-14 22:44:21'),
(21, 'LesterRenty', 'angohe@msn.com', '85121292842', 'Hei?e und saftige Frauen in Ihrer Stadt wollen Sex', 'Finde eine hei?e Frau fur guten Sex in deiner Stadt: http://jnl.io/bestsexygirlsinyourcity84226', 'waiting-confirmation', '2019-01-15 23:25:30', '2019-01-15 23:25:30'),
(22, 'LesterRenty', 'lindachu1959@aol.com', '89895639189', 'Enquetes remunerees: gagnez 3 000 € ou plus par semaine', 'Quel est le moyen le plus simple de gagner 3000 € par mois: http://ralive.de/bestinvestsystem15855', 'waiting-confirmation', '2019-01-16 14:16:13', '2019-01-16 14:16:13'),
(23, 'LesterRenty', 'starburstr@excite.com', '83225538367', 'Comment gagner de l\'argent supplementaire a la maison - seulement 3000 € par jour', 'Quel est le moyen le plus simple de gagner 3000 € par mois: https://is.gd/bestinvestsystem79029', 'waiting-confirmation', '2019-01-17 21:37:05', '2019-01-17 21:37:05'),
(24, 'LesterRenty', 'bigonly@hotmail.com', '88269648188', 'Ich bin 23. Ich habe € 3000. Wie kann ich es am besten nutzen, um mehr Geld zu verdienen', 'Bezahlte Umfragen: Verdienen Sie € 3.000 oder mehr pro Woche: http://go.fireontherim.com/5500033749', 'waiting-confirmation', '2019-01-19 08:38:31', '2019-01-19 08:38:31'),
(25, 'LesterRenty', 'bat12345@hotmail.com', '88287278917', 'Top Cryptocurrencies To Invest In 2018-2019', 'Best cryptocurrency to Invest 2019: https://lil.ink/BESTINVESTMETHOD5500025720', 'waiting-confirmation', '2019-01-20 05:38:47', '2019-01-20 05:38:47'),
(26, 'LesterRenty', 'yantra@hotmail.com', '82671881883', 'Forex 1000 To 1 Million – Turning $10,000 into $1 Million in Forex', 'Forex Prodigy Makes $10,000 in 30 Minutes! - Market Traders Institute: http://to.ht/bestforex64513', 'waiting-confirmation', '2019-01-21 16:05:05', '2019-01-21 16:05:05'),
(27, 'LesterRenty', 'cerahr@hotmail.com', '82561718822', 'Sexy girls for the night in your town', 'Sexy girls for the night in your town: http://www.vkvi.net/bestforex96360', 'waiting-confirmation', '2019-01-22 15:54:40', '2019-01-22 15:54:40'),
(28, 'LesterRenty', 'sandrasalome@msn.com', '85149561991', 'Find yourself a girl for the night in your city', 'Sexy girls for the night in your town: https://tinyurl.com/bestforex-67482', 'waiting-confirmation', '2019-01-23 15:57:55', '2019-01-23 15:57:55'),
(29, 'Keithdak', 'rdub98@msn.com', '81439975558', 'Find yourself a girl for the night in your city', 'Meet sexy girls in your city: http://ralive.de/adultdating70124', 'waiting-confirmation', '2019-01-25 02:37:06', '2019-01-25 02:37:06'),
(30, 'Tommiejible', 'romeo_12@hotmail.com', '84173712642', 'Trouvez-vous une fille pour la nuit dans votre ville', 'Filles sexy pour la nuit dans votre ville: http://valeriemace.co.uk/bestsexygirlsxxx47336', 'waiting-confirmation', '2019-01-27 00:12:08', '2019-01-27 00:12:08'),
(31, 'Tommiejible', 'woganni@msn.com', '83868225673', 'Find yourself a girl for the night in your city', 'Meet sexy girls in your city: http://jnl.io/bestsexygirlsxxx25494', 'waiting-confirmation', '2019-01-28 00:08:57', '2019-01-28 00:08:57'),
(32, 'Tommiejible', 'effd@hotmail.com', '89395568723', 'Forex trader makes $10,000 in minutes', 'Forex trader makes $10,000 in minutes: http://profit.bitcoin-forex-invest.com/how-do-i-make-money-selling-pictures-online.php', 'waiting-confirmation', '2019-01-28 14:20:06', '2019-01-28 14:20:06'),
(33, 'Tommiejible', 'downing@comcast.net', '86797812759', 'You won iPhone X', 'Win iPhone X: https://tinyurl.com/winiphonexs17538', 'waiting-confirmation', '2019-01-30 01:25:45', '2019-01-30 01:25:45'),
(34, 'Tommiejible', 'obxladybug99@aol.com', '84467815367', '10 Best Cryptocurrency to Invest in 2019', 'Cryptocurrency Investing 2019: http://jnl.io/21895', 'waiting-confirmation', '2019-01-31 16:49:01', '2019-01-31 16:49:01'),
(35, 'Tommiejible', 'barhorn@hotmail.com', '81839943857', 'Simple biz + new tool = $450 per hour', 'How would you use $30,000 to make more money: http://www.vkvi.net/140539', 'waiting-confirmation', '2019-02-01 09:50:55', '2019-02-01 09:50:55'),
(36, 'Tommiejible', 'eduardo245@hotmail.com', '88378644887', 'What\'s the easiest way to earn $30000 a month', 'Get $1500 – $6000 per DAY: http://www.abcagency.se/166497', 'waiting-confirmation', '2019-02-02 14:10:31', '2019-02-02 14:10:31'),
(37, 'Tommiejible', 'charles_hall@yahoo.com', '84632211913', 'Where to invest $ 3000 once and receive every month from $ 55000', 'How To Make Extra Money From Home - $3000 Per Day Easy: http://999.sh/149880', 'waiting-confirmation', '2019-02-03 14:39:30', '2019-02-03 14:39:30'),
(38, 'JavierFup', 'cnkbb@msn.com', '86565684792', 'Sexy girls for the night in your town', 'Sexy girls for the night in your town: http://rih.co/bestadultdatingsite67810', 'waiting-confirmation', '2019-02-04 12:59:02', '2019-02-04 12:59:02'),
(39, 'JavierFup', 'tanganyikadgaither@yahoo.com', '84517485433', 'Suchen Sie sich ein Madchen fur die Nacht in Ihrer Stadt', 'Treffen Sie sexy Madchen in Ihrer Stadt: https://aaa.moda/bestadultdating23828', 'waiting-confirmation', '2019-02-05 10:10:02', '2019-02-05 10:10:02'),
(40, 'JavierFup', 'mshsz@msn.com', '86152187443', 'Treffen Sie sexy Madchen in Ihrer Stadt', 'Treffen Sie sexy Madchen in Ihrer Stadt: https://aaa.moda/bestadultdating64008', 'waiting-confirmation', '2019-02-06 05:25:32', '2019-02-06 05:25:32'),
(41, 'JavierFup', 'only1mrs.chrisbrown@yahoo.com', '82151478335', 'Suchen Sie sich ein Madchen fur die Nacht in Ihrer Stadt', 'Treffen Sie sexy Madchen in Ihrer Stadt: http://corta.co/bestadultdating33507', 'waiting-confirmation', '2019-02-07 02:15:07', '2019-02-07 02:15:07'),
(42, 'JavierFup', 'ellis_b@budweiser.com', '83476619476', 'Forex Prodigy Makes $10,000 in 30 Minutes! - Market Traders Institute', 'Forex Trader predicts and makes $10,000 less than 3 hours: http://corta.co/1000perday37336', 'waiting-confirmation', '2019-02-08 03:06:09', '2019-02-08 03:06:09'),
(43, 'JavierFup', 'wavegoodbye@msn.com', '82121924547', 'Find yourself a girl for the night in your city', 'Find yourself a girl for the night in your city: http://awalsh.com/bestadultapp22736', 'waiting-confirmation', '2019-02-08 22:14:32', '2019-02-08 22:14:32'),
(44, 'JavierFup', 'cyb@yahoo.com', '88496998376', 'Meet sexy girls in your city', 'Meet sexy girls in your city: http://tropaadet.dk/adultdating31314', 'waiting-confirmation', '2019-02-10 05:11:01', '2019-02-10 05:11:01'),
(45, 'JavierFup', 'rheido@yahoo.com', '85938336312', 'Find yourself a girl for the night in your city', 'Sexy girls for the night in your town: http://tropaadet.dk/adultdating47883', 'waiting-confirmation', '2019-02-11 00:57:55', '2019-02-11 00:57:55'),
(46, 'JavierFup', 'tractor_girl41@yahoo.com', '89235836996', '$10000 per day Bitcoin Brokers | Top Bitcoin Brokers Reviews | Bitcoin Binary Options', '$10000 per day Bitcoin Binary Options / Best Brokers in Review: http://webhop.se/10000perday43440', 'waiting-confirmation', '2019-02-11 21:39:26', '2019-02-11 21:39:26'),
(47, 'JavierFup', 'phoebe0887@yahoo.com', '85468598542', 'Treffen Sie sexy Madchen in Ihrer Stadt', 'Sexy Girls fur die Nacht in deiner Stadt: http://perkele.ovh/appadultdating44896', 'waiting-confirmation', '2019-02-12 21:00:29', '2019-02-12 21:00:29'),
(48, 'JavierFup', 'williamwalstab@msn.com', '81742317266', 'Sexy Girls fur die Nacht in deiner Stadt', 'Suchen Sie sich ein Madchen fur die Nacht in Ihrer Stadt: http://www.abcagency.se/appadultdating88269', 'waiting-confirmation', '2019-02-13 10:03:56', '2019-02-13 10:03:56'),
(49, 'JavierFup', 'carylt@hotmail.com', '83671267552', 'Beautiful women for sex in your town', 'The best girls for sex in your town: http://yourls.site/bestadultsitesex84302', 'waiting-confirmation', '2019-02-14 11:02:47', '2019-02-14 11:02:47'),
(50, 'JavierFup', 'barhoper44@hotmail.com', '83919154754', 'The best girls for sex in your town', 'Meet sexy girls in your city: http://valeriemace.co.uk/bestadultsitesex23800', 'waiting-confirmation', '2019-02-15 04:06:59', '2019-02-15 04:06:59'),
(51, 'JavierFup', 't_jackass3@hotmail.com', '87847227472', 'Beautiful girls for sex in your city', 'The best women for sex in your town: http://corta.co/bestsexygirlinyourcity64768', 'waiting-confirmation', '2019-02-15 23:59:53', '2019-02-15 23:59:53'),
(52, 'JavierFup', 'raining79@yahoo.com', '83221837777', 'Meet sexy girls in your city', 'Find yourself a girl for the night in your city: http://awalsh.com/sexygirlsinyourcity59131', 'waiting-confirmation', '2019-02-17 04:56:28', '2019-02-17 04:56:28'),
(53, 'JavierFup', 'shonaromero@yahoo.com', '87386653952', 'Get $1000 – $6000 A Day', '$15,000 a month (30mins “work” lol): https://tinyurl.com/passiveincome-1000039329', 'waiting-confirmation', '2019-02-18 15:05:54', '2019-02-18 15:05:54'),
(54, 'JavierFup', 'dinker888@yahoo.com', '84298432426', 'Meet sexy girls in your city', 'The best girls for sex in your town: http://awalsh.com/getsexinyourcity20734', 'waiting-confirmation', '2019-02-19 21:08:12', '2019-02-19 21:08:12'),
(55, 'JavierFup', 'elleon12@msn.com', '84665863884', 'Sexy girls for the night in your town', 'Meet sexy girls in your city: http://ralive.de/getsexinyourcity85880', 'waiting-confirmation', '2019-02-20 21:53:42', '2019-02-20 21:53:42'),
(56, 'JavierFup', 'patrossiter@hotmail.com', '81266522732', 'Find yourself a girl for the night in your city', 'Find yourself a girl for the night in your city: http://valeriemace.co.uk/getsexinyourcity91008', 'waiting-confirmation', '2019-02-21 15:27:45', '2019-02-21 15:27:45'),
(57, 'StanleyDrula', 'yourmail@gmail.com', '86876468548', 'Test, just a test', 'Hello. And Bye.', 'waiting-confirmation', '2019-02-21 22:38:35', '2019-02-21 22:38:35'),
(58, 'JavierFup', 'burnsml86@yahoo.com', '85188915695', 'Find yourself a girl for the night in your city', 'The best girls for sex in your town: https://aaa.moda/getsexinyourcity76563', 'waiting-confirmation', '2019-02-23 02:27:28', '2019-02-23 02:27:28'),
(59, 'JavierFup', 'bufordjoe@hotmail.com', '88612745373', 'The best women for sex in your town', 'Beautiful women for sex in your town: http://www.abcagency.se/getsexinyourcity63444', 'waiting-confirmation', '2019-02-24 00:17:21', '2019-02-24 00:17:21'),
(60, 'Joshuaclofs', 'kuklina-m@yandex.ru', '81577498435', 'Beautiful girls for sex in your city', 'The best girls for sex in your town: http://valeriemace.co.uk/getsexinyourcity52903', 'waiting-confirmation', '2019-02-24 14:16:54', '2019-02-24 14:16:54'),
(61, 'JavierFup', 'dmtblue@aol.com', '82814626954', 'Find yourself a girl for the night in your city', 'Meet sexy girls in your city: http://www.lookweb.it/getsexinyourcity63946', 'waiting-confirmation', '2019-02-24 20:53:37', '2019-02-24 20:53:37'),
(62, 'Joshuaclofs', 'bulatova-1983@mail.ru', '82185741697', 'Find yourself a girl for the night in your city', 'Find yourself a girl for the night in your city: http://corta.co/bestsexygirlsinyourcity27225', 'waiting-confirmation', '2019-02-25 15:26:59', '2019-02-25 15:26:59'),
(63, 'JavierFup', 'rampage_9@msn.com', '81324389527', 'Meet sexy girls in your city', 'Beautiful women for sex in your town: http://webhop.se/getsexinyourcity38290', 'waiting-confirmation', '2019-02-26 13:48:10', '2019-02-26 13:48:10'),
(64, 'Joshuaclofs', 'olga282007@rambler.ru', '87644655693', 'The best women for sex in your town', 'Beautiful women for sex in your town: http://ralive.de/getsexinyourcity58292', 'waiting-confirmation', '2019-02-26 18:58:24', '2019-02-26 18:58:24'),
(65, 'HollisSlari', 'sycfok@hotmail.com', '82783478832', '$10000 per day Bitcoin Binary Options / Best Brokers in Review', '$10000 per day Bitcoin Trading with Binary Options: http://corta.co/investcrypto90078015', 'waiting-confirmation', '2019-02-26 20:47:04', '2019-02-26 20:47:04'),
(66, 'Raymondamigh', 'santospi@hotmail.com', '88771673694', 'Natural Stress Solutions CBD Relief Body Lotion', 'Natural Stress Solutions CBD Capsules Night (Sleep): http://tropaadet.dk/pills92123', 'waiting-confirmation', '2019-02-26 20:55:29', '2019-02-26 20:55:29'),
(67, 'VincentNon', 'dtiedt@hotmail.com', '87222359528', 'You almost won an New iPhone X', 'Win New Iphone X: http://yourls.site/ouinphone34837', 'waiting-confirmation', '2019-02-26 20:57:03', '2019-02-26 20:57:03'),
(68, 'Jamesseido', 'clk21@hotmail.com', '88218581219', 'Centrum High-potency Multivitamin Supplement 60 Tablets', '3 Ways to Last Longer in Bed: http://to.ht/astongerined52831', 'waiting-confirmation', '2019-02-26 21:01:36', '2019-02-26 21:01:36'),
(69, 'HollisSlari', 'info@treat-nmd.eu', '81623414862', '$10000 per day Best Bitcoin Binary Options | Crunchbase', 'LAZY way for $200 in 20 mins: http://www.abcagency.se/investcrypto90097602', 'waiting-confirmation', '2019-02-27 12:37:24', '2019-02-27 12:37:24'),
(70, 'VincentNon', 'mwbaabnwb@yahoo.com', '82272687751', 'Win an iPhone X', 'You win New iPhone X: http://webhop.se/ouinphone48379', 'waiting-confirmation', '2019-02-28 11:41:32', '2019-02-28 11:41:32'),
(71, 'Jamesseido', 'robstott@hotmail.com', '89465711641', '15 Easy Ways to Make Sex Last', '8 sex tips to last longer in bed: http://tropaadet.dk/astongerined64170', 'waiting-confirmation', '2019-02-28 14:46:06', '2019-02-28 14:46:06'),
(72, 'Raymondamigh', 'bert203@hotmail.com', '88875873791', 'Natural Stress Solutions CBD Capsules Daytime (Standard)', 'Natural Stress Solutions Pure CBD Tincture: https://arill.us/pills58404', 'waiting-confirmation', '2019-02-28 19:16:22', '2019-02-28 19:16:22'),
(73, 'HollisSlari', 'kfixen@aol.com', '89777225354', '$15,000 a month (30mins вЂњworkвЂќ lol)', 'How to make $3000 a day: http://www.abcagency.se/investcrypto90092603', 'waiting-confirmation', '2019-03-01 02:37:14', '2019-03-01 02:37:14'),
(74, 'VincentNon', 'cordelia9@hotmail.com', '84599393927', 'You win New iPhone X', 'You have a chance to win an New iPhone: http://jnl.io/ouinphone56533', 'waiting-confirmation', '2019-03-01 02:45:30', '2019-03-01 02:45:30'),
(75, 'HollisSlari', 'cova40@hotmail.com', '88524344129', 'Turning $10,000 into $1 Million in Forex | DailyForex', 'How To Make $100,000 Per Month With Forex Signals: https://lil.ink/investcrypto40824', 'waiting-confirmation', '2019-03-03 03:00:43', '2019-03-03 03:00:43'),
(76, 'Raymondamigh', 'taka210@hotmail.com', '88982651651', 'Natural Stress Solutions CBD Capsules Night (Sleep)', 'Natural Stress Solutions Pure CBD Tincture: http://corta.co/investcrypto75976', 'waiting-confirmation', '2019-03-03 03:17:03', '2019-03-03 03:17:03'),
(77, 'VincentNon', 'dnovot@hotmail.com', '82328752365', 'You have a chance to win an New iPhone', 'How To Remove \"Win iPhone X\" Pop-ups: https://aaa.moda/investcrypto58249', 'waiting-confirmation', '2019-03-03 03:36:02', '2019-03-03 03:36:02'),
(78, 'Eric', 'eric@talkwithcustomer.com', '416-385-3200', 'Do You Want Up to 100X More Conversions?', 'Hello,\r\n\r\nYou have a website, right? \r\n  \r\nOf course you do. I found oriscake.com today.\r\n\r\nIt gets traffic every day – that you’re probably spending $2 / $4 / $10 or more a click to get.  Not including all of the work you put into creating social media, videos, blog posts, emails, and so on.\r\n\r\nSo you’re investing seriously in getting people to that site.\r\n\r\nBut how’s it working?  Great? Okay?  Not so much?\r\n\r\nIf that answer could be better, then it’s likely you’re putting a lot of time, effort, and money into an approach that’s not paying off like it should.\r\n\r\nNow… imagine doubling your lead conversion in just minutes… In fact, I’ll go even better.\r\n \r\nYou could actually get up to 100X more conversions!\r\n\r\nI’m not making this up.  As Chris Smith, best-selling author of The Conversion Code says:\r\n-Speed is essential - there is a 100x decrease in Leads when a Lead is contacted within 14 minutes vs being contacted within 5 minutes.\r\n\r\nHe’s backed up by a study at MIT that found the odds of contacting a lead will increase by 100 times if attempted in 5 minutes or less.\r\n\r\nAgain, out of the 100s of visitors to your website, how many actually call to become clients?\r\n\r\nWell, you can significantly increase the number of calls you get – with ZERO extra effort.\r\n\r\nOur software, Talk With Customer, makes it easy, simple, and fast – in fact, you can start getting more calls today… and at absolutely no charge to you.\r\n\r\nCLICK HERE http://www.talkwithcustomer.com to find out how.\r\n\r\nSincerely,\r\n\r\nEric Jones\r\n\r\nPS: Don’t just take my word for it, TalkWithCustomer works:\r\n-EMA has been looking for ways to reach out to an audience. TalkWithCustomer so far is the most direct call of action. It has produced above average closing ratios and we are thrilled. Thank you for providing a real and effective tool to generate REAL leads. - Patrick MontesDeOca, Ema2Trade\r\n\r\nBest of all, act now to get a no-cost 14-Day Test Drive – our gift to you just for giving TalkWithCustomer a try. \r\n\r\nCLICK HERE http://www.talkwithcustomer.com to start converting up to 100X more leads today!\r\n\r\n\r\nIf you\'d like to unsubscribe click here. http://liveserveronline.com/talkwithcustomer.aspx?d=oriscake.com', 'waiting-confirmation', '2019-03-03 09:43:18', '2019-03-03 09:43:18'),
(79, 'Raymondamigh', 'fischt@msn.com', '87576564688', 'Beautiful girls for sex in your city', 'Find yourself a girl for the night in your city: http://valeriemace.co.uk/bestsexygirldating97509', 'waiting-confirmation', '2019-03-04 22:59:00', '2019-03-04 22:59:00'),
(80, 'HollisSlari', 'droopykbg@yahoo.com', '81665714254', 'The best women for sex in your town', 'Sexy girls for the night in your town: http://corta.co/bestsexygirlsinyourcity97023', 'waiting-confirmation', '2019-03-04 22:59:00', '2019-03-04 22:59:00'),
(81, 'Jamesseido', 'sumfun10@hotmail.com', '86852368589', 'Sexy girls for the night in your town', 'Beautiful women for sex in your town: http://to.ht/bestsexygirldating79304', 'waiting-confirmation', '2019-03-04 23:02:11', '2019-03-04 23:02:11'),
(82, 'VincentNon', 'tnyellowrose83@charter.net', '86168382841', 'Meet sexy girls in your city', 'The best women for sex in your town: https://arill.us/bestsexygirldating98713', 'waiting-confirmation', '2019-03-04 23:04:25', '2019-03-04 23:04:25'),
(83, 'HollisSlari', 'goldwingdoug@comcast.net', '89771584471', 'Beautiful women for sex in your town', 'Beautiful women for sex in your town: https://tinyurl.com/bestsexygirlsdating24304', 'waiting-confirmation', '2019-03-06 12:00:45', '2019-03-06 12:00:45'),
(84, 'VincentNon', 'enforcing@msn.com', '83893161967', 'Find yourself a girl for the night in your city', 'The best women for sex in your town: http://valeriemace.co.uk/bestsexygirlsdating61392', 'waiting-confirmation', '2019-03-06 12:08:41', '2019-03-06 12:08:41'),
(85, 'Jamesseido', 'cchen627@hotmail.com', '85476753389', 'Meet sexy girls in your city', 'Sexy girls for the night in your town: https://aaa.moda/bestsexygirlsdating34490', 'waiting-confirmation', '2019-03-06 12:08:56', '2019-03-06 12:08:56'),
(86, 'HollisSlari', 'josephsauer@yahoo.com', '87273221172', 'How To Make $10,000 a Day Trading Forex - The Most EASIEST Way For Beginners (SECRET REVEALED)', 'How To Make $10,000 a Day Trading Forex - The Most EASIEST Way For Beginners (SECRET REVEALED): https://tinyurl.com/cryptoinvestbitcoin64481', 'waiting-confirmation', '2019-03-07 12:19:37', '2019-03-07 12:19:37'),
(87, 'Jamesseido', 'juhee1@hotmail.com', '88939517113', 'Wie man aus в‚¬ 3.000 в‚¬ 128.000 macht', 'Was ist der einfachste Weg, um в‚¬ 3000 pro Monat zu verdienen?: http://www.lookweb.it/cryptoinvestbitcoin94313', 'waiting-confirmation', '2019-03-07 12:25:43', '2019-03-07 12:25:43'),
(88, 'VincentNon', 'bds359@hotmail.com', '81132575322', 'Comment transformer 3 000 € en 128 000 €', 'Comment gagner 3000 € par jour: https://tinyurl.com/cryptoinvestbitcoin29249', 'waiting-confirmation', '2019-03-07 12:26:34', '2019-03-07 12:26:34'),
(89, 'Raymondamigh', 't_nakahara@hotmail.com', '86258934816', '10 Best Cryptocurrency to Invest in 2019', 'Top Cryptocurrencies To Invest In 2018-2019: http://jnl.io/cryptoinvestbitcoin86714', 'waiting-confirmation', '2019-03-07 12:41:09', '2019-03-07 12:41:09'),
(90, 'Eric', 'eric@talkwithcustomer.com', '416-385-3200', 'Do You Want Up to 100X More Conversions for oriscake.com ?', 'Hello,\r\n\r\nYou have a website, right? \r\n  \r\nOf course you do. I found oriscake.com today.\r\n\r\nIt gets traffic every day – that you’re probably spending $2 / $4 / $10 or more a click to get.  Not including all of the work you put into creating social media, videos, blog posts, emails, and so on.\r\n\r\nSo you’re investing seriously in getting people to that site.\r\n\r\nBut how’s it working?  Great? Okay?  Not so much?\r\n\r\nIf that answer could be better, then it’s likely you’re putting a lot of time, effort, and money into an approach that’s not paying off like it should.\r\n\r\nNow… imagine doubling your lead conversion in just minutes… In fact, I’ll go even better.\r\n \r\nYou could actually get up to 100X more conversions!\r\n\r\nI’m not making this up.  As Chris Smith, best-selling author of The Conversion Code says:\r\n-Speed is essential - there is a 100x decrease in Leads when a Lead is contacted within 14 minutes vs being contacted within 5 minutes.\r\n\r\nHe’s backed up by a study at MIT that found the odds of contacting a lead will increase by 100 times if attempted in 5 minutes or less.\r\n\r\nAgain, out of the 100s of visitors to your website, how many actually call to become clients?\r\n\r\nWell, you can significantly increase the number of calls you get – with ZERO extra effort.\r\n\r\nOur software, Talk With Customer, makes it easy, simple, and fast – in fact, you can start getting more calls today… and at absolutely no charge to you.\r\n\r\nCLICK HERE http://www.talkwithcustomer.com to find out how.\r\n\r\nSincerely,\r\n\r\nEric Jones\r\n\r\nPS: Don’t just take my word for it, TalkWithCustomer works:\r\n-EMA has been looking for ways to reach out to an audience. TalkWithCustomer so far is the most direct call of action. It has produced above average closing ratios and we are thrilled. Thank you for providing a real and effective tool to generate REAL leads. - Patrick MontesDeOca, Ema2Trade\r\n\r\nBest of all, act now to get a no-cost 14-Day Test Drive – our gift to you just for giving TalkWithCustomer a try. \r\n\r\nCLICK HERE http://www.talkwithcustomer.com to start converting up to 100X more leads today!\r\n\r\n\r\nIf you\'d like to unsubscribe click here. http://liveserveronline.com/talkwithcustomer.aspx?d=oriscake.com', 'waiting-confirmation', '2019-03-08 09:03:04', '2019-03-08 09:03:04'),
(91, 'Raymondamigh', 'complex9@hotmail.com', '85568895726', 'Best cryptocurrency to Invest 2019', 'Top Cryptocurrencies To Invest In 2018-2019: http://valeriemace.co.uk/bestinvestcryptobitcoin43568', 'waiting-confirmation', '2019-03-09 11:34:39', '2019-03-09 11:34:39'),
(92, 'Jamesseido', 'bigjers@hotmail.com', '88514337176', 'Was ist der einfachste Weg, um в‚¬ 3000 pro Monat zu verdienen?', 'Wie mache ich в‚¬ 3000 pro Tag?: http://jnl.io/bestinvestcryptobitcoin77722', 'waiting-confirmation', '2019-03-09 11:49:15', '2019-03-09 11:49:15'),
(93, 'VincentNon', 'suigeneris@myself.com', '85177556825', 'J\'ai 23 000 €. Comment l\'utiliser au mieux pour gagner plus d\'argent', 'Comment gagner 3000 € Fast Fast Money | Le budget occupe: https://aaa.moda/bestinvestcryptobitcoin99393', 'waiting-confirmation', '2019-03-09 11:49:47', '2019-03-09 11:49:47'),
(94, 'HollisSlari', 'toddm66@hotmail.com', '83978279574', 'How You Can Make $100,000 Every Month Trading Forex', 'Forex Prodigy Makes $10,000 in 30 Minutes! - Market Traders Institute: http://www.abcagency.se/bestinvestcryptobitcoin38184', 'waiting-confirmation', '2019-03-11 15:38:36', '2019-03-11 15:38:36'),
(95, 'Raymondamigh', 'e_gaudreau@hotmail.com', '81552678725', 'The Power of Senuke TNG Linkbuilding software', 'SEnuke TNG - Rank #1 With Todays Top Ranking Factors: http://www.vkvi.net/bestseotools45979', 'waiting-confirmation', '2019-03-11 18:18:51', '2019-03-11 18:18:51'),
(96, 'Jamesseido', 'bruce_fernando@hotmail.com', '86254927883', 'Simple biz + new tool = $450 per hour', 'How to Make $30000 FAST | Fast Money | The Busy Budgeter: http://valeriemace.co.uk/milliondollars17955', 'waiting-confirmation', '2019-03-11 18:23:29', '2019-03-11 18:23:29'),
(97, 'VincentNon', 'bluenosedeano@hotmail.com', '81744563547', 'Forex trader makes $10,000 in minutes', 'Forex trader makes $10,000 in minutes: http://www.vkvi.net/milliondollarsforex97102', 'waiting-confirmation', '2019-03-11 18:28:58', '2019-03-11 18:28:58'),
(98, 'HollisSlari', 'ghetto_snowbunny_69@yahoo.com', '86666235597', 'What Is High Quality Traffic & How Do I Get More of It', 'How to Get More Traffic | Quality Traffic: https://lil.ink/getmoretraffic72232', 'waiting-confirmation', '2019-03-12 16:05:57', '2019-03-12 16:05:57'),
(99, 'Raymondamigh', 'batrab@hotmail.com', '85614933214', 'The Power of Senuke TNG Linkbuilding software', 'SEnuke TNG 2019 Version Reviewed: DISCOUNT & HUGE BONUS: http://to.ht/bestseotools90299', 'waiting-confirmation', '2019-03-14 01:50:25', '2019-03-14 01:50:25'),
(100, 'Jamesseido', 'duke850@hotmail.com', '81934481692', '$200 for 10 mins вЂњwork?вЂќ', 'LAZY way for $200 in 20 mins: https://aaa.moda/milliondollars59228', 'waiting-confirmation', '2019-03-14 02:37:40', '2019-03-14 02:37:40'),
(101, 'VincentNon', 'eric_stout@hotmail.com', '83783972497', 'How To Make $100,000 Per Month With Forex Signals', 'How To Make $100,000 Per Month With Forex Signals: http://swish.st/milliondollarsforex28097', 'waiting-confirmation', '2019-03-14 11:10:32', '2019-03-14 11:10:32'),
(102, 'Raymondamigh', 'spirifer@hotmail.com', '89534522576', 'Filles sexy pour la nuit dans votre ville', 'Filles sexy pour la nuit dans votre ville: http://go.fireontherim.com/bestsexygirlsadultdating63171', 'waiting-confirmation', '2019-03-15 01:24:45', '2019-03-15 01:24:45'),
(103, 'VincentNon', 'jjmcjunkin@hotmail.com', '82873234659', 'Suchen Sie sich ein Madchen fur die Nacht in Ihrer Stadt', 'Treffen Sie sexy Madchen in Ihrer Stadt: http://www.lookweb.it/bestsexygirlsadultdating26114', 'waiting-confirmation', '2019-03-15 01:32:35', '2019-03-15 01:32:35'),
(104, 'Jamesseido', 'elsindo@msn.com', '84715854193', 'Conoce chicas sexy en tu ciudad', 'Chicas sexys para pasar la noche en tu pueblo.: http://jnl.io/bestsexygirlsadultdating97879', 'waiting-confirmation', '2019-03-15 02:18:44', '2019-03-15 02:18:44'),
(105, 'HollisSlari', 'mijoe2@msn.com', '86351125252', 'The best girls for sex in your town', 'The best girls for sex in your town: http://webhop.se/bestsexygirlsadultdating12582', 'waiting-confirmation', '2019-03-15 08:03:20', '2019-03-15 08:03:20'),
(106, 'Olivia Riggs', 'oliviariggs.online@gmail.com', '012-012-0120', 'Increase traffic to your website', 'Hi\r\n\r\nWe are interested to increase traffic to your website.\r\n\r\nPlease get back to us in order to discuss the possibility in further detail. Please mention your phone number and suitable time to talk.\r\n\r\nThanks', 'waiting-confirmation', '2019-03-15 18:03:15', '2019-03-15 18:03:15');
INSERT INTO `contact_messages` (`id`, `name`, `email`, `phone`, `subject`, `message`, `status`, `created_at`, `updated_at`) VALUES
(107, 'contactkbagdn', 'margert_citrin38@rambler.ru', '123456789', 'Newsletters  via contact forms  to the sites of firms via any countries of the world in any languages.', 'Dear sir! \r\n \r\nWe will send Your messages via contact forms to the sites of companies via all countries and domain zones of the world.  \r\n \r\nYour letter is sent to E-mail address \r\n of institution one hundred percent will get to inbox! \r\n \r\nTest: \r\n10000 messages on foreign zones to your E-mail - 20 dollars. \r\nWe need from You only E-mail, title and text of the letter. \r\n \r\nIn our price list there are more 800 databases for all domain zones of the world. \r\nCommon databases: \r\nAll Europe 44 countries 60726150 of domain names - 1100$ \r\nAll European Union 28 countries 56752547 of domain names- 1000$ \r\nAll Asia 48 countries 14662004 of sites - 300$ \r\nAll Africa 50 countries 1594390 of domains - 200$ \r\nAll North and Central America in 35 countries 7441637 of domain names - 300$ \r\nAll South America 14 countries 5826884 of sites - 200$ \r\nNew domain names from around the world registered 24-48 hours ago. (A cycle of 15 mailings during the month) - 500$ \r\nEnterprises and organizations of Russia 3012045 - 300$ \r\nUkraine 605745 of domains - 100$ \r\nAll Russian-speaking countries minus Russia are 15 countries and there are 1526797 of domain names - 200$ \r\nNew sites of the RF, registered 24-48 hours ago (A cycle of 15 mailings during the month) - 250$ \r\n \r\nDatabases for sending newsletters: \r\nWhois-service databases of domains for all nations of the world. \r\nYou can purchase our databases separately from newsletter\'s service at the request. \r\n \r\nP.S. \r\nPls., do not respond to this message from your email account, as it has been generated automatically and will not reach us! \r\nContact E-mail: feedback-contact@seznam.cz \r\n \r\nPRICE LIST: \r\n \r\nTest mailing: $20 – 10000 contact forms websites \r\n \r\nAll Europe 44 countries there are 60726150 websites – $1100 \r\n \r\nAll EU 28 countries there are 56752547 websites – $1000 \r\n \r\nAll Asia 48 countries there are 14662004 websites – $500 \r\n \r\nAll Africa 50 countries there are 1594390 websites – $200 \r\n \r\nAll North and Central America is 35 countries there are 7441637 websites – $300 \r\n \r\nAll South America 14 countries there are 5826884 websites – $200 \r\n \r\nTop 1 Million World’s Best websites – $100 \r\n \r\nTop 16821856 the most visited websites in the world – $200 \r\n \r\nBusinesses and organizations of the Russian Federation – there are 3012045 websites – $300 \r\n \r\nUkraine 605745 websites – $100 \r\n \r\nAll Russian-speaking countries minus Russia – there are 15 countries and 1526797 websites – $200 \r\n \r\n1499203 of hosting websites around the world (there are selections for all countries, are excluded from databases for mailings) – $200 \r\n \r\n35439 websites of public authorities of all countries of the world (selections for all countries, are excluded from databases for mailings) – $100 \r\n \r\nCMS mailings: \r\nAmiro 2294 websites $50 \r\nBitrix 175513 websites $80 \r\nConcrete5 49721 websites $50 \r\nCONTENIDO 7769 websites $50 \r\nCubeCart 1562 websites $50 \r\nDatalife Engine 29220 websites $50 \r\nDiscuz 70252 websites $50 \r\nDotnetnuke 31114 websites $50 \r\nDrupal 802121 websites $100 \r\nHostCMS 6342 websites $50 \r\nInstantCMS 4936 websites $50 \r\nInvision Power Board 510 websites $50 \r\nJoomla 1906994 websites $200 \r\nLiferay 5937 websites $50 \r\nMagento 269488 websites $80 \r\nMODx 67023 websites $50 \r\nMovable Type 13523 websites $50 \r\nNetCat 6936 websites $50 \r\nNopCommerce 5313 websites $50 \r\nOpenCart 321057 websites $80 \r\nosCommerce 65468 websites $50 \r\nphpBB 3582 websites $50 \r\nPrestashop 92949 websites $50 \r\nShopify 365755 websites $80 \r\nSimpla 8963 websites $50 \r\nSitefinity 4883 websites $50 \r\nTYPO3 227167 websites $80 \r\nUMI.CMS 15943 websites $50 \r\nvBulletin 154677 websites $80 \r\nWix 2305768 websites $230 \r\nWordPress 14467405 websites $450 \r\nWooCommerce 2097367 websites $210 \r\n \r\n.com 133766112 websites commercial – $1950 \r\n.biz 2361884 websites business – $150 \r\n.info 6216929 websites information – $250 \r\n.net 15689222 websites network – $450 \r\n.org 10922428 websites organization – $350 \r\n \r\n.abogado 279 websites – $50 \r\n.ac 16799 websites – $50 \r\n.academy 27306 websites – $50 \r\n.accountant 96542 websites – $50 \r\n.actor 1928 websites – $50 \r\n.ad 414 websites – $50 \r\n.adult 10540 websites- $50 \r\n.ae 1821 websites International zone UAE:.com.net.biz.info.org.name.tel.mobi.asia \r\n.ae 199533 websites UAE – $50 \r\n.aero 18325 websites- $50 \r\n.af 3315 websites – $50 \r\n.africa 15056 websites- $50 \r\n.ag 10339 websites – $50 \r\n.agency 47508 websites – $50 \r\n.ai 17199 websites – $50 \r\n.airforce 560 websites – $50 \r\n.al 6078 websites – $50 \r\n.alsace 1982 websites – $50 \r\n.am 17987 websites Armenia – $50 \r\n.am 1684 websites International zone Armenia:.com.net.biz.info.org.name.tel.mobi.asia \r\n.amsterdam 28141 websites Amsterdam, Kingdom of the Netherlands – $50 \r\n.ao 518 websites – $50 \r\n.apartments 3758 websites – $50 \r\n.ar 551804 websites Argentina – $80 \r\n.ar 64008 websites International zone Argentina:.com .net .biz .info .name .tel .mobi .asia – $50 \r\n.archi 2084 websites – $50 \r\n.army 1842 websites – $50 \r\n.art 26402 websites – $50 \r\n.as 10025 websites – $50 \r\n.asia 228418 websites – $80 \r\n.associates 3340 websites – $50 \r\n.at 1356722 websites Austria – $100 \r\n.at 181907 websites International zone Austria :.com.net.biz.info.org.name.tel.mobi.asia-$50 \r\n.attorney 8224 websites- $50 \r\n.attorney 7204 websites – $50 \r\n.au 2243263 websites Australia – $150 \r\n.au 461279 websites International zone Australia:.com.net.biz.info.org.name.tel.mobi.asia-$80 \r\n.auction 3625 websites- $50 \r\n.audio 23052 websites- $50 \r\n.auto 400 websites- $50 \r\n.aw 235 websites- $50 \r\n.az 11104 websites Azerbaijan – $50 \r\n.az 2036 websites International zone Azerbaijan:.com.net.biz.info.org.name.tel.mobi.asia \r\n.ba 7012 websites – $50 \r\n.ba 2291 websites International zone Bosnia and Herzegovina:.com.net.biz.info.org.name.tel.mobi.asia \r\n.band 11515 websites – $50 \r\n.bank 1621 websites- $50 \r\n.bar 5506 websites – $50 \r\n.barcelona 7919 websites – $50 \r\n.bargains 2997 websites- $50 \r\n.bayern 32565 websites – $50 \r\n.bb 2277 websites – $50 \r\n.be 1349658 websites Belgium – $100 \r\n.be 184810 websites International zone Belgium:.com.net.biz.info.org.name.tel.mobi.asia-$50 \r\n.beer 11834 websites- $50 \r\n.berlin 58088 websites Berlin – $50 \r\n.best 2912 websites – $50 \r\n.bet 17637 websites – $50 \r\n.bf 238 websites – $50 \r\n.bg 33252 websites Bulgaria – $50 \r\n.bg 50685 websites International zone Bulgaria:.com.net.biz.info.org.name.tel.mobi.asia \r\n.bh 453 websites – $50 \r\n.bi 2328 websites Burundi- $50 \r\n.bible 1160 websites – $50 \r\n.bid 474509 websites – $80 \r\n.bike 15729 websites – $50 \r\n.bingo 1332 websites – $50 \r\n.bio 15531 websites- $50 \r\n.bj 147 websites- $50 \r\n.black 6582 websites – $50 \r\n.blackfriday 12106 websites – $50 \r\n.blog 145463 websites – $50 \r\n.blue 16852 websites – $50 \r\n.bm 8089 websites Bermuda – $50 \r\n.bo 2302 websites- $50 \r\n.boats 266 websites- $50 \r\n.boston 21762 websites- $50 \r\n.boutique 8834 websites – $50 \r\n.br 2367290 websites Brazil – $150 \r\n.br 933750 websites International zone Brazil:.com.net.biz.info.org.name.tel.mobi.asia-$100 \r\n.bradesco 129 websites- $50 \r\n.broadway 261 websites- $50 \r\n.broker 660 websites- $50 \r\n.brussels 7181 websites – $50 \r\n.bs 330 websites- $50 \r\n.bt 284 websites- $50 \r\n.build 3857 websites- $50 \r\n.builders 3906 websites- $50 \r\n.business 35168 websites – $50 \r\n.buzz 11257 websites – $50 \r\n.bw 656 websites – $50 \r\n.by 1574 websites International zone Belarus:.com.net.biz.info.org.name.tel.mobi.asia \r\n.by 92679 websites Belarus – $50 \r\n.bz 7751 websites – $50 \r\n.bzh 5403 websites – $50 \r\n.ca 2587463 websites Canada – $150 \r\n.ca 288395 websites International zone Canada:.com.net.biz.info.org.name.tel.mobi.asia-$50 \r\n.cab 3223 websites – $50 \r\n.cafe 13606 websites – $50 \r\n.cam 5156 websites – $50 \r\n.camera 5236 websites – $50 \r\n.camp 6315 websites – $50 \r\n.capetown 4750 websites – $50 \r\n.capital 11387 websites – $50 \r\n.car 342 websites – $50 \r\n.cards 5992 websites – $50 \r\n.care 18204 websites – $50 \r\n.career 1217 websites – $50 \r\n.careers 7055 websites – $50 \r\n.cars 309 websites – $50 \r\n.casa 18918 websites – $50 \r\n.cash 13193 websites – $50 \r\n.casino 4354 websites – $50 \r\n.cat 108569 websites – $50 \r\n.catering 3482 websites – $50 \r\n.cc 1920589 websites Cocos Keeling Islands- $150 \r\n.cd 5365 websites – $50 \r\n.center 35353 websites – $50 \r\n.ceo 2458 websites – $50 \r\n.cf 476142 websites Central African Republic – $50 \r\n.cg 166 – $50 \r\n.ch 1471685 websites Switzerland – $100 \r\n.ch 205292 websites International zone Switzerland:.com.net.biz.info.org.name.tel.mobi.asia-$50 \r\n.chat 11126 websites – $50 \r\n.cheap 3267 websites – $50 \r\n.christmas 15255 websites – $50 \r\n.church 21104 websites – $50 \r\n.ci 112 websites International zone Cote d’Ivoire:.com.net.biz.info.org.name.tel.mobi.asia \r\n.ci 5663 websites Cote d’Ivoire- $50 \r\n.city 46171 websites – $50 \r\n.cl 498401 websites Chile – $80 \r\n.claims 2374 websites – $50 \r\n.cleaning 2385 websites – $50 \r\n.click 181015 websites – $50 \r\n.clinic 7006 websites – $50 \r\n.clothing 13639 websites – $50 \r\n.cloud 134113 websites – $50 \r\n.club 1045323 websites – $100 \r\n.cm 12001 websites Cameroon- $50 \r\n.cn 1372416 websites International zone China:.com.net.biz.info.org.name.tel.mobi.asia-$100 \r\n.cn 7264587 websites China – $300 \r\n.co 1778923 websites Colombia – $150 \r\n.coach 12002 websites- $50 \r\n.codes 6844 websites – $50 \r\n.coffee 17257 websites – $50 \r\n.cologne 5137 websites – $50 \r\n.cologne 5198 websites – $50 \r\n.com.ar 657716 websites Argentina – $80 \r\n.com.br 942898 websites Brazil – $100 \r\n.com.cy 11153 websites Cyprus – $50 \r\n.com.ni 23747 websites – $50 \r\n.com.np 38828 websites – $50 \r\n.com.ru, .net.ru, .org.ru, .spb.ru, .msk.ru 79058 websites Russia – $50 \r\n.community 13013 websites – $50 \r\n.company 61217 websites – $50 \r\n.computer 5039 websites – $50 \r\n.condos 2192 websites – $50 \r\n.construction 6804 websites – $50 \r\n.consulting 22128 websites – $50 \r\n.contractors 3982 websites – $50 \r\n.cooking 1476 websites – $50 \r\n.cool 16008 websites – $50 \r\n.coop 7879 websites – $50 \r\n.corsica 1042 websites – $50 \r\n.country 7144 websites – $50 \r\n.cr 7934 websites – $50 \r\n.credit 4020 websites – $50 \r\n.creditcard 825 websites – $50 \r\n.creditunion 511 websites – $50 \r\n.cricket 33413 websites – $50 \r\n.cruises 2234 websites – $50 \r\n.cu 137 websites – $50 \r\n.cv 1879 websites – $50 \r\n.cx 15753 websites – $50 \r\n.cy 11092 websites Cyprus – $50 \r\n.cy 710 websites International zone Cyprus:.com.net.biz.info.org.name.tel.mobi.asia \r\n.cymru 7114 websites – $50 \r\n.cz 193400 websites International zone Czech Republic:.com.net.biz.info.org.name.tel.mobi.asia-$50 \r\n.cz 930208 websites Czech Republic – $80 \r\n.dance 6290 websites – $50 \r\n.date 123037 websites – $50 \r\n.dating 2892 websites – $50 \r\n.de 15078512 websites Germany – $450 \r\n.de 3894156 websites International zone Germany:.com.net.biz.info.org.name.tel.mobi.asia-$200 \r\n.deals 8132 websites – $50 \r\n.degree 2178 websites – $50 \r\n.delivery 4782 websites – $50 \r\n.democrat 1072 websites – $50 \r\n.dental 7541 websites – $50 \r\n.dentist 3046 websites – $50 \r\n.desi 2647 websites – $50 \r\n.design 71711 websites – $50 \r\n.diamonds 2730 websites – $50 \r\n.diet 18291 websites – $50 \r\n.digital 31449 websites – $50 \r\n.direct 10629 websites – $50 \r\n.directory 18157 websites – $50 \r\n.discount 3898 websites – $50 \r\n.dj 7280 websites – $50 \r\n.dk 1320155 websites Denmark – $100 \r\n.dk 148164 websites International zone Denmark:.com.net.biz.info.org.name.tel.mobi.asia-$50 \r\n.dm 23318 websites – $50 \r\n.do 5255 websites Dominican Republic- $50 \r\n.dog 10030 websites – $50 \r\n.domains 6553 websites – $50 \r\n.download 129223 websites – $50 \r\n.durban 2247 websites – $50 \r\n.dz 982 websites – $50 \r\n.earth 8139 websites – $50 \r\n.ec 11731 websites – $50 \r\n.edu 4445 websites – $50 \r\n.edu.np 4883 websites- $50 \r\n.education 22003 websites – $50 \r\n.ee 10490 websites International zone Estonia:.com.net.biz.info.org.name.tel.mobi.asia \r\n.ee 119701 websites Estonia- $50 \r\n.eg 1699 websites – $50 \r\n.email 77321 websites – $50 \r\n.energy 9769 websites – $50 \r\n.engineer 2785 websites – $50 \r\n.engineering 5533 websites – $50 \r\n.enterprises 6153 websites – $50 \r\n.equipment 5760 websites – $50 \r\n.es 1685048 websites Spain – $100 \r\n.es 541916 websites International zone Spain:.com.net.biz.info.org.name.tel.mobi.asia-$80 \r\n.estate 9185 websites – $50 \r\n.et 124 websites – $50 \r\n.eu 3321576 websites Europe – $150 \r\n.eu 633384 websites International zone Europe:.com.net.biz.info.org.name.tel.mobi.asia-$80 \r\n.eus 8116 websites – $50 \r\n.events 22115 websites – $50 \r\n.exchange 9432 websites – $50 \r\n.expert 31240 websites – $50 \r\n.exposed 3147 websites – $50 \r\n.express 6919 websites – $50 \r\n.fail 3322 websites – $50 \r\n.faith 54195 websites – $50 \r\n.family 15577 websites – $50 \r\n.fans 1388 websites – $50 \r\n.farm 13499 websites – $50 \r\n.fashion 12475 websites – $50 \r\n.feedback 2301 websites – $50 \r\n.fi 178337 websites Finland – $50 \r\n.fi 69631 websites International zone Finland:.com.net.biz.info.org.name.tel.mobi.asia – $50 \r\n.film 3601 websites – $50 \r\n.finance 7982 websites – $50 \r\n.financial 4086 websites – $50 \r\n.fish 4162 websites – $50 \r\n.fishing 1423 websites – $50 \r\n.fit 17007 websites – $50 \r\n.fitness 9689 websites – $50 \r\n.flights 2119 websites – $50 \r\n.florist 2286 websites – $50 \r\n.flowers 25590 websites – $50 \r\n.fm 5407 websites – $50 \r\n.fo 3098 websites- $50 \r\n.football 4877 websites – $50 \r\n.forex 212 websites – $50 \r\n.forsale 7118 websites – $50 \r\n.foundation 10118 websites – $50 \r\n.fr 2391045 websites France – $150 \r\n.fr 639546 websites International zone France:.com.net.biz.info.org.name.tel.mobi.asia-$80 \r\n.frl 14028 websites – $50 \r\n.fun 86419 websites – $50 \r\n.fund 11205 websites – $50 \r\n.furniture 2246 websites – $50 \r\n.futbol 2783 websites – $50 \r\n.fyi 9772 websites – $50 \r\n.ga 12048 websites Gabon – $50 \r\n.gal 4606 websites – $50 \r\n.gallery 17263 websites – $50 \r\n.game 1996 websites – $50 \r\n.games 13234 websites – $50 \r\n.garden 914 websites – $50 \r\n.gd 4238 websites – $50 \r\n.ge 1676 websites International zone Georgia:.com.net.biz.info.org.name.tel.mobi.asia \r\n.ge 17361 websites Georgia – $50 \r\n.gent 3389 websites – $50 \r\n.gf 121 websites – $50 \r\n.gg 9443 websites – $50 \r\n.gh 693 websites – $50 \r\n.gi 1063 websites – $50 \r\n.gift 6281 websites – $50 \r\n.gifts 3757 websites – $50 \r\n.gives 1563 websites – $50 \r\n.gl 3575 websites – $50 \r\n.glass 3539 websites – $50 \r\n.global 38972 websites – $50 \r\n.gm 468 websites – $50 \r\n.gmbh 19186 websites – $50 \r\n.gold 9081 websites – $50 \r\n.golf 8319 websites – $50 \r\n.gop 1341 websites – $50 \r\n.gov 4525 websites – $50 \r\n.gov.np 1937 websites- $50 \r\n.gp 416 websites – $50 \r\n.gq 23306 websites – $50 \r\n.gr 356168 websites Greece – $80 \r\n.gr 57984 websites International zone Greece:.com.net.biz.info.org.name.tel.mobi.asia-$50 \r\n.graphics 7155 websites – $50 \r\n.gratis 4283 websites – $50 \r\n.green 3661 websites – $50 \r\n.gripe 1075 websites – $50 \r\n.group 54983 websites – $50 \r\n.gs 5108 websites – $50 \r\n.gt 15351 websites – $50 \r\n.guide 16044 websites – $50 \r\n.guitars 1278 websites – $50 \r\n.guru 60588 websites – $50 \r\n.gy 2447 websites – $50 \r\n.hamburg 23885 websites – $50 \r\n.haus 5186 websites – $50 \r\n.health 6211 websites – $50 \r\n.healthcare 8051 websites – $50 \r\n.help 13500 websites – $50 \r\n.hiphop 1064 websites – $50 \r\n.hiv 331 websites – $50 \r\n.hk 116093 websites – $50 \r\n.hm 249 websites – $50 \r\n.hn 4732 websites – $50 \r\n.hockey 1102 websites – $50 \r\n.holdings 5412 websites – $50 \r\n.holiday 5017 websites – $50 \r\n.homes 432 websites – $50 \r\n.horse 2116 websites – $50 \r\n.host 31309 websites – $50 \r\n.hosting 4132 websites – $50 \r\n.house 18096 websites – $50 \r\n.how 1957 websites – $50 \r\n.hr 16592 websites International zone Croatia:.com.net.biz.info.org.name.tel.mobi.asia \r\n.hr 43565 websites Croatia – $50 \r\n.ht 2559 websites – $50 \r\n.hu 53940 websites International zone Hungary:.com.net.biz.info.org.name.tel.mobi.asia – $50 \r\n.hu 618532 websites Hungary – $80 \r\n.id 37212 websites – $50 \r\n.ie 195987 websites Ireland – $50 \r\n.ie 49861 websites International zone Ireland:.com.net.biz.info.org.name.tel.mobi.asia – $50 \r\n.il 224167 websites Israel – $80 \r\n.il 38537 websites International zone Israel:.com.net.biz.info.org.name.tel.mobi.asia-$50 \r\n.im 20701 websites – $50 \r\n.immo 16009 websites – $50 \r\n.immobilien 7094 websites – $50 \r\n.in 1143482 websites India – $100 \r\n.in 266179 websites International zone India:.com.net.biz.info.org.name.tel.mobi.asia – $50 \r\n.industries 3749 websites – $50 \r\n.ink 27117 websites – $50 \r\n.institute 10134 websites – $50 \r\n.insure 4615 websites – $50 \r\n.int 191 websites – $50 \r\n.international 23930 websites – $50 \r\n.investments 4113 websites – $50 \r\n.io 314287 websites British Indian Ocean – $50 \r\n.iq 1118 websites – $50 \r\n.ir 15487 websites International zone Iran:.com.net.biz.info.org.name.tel.mobi.asia \r\n.ir 427735 websites Iran- $80 \r\n.irish 3326 websites – $50 \r\n.is 31176 websites Iceland – $50 \r\n.ist 10060 websites – $50 \r\n.istanbul 13139 websites – $50 \r\n.it 2258105 websites Italy – $200 \r\n.it 954040 websites International zone Italy:.com.net.biz.info.org.name.tel.mobi.asia-$100 \r\n.je 2716 websites – $50 \r\n.jetzt 11118 websites – $50 \r\n.jetzt 11704 websites – $50 \r\n.jewelry 3350 websites – $50 \r\n.jo 555 websites – $50 \r\n.jobs 46350 websites- $50 \r\n.joburg 3139 websites – $50 \r\n.jp 1146243 websites Japan – $100 \r\n.juegos 844 websites – $50 \r\n.kaufen 7134 websites – $50 \r\n.kg 664 websites International zone Kyrgyzstan:.com.net.biz.info.org.name.tel.mobi.asia \r\n.kg 8919 websites Kyrgyzstan – $50 \r\n.ki 1146 websites – $50 \r\n.kim 16637 websites- $50 \r\n.kitchen 6581 websites – $50 \r\n.kiwi 19426 websites – $50 \r\n.kn 1271 websites – $50 \r\n.koeln 23489 websites – $50 \r\n.kr 254447 websites Korea- $50 \r\n.krd 375 websites – $50 \r\n.kred 6120 websites – $50 \r\n.kw 423 websites – $50 \r\n.ky 1201 websites – $50 \r\n.kyoto 659 websites – $50 \r\n.kz 112459 websites Kazakhstan – $50 \r\n.kz 5876 websites International zone Kazakhstan:.com.net.biz.info.org.name.tel.mobi.asia \r\n.la 32189 websites Laos – $50 \r\n.land 14474 websites- $50 \r\n.lat 2971 websites – $50 \r\n.law 11842 websites – $50 \r\n.lawyer 11600 websites- $50 \r\n.lc 651 websites- $50 \r\n.lease 1855 websites- $50 \r\n.leclerc 127 websites- $50 \r\n.legal 11047 websites- $50 \r\n.lgbt 2249 websites- $50 \r\n.li 12044 websites – $50 \r\n.life 170053 websites – $50 \r\n.lighting 6096 websites – $50 \r\n.limited 5365 websites – $50 \r\n.limo 2409 websites- $50 \r\n.link 133123 websites – $50 \r\n.live 160896 websites – $50 \r\n.lk 6601 websites – $50 \r\n.loan 1932173 websites- $200 \r\n.loans 3914 websites – $50 \r\n.lol 7470 websites- $50 \r\n.london 82443 websites London, United Kingdom- $50 \r\n.love 22287 websites- $50 \r\n.lt 27710 websites International zone Lithuania:.com.net.biz.info.org.name.tel.mobi.asia \r\n.lt 89073 websites Lithuania- $50 \r\n.ltd 329225 websites – $50 \r\n.lu 43052 websites Luxembourg – $50 \r\n.lu 4125 websites International zone Luxembourg:.com.net.biz.info.org.name.tel.mobi.asia \r\n.luxury 905 websites – $50 \r\n.lv 61886 websites Latvia- $50 \r\n.lv 8887 websites International zone Latvia:.com.net.biz.info.org.name.tel.mobi.asia \r\n.ly 8013 websites – $50 \r\n.ma 51862 websites Morocco – $50 \r\n.maison 1123 websites – $50 \r\n.management 10088 websites- $50 \r\n.market 22167 websites- $50 \r\n.marketing 18262 websites- $50 \r\n.markets 816 websites- $50 \r\n.mba 3023 websites- $50 \r\n.mc 3036 websites Monaco – $50 \r\n.md 15435 websites Moldova – $50 \r\n.md 1293 websites International zone Moldova:.com.net.biz.info.org.name.tel.mobi.asia \r\n.me 761596 websites Montenegro – $80 \r\n.me 86897 websites International zone Montenegro:.com.net.biz.info.org.name.tel.mobi.asia – $50 \r\n.media 40573 websites – $50 \r\n.melbourne 10041 websites – $50 \r\n.memorial 512 websites – $50 \r\n.men 470140 websites – $80 \r\n.menu 5512 websites restaurants- $50 \r\n.mg 3380 websites Madagascar- $50 \r\n.miami 14431 websites Miami, USA – $50 \r\n.mk 12704 websites – $50 \r\n.ml 70170 websites – $50 \r\n.mma 1706 websites – $50 \r\n.mn 16710 websites – $50 \r\n.mo 573 websites – $50 \r\n.mobi 462419 websites- $80 \r\n.moda 3015 websites – $50 \r\n.moe 7709 websites – $50 \r\n.mom 3145 websites – $50 \r\n.money 9731 websites – $50 \r\n.mortgage 3126 websites – $50 \r\n.moscow 22097 websites Moscow Russian Federation- $50 \r\n.movie 2575 websites – $50 \r\n.mr 1725 websites – $50 \r\n.ms 7565 websites – $50 \r\n.mt 1623 websites Malta – $50 \r\n.mu 6175 websites – $50 \r\n.museum 34464 websites – $50 \r\n.mv 1907 websites – $50 \r\n.mw 8579 websites Malawi – $50 \r\n.mx 609278 websites Mexico- $80 \r\n.mx 86593 websites International zone Mexico:.com.net.biz.info.org.name.tel.mobi.asia-$50 \r\n.my 283349 websites Malaysia- $80 \r\n.mz 263 websites – $50 \r\n.na 781 websites – $50 \r\n.nagoya 7807 websites – $50 \r\n.name 135818 websites- $50 \r\n.navy 698 websites – $50 \r\n.nc 1240 websites – $50 \r\n.network 40018 websites – $50 \r\n.news 65546 websites – $50 \r\n.ng 16608 websites – $50 \r\n.ngo 3422 websites – $50 \r\n.ninja 49007 websites – $50 \r\n.nl 1019697 websites International zone Netherlands:.com.net.biz.info.org.name.tel.mobi.asia-$100 \r\n.nl 3566172 websites Netherlands – $200 \r\n.no 579185 websites Norway – $80 \r\n.no 74318 websites International zone Norway:.com.net.biz.info.org.name.tel.mobi.asia-$50 \r\n.nra 144 websites – $50 \r\n.nrw 18487 websites – $50 \r\n.nu 489605 websites Niue- $50 \r\n.nyc 67153 websites – $50 \r\n.nz 576127 websites New Zealand – $80 \r\n.om 1603 websites – $50 \r\n.one 63063 websites – $50 \r\n.ong 3422 websites – $50 \r\n.onl 3790 websites – $50 \r\n.online 695007 websites – $50 \r\n.ooo 6400 websites – $50 \r\n.org.np 7082 websites – $50 \r\n.org.ua 41362 websites – $50 \r\n.organic 1633 websites – $50 \r\n.osaka 628 websites – $50 \r\n.ovh 50056 websites – $50 \r\n.pa 1558 websites – $50 \r\n.paris 19283 websites – $50 \r\n.partners 6576 websites – $50 \r\n.parts 5514 websites – $50 \r\n.party 115578 websites- $50 \r\n.pe 59157 websites International zone Peru:.com.net.biz.info.org.name.tel.mobi.asia – $50 \r\n.pe 69367 websites Peru – $50 \r\n.pet 8487 websites – $50 \r\n.pf 349 websites – $50 \r\n.pg 1979 websites Papua – $50 \r\n.ph 17940 websites Philippines – $50 \r\n.photo 17165 websites- $50 \r\n.photography 50434 websites- $50 \r\n.photos 19107 websites- $50 \r\n.physio 1160 websites- $50 \r\n.pics 7659 websites- $50 \r\n.pictures 7807 websites- $50 \r\n.pink 6373 websites- $50 \r\n.pizza 5949 websites – $50 \r\n.pk 44464 websites Pakistan – $50 \r\n.pl 1675325 websites Poland – $100 \r\n.pl 327587 websites International zone Poland:.com.net.biz.info.org.name.tel.mobi.asia-$50 \r\n.place 7002 websites – $50 \r\n.plumbing 3005 websites – $50 \r\n.plus 9892 websites – $50 \r\n.pm 4401 websites – $50 \r\n.poker 3209 websites – $50 \r\n.porn 10023 websites- $50 \r\n.post 3481 websites – $50 \r\n.pr 18419 websites – $50 \r\n.press 35032 websites – $50 \r\n.pro 280776 websites – $50 \r\n.productions 6144 websites – $50 \r\n.promo 5020 websites – $50 \r\n.properties 14144 websites – $50 \r\n.property 4366 websites – $50 \r\n.ps 2050 websites – $50 \r\n.pt 17691 websites International zone Portugal:.com.net.biz.info.org.name.tel.mobi.asia-$50 \r\n.pt 225136 websites Portugal – $80 \r\n.pub 16703 websites – $50 \r\n.pw 16206 websites – $50 \r\n.py 4635 websites – $50 \r\n.qa 8503 websites – $50 \r\n.quebec 8042 websites – $50 \r\n.racing 55001 websites – $50 \r\n.re 11082 websites – $50 \r\n.realtor 39465 websites – $50 \r\n.realty 55291 websites – $50 \r\n.recipes 3251 websites – $50 \r\n.red 25701 websites – $50 \r\n.rehab 1816 websites – $50 \r\n.reise 4146 websites – $50 \r\n.reisen 9228 websites – $50 \r\n.reit 126 websites – $50 \r\n.ren 108303 websites – $50 \r\n.rent 3788 websites – $50 \r\n.rentals 11823 websites- $50 \r\n.repair 6628 websites- $50 \r\n.report 5869 websites – $50 \r\n.republican 952 websites – $50 \r\n.rest 1894 websites – $50 \r\n.restaurant 7703 websites – $50 \r\n.review 229514 websites – $80 \r\n.reviews 16132 websites- $50 \r\n.rio 1062 websites- $50 \r\n.rip 3199 websites- $50 \r\n.ro 42046 websites International zone Romania:.com.net.biz.info.org.name.tel.mobi.asia-$50 \r\n.ro 483266 websites Romania – $80 \r\n.rocks 81108 websites – $50 \r\n.rs 75503 websites Serbia – $50 \r\n.ru 5292104 websites Russian- $250 \r\n.ru 514668 websites International zone Russian:.com.net.biz.info.org.name.tel.mobi.asia -$80 \r\n.ru.com 6499 websites Russia – $50 \r\n.ruhr 9887 websites – $50 \r\n.run 10622 websites – $50 \r\n.rw 244 websites – $50 \r\n.sa 5064 websites International zone Saudi Arabia:.com.net.biz.info.org.name.tel.mobi.asia \r\n.sa 11604 websites Saudi Arabia- $50 \r\n.saarland 3825 websites – $50 \r\n.sale 35407 websites – $50 \r\n.salon 1782 websites – $50 \r\n.sarl 939 websites – $50 \r\n.sc 4942 websites Seychelles- $50 \r\n.school 11672 websites – $50 \r\n.schule 4678 websites – $50 \r\n.science 74115 websites – $80 \r\n.scot 11175 websites – $50 \r\n.sd 619 websites – $50 \r\n.se 293316 websites International zone Sweden:.com.net.biz.info.org.name.tel.mobi.asia-$50 \r\n.se 1799365 websites Sweden – $150 \r\n.seat 618 websites – $50 \r\n.security 255 websites – $50 \r\n.services 52098 websites – $50 \r\n.sex 7655 websites – $50 \r\n.sexy 18260 websites – $50 \r\n.sg 164351 websites Republic Of Singapore – $50 \r\n.sh 9408 websites – $50 \r\n.shiksha 1626 websites – $50 \r\n.shoes 5007 websites – $50 \r\n.shop 402139 websites – $80 \r\n.shopping 6297 websites – $50 \r\n.show 6946 websites – $50 \r\n.si 12879 websites International zone Slovenia:.com.net.biz.info.org.name.tel.mobi.asia – $50 \r\n.si 39749 websites Slovenia- $50 \r\n.singles 3559 websites – $50 \r\n.site 384414 websites – $80 \r\n.sk 31572 websites International zone Slovakia:.com.net.biz.info.org.name.tel.mobi.asia – $50 \r\n.sk 347101 websites Slovakia- $80 \r\n.ski 5071 websites – $50 \r\n.sl 418 websites – $50 \r\n.sm 2009 websites – $50 \r\n.sn 344 websites International zone Senegal:.com.net.biz.info.org.name.tel.mobi.asia \r\n.sn 4954 websites Senegal – $50 \r\n.so 9003 websites – $50 \r\n.soccer 2833 websites – $50 \r\n.social 19134 websites – $50 \r\n.software 13001 websites – $50 \r\n.solar 7095 websites – $50 \r\n.solutions 81282 websites – $50 \r\n.soy 1200 websites – $50 \r\n.space 220771 websites – $80 \r\n.sr 638 – $50 \r\n.srl 4391 – $50 \r\n.st 7683 websites – $50 \r\n.storage 623 – $50 \r\n.store 167141 websites – $50 \r\n.stream 239795 websites – $80 \r\n.studio 39132 websites – $50 \r\n.study 5132 websites – $50 \r\n.style 9248 websites – $50 \r\n.su 125358 websites Russian- $50 \r\n.sucks 7629 websites – $50 \r\n.supplies 3082 websites – $50 \r\n.supply 4580 websites – $50 \r\n.support 20377 websites – $50 \r\n.surf 2573 websites – $50 \r\n.surgery 1778 websites – $50 \r\n.sv 8132 websites Salvador- $50 \r\n.swiss 17323 websites – $50 \r\n.sx 3240 websites – $50 \r\n.sy 2063 websites – $50 \r\n.sydney 10073 websites – $50 \r\n.systems 26094 websites – $50 \r\n.sz 194 websites – $50 \r\n.taipei 5664 websites – $50 \r\n.tattoo 2004 websites- $50 \r\n.tax 5788 websites – $50 \r\n.taxi 5784 websites – $50 \r\n.tc 16384 websites Turks and Caicos Islands- $50 \r\n.team 18421 websites- $50 \r\n.tech 196321 websites – $50 \r\n.technology 40181 websites- $50 \r\n.tel 80752 websites- $50 \r\n.tennis 1494 websites – $50 \r\n.tf 2049 websites – $50 \r\n.tg 521 websites – $50 \r\n.th 23968 websites Kingdom Of Thailand- $50 \r\n.theater 1053 websites – $50 \r\n.tickets 1141 websites – $50 \r\n.tienda 2951 websites – $50 \r\n.tips 31287 websites- $50 \r\n.tires 960 websites – $50 \r\n.tirol 5412 websites – $50 \r\n.tj 34 websites International zone Tajikistan:.com.net.biz.info.org.name.tel.mobi.asia \r\n.tj 6804 websites Tajikistan- $50 \r\n.tk 12799253 websites Tokelau – $450 \r\n.tl 2728 websites – $50 \r\n.tm 6056 websites Turkmenistan- $50 \r\n.tm 44 websites International zone Turkmenistan:.com.net.biz.info.org.name.tel.mobi.asia \r\n.tn 27383 websites – $50 \r\n.to 16987 websites Tonga- $50 \r\n.today 98034 websites – $50 \r\n.tokyo 90132 websites – $50 \r\n.tools 10439 websites – $50 \r\n.top 2140480 websites – $200 \r\n.tours 9151 websites – $50 \r\n.town 3328 websites – $50 \r\n.toys 4391 websites – $50 \r\n.tr 138818 International zone Turkey:.com.net.biz.info.org.name.tel.mobi.asia -$50 \r\n.tr 317690 websites Turkey – $80 \r\n.trade 151130 websites – $50 \r\n.trading 896 websites – $50 \r\n.training 18192 websites – $50 \r\n.travel 18054 websites – $50 \r\n.tt 516 websites – $50 \r\n.tube 2104 websites – $50 \r\n.tv 559502 websites Tuvalu – $80 \r\n.tw 243062 websites Taiwan – $50 \r\n.tz 1874 websites – $50 \r\n.ua 117701 websites International zone Ukraine:.com.net.biz.info.org.name.tel.mobi.asia-$50 \r\n.ua 553216 websites Ukraina – $80 \r\n.ug 2331 websites Ukraina \r\n.uk 3304606 websites International zone United Kingdom:.com.net.biz.info.org.name.tel.mobi.asia-$150 \r\n.uk 5496382 websites United Kingdom – $250 \r\n.university 5799 websites – $50 \r\n.uno 16394 websites – $50 \r\n.us 3139563 websites USA – $200 \r\n.us 578927 websites International zone USA:.com.net.biz.info.org.name.tel.mobi.asia-$80 \r\n.uy 14683 websites Uruguay – $50 \r\n.uz 365 websites International zone Uzbekistan:.com.net.biz.info.org.name.tel.mobi.asia \r\n.uz 14689 websites Uzbekistan – $50 \r\n.vacations 3926 websites – $50 \r\n.vc 16606 websites – $50 \r\n.ve 14015 websites Venezuela – $50 \r\n.vegas 18008 websites Las Vegas NV United States of America – $50 \r\n.ventures 10066 websites – $50 \r\n.versicherung 2009 websites – $50 \r\n.vet 5760 websites – $50 \r\n.vg 8389 site – $50 \r\n.viajes 2512 site – $50 \r\n.video 16204 websites- $50 \r\n.villas 1940 site – $50 \r\n.vin 5094 websites – $50 \r\n.vip 755289 websites – $80 \r\n.vision 6420 websites – $50 \r\n.vlaanderen 6114 websites – $50 \r\n.vn 161855 websites International zone Vietnam:.com.net.biz.info.org.name.tel.mobi.asia – $50 \r\n.vn 436005 websites Vietnam – $80 \r\n.vodka 1420 websites – $50 \r\n.vote 2316 websites – $50 \r\n.voto 269 websites – $50 \r\n.voyage 2773 websites – $50 \r\n.vu 1201 websites – $50 \r\n.wales 13063 websites – $50 \r\n.wang 604718 websites – $80 \r\n.watch 6120 websites – $50 \r\n.webcam 60142 websites – $50 \r\n.website 207002 websites -$50 \r\n.wedding 22162 websites – $50 \r\n.wf 1188 websites – $50 \r\n.wien 14464 websites – $50 \r\n.wien 14913 websites – $50 \r\n.wiki 14129 websites wikis – $50 \r\n.win 655735 websites – $80 \r\n.wine 13095 websites – $50 \r\n.work 256086 websites – $80 \r\n.works 15112 websites – $50 \r\n.world 86012 websites – $50 \r\n.ws 97008 websites Samoa- $80 \r\n.wtf 10038 websites – $50 \r\n.xin 178450 websites – $50 \r\n.xn--3ds443g 44019 websites – $50 \r\n.xn--55qx5d 47456 websites – $50 \r\n.xn--6qq986b3xl 18188 websites – $50 \r\n.xn--czr694b 21109 websites – $50 \r\n.xn--czru2d 9020 websites – $50 \r\n.xn--fiq228c5hs 12145 websites – $50 \r\n.xn--io0a7i 30199 websites – $50 \r\n.xn--j6w193g 31764 websites – $50 \r\n.xn--kput3i 31856 websites – $50 \r\n.xn--mgbaam7a8h 2038 websites – $50 \r\n.xn--mgberp4a5d4ar 2534 websites – $50 \r\n.xn--mk1bu44c 6001 websites – $50 \r\n.xn--rhqv96g 7723 websites – $50 \r\n.xn--ses554g 198539 websites – $80 \r\n.xn--tckwe 6197 websites – $50 \r\n.xn--vuq861b 19706 websites – $50 \r\n.xxx 119879 websites- $50 \r\n.xyz 1888763 websites – $150 \r\n.yachts 154 websites – $50 \r\n.yoga 7963 websites – $50 \r\n.yokohama 8140 websites – $50 \r\n.yt 2004 websites – $50 \r\n.za 837189 websites South Africa – $100 \r\n.zm 224 websites – $50 \r\n.zone 25798 websites – $50 \r\n.бг (.xn--90ae) 2470 websites – $50 \r\n.дети 169 websites – $50 \r\n.москва (.xn--80adxhks) 19582 websites Moscow – $50 \r\n.онлайн 3403 websites – $50 \r\n.орг 1160 websites – $50 \r\n.рус (.xn--p1acf) 110789 websites – $50 \r\n.рф (.xn--p1ai) 869759 websites Russia – $80 \r\n.сайт 1146 websites – $50 \r\n.укр (.xn--j1amh) 10563 websites- $50 \r\n.संगठन 105  sites – $50 \r\n>.みんな 166  sites – $50 \r\n.コム    6532  sites – $50 \r\n.世界    4171  sites – $50 \r\n.公司    46161  sites – $50 \r\n.商城    6 907  sites – $50 \r\n.商标    9865  sites – $50 \r\n.我爱你 15465  sites – $50 \r\n.手机    31543  sites – $50 \r\n.机构    243  sites – $50 \r\n.游戏    187  sites – $50 \r\n.移动    3277  sites – $50 \r\n.网店    3711  sites – $50 \r\n.网络    30808  sites – $50 \r\n.닷컴    5937  sites – $50 \r\n.بازار  sites 648 – $50 \r\n.شبكة   sites 833 – $50 \r\n.موقع   sites 478 – $50', 'waiting-confirmation', '2019-03-15 21:07:43', '2019-03-15 21:07:43'),
(108, 'Raymondamigh', 'ledled@msn.com', '86997151325', 'Rencontrez des filles sexy dans votre ville', 'Filles sexy pour la nuit dans votre ville: http://go.fireontherim.com/bestsexygirlsadultdating36221', 'waiting-confirmation', '2019-03-17 18:41:24', '2019-03-17 18:41:24'),
(109, 'VincentNon', 'baseman35@hotmail.com', '86919868293', 'Sexy Girls fur die Nacht in deiner Stadt', 'Sexy Girls fur die Nacht in deiner Stadt: http://go.fireontherim.com/bestsexygirlsadultdating42315', 'waiting-confirmation', '2019-03-17 18:44:12', '2019-03-17 18:44:12'),
(110, 'Jamesseido', 'bgray999@hotmail.com', '87843273787', 'Encuentra una chica para pasar la noche en tu ciudad.', 'Conoce chicas sexy en tu ciudad: https://lil.ink/bestsexygirlsadultdating40892', 'waiting-confirmation', '2019-03-17 19:24:13', '2019-03-17 19:24:13'),
(111, 'HollisSlari', 'rprivee@cfl.rr.com', '88456184551', 'Beautiful women for sex in your town', 'Sexy girls for the night in your town: http://go.fireontherim.com/bestsexygirlsadultdating73962', 'waiting-confirmation', '2019-03-18 05:51:47', '2019-03-18 05:51:47'),
(112, 'Raymondamigh', 'luz_46@hotmail.com', '85961335256', '10 Best Cryptocurrency to Invest in 2019', '2019 Cryptocurrency Investment Guide: http://valeriemace.co.uk/15000investbinarycrypto50964', 'waiting-confirmation', '2019-03-18 10:06:18', '2019-03-18 10:06:18'),
(113, 'Jamesseido', 'ior_lover@msn.com', '82983894192', 'How To Make $10,000 a Day Trading Forex - The Most EASIEST Way For Beginners (SECRET REVEALED)', 'How To Make Over $10,000 In One Trade | Live Forex Trading and Analysis: http://perkele.ovh/15000investbinarycrypto12635', 'waiting-confirmation', '2019-03-18 10:08:40', '2019-03-18 10:08:40'),
(114, 'VincentNon', 'fkauzlarich@yahoo.com', '88852327328', '$10000 per day Best Bitcoin Binary Options | Crunchbase', '$10000 per day New Binary Options Trading Service Takes Bitcoin-Only Payments: http://webhop.se/15000investbinarycrypto50704', 'waiting-confirmation', '2019-03-18 10:11:42', '2019-03-18 10:11:42'),
(115, 'nakatsu', 'gregorAftesteZerT@gmail.com', '17329960361', 'We offer you the opportunity to advertise your products and services.', 'We offer you the opportunity to advertise your products and services. \r\n \r\nDear Sir / Madam That is an amazing offers for you. I can help you with sending  your commercial offers or messages through feedback forms. The advantage of this method is that the messages sent through the feedback forms are included in the white list. This method increases the chance that your message will be read. Mailing is made in the same way you received this message. \r\nSending via Feedback Forms to any domain zones of the world. (more than 1000 domain zones.). \r\nThe cost of sending 1 million messages for any domain zone of the world is $ 49 instead of $ 99. \r\nDomain zone .com - (12 million messages sent) - $399 instead of $699 \r\nAll domain zones in Europe- (8 million messages sent) - $ 299 instead of $599 \r\nAll sites in the world (25 million messages sent) - $499 instead of $999 \r\nDomain zone .de - (2 million messages sent) - $99 instead of $199 \r\nDomain zone .uk - (1.5 million messages sent) - $69 instead of $139 \r\nDomain zone .nl - (700 000 sent messages) - $39 instead of $79 \r\n \r\n \r\nDiscounts are valid until March 25. \r\n \r\nContact us. \r\nTelegram - @FeedbackFormEU \r\nSkype – FeedbackForm2019 \r\nEmail - FeedbackForm2019@gmail.com \r\n \r\nIt\'s time to finish.', 'waiting-confirmation', '2019-03-18 12:16:04', '2019-03-18 12:16:04'),
(116, 'HollisSlari', 'rdehner@hotmail.com', '85535528233', 'I\'m 23. I have $30000. How can I best use it to make more money', 'How To Make Extra Money From Home - $3000 Per Day Easy: http://www.lookweb.it/$15000investbinarycrypto49316', 'waiting-confirmation', '2019-03-18 16:15:05', '2019-03-18 16:15:05'),
(117, 'Jamesseido', 'alexicus@hotmail.com', '86341975929', 'How To Make $10,000 a Day Trading Forex - The Most EASIEST Way For Beginners (SECRET REVEALED)', 'Forex Prodigy Makes $10,000 in 30 Minutes! - Market Traders Institute: http://goto.iamaws.com/15000investbinarycrypto21509', 'waiting-confirmation', '2019-03-20 01:22:10', '2019-03-20 01:22:10'),
(118, 'VincentNon', 'plantman3@sbcglobal.net', '83585435672', '$10000 per day Bitcoin Trading with Binary Options', '$10000 per day Bitcoin Trading with Binary Options: http://jnl.io/15000investbinarycrypto93560', 'waiting-confirmation', '2019-03-20 11:39:10', '2019-03-20 11:39:10'),
(119, 'Raymondamigh', 'cloudstrife15@msn.com', '89932969163', 'The best women for sex in your town', 'The best girls for sex in your town: http://goto.iamaws.com/adultdating99170', 'waiting-confirmation', '2019-03-21 01:30:42', '2019-03-21 01:30:42'),
(120, 'Jamesseido', 'andreasolo@hotmail.com', '86359863779', 'Sexy girls for the night in your town', 'Sexy girls for the night in your town: http://perkele.ovh/adultdating65490', 'waiting-confirmation', '2019-03-21 01:31:16', '2019-03-21 01:31:16'),
(121, 'HollisSlari', 'beastofbourbon@hotmail.com', '81682275617', 'The best girls for sex in your town', 'Beautiful women for sex in your town: http://www.vkvi.net/adultdating29508', 'waiting-confirmation', '2019-03-21 07:55:15', '2019-03-21 07:55:15'),
(122, 'Jamesseido', 'wbasara@hotmail.com', '84578168729', 'The best women for sex in your town', 'Sexy girls for the night in your town: https://aaa.moda/adultdating70737', 'waiting-confirmation', '2019-03-23 01:33:59', '2019-03-23 01:33:59'),
(123, 'Raymondamigh', 'bellevuefreak@hotmail.com', '84576195586', 'The best women for sex in your town', 'The best girls for sex in your town: http://goto.iamaws.com/adultdating75954', 'waiting-confirmation', '2019-03-23 01:56:01', '2019-03-23 01:56:01'),
(124, 'VincentNon', 'corky_06@msn.com', '83496585526', 'Beautiful women for sex in your town', 'The best women for sex in your town: http://www.lookweb.it/adultdating50682', 'waiting-confirmation', '2019-03-23 01:56:11', '2019-03-23 01:56:11'),
(125, 'Raymondamigh', 'compher4@msn.com', '84865281155', 'Ich bin 23. Ich habe в‚¬ 3000. Wie kann ich es am besten nutzen, um mehr Geld zu verdienen', 'Wie man в‚¬ 3000 schnell macht Schnelles Geld | Der beschГ¤ftigte Budgeter: http://rih.co/bestinvestcrepto75301', 'waiting-confirmation', '2019-03-23 11:27:48', '2019-03-23 11:27:48'),
(126, 'Jamesseido', 'cojocojo@hotmail.com', '82191964964', 'Comment utiliseriez-vous 3000 в‚¬ pour gagner plus d\'argent', 'J\'ai 23 000 в‚¬. Comment l\'utiliser au mieux pour gagner plus d\'argent: http://valeriemace.co.uk/bestinvestcrepto70951', 'waiting-confirmation', '2019-03-23 11:29:17', '2019-03-23 11:29:17'),
(127, 'VincentNon', 'samuel.rouah@gmail.com', '82952574128', 'Top cryptocurrencies to invest in 2019', 'The Top 5 Best Cryptocurrencies 2019: http://jnl.io/bestinvestcrepto94864', 'waiting-confirmation', '2019-03-23 11:30:17', '2019-03-23 11:30:17'),
(128, 'HollisSlari', 's_elkin@hotmail.com', '82675731585', 'Auto Mass Money Maker', 'Simple biz + new tool = $450 per hour: https://aaa.moda/bestinvestcrepto99959', 'waiting-confirmation', '2019-03-23 17:21:13', '2019-03-23 17:21:13'),
(129, 'Eric', 'eric@talkwithcustomer.com', '416-385-3200', 'Do You Want Up to 100X More Conversions?', 'Hello,\r\n\r\nYou have a website, right? \r\n  \r\nOf course you do. I found oriscake.com today.\r\n\r\nIt gets traffic every day – that you’re probably spending $2 / $4 / $10 or more a click to get.  Not including all of the work you put into creating social media, videos, blog posts, emails, and so on.\r\n\r\nSo you’re investing seriously in getting people to that site.\r\n\r\nBut how’s it working?  Great? Okay?  Not so much?\r\n\r\nIf that answer could be better, then it’s likely you’re putting a lot of time, effort, and money into an approach that’s not paying off like it should.\r\n\r\nNow… imagine doubling your lead conversion in just minutes… In fact, I’ll go even better.\r\n \r\nYou could actually get up to 100X more conversions!\r\n\r\nI’m not making this up.  As Chris Smith, best-selling author of The Conversion Code says:\r\n-Speed is essential - there is a 100x decrease in Leads when a Lead is contacted within 14 minutes vs being contacted within 5 minutes.\r\n\r\nHe’s backed up by a study at MIT that found the odds of contacting a lead will increase by 100 times if attempted in 5 minutes or less.\r\n\r\nAgain, out of the 100s of visitors to your website, how many actually call to become clients?\r\n\r\nWell, you can significantly increase the number of calls you get – with ZERO extra effort.\r\n\r\nOur software, Talk With Customer, makes it easy, simple, and fast – in fact, you can start getting more calls today… and at absolutely no charge to you.\r\n\r\nCLICK HERE http://www.talkwithcustomer.com to find out how.\r\n\r\nSincerely,\r\n\r\nEric Jones\r\n\r\nPS: Don’t just take my word for it, TalkWithCustomer works:\r\n-EMA has been looking for ways to reach out to an audience. TalkWithCustomer so far is the most direct call of action. It has produced above average closing ratios and we are thrilled. Thank you for providing a real and effective tool to generate REAL leads. - Patrick MontesDeOca, Ema2Trade\r\n\r\nBest of all, act now to get a no-cost 14-Day Test Drive – our gift to you just for giving TalkWithCustomer a try. \r\n\r\nCLICK HERE http://www.talkwithcustomer.com to start converting up to 100X more leads today!\r\n\r\n\r\nIf you\'d like to unsubscribe click here. http://liveserveronline.com/talkwithcustomer.aspx?d=oriscake.com', 'waiting-confirmation', '2019-03-23 21:33:27', '2019-03-23 21:33:27'),
(130, 'Raymondamigh', 'amypic@hotmail.com', '82175315129', 'Filles sexy pour la nuit dans votre ville', 'Filles sexy pour la nuit dans votre ville: http://corta.co/bestadultdating87519', 'waiting-confirmation', '2019-03-25 11:33:38', '2019-03-25 11:33:38'),
(131, 'Jamesseido', 'tinah_msp@msn.com', '88686998415', 'Sexy Girls fГјr die Nacht in deiner Stadt', 'Sexy Girls fГјr die Nacht in deiner Stadt: http://to.ht/bestadultdating92735', 'waiting-confirmation', '2019-03-25 11:36:05', '2019-03-25 11:36:05'),
(132, 'VincentNon', 'kanne6@hotmail.com', '81795343318', 'Chicas sexys para pasar la noche en tu pueblo.', 'Conoce chicas sexy en tu ciudad: http://goto.iamaws.com/bestadultdating82137', 'waiting-confirmation', '2019-03-25 11:36:05', '2019-03-25 11:36:05'),
(133, 'HollisSlari', 'grosso_frank@yahoo.com', '87312187844', 'The best women for sex in your town', 'Find yourself a girl for the night in your city: https://lil.ink/bestadultdating50637', 'waiting-confirmation', '2019-03-25 17:34:41', '2019-03-25 17:34:41'),
(134, 'Eric', 'eric@talkwithcustomer.com', '416-385-3200', 'Do You Want Up to 100X More Conversions for oriscake.com ?', 'Hello,\r\n\r\nYou have a website, right? \r\n  \r\nOf course you do. I found oriscake.com today.\r\n\r\nIt gets traffic every day – that you’re probably spending $2 / $4 / $10 or more a click to get.  Not including all of the work you put into creating social media, videos, blog posts, emails, and so on.\r\n\r\nSo you’re investing seriously in getting people to that site.\r\n\r\nBut how’s it working?  Great? Okay?  Not so much?\r\n\r\nIf that answer could be better, then it’s likely you’re putting a lot of time, effort, and money into an approach that’s not paying off like it should.\r\n\r\nNow… imagine doubling your lead conversion in just minutes… In fact, I’ll go even better.\r\n \r\nYou could actually get up to 100X more conversions!\r\n\r\nI’m not making this up.  As Chris Smith, best-selling author of The Conversion Code says:\r\n-Speed is essential - there is a 100x decrease in Leads when a Lead is contacted within 14 minutes vs being contacted within 5 minutes.\r\n\r\nHe’s backed up by a study at MIT that found the odds of contacting a lead will increase by 100 times if attempted in 5 minutes or less.\r\n\r\nAgain, out of the 100s of visitors to your website, how many actually call to become clients?\r\n\r\nWell, you can significantly increase the number of calls you get – with ZERO extra effort.\r\n\r\nOur software, Talk With Customer, makes it easy, simple, and fast – in fact, you can start getting more calls today… and at absolutely no charge to you.\r\n\r\nCLICK HERE http://www.talkwithcustomer.com to find out how.\r\n\r\nSincerely,\r\n\r\nEric Jones\r\n\r\nPS: Don’t just take my word for it, TalkWithCustomer works:\r\n-EMA has been looking for ways to reach out to an audience. TalkWithCustomer so far is the most direct call of action. It has produced above average closing ratios and we are thrilled. Thank you for providing a real and effective tool to generate REAL leads. - Patrick MontesDeOca, Ema2Trade\r\n\r\nBest of all, act now to get a no-cost 14-Day Test Drive – our gift to you just for giving TalkWithCustomer a try. \r\n\r\nCLICK HERE http://www.talkwithcustomer.com to start converting up to 100X more leads today!\r\n\r\n\r\nIf you\'d like to unsubscribe click here. http://liveserveronline.com/talkwithcustomer.aspx?d=oriscake.com', 'waiting-confirmation', '2019-03-26 20:06:35', '2019-03-26 20:06:35'),
(135, 'Raymondamigh', 'r_martel@msn.com', '87435783779', 'Find yourself a girl for the night in your city', 'Beautiful girls for sex in your city: http://webhop.se/bestadultdating57461', 'waiting-confirmation', '2019-03-27 02:33:42', '2019-03-27 02:33:42'),
(136, 'Jamesseido', 'zmartino@hotmail.com', '85446392872', 'Beautiful women for sex in your town', 'The best women for sex in your town: http://valeriemace.co.uk/adultdatinginyourcity47308', 'waiting-confirmation', '2019-03-27 02:35:37', '2019-03-27 02:35:37'),
(137, 'VincentNon', 'spots9@hotmail.com', '81391856835', 'Sexy girls for the night in your town', 'Beautiful women for sex in your town: https://tinyurl.com/bestadultdating59145', 'waiting-confirmation', '2019-03-27 03:09:11', '2019-03-27 03:09:11'),
(138, 'HollisSlari', 'xanthakis@hotmail.com', '81878167277', 'Beautiful girls for sex in your city', 'Beautiful girls for sex in your city: http://www.lookweb.it/bestadultdating21957', 'waiting-confirmation', '2019-03-27 08:38:10', '2019-03-27 08:38:10'),
(139, 'Raymondamigh', 'rogalski@hotmail.com', '82453142349', 'Beautiful girls for sex in your city', 'The best girls for sex in your town: http://to.ht/bestadultdating45314', 'waiting-confirmation', '2019-03-28 11:05:18', '2019-03-28 11:05:18'),
(140, 'VincentNon', 'spendola@hotmail.com', '87461278852', 'Find yourself a girl for the night in your city', 'Beautiful girls for sex in your city: http://to.ht/bestadultdating86781', 'waiting-confirmation', '2019-03-28 11:05:54', '2019-03-28 11:05:54'),
(141, 'Jamesseido', 'chandan22@hotmail.com', '83936593123', 'The best girls for sex in your town', 'Beautiful girls for sex in your city: http://www.vkvi.net/adultdating54060', 'waiting-confirmation', '2019-03-28 11:09:56', '2019-03-28 11:09:56'),
(142, 'HollisSlari', 'raustin21@hotmail.com', '85622576832', 'Meet sexy girls in your city', 'Meet sexy girls in your city: http://webhop.se/adultdatinginyourcity88444', 'waiting-confirmation', '2019-03-29 07:20:46', '2019-03-29 07:20:46'),
(143, 'Raymondamigh', 'hilderbrand3@hotmail.com', '81842985189', 'What\'s the easiest way to earn $30000 a month', 'Get $1000 вЂ“ $6000 A Day: https://lil.ink/5000perday39789', 'waiting-confirmation', '2019-03-29 11:18:36', '2019-03-29 11:18:36'),
(144, 'HollisSlari', 'rudeboy88@msn.com', '81877749969', 'What\'s the easiest way to earn $30000 a month', 'Make $200 per hour doing this: http://jnl.io/5000perday49060', 'waiting-confirmation', '2019-03-29 17:42:20', '2019-03-29 17:42:20'),
(145, 'Jamesseido', 'ogulliver7cwl@yahoo.com', '89943942838', 'The best girls for sex in your town', 'Beautiful women for sex in your town: http://webhop.se/bestadultdating82313', 'waiting-confirmation', '2019-03-30 10:11:02', '2019-03-30 10:11:02');
INSERT INTO `contact_messages` (`id`, `name`, `email`, `phone`, `subject`, `message`, `status`, `created_at`, `updated_at`) VALUES
(146, 'VincentNon', 'gminnie1@bellsouth.net', '83331157518', 'The best women for sex in your town', 'Meet sexy girls in your city: http://goto.iamaws.com/adultdatinginyourcity98318', 'waiting-confirmation', '2019-03-30 10:20:31', '2019-03-30 10:20:31'),
(147, 'Raymondamigh', 'jaxon47@msn.com', '87846441954', 'Get $1500 вЂ“ $6000 per DAY', 'Paid Surveys: Earn $30,000 Or More Per Week: http://to.ht/5000perday20791', 'waiting-confirmation', '2019-03-31 10:27:47', '2019-03-31 10:27:47'),
(148, 'HollisSlari', 'cbbhagat@hotmail.com', '81957999981', 'How to Make $30000 FAST | Fast Money | The Busy Budgeter', 'How to make $450 per hour: https://lil.ink/5000perday27507', 'waiting-confirmation', '2019-03-31 16:14:10', '2019-03-31 16:14:10'),
(149, 'VincentNon', 'dogdays@hotmail.com', '89788517943', 'Beautiful women for sex in your town', 'Beautiful women for sex in your town: http://to.ht/bestadultdating70929', 'waiting-confirmation', '2019-04-01 01:57:45', '2019-04-01 01:57:45'),
(150, 'Jamesseido', 'brandiforrester@hotmail.com', '86914683194', 'Meet sexy girls in your city', 'Beautiful girls for sex in your city: http://www.abcagency.se/bestadultdating79227', 'waiting-confirmation', '2019-04-01 02:40:43', '2019-04-01 02:40:43'),
(151, 'TerryTes', 'pete-gvm-affiliate@gmail.com', '211734744', 'Question…', 'My name is Pete and I want to share a proven system with you that makes me money while I sleep! This system allows you to TRY the whole thing for F R E E for a whole 30 days! That\'s right, you can finally change your future without giving up any sensitive information in advance! I signed up myself just a while ago and I\'m already making a nice profit. \r\n \r\nIn short, this is probably the BEST THING THAT EVER HAPPENED TO YOU IF YOU TAKE ACTION NOW!!! \r\n \r\nIf you\'re interested in knowing more about this system, go to http://globalviralmarketing.com/?ref=qkgWOPkN5RoC1NWh and try it out. Again, it’s FREE! \r\n \r\nYou can thank me later \r\n \r\n/Pete', 'waiting-confirmation', '2019-04-01 07:43:57', '2019-04-01 07:43:57'),
(152, 'contacttcybrm', 'shiloh_fischhaber71@rambler.ru', '123456789', 'We offer a service of sending newsletters via contact forms to the sites of business organizations via all countries of the world.', 'Good whatever time of day it is where you are! \r\n \r\nNewsletters  of Your commercial offers via contact forms  to the sites of business organizations via all countries and domain zones of the world.  \r\n \r\nhttp://xn----7sbb1bbndheurc1a.xn--p1ai \r\n \r\nYour message is sent to electronic box of institution one hundred percent will get to inbox folder! \r\n \r\nTest: \r\n10000 messages on foreign zones to your email address - twenty $. \r\nWe need from You only email address, title and text of the letter. \r\n \r\nIn our price there are more 800 databases for all domains of the world. \r\nCommon databases: \r\nAll Europe 44 countries 60726150 of domain names - 1100$ \r\nAll European Union 28 countries 56752547 of sites- 1000$ \r\nAll Asia 48 countries 14662004 of sites - 300$ \r\nAll Africa 50 countries 1594390 of domains - 200$ \r\nAll North and Central America in 35 countries 7441637 of domains - 300$ \r\nAll South America 14 countries 5826884 of sites - 200$ \r\nNew domain names from around the world registered 24-48 hours ago. (A cycle of 15 mailings during the month) - 500$ \r\nCompanies of Russia 3012045 - 300$ \r\nUkraine 605745 of domain names - 100$ \r\nAll Russian-speaking countries minus Russia are 15 countries and there are 1526797 of domains - 200$ \r\nNew sites of the RF, registered 24-48 hours ago (A cycle of 15 mailings during the month) - 250$ \r\n \r\nOur databases: \r\nWHOIS databases of domains for all countries of the world. \r\nYou can purchase our databases separately from newsletter\'s service at the request. \r\n \r\nP.S. \r\nPlease, do not respond to this commercial offer from your electronic box, as it has been generated automatically and will not get anywhere! \r\nUse the contact form from the site http://xn----7sbb1bbndheurc1a.xn--p1ai \r\n \r\n \r\nPRICE LIST: \r\n \r\nTest mailing: $20 – 10000 contact forms websites \r\n \r\nAll Europe 44 countries there are 60726150 websites – $1100 \r\n \r\nAll EU 28 countries there are 56752547 websites – $1000 \r\n \r\nAll Asia 48 countries there are 14662004 websites – $500 \r\n \r\nAll Africa 50 countries there are 1594390 websites – $200 \r\n \r\nAll North and Central America is 35 countries there are 7441637 websites – $300 \r\n \r\nAll South America 14 countries there are 5826884 websites – $200 \r\n \r\nTop 1 Million World’s Best websites – $100 \r\n \r\nTop 16821856 the most visited websites in the world – $200 \r\n \r\nBusinesses and organizations of the Russian Federation – there are 3012045 websites – $300 \r\n \r\nUkraine 605745 websites – $100 \r\n \r\nAll Russian-speaking countries minus Russia – there are 15 countries and 1526797 websites – $200 \r\n \r\n1499203 of hosting websites around the world (there are selections for all countries, are excluded from databases for mailings) – $200 \r\n \r\n35439 websites of public authorities of all countries of the world (selections for all countries, are excluded from databases for mailings) – $100 \r\n \r\nCMS mailings: \r\nAmiro 2294 websites $50 \r\nBitrix 175513 websites $80 \r\nConcrete5 49721 websites $50 \r\nCONTENIDO 7769 websites $50 \r\nCubeCart 1562 websites $50 \r\nDatalife Engine 29220 websites $50 \r\nDiscuz 70252 websites $50 \r\nDotnetnuke 31114 websites $50 \r\nDrupal 802121 websites $100 \r\nHostCMS 6342 websites $50 \r\nInstantCMS 4936 websites $50 \r\nInvision Power Board 510 websites $50 \r\nJoomla 1906994 websites $200 \r\nLiferay 5937 websites $50 \r\nMagento 269488 websites $80 \r\nMODx 67023 websites $50 \r\nMovable Type 13523 websites $50 \r\nNetCat 6936 websites $50 \r\nNopCommerce 5313 websites $50 \r\nOpenCart 321057 websites $80 \r\nosCommerce 65468 websites $50 \r\nphpBB 3582 websites $50 \r\nPrestashop 92949 websites $50 \r\nShopify 365755 websites $80 \r\nSimpla 8963 websites $50 \r\nSitefinity 4883 websites $50 \r\nTYPO3 227167 websites $80 \r\nUMI.CMS 15943 websites $50 \r\nvBulletin 154677 websites $80 \r\nWix 2305768 websites $230 \r\nWordPress 14467405 websites $450 \r\nWooCommerce 2097367 websites $210 \r\n \r\n.com 133766112 websites commercial – $1950 \r\n.biz 2361884 websites business – $150 \r\n.info 6216929 websites information – $250 \r\n.net 15689222 websites network – $450 \r\n.org 10922428 websites organization – $350 \r\n \r\n.abogado 279 websites – $50 \r\n.ac 16799 websites – $50 \r\n.academy 27306 websites – $50 \r\n.accountant 96542 websites – $50 \r\n.actor 1928 websites – $50 \r\n.ad 414 websites – $50 \r\n.adult 10540 websites- $50 \r\n.ae 1821 websites International zone UAE:.com.net.biz.info.org.name.tel.mobi.asia \r\n.ae 199533 websites UAE – $50 \r\n.aero 18325 websites- $50 \r\n.af 3315 websites – $50 \r\n.africa 15056 websites- $50 \r\n.ag 10339 websites – $50 \r\n.agency 47508 websites – $50 \r\n.ai 17199 websites – $50 \r\n.airforce 560 websites – $50 \r\n.al 6078 websites – $50 \r\n.alsace 1982 websites – $50 \r\n.am 17987 websites Armenia – $50 \r\n.am 1684 websites International zone Armenia:.com.net.biz.info.org.name.tel.mobi.asia \r\n.amsterdam 28141 websites Amsterdam, Kingdom of the Netherlands – $50 \r\n.ao 518 websites – $50 \r\n.apartments 3758 websites – $50 \r\n.ar 551804 websites Argentina – $80 \r\n.ar 64008 websites International zone Argentina:.com .net .biz .info .name .tel .mobi .asia – $50 \r\n.archi 2084 websites – $50 \r\n.army 1842 websites – $50 \r\n.art 26402 websites – $50 \r\n.as 10025 websites – $50 \r\n.asia 228418 websites – $80 \r\n.associates 3340 websites – $50 \r\n.at 1356722 websites Austria – $100 \r\n.at 181907 websites International zone Austria :.com.net.biz.info.org.name.tel.mobi.asia-$50 \r\n.attorney 8224 websites- $50 \r\n.attorney 7204 websites – $50 \r\n.au 2243263 websites Australia – $150 \r\n.au 461279 websites International zone Australia:.com.net.biz.info.org.name.tel.mobi.asia-$80 \r\n.auction 3625 websites- $50 \r\n.audio 23052 websites- $50 \r\n.auto 400 websites- $50 \r\n.aw 235 websites- $50 \r\n.az 11104 websites Azerbaijan – $50 \r\n.az 2036 websites International zone Azerbaijan:.com.net.biz.info.org.name.tel.mobi.asia \r\n.ba 7012 websites – $50 \r\n.ba 2291 websites International zone Bosnia and Herzegovina:.com.net.biz.info.org.name.tel.mobi.asia \r\n.band 11515 websites – $50 \r\n.bank 1621 websites- $50 \r\n.bar 5506 websites – $50 \r\n.barcelona 7919 websites – $50 \r\n.bargains 2997 websites- $50 \r\n.bayern 32565 websites – $50 \r\n.bb 2277 websites – $50 \r\n.be 1349658 websites Belgium – $100 \r\n.be 184810 websites International zone Belgium:.com.net.biz.info.org.name.tel.mobi.asia-$50 \r\n.beer 11834 websites- $50 \r\n.berlin 58088 websites Berlin – $50 \r\n.best 2912 websites – $50 \r\n.bet 17637 websites – $50 \r\n.bf 238 websites – $50 \r\n.bg 33252 websites Bulgaria – $50 \r\n.bg 50685 websites International zone Bulgaria:.com.net.biz.info.org.name.tel.mobi.asia \r\n.bh 453 websites – $50 \r\n.bi 2328 websites Burundi- $50 \r\n.bible 1160 websites – $50 \r\n.bid 474509 websites – $80 \r\n.bike 15729 websites – $50 \r\n.bingo 1332 websites – $50 \r\n.bio 15531 websites- $50 \r\n.bj 147 websites- $50 \r\n.black 6582 websites – $50 \r\n.blackfriday 12106 websites – $50 \r\n.blog 145463 websites – $50 \r\n.blue 16852 websites – $50 \r\n.bm 8089 websites Bermuda – $50 \r\n.bo 2302 websites- $50 \r\n.boats 266 websites- $50 \r\n.boston 21762 websites- $50 \r\n.boutique 8834 websites – $50 \r\n.br 2367290 websites Brazil – $150 \r\n.br 933750 websites International zone Brazil:.com.net.biz.info.org.name.tel.mobi.asia-$100 \r\n.bradesco 129 websites- $50 \r\n.broadway 261 websites- $50 \r\n.broker 660 websites- $50 \r\n.brussels 7181 websites – $50 \r\n.bs 330 websites- $50 \r\n.bt 284 websites- $50 \r\n.build 3857 websites- $50 \r\n.builders 3906 websites- $50 \r\n.business 35168 websites – $50 \r\n.buzz 11257 websites – $50 \r\n.bw 656 websites – $50 \r\n.by 1574 websites International zone Belarus:.com.net.biz.info.org.name.tel.mobi.asia \r\n.by 92679 websites Belarus – $50 \r\n.bz 7751 websites – $50 \r\n.bzh 5403 websites – $50 \r\n.ca 2587463 websites Canada – $150 \r\n.ca 288395 websites International zone Canada:.com.net.biz.info.org.name.tel.mobi.asia-$50 \r\n.cab 3223 websites – $50 \r\n.cafe 13606 websites – $50 \r\n.cam 5156 websites – $50 \r\n.camera 5236 websites – $50 \r\n.camp 6315 websites – $50 \r\n.capetown 4750 websites – $50 \r\n.capital 11387 websites – $50 \r\n.car 342 websites – $50 \r\n.cards 5992 websites – $50 \r\n.care 18204 websites – $50 \r\n.career 1217 websites – $50 \r\n.careers 7055 websites – $50 \r\n.cars 309 websites – $50 \r\n.casa 18918 websites – $50 \r\n.cash 13193 websites – $50 \r\n.casino 4354 websites – $50 \r\n.cat 108569 websites – $50 \r\n.catering 3482 websites – $50 \r\n.cc 1920589 websites Cocos Keeling Islands- $150 \r\n.cd 5365 websites – $50 \r\n.center 35353 websites – $50 \r\n.ceo 2458 websites – $50 \r\n.cf 476142 websites Central African Republic – $50 \r\n.cg 166 – $50 \r\n.ch 1471685 websites Switzerland – $100 \r\n.ch 205292 websites International zone Switzerland:.com.net.biz.info.org.name.tel.mobi.asia-$50 \r\n.chat 11126 websites – $50 \r\n.cheap 3267 websites – $50 \r\n.christmas 15255 websites – $50 \r\n.church 21104 websites – $50 \r\n.ci 112 websites International zone Cote d’Ivoire:.com.net.biz.info.org.name.tel.mobi.asia \r\n.ci 5663 websites Cote d’Ivoire- $50 \r\n.city 46171 websites – $50 \r\n.cl 498401 websites Chile – $80 \r\n.claims 2374 websites – $50 \r\n.cleaning 2385 websites – $50 \r\n.click 181015 websites – $50 \r\n.clinic 7006 websites – $50 \r\n.clothing 13639 websites – $50 \r\n.cloud 134113 websites – $50 \r\n.club 1045323 websites – $100 \r\n.cm 12001 websites Cameroon- $50 \r\n.cn 1372416 websites International zone China:.com.net.biz.info.org.name.tel.mobi.asia-$100 \r\n.cn 7264587 websites China – $300 \r\n.co 1778923 websites Colombia – $150 \r\n.coach 12002 websites- $50 \r\n.codes 6844 websites – $50 \r\n.coffee 17257 websites – $50 \r\n.cologne 5137 websites – $50 \r\n.cologne 5198 websites – $50 \r\n.com.ar 657716 websites Argentina – $80 \r\n.com.br 942898 websites Brazil – $100 \r\n.com.cy 11153 websites Cyprus – $50 \r\n.com.ni 23747 websites – $50 \r\n.com.np 38828 websites – $50 \r\n.com.ru, .net.ru, .org.ru, .spb.ru, .msk.ru 79058 websites Russia – $50 \r\n.community 13013 websites – $50 \r\n.company 61217 websites – $50 \r\n.computer 5039 websites – $50 \r\n.condos 2192 websites – $50 \r\n.construction 6804 websites – $50 \r\n.consulting 22128 websites – $50 \r\n.contractors 3982 websites – $50 \r\n.cooking 1476 websites – $50 \r\n.cool 16008 websites – $50 \r\n.coop 7879 websites – $50 \r\n.corsica 1042 websites – $50 \r\n.country 7144 websites – $50 \r\n.cr 7934 websites – $50 \r\n.credit 4020 websites – $50 \r\n.creditcard 825 websites – $50 \r\n.creditunion 511 websites – $50 \r\n.cricket 33413 websites – $50 \r\n.cruises 2234 websites – $50 \r\n.cu 137 websites – $50 \r\n.cv 1879 websites – $50 \r\n.cx 15753 websites – $50 \r\n.cy 11092 websites Cyprus – $50 \r\n.cy 710 websites International zone Cyprus:.com.net.biz.info.org.name.tel.mobi.asia \r\n.cymru 7114 websites – $50 \r\n.cz 193400 websites International zone Czech Republic:.com.net.biz.info.org.name.tel.mobi.asia-$50 \r\n.cz 930208 websites Czech Republic – $80 \r\n.dance 6290 websites – $50 \r\n.date 123037 websites – $50 \r\n.dating 2892 websites – $50 \r\n.de 15078512 websites Germany – $450 \r\n.de 3894156 websites International zone Germany:.com.net.biz.info.org.name.tel.mobi.asia-$200 \r\n.deals 8132 websites – $50 \r\n.degree 2178 websites – $50 \r\n.delivery 4782 websites – $50 \r\n.democrat 1072 websites – $50 \r\n.dental 7541 websites – $50 \r\n.dentist 3046 websites – $50 \r\n.desi 2647 websites – $50 \r\n.design 71711 websites – $50 \r\n.diamonds 2730 websites – $50 \r\n.diet 18291 websites – $50 \r\n.digital 31449 websites – $50 \r\n.direct 10629 websites – $50 \r\n.directory 18157 websites – $50 \r\n.discount 3898 websites – $50 \r\n.dj 7280 websites – $50 \r\n.dk 1320155 websites Denmark – $100 \r\n.dk 148164 websites International zone Denmark:.com.net.biz.info.org.name.tel.mobi.asia-$50 \r\n.dm 23318 websites – $50 \r\n.do 5255 websites Dominican Republic- $50 \r\n.dog 10030 websites – $50 \r\n.domains 6553 websites – $50 \r\n.download 129223 websites – $50 \r\n.durban 2247 websites – $50 \r\n.dz 982 websites – $50 \r\n.earth 8139 websites – $50 \r\n.ec 11731 websites – $50 \r\n.edu 4445 websites – $50 \r\n.edu.np 4883 websites- $50 \r\n.education 22003 websites – $50 \r\n.ee 10490 websites International zone Estonia:.com.net.biz.info.org.name.tel.mobi.asia \r\n.ee 119701 websites Estonia- $50 \r\n.eg 1699 websites – $50 \r\n.email 77321 websites – $50 \r\n.energy 9769 websites – $50 \r\n.engineer 2785 websites – $50 \r\n.engineering 5533 websites – $50 \r\n.enterprises 6153 websites – $50 \r\n.equipment 5760 websites – $50 \r\n.es 1685048 websites Spain – $100 \r\n.es 541916 websites International zone Spain:.com.net.biz.info.org.name.tel.mobi.asia-$80 \r\n.estate 9185 websites – $50 \r\n.et 124 websites – $50 \r\n.eu 3321576 websites Europe – $150 \r\n.eu 633384 websites International zone Europe:.com.net.biz.info.org.name.tel.mobi.asia-$80 \r\n.eus 8116 websites – $50 \r\n.events 22115 websites – $50 \r\n.exchange 9432 websites – $50 \r\n.expert 31240 websites – $50 \r\n.exposed 3147 websites – $50 \r\n.express 6919 websites – $50 \r\n.fail 3322 websites – $50 \r\n.faith 54195 websites – $50 \r\n.family 15577 websites – $50 \r\n.fans 1388 websites – $50 \r\n.farm 13499 websites – $50 \r\n.fashion 12475 websites – $50 \r\n.feedback 2301 websites – $50 \r\n.fi 178337 websites Finland – $50 \r\n.fi 69631 websites International zone Finland:.com.net.biz.info.org.name.tel.mobi.asia – $50 \r\n.film 3601 websites – $50 \r\n.finance 7982 websites – $50 \r\n.financial 4086 websites – $50 \r\n.fish 4162 websites – $50 \r\n.fishing 1423 websites – $50 \r\n.fit 17007 websites – $50 \r\n.fitness 9689 websites – $50 \r\n.flights 2119 websites – $50 \r\n.florist 2286 websites – $50 \r\n.flowers 25590 websites – $50 \r\n.fm 5407 websites – $50 \r\n.fo 3098 websites- $50 \r\n.football 4877 websites – $50 \r\n.forex 212 websites – $50 \r\n.forsale 7118 websites – $50 \r\n.foundation 10118 websites – $50 \r\n.fr 2391045 websites France – $150 \r\n.fr 639546 websites International zone France:.com.net.biz.info.org.name.tel.mobi.asia-$80 \r\n.frl 14028 websites – $50 \r\n.fun 86419 websites – $50 \r\n.fund 11205 websites – $50 \r\n.furniture 2246 websites – $50 \r\n.futbol 2783 websites – $50 \r\n.fyi 9772 websites – $50 \r\n.ga 12048 websites Gabon – $50 \r\n.gal 4606 websites – $50 \r\n.gallery 17263 websites – $50 \r\n.game 1996 websites – $50 \r\n.games 13234 websites – $50 \r\n.garden 914 websites – $50 \r\n.gd 4238 websites – $50 \r\n.ge 1676 websites International zone Georgia:.com.net.biz.info.org.name.tel.mobi.asia \r\n.ge 17361 websites Georgia – $50 \r\n.gent 3389 websites – $50 \r\n.gf 121 websites – $50 \r\n.gg 9443 websites – $50 \r\n.gh 693 websites – $50 \r\n.gi 1063 websites – $50 \r\n.gift 6281 websites – $50 \r\n.gifts 3757 websites – $50 \r\n.gives 1563 websites – $50 \r\n.gl 3575 websites – $50 \r\n.glass 3539 websites – $50 \r\n.global 38972 websites – $50 \r\n.gm 468 websites – $50 \r\n.gmbh 19186 websites – $50 \r\n.gold 9081 websites – $50 \r\n.golf 8319 websites – $50 \r\n.gop 1341 websites – $50 \r\n.gov 4525 websites – $50 \r\n.gov.np 1937 websites- $50 \r\n.gp 416 websites – $50 \r\n.gq 23306 websites – $50 \r\n.gr 356168 websites Greece – $80 \r\n.gr 57984 websites International zone Greece:.com.net.biz.info.org.name.tel.mobi.asia-$50 \r\n.graphics 7155 websites – $50 \r\n.gratis 4283 websites – $50 \r\n.green 3661 websites – $50 \r\n.gripe 1075 websites – $50 \r\n.group 54983 websites – $50 \r\n.gs 5108 websites – $50 \r\n.gt 15351 websites – $50 \r\n.guide 16044 websites – $50 \r\n.guitars 1278 websites – $50 \r\n.guru 60588 websites – $50 \r\n.gy 2447 websites – $50 \r\n.hamburg 23885 websites – $50 \r\n.haus 5186 websites – $50 \r\n.health 6211 websites – $50 \r\n.healthcare 8051 websites – $50 \r\n.help 13500 websites – $50 \r\n.hiphop 1064 websites – $50 \r\n.hiv 331 websites – $50 \r\n.hk 116093 websites – $50 \r\n.hm 249 websites – $50 \r\n.hn 4732 websites – $50 \r\n.hockey 1102 websites – $50 \r\n.holdings 5412 websites – $50 \r\n.holiday 5017 websites – $50 \r\n.homes 432 websites – $50 \r\n.horse 2116 websites – $50 \r\n.host 31309 websites – $50 \r\n.hosting 4132 websites – $50 \r\n.house 18096 websites – $50 \r\n.how 1957 websites – $50 \r\n.hr 16592 websites International zone Croatia:.com.net.biz.info.org.name.tel.mobi.asia \r\n.hr 43565 websites Croatia – $50 \r\n.ht 2559 websites – $50 \r\n.hu 53940 websites International zone Hungary:.com.net.biz.info.org.name.tel.mobi.asia – $50 \r\n.hu 618532 websites Hungary – $80 \r\n.id 37212 websites – $50 \r\n.ie 195987 websites Ireland – $50 \r\n.ie 49861 websites International zone Ireland:.com.net.biz.info.org.name.tel.mobi.asia – $50 \r\n.il 224167 websites Israel – $80 \r\n.il 38537 websites International zone Israel:.com.net.biz.info.org.name.tel.mobi.asia-$50 \r\n.im 20701 websites – $50 \r\n.immo 16009 websites – $50 \r\n.immobilien 7094 websites – $50 \r\n.in 1143482 websites India – $100 \r\n.in 266179 websites International zone India:.com.net.biz.info.org.name.tel.mobi.asia – $50 \r\n.industries 3749 websites – $50 \r\n.ink 27117 websites – $50 \r\n.institute 10134 websites – $50 \r\n.insure 4615 websites – $50 \r\n.int 191 websites – $50 \r\n.international 23930 websites – $50 \r\n.investments 4113 websites – $50 \r\n.io 314287 websites British Indian Ocean – $50 \r\n.iq 1118 websites – $50 \r\n.ir 15487 websites International zone Iran:.com.net.biz.info.org.name.tel.mobi.asia \r\n.ir 427735 websites Iran- $80 \r\n.irish 3326 websites – $50 \r\n.is 31176 websites Iceland – $50 \r\n.ist 10060 websites – $50 \r\n.istanbul 13139 websites – $50 \r\n.it 2258105 websites Italy – $200 \r\n.it 954040 websites International zone Italy:.com.net.biz.info.org.name.tel.mobi.asia-$100 \r\n.je 2716 websites – $50 \r\n.jetzt 11118 websites – $50 \r\n.jetzt 11704 websites – $50 \r\n.jewelry 3350 websites – $50 \r\n.jo 555 websites – $50 \r\n.jobs 46350 websites- $50 \r\n.joburg 3139 websites – $50 \r\n.jp 1146243 websites Japan – $100 \r\n.juegos 844 websites – $50 \r\n.kaufen 7134 websites – $50 \r\n.kg 664 websites International zone Kyrgyzstan:.com.net.biz.info.org.name.tel.mobi.asia \r\n.kg 8919 websites Kyrgyzstan – $50 \r\n.ki 1146 websites – $50 \r\n.kim 16637 websites- $50 \r\n.kitchen 6581 websites – $50 \r\n.kiwi 19426 websites – $50 \r\n.kn 1271 websites – $50 \r\n.koeln 23489 websites – $50 \r\n.kr 254447 websites Korea- $50 \r\n.krd 375 websites – $50 \r\n.kred 6120 websites – $50 \r\n.kw 423 websites – $50 \r\n.ky 1201 websites – $50 \r\n.kyoto 659 websites – $50 \r\n.kz 112459 websites Kazakhstan – $50 \r\n.kz 5876 websites International zone Kazakhstan:.com.net.biz.info.org.name.tel.mobi.asia \r\n.la 32189 websites Laos – $50 \r\n.land 14474 websites- $50 \r\n.lat 2971 websites – $50 \r\n.law 11842 websites – $50 \r\n.lawyer 11600 websites- $50 \r\n.lc 651 websites- $50 \r\n.lease 1855 websites- $50 \r\n.leclerc 127 websites- $50 \r\n.legal 11047 websites- $50 \r\n.lgbt 2249 websites- $50 \r\n.li 12044 websites – $50 \r\n.life 170053 websites – $50 \r\n.lighting 6096 websites – $50 \r\n.limited 5365 websites – $50 \r\n.limo 2409 websites- $50 \r\n.link 133123 websites – $50 \r\n.live 160896 websites – $50 \r\n.lk 6601 websites – $50 \r\n.loan 1932173 websites- $200 \r\n.loans 3914 websites – $50 \r\n.lol 7470 websites- $50 \r\n.london 82443 websites London, United Kingdom- $50 \r\n.love 22287 websites- $50 \r\n.lt 27710 websites International zone Lithuania:.com.net.biz.info.org.name.tel.mobi.asia \r\n.lt 89073 websites Lithuania- $50 \r\n.ltd 329225 websites – $50 \r\n.lu 43052 websites Luxembourg – $50 \r\n.lu 4125 websites International zone Luxembourg:.com.net.biz.info.org.name.tel.mobi.asia \r\n.luxury 905 websites – $50 \r\n.lv 61886 websites Latvia- $50 \r\n.lv 8887 websites International zone Latvia:.com.net.biz.info.org.name.tel.mobi.asia \r\n.ly 8013 websites – $50 \r\n.ma 51862 websites Morocco – $50 \r\n.maison 1123 websites – $50 \r\n.management 10088 websites- $50 \r\n.market 22167 websites- $50 \r\n.marketing 18262 websites- $50 \r\n.markets 816 websites- $50 \r\n.mba 3023 websites- $50 \r\n.mc 3036 websites Monaco – $50 \r\n.md 15435 websites Moldova – $50 \r\n.md 1293 websites International zone Moldova:.com.net.biz.info.org.name.tel.mobi.asia \r\n.me 761596 websites Montenegro – $80 \r\n.me 86897 websites International zone Montenegro:.com.net.biz.info.org.name.tel.mobi.asia – $50 \r\n.media 40573 websites – $50 \r\n.melbourne 10041 websites – $50 \r\n.memorial 512 websites – $50 \r\n.men 470140 websites – $80 \r\n.menu 5512 websites restaurants- $50 \r\n.mg 3380 websites Madagascar- $50 \r\n.miami 14431 websites Miami, USA – $50 \r\n.mk 12704 websites – $50 \r\n.ml 70170 websites – $50 \r\n.mma 1706 websites – $50 \r\n.mn 16710 websites – $50 \r\n.mo 573 websites – $50 \r\n.mobi 462419 websites- $80 \r\n.moda 3015 websites – $50 \r\n.moe 7709 websites – $50 \r\n.mom 3145 websites – $50 \r\n.money 9731 websites – $50 \r\n.mortgage 3126 websites – $50 \r\n.moscow 22097 websites Moscow Russian Federation- $50 \r\n.movie 2575 websites – $50 \r\n.mr 1725 websites – $50 \r\n.ms 7565 websites – $50 \r\n.mt 1623 websites Malta – $50 \r\n.mu 6175 websites – $50 \r\n.museum 34464 websites – $50 \r\n.mv 1907 websites – $50 \r\n.mw 8579 websites Malawi – $50 \r\n.mx 609278 websites Mexico- $80 \r\n.mx 86593 websites International zone Mexico:.com.net.biz.info.org.name.tel.mobi.asia-$50 \r\n.my 283349 websites Malaysia- $80 \r\n.mz 263 websites – $50 \r\n.na 781 websites – $50 \r\n.nagoya 7807 websites – $50 \r\n.name 135818 websites- $50 \r\n.navy 698 websites – $50 \r\n.nc 1240 websites – $50 \r\n.network 40018 websites – $50 \r\n.news 65546 websites – $50 \r\n.ng 16608 websites – $50 \r\n.ngo 3422 websites – $50 \r\n.ninja 49007 websites – $50 \r\n.nl 1019697 websites International zone Netherlands:.com.net.biz.info.org.name.tel.mobi.asia-$100 \r\n.nl 3566172 websites Netherlands – $200 \r\n.no 579185 websites Norway – $80 \r\n.no 74318 websites International zone Norway:.com.net.biz.info.org.name.tel.mobi.asia-$50 \r\n.nra 144 websites – $50 \r\n.nrw 18487 websites – $50 \r\n.nu 489605 websites Niue- $50 \r\n.nyc 67153 websites – $50 \r\n.nz 576127 websites New Zealand – $80 \r\n.om 1603 websites – $50 \r\n.one 63063 websites – $50 \r\n.ong 3422 websites – $50 \r\n.onl 3790 websites – $50 \r\n.online 695007 websites – $50 \r\n.ooo 6400 websites – $50 \r\n.org.np 7082 websites – $50 \r\n.org.ua 41362 websites – $50 \r\n.organic 1633 websites – $50 \r\n.osaka 628 websites – $50 \r\n.ovh 50056 websites – $50 \r\n.pa 1558 websites – $50 \r\n.paris 19283 websites – $50 \r\n.partners 6576 websites – $50 \r\n.parts 5514 websites – $50 \r\n.party 115578 websites- $50 \r\n.pe 59157 websites International zone Peru:.com.net.biz.info.org.name.tel.mobi.asia – $50 \r\n.pe 69367 websites Peru – $50 \r\n.pet 8487 websites – $50 \r\n.pf 349 websites – $50 \r\n.pg 1979 websites Papua – $50 \r\n.ph 17940 websites Philippines – $50 \r\n.photo 17165 websites- $50 \r\n.photography 50434 websites- $50 \r\n.photos 19107 websites- $50 \r\n.physio 1160 websites- $50 \r\n.pics 7659 websites- $50 \r\n.pictures 7807 websites- $50 \r\n.pink 6373 websites- $50 \r\n.pizza 5949 websites – $50 \r\n.pk 44464 websites Pakistan – $50 \r\n.pl 1675325 websites Poland – $100 \r\n.pl 327587 websites International zone Poland:.com.net.biz.info.org.name.tel.mobi.asia-$50 \r\n.place 7002 websites – $50 \r\n.plumbing 3005 websites – $50 \r\n.plus 9892 websites – $50 \r\n.pm 4401 websites – $50 \r\n.poker 3209 websites – $50 \r\n.porn 10023 websites- $50 \r\n.post 3481 websites – $50 \r\n.pr 18419 websites – $50 \r\n.press 35032 websites – $50 \r\n.pro 280776 websites – $50 \r\n.productions 6144 websites – $50 \r\n.promo 5020 websites – $50 \r\n.properties 14144 websites – $50 \r\n.property 4366 websites – $50 \r\n.ps 2050 websites – $50 \r\n.pt 17691 websites International zone Portugal:.com.net.biz.info.org.name.tel.mobi.asia-$50 \r\n.pt 225136 websites Portugal – $80 \r\n.pub 16703 websites – $50 \r\n.pw 16206 websites – $50 \r\n.py 4635 websites – $50 \r\n.qa 8503 websites – $50 \r\n.quebec 8042 websites – $50 \r\n.racing 55001 websites – $50 \r\n.re 11082 websites – $50 \r\n.realtor 39465 websites – $50 \r\n.realty 55291 websites – $50 \r\n.recipes 3251 websites – $50 \r\n.red 25701 websites – $50 \r\n.rehab 1816 websites – $50 \r\n.reise 4146 websites – $50 \r\n.reisen 9228 websites – $50 \r\n.reit 126 websites – $50 \r\n.ren 108303 websites – $50 \r\n.rent 3788 websites – $50 \r\n.rentals 11823 websites- $50 \r\n.repair 6628 websites- $50 \r\n.report 5869 websites – $50 \r\n.republican 952 websites – $50 \r\n.rest 1894 websites – $50 \r\n.restaurant 7703 websites – $50 \r\n.review 229514 websites – $80 \r\n.reviews 16132 websites- $50 \r\n.rio 1062 websites- $50 \r\n.rip 3199 websites- $50 \r\n.ro 42046 websites International zone Romania:.com.net.biz.info.org.name.tel.mobi.asia-$50 \r\n.ro 483266 websites Romania – $80 \r\n.rocks 81108 websites – $50 \r\n.rs 75503 websites Serbia – $50 \r\n.ru 5292104 websites Russian- $250 \r\n.ru 514668 websites International zone Russian:.com.net.biz.info.org.name.tel.mobi.asia -$80 \r\n.ru.com 6499 websites Russia – $50 \r\n.ruhr 9887 websites – $50 \r\n.run 10622 websites – $50 \r\n.rw 244 websites – $50 \r\n.sa 5064 websites International zone Saudi Arabia:.com.net.biz.info.org.name.tel.mobi.asia \r\n.sa 11604 websites Saudi Arabia- $50 \r\n.saarland 3825 websites – $50 \r\n.sale 35407 websites – $50 \r\n.salon 1782 websites – $50 \r\n.sarl 939 websites – $50 \r\n.sc 4942 websites Seychelles- $50 \r\n.school 11672 websites – $50 \r\n.schule 4678 websites – $50 \r\n.science 74115 websites – $80 \r\n.scot 11175 websites – $50 \r\n.sd 619 websites – $50 \r\n.se 293316 websites International zone Sweden:.com.net.biz.info.org.name.tel.mobi.asia-$50 \r\n.se 1799365 websites Sweden – $150 \r\n.seat 618 websites – $50 \r\n.security 255 websites – $50 \r\n.services 52098 websites – $50 \r\n.sex 7655 websites – $50 \r\n.sexy 18260 websites – $50 \r\n.sg 164351 websites Republic Of Singapore – $50 \r\n.sh 9408 websites – $50 \r\n.shiksha 1626 websites – $50 \r\n.shoes 5007 websites – $50 \r\n.shop 402139 websites – $80 \r\n.shopping 6297 websites – $50 \r\n.show 6946 websites – $50 \r\n.si 12879 websites International zone Slovenia:.com.net.biz.info.org.name.tel.mobi.asia – $50 \r\n.si 39749 websites Slovenia- $50 \r\n.singles 3559 websites – $50 \r\n.site 384414 websites – $80 \r\n.sk 31572 websites International zone Slovakia:.com.net.biz.info.org.name.tel.mobi.asia – $50 \r\n.sk 347101 websites Slovakia- $80 \r\n.ski 5071 websites – $50 \r\n.sl 418 websites – $50 \r\n.sm 2009 websites – $50 \r\n.sn 344 websites International zone Senegal:.com.net.biz.info.org.name.tel.mobi.asia \r\n.sn 4954 websites Senegal – $50 \r\n.so 9003 websites – $50 \r\n.soccer 2833 websites – $50 \r\n.social 19134 websites – $50 \r\n.software 13001 websites – $50 \r\n.solar 7095 websites – $50 \r\n.solutions 81282 websites – $50 \r\n.soy 1200 websites – $50 \r\n.space 220771 websites – $80 \r\n.sr 638 – $50 \r\n.srl 4391 – $50 \r\n.st 7683 websites – $50 \r\n.storage 623 – $50 \r\n.store 167141 websites – $50 \r\n.stream 239795 websites – $80 \r\n.studio 39132 websites – $50 \r\n.study 5132 websites – $50 \r\n.style 9248 websites – $50 \r\n.su 125358 websites Russian- $50 \r\n.sucks 7629 websites – $50 \r\n.supplies 3082 websites – $50 \r\n.supply 4580 websites – $50 \r\n.support 20377 websites – $50 \r\n.surf 2573 websites – $50 \r\n.surgery 1778 websites – $50 \r\n.sv 8132 websites Salvador- $50 \r\n.swiss 17323 websites – $50 \r\n.sx 3240 websites – $50 \r\n.sy 2063 websites – $50 \r\n.sydney 10073 websites – $50 \r\n.systems 26094 websites – $50 \r\n.sz 194 websites – $50 \r\n.taipei 5664 websites – $50 \r\n.tattoo 2004 websites- $50 \r\n.tax 5788 websites – $50 \r\n.taxi 5784 websites – $50 \r\n.tc 16384 websites Turks and Caicos Islands- $50 \r\n.team 18421 websites- $50 \r\n.tech 196321 websites – $50 \r\n.technology 40181 websites- $50 \r\n.tel 80752 websites- $50 \r\n.tennis 1494 websites – $50 \r\n.tf 2049 websites – $50 \r\n.tg 521 websites – $50 \r\n.th 23968 websites Kingdom Of Thailand- $50 \r\n.theater 1053 websites – $50 \r\n.tickets 1141 websites – $50 \r\n.tienda 2951 websites – $50 \r\n.tips 31287 websites- $50 \r\n.tires 960 websites – $50 \r\n.tirol 5412 websites – $50 \r\n.tj 34 websites International zone Tajikistan:.com.net.biz.info.org.name.tel.mobi.asia \r\n.tj 6804 websites Tajikistan- $50 \r\n.tk 12799253 websites Tokelau – $450 \r\n.tl 2728 websites – $50 \r\n.tm 6056 websites Turkmenistan- $50 \r\n.tm 44 websites International zone Turkmenistan:.com.net.biz.info.org.name.tel.mobi.asia \r\n.tn 27383 websites – $50 \r\n.to 16987 websites Tonga- $50 \r\n.today 98034 websites – $50 \r\n.tokyo 90132 websites – $50 \r\n.tools 10439 websites – $50 \r\n.top 2140480 websites – $200 \r\n.tours 9151 websites – $50 \r\n.town 3328 websites – $50 \r\n.toys 4391 websites – $50 \r\n.tr 138818 International zone Turkey:.com.net.biz.info.org.name.tel.mobi.asia -$50 \r\n.tr 317690 websites Turkey – $80 \r\n.trade 151130 websites – $50 \r\n.trading 896 websites – $50 \r\n.training 18192 websites – $50 \r\n.travel 18054 websites – $50 \r\n.tt 516 websites – $50 \r\n.tube 2104 websites – $50 \r\n.tv 559502 websites Tuvalu – $80 \r\n.tw 243062 websites Taiwan – $50 \r\n.tz 1874 websites – $50 \r\n.ua 117701 websites International zone Ukraine:.com.net.biz.info.org.name.tel.mobi.asia-$50 \r\n.ua 553216 websites Ukraina – $80 \r\n.ug 2331 websites Ukraina \r\n.uk 3304606 websites International zone United Kingdom:.com.net.biz.info.org.name.tel.mobi.asia-$150 \r\n.uk 5496382 websites United Kingdom – $250 \r\n.university 5799 websites – $50 \r\n.uno 16394 websites – $50 \r\n.us 3139563 websites USA – $200 \r\n.us 578927 websites International zone USA:.com.net.biz.info.org.name.tel.mobi.asia-$80 \r\n.uy 14683 websites Uruguay – $50 \r\n.uz 365 websites International zone Uzbekistan:.com.net.biz.info.org.name.tel.mobi.asia \r\n.uz 14689 websites Uzbekistan – $50 \r\n.vacations 3926 websites – $50 \r\n.vc 16606 websites – $50 \r\n.ve 14015 websites Venezuela – $50 \r\n.vegas 18008 websites Las Vegas NV United States of America – $50 \r\n.ventures 10066 websites – $50 \r\n.versicherung 2009 websites – $50 \r\n.vet 5760 websites – $50 \r\n.vg 8389 site – $50 \r\n.viajes 2512 site – $50 \r\n.video 16204 websites- $50 \r\n.villas 1940 site – $50 \r\n.vin 5094 websites – $50 \r\n.vip 755289 websites – $80 \r\n.vision 6420 websites – $50 \r\n.vlaanderen 6114 websites – $50 \r\n.vn 161855 websites International zone Vietnam:.com.net.biz.info.org.name.tel.mobi.asia – $50 \r\n.vn 436005 websites Vietnam – $80 \r\n.vodka 1420 websites – $50 \r\n.vote 2316 websites – $50 \r\n.voto 269 websites – $50 \r\n.voyage 2773 websites – $50 \r\n.vu 1201 websites – $50 \r\n.wales 13063 websites – $50 \r\n.wang 604718 websites – $80 \r\n.watch 6120 websites – $50 \r\n.webcam 60142 websites – $50 \r\n.website 207002 websites -$50 \r\n.wedding 22162 websites – $50 \r\n.wf 1188 websites – $50 \r\n.wien 14464 websites – $50 \r\n.wien 14913 websites – $50 \r\n.wiki 14129 websites wikis – $50 \r\n.win 655735 websites – $80 \r\n.wine 13095 websites – $50 \r\n.work 256086 websites – $80 \r\n.works 15112 websites – $50 \r\n.world 86012 websites – $50 \r\n.ws 97008 websites Samoa- $80 \r\n.wtf 10038 websites – $50 \r\n.xin 178450 websites – $50 \r\n.xn--3ds443g 44019 websites – $50 \r\n.xn--55qx5d 47456 websites – $50 \r\n.xn--6qq986b3xl 18188 websites – $50 \r\n.xn--czr694b 21109 websites – $50 \r\n.xn--czru2d 9020 websites – $50 \r\n.xn--fiq228c5hs 12145 websites – $50 \r\n.xn--io0a7i 30199 websites – $50 \r\n.xn--j6w193g 31764 websites – $50 \r\n.xn--kput3i 31856 websites – $50 \r\n.xn--mgbaam7a8h 2038 websites – $50 \r\n.xn--mgberp4a5d4ar 2534 websites – $50 \r\n.xn--mk1bu44c 6001 websites – $50 \r\n.xn--rhqv96g 7723 websites – $50 \r\n.xn--ses554g 198539 websites – $80 \r\n.xn--tckwe 6197 websites – $50 \r\n.xn--vuq861b 19706 websites – $50 \r\n.xxx 119879 websites- $50 \r\n.xyz 1888763 websites – $150 \r\n.yachts 154 websites – $50 \r\n.yoga 7963 websites – $50 \r\n.yokohama 8140 websites – $50 \r\n.yt 2004 websites – $50 \r\n.za 837189 websites South Africa – $100 \r\n.zm 224 websites – $50 \r\n.zone 25798 websites – $50 \r\n.бг (.xn--90ae) 2470 websites – $50 \r\n.дети 169 websites – $50 \r\n.москва (.xn--80adxhks) 19582 websites Moscow – $50 \r\n.онлайн 3403 websites – $50 \r\n.орг 1160 websites – $50 \r\n.рус (.xn--p1acf) 110789 websites – $50 \r\n.рф (.xn--p1ai) 869759 websites Russia – $80 \r\n.сайт 1146 websites – $50 \r\n.укр (.xn--j1amh) 10563 websites- $50 \r\n.संगठन 105  sites – $50 \r\n>.みんな 166  sites – $50 \r\n.コム    6532  sites – $50 \r\n.世界    4171  sites – $50 \r\n.公司    46161  sites – $50 \r\n.商城    6 907  sites – $50 \r\n.商标    9865  sites – $50 \r\n.我爱你 15465  sites – $50 \r\n.手机    31543  sites – $50 \r\n.机构    243  sites – $50 \r\n.游戏    187  sites – $50 \r\n.移动    3277  sites – $50 \r\n.网店    3711  sites – $50 \r\n.网络    30808  sites – $50 \r\n.닷컴    5937  sites – $50 \r\n.بازار  sites 648 – $50 \r\n.شبكة   sites 833 – $50 \r\n.موقع   sites 478 – $50', 'waiting-confirmation', '2019-04-15 18:42:03', '2019-04-15 18:42:03'),
(153, 'Louisknina', 'svetlanacol0sova@yandex.ua', '383772863', 'Check it please?', 'Grow your bitcoins by 10% per 2 days. \r\nProfit comes to your btc wallet automatically. \r\n \r\nTry  http://bm-syst.xyz \r\nit takes 2 minutes only and let your btc works for you! \r\n \r\nGuaranteed by the blockchain technology!', 'waiting-confirmation', '2019-04-20 20:24:25', '2019-04-20 20:24:25'),
(154, 'JamesShome', 'cgorillamail@gmail.com', '384143282', 'Feedback and a proposal', 'Hi, oriscake.com \r\n \r\nI\'ve been visiting your website a few times and decided to give you some positive feedback because I find it very useful. Well done. \r\n \r\nI was wondering if you as someone with experience of creating a useful website could help me out with my new site by giving some feedback about what I could improve? \r\n \r\nYou can find my site by searching for \"casino gorilla\" in Google (it\'s the gorilla themed online casino comparison). \r\n \r\nI would appreciate if you could check it out quickly and tell me what you think. \r\n \r\ncasinogorilla.com \r\n \r\nThank you for help and I wish you a great week!', 'waiting-confirmation', '2019-04-21 16:14:32', '2019-04-21 16:14:32'),
(155, 'Davidquent', 'gunrussia@scryptmail.com', '238285835', 'Illegal weapons from Russia. Black Market - Simple and inexpensive!', '25 charging traumatic pistols shooting automatic fire! Modified Makarov pistols with a silencer! Combat Glock 17 original or with a silencer! And many other types of firearms without a license, without documents, without problems! \r\nDetailed video reviews of our products you can see on our website. \r\n \r\nIf the site is unavailable or blocked, email us at - Gunrussia@secmail.pro   or  Gunrussia@elude.in \r\nAnd we will send you the address of the backup site!', 'waiting-confirmation', '2019-04-30 20:40:20', '2019-04-30 20:40:20'),
(156, 'Eric', 'eric@talkwithcustomer.com', '416-385-3200', 'You deserve more clients for oriscake.com and it’s THIS easy', 'Hello,\r\n\r\nYou know it’s true.\r\n\r\nYour competition just can’t hold a candle to the way you DELIVER real solutions to your customers at oriscake.com\r\n\r\nBut it’s a shame when good people who need what you have to offer wind up settling for second best or even worse.\r\n  \r\nNot only do they deserve better, you deserve to be at the top of their list.\r\n \r\nTalkWithCustomer can reliably turn your website into a serious, lead generating machine.\r\n \r\nWith TalkWithCustomer installed on your site, visitors can either call you immediately or schedule a call for you in the future.\r\n  \r\nAnd the difference to your business can be staggering – up to 100X more leads could be yours, just by giving TalkWithCustomer a FREE 14 Day Test Drive.\r\n \r\nThere’s absolutely NO risk to you, so CLICK HERE http://www.talkwithcustomer.com to sign up for this free test drive now. \r\n \r\nTons more leads? You deserve it.\r\n\r\nSincerely,\r\nAndy\r\n\r\nPS:  Odds are, you won’t have long to wait before seeing results:\r\n-This service makes an immediate difference in getting people on the phone right away before they have a chance to turn around and surf off to a competitor’s website.- David Traylor, Traylor Law Firm\r\n  \r\nWhy wait any longer?  \r\n\r\nCLICK HERE http://www.talkwithcustomer.com to take the FREE 14-Day Test Drive and start converting up to 100X more leads today!\r\n\r\nIf you\'d like to unsubscribe click here. http://liveserveronline.com/talkwithcustomer.aspx?d=oriscake.com', 'waiting-confirmation', '2019-05-08 16:54:53', '2019-05-08 16:54:53'),
(157, 'Danielgoarp', 'michaelKnori@gmail.com', '161652828', 'Look what we organize on account of you! a first-classoffers  To moderate click on the tie-up unbefitting', 'Hey What we have here is , a seemlyoffer \r\n Are you in?  \r\n \r\nhttps://drive.google.com/file/d/1-nE-MW-UPDmRpgHeIl-y1DluHgJ359mT/preview', 'waiting-confirmation', '2019-05-12 01:40:35', '2019-05-12 01:40:35'),
(158, 'PhilipDib', 'chiptuffer@yahoo.com', '254732344', 'Build A House In Paradise', 'Dear Muslim, \r\nAmong your monthly business bills is the unexciting – paper, printer ink, phone lines. But what if there could be something among these payments could improve how well Islamic classes were taught, or how secure your local Mosques are? \r\n \r\nThe Messenger of Allah, peace and blessings be upon him… \r\n \r\n“Whoever builds a mosque for Allah, then Allah will build for him a house like it in Paradise” \r\n \r\nBut this isn’t about simple donations. This is about making a real, tangible difference. \r\n \r\nTangible differences like the secure door that was funded for Madina Masjid, in Rochdale - which is keeping the Mosque safe, following two break-ins over the two months previous. \r\n \r\nMaking charity donations benefits your business. It shines a light on the positive ways in which your company supports the local community. \r\n \r\nFunding our projects with a monthly donation provides you with a SHARE of the reward whenever someone prays Salah, recites Quran and does Tasbeeh in the mosque. \r\n \r\nWe share our thanks  and the gratitude of our Mosque worshippers – in advance. \r\n \r\nPlease feel free to contact us directly to talk about the ways in which your monthly support could change our local Mosques and communities for the better. \r\n? \r\nMessenger of Allah (peace and blessings of Allah be upon him) said… \r\n“Allah said: ‘Spend, O son of Adam, and I shall spend on you.’” \r\n \r\nhttps://www.houseinparadise.org/', 'waiting-confirmation', '2019-05-17 09:01:51', '2019-05-17 09:01:51'),
(159, 'PatrickZerty', 'ddggabogados@mail.com', '271148423', 'A Letter from Barrister David Gomez Gonzalez', 'Dearest in mind, \r\nI would like to introduce myself for the first time. My name is Barrister David Gómez González, the personal lawyer to my late client. \r\nHe worked as a private businessman in the international field. In 2012, my client succumbed to an unfortunate car accident. My client was single and childless. \r\nHe left a fortune worth $24,500,000.00 Dollars in a bank in Spain. The bank sent me message that I have to introduce a beneficiary or the money in their bank will be confiscate. My purpose of contacting you is to make you the Next of Kin. \r\nMy late client left no will, I as his personal lawyer, was commissioned by the Spanish Bank to search for relatives to whom the money left behind could be paid by my deceased client. I have been looking for his relatives for the past 3 months continuously without success. Now I explain why I need your support, I have decided to make a citizen of the same country with my late client the Next of Kin. \r\nI hereby ask you if you give me your consent to present you as the next of kin to my deceased client to the Spanish Bank as the beneficiary.  I would like to point out that you will receive 45% of the share of this money, 45% then I would be entitled to, 10% percent will be donated to charitable organizations. \r\nIf you are interested, please contact me at my private contact details by Tel: 0034-604-284-281, Fax: 0034-911-881-353, Email: ddggabogados@mail.com \r\nI am waiting for your answer \r\nBest regards, \r\nLawyer: - David Gómez González', 'waiting-confirmation', '2019-05-21 16:30:39', '2019-05-21 16:30:39'),
(160, 'RobertGox', 'angelaAftesteZerT@gmail.com', '13028368923', 'FREE TEST MAILING', 'Good day!  oriscake.com \r\n \r\nWe advance \r\n \r\nSending your commercial offer through the feedback form which can be found on the sites in the Communication section. Feedback forms are filled in by our software and the captcha is solved. The profit of this method is that messages sent through feedback forms are whitelisted. This technique raise the chances that your message will be read. Mailing is done in the same way as you received this message. \r\nYour  commercial proposal will be seen by millions of site administrators and those who have access to the sites! \r\n \r\nThe cost of sending 1 million messages is $ 49 instead of $ 99. (you can select any country or country domain) \r\nAll USA - (10 million messages sent) - $399 instead of $699 \r\nAll Europe (7 million messages sent)- $ 299 instead of $599 \r\nAll sites in the world (25 million messages sent) - $499 instead of $999 \r\nThere is a possibility of FREE TEST MAILING. \r\n \r\n \r\nDiscounts are valid until May 31. \r\nFeedback and warranty! \r\nDelivery report! \r\nIn the process of sending messages we don\'t break the rules GDRP. \r\n \r\nThis message is automatically generated to use our contacts for communication. \r\n \r\n \r\n \r\nContact us. \r\nTelegram - @FeedbackFormEU \r\nSkype – FeedbackForm2019 \r\nEmail - FeedbackForm@make-success.com \r\nWhatsApp - +44 7598 509161 \r\nhttp://bit.ly/2W5ksl2 \r\n \r\nIt\'s time to finish.', 'waiting-confirmation', '2019-05-31 21:38:50', '2019-05-31 21:38:50'),
(161, 'How would you use $30,000 to make more money http://box9.ru/invest-bitcoin/?p=19915', 'sheikhnuman78@gmail.com', '83835245436', 'I\'m 23. I have $30000. How can I best use it to make more money', 'Get $1500 – $6000 per DAY: http://box9.ru/invest-bitcoin/?p=46779', 'waiting-confirmation', '2019-06-02 20:30:56', '2019-06-02 20:30:56'),
(162, 'Bryantexant', 'ratclub@msn.com', '88627962943', 'Forex trader makes $10,000 in minutes', 'Forex 1000 To 1 Million вЂ“ Turning $10,000 into $1 Million in Forex: http://box9.ru/invest-bitcoin/?p=2582', 'waiting-confirmation', '2019-06-02 20:42:50', '2019-06-02 20:42:50'),
(163, 'Raymondamigh', 'daniel13sotaa@hotmail.com', '82711255781', 'How to invest in bitcoins $ 5000 - get a return of up to 2000%', 'How to invest in bitcoins in 2019 and receive passive income of $ 7,000 per month: http://box9.ru/get-bitcoin/?p=22355', 'waiting-confirmation', '2019-06-02 20:48:05', '2019-06-02 20:48:05'),
(164, 'How to invest in bitcoins $ 5000 - get a return of up to 2000% http://cutt.us/eiAbzlFQt', 'erickepner@aol.com', '82822872915', 'Cryptocurrency Trading & Investing Strategy for 2019. Receive passive income of $ 7,000 per month', 'Invest $ 5,000 in Bitcoin once and get $ 7,000 passive income per month: http://v.ht/CG93X', 'waiting-confirmation', '2019-06-04 22:11:32', '2019-06-04 22:11:32'),
(165, 'Simple biz + new tool = $450 per hour http://soo.gd/XuKya', 'Gabyswelt@web.de', '84776577634', 'Auto Mass Money Maker', 'Make $200 per hour doing this: http://v.ht/0YK7xK', 'waiting-confirmation', '2019-06-04 22:15:04', '2019-06-04 22:15:04'),
(166, 'Forex trader makes $10,000 in minutes http://v.ht/3vQFZGb', 'muaddibe@hotmail.com', '87473944743', 'How You Can Make $100,000 Every Month Trading Forex', 'How To Make Over $10,000 In One Trade | Live Forex Trading and Analysis: https://clc.la/ffPwl', 'waiting-confirmation', '2019-06-04 23:18:22', '2019-06-04 23:18:22'),
(167, 'Jamesseido', 'rebaritl@hotmail.com', '82657297827', '$ 10000 pro Tag Bitcoin-Handel mit binГ¤ren Optionen', '$ 10000 pro Tag Handelsplattform fГјr Bitcoin-BinГ¤roptionen: http://tinyurl.com/yx9racye', 'waiting-confirmation', '2019-06-06 19:52:43', '2019-06-06 19:52:43'),
(168, 'Marvinlix', 'craftec@hotmail.com', '88762189116', 'Wie kГ¶nnen Sie 2019 in Bitcoins investieren und ein passives Einkommen von 7.000 USD pro Monat erzielen?', 'Wenn Sie im Jahr 2011 1.000 USD in Bitcoin investiert haben, haben Sie jetzt 4 Millionen USD: https://clck.ru/GR5ZZ', 'waiting-confirmation', '2019-06-06 19:54:45', '2019-06-06 19:54:45'),
(169, 'Lowellimapy', 'shayne_w@msn.com', '86231685613', 'Bezahlte Umfragen: Verdienen Sie € 3.000 oder mehr pro Woche', 'Wie wurden Sie € 3.000 einsetzen, um mehr Geld zu verdienen?: https://clck.ru/GQzEo', 'waiting-confirmation', '2019-06-06 19:56:53', '2019-06-06 19:56:53'),
(170, 'Lowellimapy', 'chalker@hotmail.com', '89733647233', 'Wie man € 10.000 pro Tag SCHNELL macht', 'Wie mache ich € 3000 pro Tag?: https://clck.ru/GQqkj', 'waiting-confirmation', '2019-06-07 01:22:24', '2019-06-07 01:22:24'),
(171, 'Jamesseido', 'simorad@yahoo.fr', '87415873862', '$ 10000 pro Tag Handelsplattform fГјr Bitcoin-BinГ¤roptionen', '$ 10000 pro Tag Handelsplattform fГјr Bitcoin-BinГ¤roptionen: http://tinyurl.com/y3yefrtg', 'waiting-confirmation', '2019-06-07 06:39:20', '2019-06-07 06:39:20'),
(172, 'Marvinlix', 'wallyhn@hotmail.com', '85721478271', 'Investieren Sie einmalig in den Abbau von KryptowГ¤hrung 5.000 USD und erzielen Sie ein passives Einkommen von 7.000 USD pro Monat', 'Investieren Sie einmalig in den Abbau von KryptowГ¤hrung 5.000 USD und erzielen Sie ein passives Einkommen von 7.000 USD pro Monat: https://clck.ru/GR3rP', 'waiting-confirmation', '2019-06-07 08:29:57', '2019-06-07 08:29:57'),
(173, 'oriscake.com', 'micgyhaelKnori@gmail.com', '254611127', 'Please note a goodpromotion for victory. oriscake.com', 'Look at an important  prize for win. oriscake.com \r\nhttp://bit.ly/2KBEuAz', 'waiting-confirmation', '2019-06-07 19:48:09', '2019-06-07 19:48:09'),
(174, 'Marvinlix', 'hiramcq@hotmail.com', '88474124759', 'Adult online dating phone numbers', 'Adult american dating free online usa: http://tinyurl.com/y3e96dsx', 'waiting-confirmation', '2019-06-08 23:41:51', '2019-06-08 23:41:51'),
(175, 'PhillipNaw', 'dohunt@hotmail.com', '83362123155', 'Adult zoosk 1 application de rencontres itunes', 'Adulte meilleur site de rencontres gratuit canadien: http://tinyurl.com/y2qbcv7g', 'waiting-confirmation', '2019-06-08 23:44:59', '2019-06-08 23:44:59'),
(176, 'Lowellimapy', 'emmanuelds@msn.com', '84778125544', 'Schone Madchen fur Sex in Ihrer Stadt Gro?britannien', 'Adult Online-Dating nach Zahlen: http://tinyurl.com/yyalwtsg', 'waiting-confirmation', '2019-06-08 23:56:23', '2019-06-08 23:56:23'),
(177, 'Lowellimapy', 'benhansen@hotmail.com', '83256672539', 'Adult American Dating-Websites online', 'Adult 1 Dating-App: http://tinyurl.com/y6636agq', 'waiting-confirmation', '2019-06-09 05:13:26', '2019-06-09 05:13:26');
INSERT INTO `contact_messages` (`id`, `name`, `email`, `phone`, `subject`, `message`, `status`, `created_at`, `updated_at`) VALUES
(178, 'PhillipNaw', 'cramos02@hotmail.com', '84351642846', 'Sites de rencontre gratuits pour adultes Г  East London', 'Belles femmes pour le sexe dans ta ville AU: http://tinyurl.com/y4ghao9p', 'waiting-confirmation', '2019-06-09 09:45:07', '2019-06-09 09:45:07'),
(179, 'Marvinlix', 'taylor_dm@hotmail.com', '81317736253', 'Adult online dating swapping numbers', 'The best women for sex in your town UK: http://tinyurl.com/y4urawao', 'waiting-confirmation', '2019-06-09 10:49:04', '2019-06-09 10:49:04'),
(180, 'The best girls for sex in your town Canada http://tinyurl.com/yypeyo62', 'cornellmrfoot@aol.com', '86313947833', 'Meet sexy girls in your city AU', 'The best girls for sex in your town USA: http://tinyurl.com/y6ogvdvg', 'waiting-confirmation', '2019-06-10 02:39:23', '2019-06-10 02:39:23'),
(181, 'Charlesmub', 'vulckan@hotmail.com', '88558287827', 'Meet sexy girls in your city UK', 'Beautiful girls for sex in your city UK: http://qps.ru/riy0a', 'waiting-confirmation', '2019-06-10 20:27:37', '2019-06-10 20:27:37'),
(182, 'Jamespat', 'Gtclub01@yahoo.com', '81112325393', 'Meet sexy girls in your city UK', 'Meet sexy girls in your city USA: http://qps.ru/jtrxv', 'waiting-confirmation', '2019-06-10 20:27:39', '2019-06-10 20:27:39'),
(183, 'Jamesseido', 'claytonvdq@yahoo.com', '86187615397', 'Beautiful girls for sex in your city USA', 'Meet sexy girls in your city USA: https://clck.ru/GWDUm', 'waiting-confirmation', '2019-06-10 20:37:39', '2019-06-10 20:37:39'),
(184, 'Jamespat', 'rob10@hotmail.co.uk', '88819917721', 'Meet sexy girls in your city USA', 'The best women for sex in your town USA: http://tinyurl.com/y4zn5jme', 'waiting-confirmation', '2019-06-11 01:29:26', '2019-06-11 01:29:26'),
(185, 'Charlesmub', 'fagrehn@hotmail.com', '89575263232', 'Find yourself a girl for the night in your city Canada', 'The best women for sex in your town UK: http://qps.ru/E3SMj', 'waiting-confirmation', '2019-06-11 07:32:37', '2019-06-11 07:32:37'),
(186, 'Jamesseido', 'keerthikakeerthi93@gmail.com', '86525119751', 'Meet sexy girls in your city UK', 'Beautiful women for sex in your town USA: http://qps.ru/J4dtA', 'waiting-confirmation', '2019-06-11 10:24:57', '2019-06-11 10:24:57'),
(187, 'Jamespat', 'amiebereson@hotmail.com', '83586437318', 'Only for Australians. Invest $ 5,000 and get from $ 15,000 per week', 'Cryptocurrency investment for Australians: http://tinyurl.com/y23n6f9f', 'waiting-confirmation', '2019-06-11 20:18:03', '2019-06-11 20:18:03'),
(188, 'Marvinlix', 'whyfer@msn.com', '86781626837', 'If you Australian get passive income of $ 15,000 per week', 'Passive income for Australians from $ 15,000 per week: http://to.ht/bestinvestt68934', 'waiting-confirmation', '2019-06-11 20:18:05', '2019-06-11 20:18:05'),
(189, 'PhillipNaw', 'eisenmond@hotmail.com', '85265274247', 'Only for Australians. Invest $ 5,000 and get from $ 15,000 per week', 'Passive income of $ 15,000 if you are from Australia: http://herrirele.tk/m7ao', 'waiting-confirmation', '2019-06-11 20:20:59', '2019-06-11 20:20:59'),
(190, 'Investments for Australians http://niolectiping.tk/zaf3n', 'sillysally1@msn.com', '89434365844', 'If you Australian get passive income of $ 15,000 per week', 'Bitcoin Investment Australia: http://lieterchicu.tk/vv9n', 'waiting-confirmation', '2019-06-11 20:53:41', '2019-06-11 20:53:41'),
(191, 'Jamespat', 'tabrisss@gmail.com', '82394758379', 'Passive income of $ 15,000 if you are from Australia', '10 top investments for Australians: http://scotocolres.tk/3cvs', 'waiting-confirmation', '2019-06-12 01:27:56', '2019-06-12 01:27:56'),
(192, 'Marvinlix', 'cran11@hotmail.com', '89796971771', '10 top investments for Australians', 'If you Australian get passive income of $ 15,000 per week: http://qps.ru/H1PVM', 'waiting-confirmation', '2019-06-12 08:07:34', '2019-06-12 08:07:34'),
(193, 'PhillipNaw', 'sarriasun@aol.com', '85372919351', 'Passive income of $ 15,000 if you are from Australia', 'Passive income of $ 15,000 if you are from Australia: http://nabpenesring.tk/ug2a5', 'waiting-confirmation', '2019-06-12 21:14:33', '2019-06-12 21:14:33'),
(194, 'Charlesmub', 'je.ss.ehu.n.gerfo.rd68.9.8@gmail.com', '83531723167', 'Was ist der beste Weg, um 10.000 US-Dollar fГјr die Deutschen zu investieren?', 'Intelligente MГ¶glichkeiten, 10.000 US-Dollar fГјr die Deutschen zu investieren: http://xurl.es/Request', 'waiting-confirmation', '2019-06-13 04:19:08', '2019-06-13 04:19:08'),
(195, 'Jamesseido', 'lisseth1094@hotmail.com', '84136498519', 'Investitionen fГјr die Deutschen', 'Nur fГјr die Deutschen. Investieren Sie 5.000 USD und verdienen Sie 15.000 USD pro Woche: http://eb.by/Zq9D', 'waiting-confirmation', '2019-06-13 04:19:12', '2019-06-13 04:19:12'),
(196, 'HollisSlari', 'wendykolbe@gmail.com', '89318663825', 'Was ist der beste Weg, um 10.000 US-Dollar fГјr die Deutschen zu investieren?', 'Passives Einkommen von 15.000 USD, wenn Sie aus Deutschland kommen: http://xurl.es/8xwwi', 'waiting-confirmation', '2019-06-13 04:21:36', '2019-06-13 04:21:36'),
(197, 'Jamespat', 'dmsm24@yahoo.com', '87678217447', 'Passives Einkommen von 15.000 USD, wenn Sie aus Deutschland kommen', 'Passives Einkommen von 15.000 USD, wenn Sie aus Deutschland kommen: http://eb.by/ZTFU', 'waiting-confirmation', '2019-06-13 06:52:41', '2019-06-13 06:52:41'),
(198, 'Jamespat', 'nataliechandler124@hotmail.com', '84355931482', 'Kryptowahrungsinvestition fur die Deutschen', '5 beliebte Investment Apps in Deutschland: http://eb.by/ZqzY', 'waiting-confirmation', '2019-06-13 14:17:49', '2019-06-13 14:17:49'),
(199, 'Charlesmub', 'huukinhbk@gmail.com', '84965417275', 'Investitionen fГјr die Deutschen', 'Nur fГјr die Deutschen. So investieren Sie 10.000 US-Dollar und verdienen 1.500.000 US-Dollar dafГјr: http://soo.gd/Ut4fNs5', 'waiting-confirmation', '2019-06-13 16:14:19', '2019-06-13 16:14:19'),
(200, 'Jamesseido', 'leajsxj@hotmail.com', '85378631611', 'Die besten InvestitionsmГ¶glichkeiten fГјr die Deutschen', 'Investitionen fГјr die Deutschen: http://eb.by/ZTfe', 'waiting-confirmation', '2019-06-13 16:49:19', '2019-06-13 16:49:19'),
(201, 'HollisSlari', 'dsjonathan18@yahoo.com', '88157832858', 'Nur fГјr die Deutschen. So investieren Sie 10.000 US-Dollar und verdienen 1.500.000 US-Dollar dafГјr', 'Investitionen fГјr die Deutschen: http://xurl.es/nhjpf', 'waiting-confirmation', '2019-06-13 18:09:52', '2019-06-13 18:09:52'),
(202, 'Charlesmub', 'wallyramnarase4@yahoo.com', '88931747881', 'How to invest in bitcoins in 2019 and receive passive income of $ 70,000 per month', 'UPDATE: Cryptocurrency Investing Strategy - Q2 2019. Receive passive income of $ 70,000 per month: http://v.ht/s7goIc', 'waiting-confirmation', '2019-06-14 08:47:11', '2019-06-14 08:47:11'),
(203, 'Jamesseido', 'panchatcharaprabu@gmail.com', '89232255184', 'How to invest in bitcoins in 2019 and receive passive income of $ 70,000 per month', 'Invest $ 15,000 in Bitcoin mining once and get $ 70,000 passive income per month: https://is.gd/TO1iYU', 'waiting-confirmation', '2019-06-14 08:47:48', '2019-06-14 08:47:48'),
(204, 'Jamespat', 'sutepwinner@gmail.com', '87588369416', 'Invest $ 15,000 in cryptocurrency once and get $ 70,000 passive income per month', 'If you invested $1,000 in bitcoin in 2011, now you have $4 million: https://is.gd/I0aM9E', 'waiting-confirmation', '2019-06-14 09:00:13', '2019-06-14 09:00:13'),
(205, 'Jamespat', 'dowlovedog@hotmail.com', '82231835776', 'Invest $ 15,000 in Bitcoin mining once and get $ 70,000 passive income per month', 'Invest $ 15,000 in cryptocurrency once and get $ 70,000 passive income per month: http://cutt.us/DJ2ZjY', 'waiting-confirmation', '2019-06-14 15:24:41', '2019-06-14 15:24:41'),
(206, 'Jamesseido', 'poomkung38@hotmail.com', '84448842668', 'Cryptocurrency Trading & Investing Strategy for 2019. Receive passive income of $ 70,000 per month', 'Invest $ 15,000 in cryptocurrency once and get $ 70,000 passive income per month: http://v.ht/Kto1ZpC', 'waiting-confirmation', '2019-06-14 18:53:41', '2019-06-14 18:53:41'),
(207, 'Charlesmub', 'hohoho_hjhjhj_hehehe@yahoo.com', '85575946413', 'Invest $ 15,000 in Bitcoin once and get $ 70,000 passive income per month', 'How to invest in bitcoins $ 15000 - get a return of up to 2000%: https://is.gd/z7yE2M', 'waiting-confirmation', '2019-06-14 19:58:58', '2019-06-14 19:58:58'),
(208, 'HollisSlari', 'castworks@hotmail.com', '88921468774', 'Invest $ 15,000 in Bitcoin mining once and get $ 70,000 passive income per month', 'Invest $ 15,000 in Bitcoin once and get $ 70,000 passive income per month: https://is.gd/mabduF', 'waiting-confirmation', '2019-06-15 22:59:54', '2019-06-15 22:59:54'),
(209, 'ContactForm', 'raphaeAftesteZerT@gmail.com', '375627528', 'A new method of email distribution.', 'Hi!  oriscake.com \r\n \r\nWe make offer for you \r\n \r\nSending your commercial offer through the feedback form which can be found on the sites in the Communication section. Feedback forms are filled in by our software and the captcha is solved. The advantage of this method is that messages sent through feedback forms are whitelisted. This technique improve the probability that your message will be read. \r\n \r\nOur database contains more than 25 million sites around the world to which we can send your message. \r\n \r\nThe cost of one million messages 49 USD \r\n \r\nFREE TEST mailing of 50,000 messages to any country of your choice. \r\n \r\n \r\nThis message is automatically generated to use our contacts for communication. \r\n \r\n \r\n \r\nContact us. \r\nTelegram - @FeedbackFormEU \r\nSkype  FeedbackForm2019 \r\nEmail - FeedbackForm@make-success.com \r\nWhatsApp - +44 7598 509161', 'waiting-confirmation', '2019-06-16 02:55:18', '2019-06-16 02:55:18'),
(210, 'oriscake.com', 'micgyhaelKnori@gmail.com', '254611127', 'There is an importantbonus for victory. oriscake.com', 'Behold is  an important  offers for winning. oriscake.com \r\nhttp://bit.ly/2KA6iW3', 'waiting-confirmation', '2019-06-16 14:13:10', '2019-06-16 14:13:10'),
(211, 'Marvinlix', 'chuang16@hotmail.com', '88972534447', 'Creating Better Days Kratom Powder Uplift', 'Guru Kratom Powder Gold Maeng Da: http://goto.iamaws.com/qwe93700', 'waiting-confirmation', '2019-06-16 17:21:33', '2019-06-16 17:21:33'),
(212, 'ScottSwarl', 'rodriguez_evelyn@hotmail.com', '82857245874', 'Help now Jennifer Lopez and she will help you in an hour', 'Hey. I\'m Jennifer Lopez, I\'m sorry to bother you. \r\nIs no time to explain what is happening, but I urgently need $ 750 in Bitcoins. \r\nI have money but they are on Paypal’s wallet and I need it in Bitcoins for an hour, urgently. \r\nI offer you an exchange, you are now throwing me $ 750 on a bitcoin wallet 1CUy8HNo6vebvyGaBQuEdDtv5LSkibKGE3 and I will give you a $ 8,000 PayPal transfer as soon as I receive your transfer. \r\nAs your money comes, I will immediately email you to clarify your details. \r\nI am ready to pay $ 8000 for your service without a hitch immediately after your $ 750 goes into my Bitcoin wallet. \r\nJust please send money during this hour. My bitcoin wallet 1CUy8HNo6vebvyGaBQuEdDtv5LSkibKGE3 I need $ 750.', 'waiting-confirmation', '2019-06-16 18:00:25', '2019-06-16 18:00:25'),
(213, 'Alfredlaupe', 'ray3253@msn.com', '88658712965', 'Help now Jennifer Lopez and she will help you in an hour', 'Hey. I\'m Jennifer Lopez, I\'m sorry to bother you. \r\nIs no time to explain what is happening, but I urgently need $ 750 in Bitcoins. \r\nI have money but they are on Paypal’s wallet and I need it in Bitcoins for an hour, urgently. \r\nI offer you an exchange, you are now throwing me $ 750 on a bitcoin wallet 1CUy8HNo6vebvyGaBQuEdDtv5LSkibKGE3 and I will give you a $ 8,000 PayPal transfer as soon as I receive your transfer. \r\nAs your money comes, I will immediately email you to clarify your details. \r\nI am ready to pay $ 8000 for your service without a hitch immediately after your $ 750 goes into my Bitcoin wallet. \r\nJust please send money during this hour. My bitcoin wallet 1CUy8HNo6vebvyGaBQuEdDtv5LSkibKGE3 I need $ 750.', 'waiting-confirmation', '2019-06-16 18:09:40', '2019-06-16 18:09:40'),
(214, 'ScottSwarl', 'elainaloveland@msn.com', '84523879219', 'Help now Jennifer Lopez and she will help you in an hour', 'Hey. I\'m Jennifer Lopez, I\'m sorry to bother you. \r\nIs no time to explain what is happening, but I urgently need $ 750 in Bitcoins. \r\nI have money but they are on Paypal’s wallet and I need it in Bitcoins for an hour, urgently. \r\nI offer you an exchange, you are now throwing me $ 750 on a bitcoin wallet 1CUy8HNo6vebvyGaBQuEdDtv5LSkibKGE3 and I will give you a $ 8,000 PayPal transfer as soon as I receive your transfer. \r\nAs your money comes, I will immediately email you to clarify your details. \r\nI am ready to pay $ 8000 for your service without a hitch immediately after your $ 750 goes into my Bitcoin wallet. \r\nJust please send money during this hour. My bitcoin wallet 1CUy8HNo6vebvyGaBQuEdDtv5LSkibKGE3 I need $ 750.', 'waiting-confirmation', '2019-06-16 23:01:37', '2019-06-16 23:01:37'),
(215, 'Alfredlaupe', 'queen0@msn.com', '87255792579', 'Help now Jennifer Lopez and she will help you in an hour', 'Hey. I\'m Jennifer Lopez, I\'m sorry to bother you. \r\nIs no time to explain what is happening, but I urgently need $ 750 in Bitcoins. \r\nI have money but they are on Paypal’s wallet and I need it in Bitcoins for an hour, urgently. \r\nI offer you an exchange, you are now throwing me $ 750 on a bitcoin wallet 1CUy8HNo6vebvyGaBQuEdDtv5LSkibKGE3 and I will give you a $ 8,000 PayPal transfer as soon as I receive your transfer. \r\nAs your money comes, I will immediately email you to clarify your details. \r\nI am ready to pay $ 8000 for your service without a hitch immediately after your $ 750 goes into my Bitcoin wallet. \r\nJust please send money during this hour. My bitcoin wallet 1CUy8HNo6vebvyGaBQuEdDtv5LSkibKGE3 I need $ 750.', 'waiting-confirmation', '2019-06-17 04:31:04', '2019-06-17 04:31:04'),
(216, 'Marvinlix', 'hermesf@hotmail.com', '82843176472', 'Terahemp TERACAT CBD Oil For Cats', 'Natural Stress Solutions Pure CBD Pet Dropper: http://to.ht/cbd25444', 'waiting-confirmation', '2019-06-17 05:54:02', '2019-06-17 05:54:02'),
(217, 'Richardestaf', 'angelbi45@hotmail.com', '83393478162', 'Sexy girls for the night in your town', 'The best women for sex in your town UK: http://xurl.es/zczh0', 'waiting-confirmation', '2019-06-17 11:19:02', '2019-06-17 11:19:02'),
(218, 'Charlesmub', 'njgroves@gmail.com', '81581265839', 'Beautiful girls for sex in your city USA', 'Beautiful women for sex in your town Canada: http://soo.gd/Ge43TZ', 'waiting-confirmation', '2019-06-17 11:27:55', '2019-06-17 11:27:55'),
(219, 'Jamespat', 'pifgio@gmail.com', '89821721874', 'Sexy girls for the night in your town', 'Beautiful women for sex in your town AU: http://xurl.es/zdm05', 'waiting-confirmation', '2019-06-17 11:27:56', '2019-06-17 11:27:56'),
(220, 'Jamesseido', 'arietaleon.r@gmail.com', '87477713759', 'Beautiful women for sex in your town UK', 'Beautiful women for sex in your town: http://xurl.es/a7cxk', 'waiting-confirmation', '2019-06-17 11:45:33', '2019-06-17 11:45:33'),
(221, 'Jamespat', 'kirstendavis68@yahoo.com', '86365716943', 'The best girls for sex in your town Canada', 'Sexy girls for the night in your town AU: https://tiny.cc/jj1d8y', 'waiting-confirmation', '2019-06-17 16:54:45', '2019-06-17 16:54:45'),
(222, 'Charlesmub', 'cyrille058@hotmail.fr', '86167276173', 'The best women for sex in your town AU', 'Find yourself a girl for the night in your city UK: http://xurl.es/kbax6', 'waiting-confirmation', '2019-06-17 23:13:00', '2019-06-17 23:13:00'),
(223, 'Find yourself a girl for the night in your city http://xurl.es/1wer8', 'cjma333@yahoo.com', '81352646959', 'The best women for sex in your town', 'The best women for sex in your town: http://xurl.es/56dmd', 'waiting-confirmation', '2019-06-18 00:50:19', '2019-06-18 00:50:19'),
(224, 'DylanDwelf', 'suzan.chukcha@gmail.com', '85265161316', 'The best women for sex in your town UK', 'Find yourself a girl for the night in your city: http://xurl.es/qpaku', 'waiting-confirmation', '2019-06-18 01:02:43', '2019-06-18 01:02:43'),
(225, 'WilliamAgego', 'rchelebekov@bk.ru', '87636354892', 'Sexy Girls fur die Nacht in deiner Stadt', 'Schone Madchen fur Sex in deiner Stadt: http://soo.gd/Yp184lX', 'waiting-confirmation', '2019-06-18 01:55:07', '2019-06-18 01:55:07'),
(226, 'Jamesseido', 'marjorie31140@hotmail.fr', '82772746459', 'Sexy girls for the night in your town UK', 'Sexy girls for the night in your town USA: http://xurl.es/m1zwh', 'waiting-confirmation', '2019-06-18 02:47:08', '2019-06-18 02:47:08'),
(227, 'EduardoAppag', 'lagoadanta@sapo.pt', '89313124848', 'Belles Femmes Pour Le Sexe Dans Votre Ville', 'Belles Femmes Pour Le Sexe Dans Votre Ville: http://soo.gd/Hc4Zof', 'waiting-confirmation', '2019-06-18 03:43:45', '2019-06-18 03:43:45'),
(228, 'WilliamAgego', 'orka77@gmx.de', '83351577534', 'Schone Frauen fur Sex in deiner Stadt', 'Sexy Girls fur die Nacht in deiner Stadt: http://xurl.es/i44fg', 'waiting-confirmation', '2019-06-18 07:39:49', '2019-06-18 07:39:49'),
(229, 'DylanDwelf', 'jk-667142@versanet.de', '82798462857', 'Find yourself a girl for the night in your city', 'The best girls for sex in your town USA: http://xurl.es/f7w3t', 'waiting-confirmation', '2019-06-18 13:22:11', '2019-06-18 13:22:11'),
(230, 'Beautiful girls for sex in your city http://xurl.es/boud1', 'dmsd30@hotmail.com', '86879234899', 'The best girls for sex in your town', 'Beautiful girls for sex in your city: http://xurl.es/xu4sx', 'waiting-confirmation', '2019-06-18 15:20:36', '2019-06-18 15:20:36'),
(231, 'EduardoAppag', 'danielakpan@outlook.fr', '87183288529', 'Pictoa Filles Sexy Pour La Nuit Dans Votre Ville Etats-Unis', 'Trouvez-vous une fille pour la nuit dans votre ville: http://xurl.es/56ny8', 'waiting-confirmation', '2019-06-18 16:10:21', '2019-06-18 16:10:21'),
(232, 'WilliamAgego', 'ajb@inter-line.co.uk', '88434625612', 'Schone Frauen fur Sex in deiner Stadt', 'Sexy Girls fur die Nacht in deiner Stadt: http://xurl.es/msxgc', 'waiting-confirmation', '2019-06-19 20:11:10', '2019-06-19 20:11:10'),
(233, 'WilliamAgego', 'dk8vk@darc.de', '82839369255', 'Schone Frauen fur Sex in deiner Stadt', 'Die besten Girls fur Sex in deiner Stadt: http://soo.gd/Zy1Syp', 'waiting-confirmation', '2019-06-20 03:19:35', '2019-06-20 03:19:35'),
(234, 'EduardoAppag', 'syedanwar88@yahoo.com.my', '82791213495', 'Belles Filles Pour Le Sexe Dans Votre Ville', 'Trouvez-vous une fille pour la nuit dans votre ville: http://soo.gd/hDUab', 'waiting-confirmation', '2019-06-20 04:55:29', '2019-06-20 04:55:29'),
(235, 'EduardoAppag', 'fpln.villard@gmail.com', '84633437249', 'Belles Femmes Pour Le Sexe Dans Votre Ville', 'Rencontrez des filles sexy dans votre ville: http://xurl.es/dlntt', 'waiting-confirmation', '2019-06-23 04:31:03', '2019-06-23 04:31:03'),
(236, 'DylanDwelf', 'momolebikerdu62@live.fr', '88995391232', 'Beautiful women for sex in your town', 'Meet sexy girls in your city USA: http://xurl.es/0h5iw', 'waiting-confirmation', '2019-06-23 04:31:45', '2019-06-23 04:31:45'),
(237, 'WilliamAgego', 'huaji51739jihuang@163.com', '88173996883', 'Schone Frauen fur Sex in deiner Stadt', 'Schone Frauen fur Sex in deiner Stadt: http://xurl.es/jsn3q', 'waiting-confirmation', '2019-06-23 04:38:51', '2019-06-23 04:38:51'),
(238, 'WilliamAgego', 'friscicdaniel@gmail.com', '89284437363', 'Die besten Girls fur Sex in deiner Stadt', 'Sexy Girls fur die Nacht in deiner Stadt: http://xurl.es/9e7ul', 'waiting-confirmation', '2019-06-23 10:09:07', '2019-06-23 10:09:07'),
(239, 'EduardoAppag', 'alainevelynegerard@aliceadsl.fr', '88365732472', 'Belles Filles Pour Le Sexe Dans Votre Ville', 'Belles Filles Pour Le Sexe Dans Votre Ville: http://soo.gd/zltxd', 'waiting-confirmation', '2019-06-23 14:43:26', '2019-06-23 14:43:26'),
(240, 'DylanDwelf', 'smartfix@web.de', '82178795277', 'Find yourself a girl for the night in your city', 'Beautiful women for sex in your town: http://xurl.es/4pkts', 'waiting-confirmation', '2019-06-23 16:43:53', '2019-06-23 16:43:53'),
(241, 'DylanDwelf', 'rocherjennifer@orange.fr', '84915331947', 'Invest $ 15,000 in Bitcoin mining once and get $ 70,000 passive income per month', 'UPDATE: Cryptocurrency Investing Strategy - Q2 2019. Receive passive income of $ 70,000 per month: https://is.gd/Vm37vN', 'waiting-confirmation', '2019-06-23 19:38:19', '2019-06-23 19:38:19'),
(242, 'EduardoAppag', 'istersen_tabi@bursaprensi.com', '88264358215', 'Comment investir en bitcoins en 2019 et recevoir un revenu passif de 7 000 USD par mois', 'Investissez 5 000 dollars une fois dans les mines de bitcoins et gagnez 7 000 dollars de revenus passifs par mois: http://cutt.us/xfhipE5', 'waiting-confirmation', '2019-06-23 19:42:09', '2019-06-23 19:42:09'),
(243, 'WilliamAgego', 'nadezhda_yarovenko@mail.ru', '86257472666', 'Wie man in bitcoins $ 5000 investiert - erzielt eine Rendite von bis zu 2000%', 'Investieren Sie einmalig 5.000 USD in den Bitcoin-Abbau und erzielen Sie ein passives Einkommen von 7.000 USD pro Monat: http://v.ht/qJJxUH', 'waiting-confirmation', '2019-06-23 19:45:41', '2019-06-23 19:45:41'),
(244, 'WilliamAgego', 's.reber@iname.com', '88717282721', 'UPDATE: Cryptocurrency-Investing-Strategie - 2. Quartal 2019. Erhalten Sie ein passives Einkommen von 7.000 USD pro Monat', 'Wenn Sie im Jahr 2011 1.000 USD in Bitcoin investiert haben, haben Sie jetzt 4 Millionen USD: http://cutt.us/EnbH2M', 'waiting-confirmation', '2019-06-24 01:11:53', '2019-06-24 01:11:53'),
(245, 'EduardoAppag', 'no7wallstreet@hotmail.com', '84526479529', 'Investissez 5 000 $ une fois dans la crypto-monnaie et gagnez 7 000 $ de revenu passif par mois', 'Comment investir en bitcoins en 2019 et recevoir un revenu passif de 7 000 USD par mois: https://is.gd/JyJxGo', 'waiting-confirmation', '2019-06-24 08:52:21', '2019-06-24 08:52:21'),
(246, 'DylanDwelf', 'remue2000@yahoo.de', '87488225497', 'Invest $ 15,000 in Bitcoin mining once and get $ 70,000 passive income per month', 'Invest $ 15,000 in Bitcoin once and get $ 70,000 passive income per month: http://cutt.us/Mza3fk', 'waiting-confirmation', '2019-06-24 10:46:23', '2019-06-24 10:46:23'),
(247, 'Lowellimapy', 'zthuringer@hotmail.com', '87866847637', 'Investieren Sie einmalig 5.000 USD in Bitcoin und erhalten Sie ein passives Einkommen von 7.000 USD pro Monat', 'Wie konnen Sie 2019 in Bitcoins investieren und ein passives Einkommen von 7.000 USD pro Monat erzielen?: https://is.gd/9nuqLh', 'waiting-confirmation', '2019-06-24 19:27:36', '2019-06-24 19:27:36'),
(248, 'Alfredlaupe', 'bigjohnnyj@hotmail.com', '85354165877', 'Investissez 5 000 dollars une fois dans les mines de bitcoins et gagnez 7 000 dollars de revenus passifs par mois', 'Investissez une fois dans la crypto-monnaie miniГЁre Г  hauteur de 5 000 dollars et obtenez un revenu passif de 7 000 dollars par mois: https://is.gd/aXk25O', 'waiting-confirmation', '2019-06-24 19:38:49', '2019-06-24 19:38:49'),
(249, 'Lowellimapy', 'belch_k@hotmail.com', '88389672659', 'Wie konnen Sie 2019 in Bitcoins investieren und ein passives Einkommen von 7.000 USD pro Monat erzielen?', 'Wenn Sie im Jahr 2011 1.000 USD in Bitcoin investiert haben, haben Sie jetzt 4 Millionen USD: http://v.ht/qL6Qn4l', 'waiting-confirmation', '2019-06-25 02:38:25', '2019-06-25 02:38:25'),
(250, 'Alfredlaupe', 'tanyalao@hotmail.com', '81377735892', 'Comment investir en bitcoins en 2019 et recevoir un revenu passif de 7 000 USD par mois', 'Comment investir en bitcoins en 2019 et recevoir un revenu passif de 7 000 USD par mois: http://cutt.us/bpy1Tfwt2', 'waiting-confirmation', '2019-06-25 10:21:28', '2019-06-25 10:21:28'),
(251, 'Lowellimapy', 'sweetpee24@hotmail.com', '88828686261', 'Cannabis investoi Lontooseen', 'Invest cannabis UK: https://hec.su/hXME', 'waiting-confirmation', '2019-06-26 07:30:11', '2019-06-26 07:30:11'),
(252, 'Alfredlaupe', 'mirianaz@msn.com', '87275471358', 'DE WETTELIJKE VERDELING VAN CANNABIS OVER NOORD-AMERIKA BIEDT DE GELDMOGELIJKHEID WAAROP WIJ ALLEMAAL WACHTEN', 'Investeer in legale cannabisbedrijven: https://hec.su/hOCt', 'waiting-confirmation', '2019-06-26 07:30:31', '2019-06-26 07:30:31'),
(253, 'DylanDwelf', 'deepak_raj_bahadur@yahoo.com.au', '83169436673', 'Investieren Sie in Cannabis NZ', 'Sollten Sie in Marihuana-Aktien investieren - BEST Cannabis Stocks: https://hideuri.com/KmR4em', 'waiting-confirmation', '2019-06-26 07:30:47', '2019-06-26 07:30:47'),
(254, 'Lowellimapy', 'droow@hotmail.com', '85933662953', 'Pitaisiko investoida Marihuanan osakkeisiin - BEST Cannabis Stocks', 'Investoi kannabista Kaliforniassa: https://hec.su/hRxx', 'waiting-confirmation', '2019-06-26 13:26:18', '2019-06-26 13:26:18'),
(255, 'Alfredlaupe', 'ssbbg@hotmail.com', '88873694429', 'Hoe te beleggen in de cannabisindustrie', 'Marihuanavoorraden en andere manieren om in cannabis te investeren: http://v.ht/82nv5bU', 'waiting-confirmation', '2019-06-26 19:42:31', '2019-06-26 19:42:31'),
(256, 'DylanDwelf', 'hschweichert-siewing@web.de', '84184915457', 'Investiere Cannabis Australien', 'Investiere Cannabis Australien: https://chogoon.com/srt/hdh66', 'waiting-confirmation', '2019-06-26 21:52:37', '2019-06-26 21:52:37'),
(257, 'Lowellimapy', 'bren930@hotmail.com', '83985337375', 'Miten sijoittaa Marihuanaan 5 000 dollaria Varastot 2019', 'Kannabiksen sijoittaminen - 3 parasta marihuanan varastoa 2019: http://v.ht/mEw7IEc', 'waiting-confirmation', '2019-06-27 22:43:50', '2019-06-27 22:43:50'),
(258, 'Anthonydop', 'gulfsrv94@gmail.com', '267128458', 'Process for Benefit', 'Hello!, oriscake.com \r\n \r\nOur customer want to venture in your area for good benefit. \r\n \r\nPlease contact us for more information on  +973 650 09688 or mh@indobsc.com \r\n \r\nBest regards \r\nMr. Mat Hernandez', 'waiting-confirmation', '2019-06-28 10:21:44', '2019-06-28 10:21:44'),
(259, 'Lowellimapy', 'bhikku@hotmail.com', '86987113169', 'Pitaisiko investoida Marihuanan osakkeisiin - BEST Cannabis Stocks', 'Miten investoida Kanadan oikeudelliseen kannabis-teollisuuteen: https://chogoon.com/srt/syv1l', 'waiting-confirmation', '2019-06-28 12:53:08', '2019-06-28 12:53:08'),
(260, 'DylanDwelf', 'dachveredelung-nord@gmx.de', '87971747756', 'Investieren Sie in Cannabisaktien ab 5.000 USD und verdienen Sie 1.350.000 USD pro Jahr', 'Wie man in die Cannabisindustrie investiert: https://hec.su/hWjQ', 'waiting-confirmation', '2019-06-28 18:44:49', '2019-06-28 18:44:49'),
(261, 'oriscake.com', 'micgyhaelKnori@gmail.com', '217448616', 'Suit note a appropriateoffering towards win. oriscake.com', 'Here is  charming  loot prize for your team. oriscake.com \r\nhttp://bit.ly/2KyusA9', 'waiting-confirmation', '2019-06-28 23:16:09', '2019-06-28 23:16:09'),
(262, 'Alfredlaupe', 'esdrat@msn.com', '84777376367', 'DE WETTELIJKE VERDELING VAN CANNABIS OVER NOORD-AMERIKA BIEDT DE GELDMOGELIJKHEID WAAROP WIJ ALLEMAAL WACHTEN', 'Investeer cannabis in het VK: http://go-4.net/eyXl', 'waiting-confirmation', '2019-06-29 09:33:31', '2019-06-29 09:33:31'),
(263, 'DylanDwelf', 'melany16450@hotmail.fr', '82328486717', 'So investieren Sie am besten 5.000 US-Dollar in die Marihuana-Industrie', 'Investieren Sie in Cannabisaktien ab 5.000 USD und verdienen Sie 1.350.000 USD pro Jahr: https://hec.su/hZL2', 'waiting-confirmation', '2019-06-29 11:25:15', '2019-06-29 11:25:15'),
(264, 'Lowellimapy', 'josefob@hotmail.com', '86154433951', 'Young girls are looking for sex in your city', 'Beautiful girls are looking for sex in your city: https://hec.su/juGE', 'waiting-confirmation', '2019-06-29 12:48:02', '2019-06-29 12:48:02'),
(265, 'Alfredlaupe', 'zumach@hotmail.com', '87359553714', 'The best girls for sex in your town', 'Sexy girls in your city are looking for dating: http://v.ht/vrL00d8', 'waiting-confirmation', '2019-06-29 12:54:14', '2019-06-29 12:54:14'),
(266, 'Lowellimapy', 'bradleytalbot@hotmail.com', '89693599392', 'Sexy women in your city are looking for dating', 'Beautiful women are looking for sex in your city: http://v.ht/TND9g2', 'waiting-confirmation', '2019-06-29 18:20:58', '2019-06-29 18:20:58'),
(267, 'DylanDwelf', 'unaxaspizua@hotmail.es', '84174633586', 'The best women for sex in your town', 'Beautiful women for sex in your town: https://hideuri.com/K3Y2k0', 'waiting-confirmation', '2019-06-29 18:56:53', '2019-06-29 18:56:53'),
(268, 'Alfredlaupe', 'cool2102@msn.com', '88938634429', 'Meet a beautiful girl for sex right now', 'Beautiful girls are looking for sex in your city: http://v.ht/zohhp', 'waiting-confirmation', '2019-06-30 01:46:04', '2019-06-30 01:46:04'),
(269, 'DylanDwelf', 'trelorsabbadin@yahoo.ca', '81628622435', 'Single girls want sex in your city', 'Sexy girls in your city are looking for dating: http://v.ht/q32h6O', 'waiting-confirmation', '2019-06-30 13:33:37', '2019-06-30 13:33:37'),
(270, 'ContactForm', 'raphaeAftesteZerT@gmail.com', '375627528', 'A new method of email distribution.', 'Ciao!  oriscake.com \r\n \r\nWe present oneself \r\n \r\nSending your message through the Contact us form which can be found on the sites in the contact partition. Contact form are filled in by our software and the captcha is solved. The profit of this method is that messages sent through feedback forms are whitelisted. This method increases the chances that your message will be read. \r\n \r\nOur database contains more than 25 million sites around the world to which we can send your message. \r\n \r\nThe cost of one million messages 49 USD \r\n \r\nFREE TEST mailing of 50,000 messages to any country of your choice. \r\n \r\n \r\nThis message is automatically generated to use our contacts for communication. \r\n \r\n \r\n \r\nContact us. \r\nTelegram - @FeedbackFormEU \r\nSkype  FeedbackForm2019 \r\nWhatsApp - +44 7598 509161 \r\nEmail - FeedbackForm@make-success.com', 'waiting-confirmation', '2019-06-30 18:33:23', '2019-06-30 18:33:23'),
(271, 'Lowellimapy', 'brown_36@hotmail.com', '84287822512', 'Sexy women in your city are looking for dating', 'Meet a beautiful girl for sex right now: https://hideuri.com/qJJM1j', 'waiting-confirmation', '2019-07-01 07:06:04', '2019-07-01 07:06:04'),
(272, 'Lowellimapy', 'bdharper@hotmail.com', '89382473731', 'Sexy girls in your city are looking for dating', 'Single women want sex in your city: https://hideuri.com/xprvZl', 'waiting-confirmation', '2019-07-01 14:16:15', '2019-07-01 14:16:15'),
(273, 'Alfredlaupe', 'aplpucker@hotmail.com', '82974277778', 'Sexy girls in your city are looking for dating', 'Beautiful women for sex in your town: http://v.ht/zOhi66v', 'waiting-confirmation', '2019-07-01 17:42:45', '2019-07-01 17:42:45'),
(274, 'Alfredlaupe', 'cfdfireman@hotmail.com', '87266529926', 'Young girls want sex in your city', 'Meet a sexy girl right now: https://hideuri.com/KgAVL3', 'waiting-confirmation', '2019-07-02 06:13:09', '2019-07-02 06:13:09'),
(275, 'DylanDwelf', 'dani-muth@web.de', '83788277736', 'Young girls want sex in your city', 'The best women for sex in your town: http://v.ht/HVFPBX', 'waiting-confirmation', '2019-07-02 10:33:33', '2019-07-02 10:33:33'),
(276, 'DylanDwelf', 'nussi.petra@yahoo.de', '85687329634', 'Meet a sexy girl right now', 'Sexy girls for the night in your town: https://hec.su/juUm', 'waiting-confirmation', '2019-07-03 01:34:43', '2019-07-03 01:34:43'),
(277, 'Jamespat', 'nicholefletcher77@yahoo.com', '83721385829', 'Young girls want sex in your city', 'Women are looking for sex in your city: https://chogoon.com/srt/f6gtm', 'waiting-confirmation', '2019-07-03 20:02:46', '2019-07-03 20:02:46'),
(278, 'Jamesseido', 'mpinjari500@gmail.com', '86154112833', 'Sexy girls in your city are looking for dating', 'Single girls want sex in your city: http://v.ht/OYVh4HQ', 'waiting-confirmation', '2019-07-03 20:02:46', '2019-07-03 20:02:46'),
(279, 'Marvinlix', 'arkboy1@msn.com', '84252712492', 'Meet a sexy woman right now', 'Sexy girls in your city are looking for dating: http://v.ht/vmB1E', 'waiting-confirmation', '2019-07-03 20:35:34', '2019-07-03 20:35:34'),
(280, 'Jamespat', 'froo2009@hotmail.co.ch', '84843991962', 'Single girls want sex in your city', 'Meet sexy girls in your city: http://v.ht/bQbJez', 'waiting-confirmation', '2019-07-04 01:37:22', '2019-07-04 01:37:22'),
(281, 'Jamesseido', 'kamranwarsi@gmail.com', '85178224257', 'The best girls for sex in your town', 'Beautiful girls for sex in your city: https://chogoon.com/srt/xea12', 'waiting-confirmation', '2019-07-04 07:22:48', '2019-07-04 07:22:48'),
(282, 'Marvinlix', 'yurigf@hotmail.com', '84955288496', 'Meet a sexy woman right now', 'Sexy girls for the night in your town: https://hideuri.com/a8vNR7', 'waiting-confirmation', '2019-07-04 12:33:12', '2019-07-04 12:33:12');

-- --------------------------------------------------------

--
-- Table structure for table `emails`
--

CREATE TABLE `emails` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `receipt` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `emails`
--

INSERT INTO `emails` (`id`, `name`, `email`, `receipt`, `created_at`, `updated_at`) VALUES
(1, 'Mariasa Gusti', 'gstmariasa@gmail.com', '1', '2017-08-14 20:21:39', '2017-08-15 01:46:12');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `with_link` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `conten` text COLLATE utf8mb4_unicode_ci,
  `article_id` int(10) UNSIGNED NOT NULL,
  `lang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'id',
  `equal_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `name`, `image`, `link`, `with_link`, `type`, `conten`, `article_id`, `lang`, `equal_id`, `created_at`, `updated_at`) VALUES
(1, 'Slider 1', 'slider-1-oriscakezMB.jpeg', NULL, '0', 'home-slider', '<h1>Oriscake Title Lorem ipsum dolor.</h1>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur, consequatur.</p>', 2, '', '', '2018-07-09 20:39:57', '2018-10-01 21:25:16'),
(3, 'testimoni pelanggan', 'testimoni-pelanggan-oriscakeLMz.png', NULL, '0', 'gallery-testimoni', 'testimoni baik pelanggan', 5, 'id', 'IMG5bd1888c744b40.33061394', '2018-10-25 16:10:37', '2018-10-25 16:13:15');

-- --------------------------------------------------------

--
-- Table structure for table `images_category`
--

CREATE TABLE `images_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `image_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sociallinks`
--

CREATE TABLE `sociallinks` (
  `id` int(10) UNSIGNED NOT NULL,
  `platform` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `published` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sociallinks`
--

INSERT INTO `sociallinks` (`id`, `platform`, `link`, `published`, `created_at`, `updated_at`) VALUES
(1, 'twitter', 'https://twitter.com', '1', '2017-08-14 20:21:39', '2018-06-13 18:34:46'),
(3, 'instagram', 'https://www.instagram.com/oris_cake/?hl=id', '1', '2017-08-14 20:21:39', '2019-01-14 11:49:25'),
(5, 'facebook', 'https://www.facebook.com/oriscakehomeindustry/', '1', '2019-01-14 11:47:44', '2019-01-14 11:47:44');

-- --------------------------------------------------------

--
-- Table structure for table `tb_alamat`
--

CREATE TABLE `tb_alamat` (
  `id_alamat` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED DEFAULT NULL,
  `id_wilayah` int(11) DEFAULT NULL,
  `sebutan_alamat` varchar(225) DEFAULT NULL,
  `nama_alamat` varchar(225) DEFAULT NULL,
  `lat` varchar(225) DEFAULT NULL,
  `long` varchar(225) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_biaya_delivery`
--

CREATE TABLE `tb_biaya_delivery` (
  `id_biaya_delivery` int(10) UNSIGNED NOT NULL,
  `id_wilayah` int(11) DEFAULT NULL,
  `biaya` bigint(20) DEFAULT NULL,
  `crated_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_cake_image`
--

CREATE TABLE `tb_cake_image` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_cake` int(10) UNSIGNED DEFAULT NULL,
  `file_image` varchar(225) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_cake_image`
--

INSERT INTO `tb_cake_image` (`id`, `id_cake`, `file_image`, `created_at`, `updated_at`) VALUES
(72, 38, 'little-pony-1-oriscakeLz8.jpeg', '2019-01-15 05:41:30', '2019-01-15 05:41:30'),
(74, 41, 'little-pony-4-oriscakeBAK.jpeg', '2019-01-15 05:42:38', '2019-01-15 05:42:38'),
(75, 42, 'little-pony-5-oriscakek2Q.jpeg', '2019-01-15 05:43:10', '2019-01-15 05:43:10'),
(76, 43, 'little-pony-6-oriscakeOi1.jpeg', '2019-01-15 05:43:22', '2019-01-15 05:43:22'),
(77, 44, 'little-pony-7-oriscakejL5.jpeg', '2019-01-15 05:43:46', '2019-01-15 05:43:46'),
(79, 46, 'little-pony-9-oriscake2Aa.jpeg', '2019-01-15 05:44:16', '2019-01-15 05:44:16'),
(80, 47, 'little-pony-10-oriscakelIb.jpeg', '2019-01-15 05:44:28', '2019-01-15 05:44:28'),
(81, 48, 'little-pony-11-oriscake71q.jpeg', '2019-01-15 05:44:54', '2019-01-15 05:44:54'),
(82, 49, 'little-pony-13-oriscakeY34.jpeg', '2019-01-15 05:45:00', '2019-01-15 05:45:00'),
(83, 50, 'little-pony-13-oriscakeFVf.jpeg', '2019-01-15 05:45:17', '2019-01-15 05:45:17'),
(85, 39, 'little-pony-3-oriscakeHYt.jpeg', '2019-01-15 05:48:13', '2019-01-15 05:48:13'),
(86, 52, 'mcqueen-theme-1-oriscake9o4.jpeg', '2019-01-15 05:55:16', '2019-01-15 05:55:16'),
(87, 55, 'mcqueen-theme-2-oriscakeueU.jpeg', '2019-01-15 05:55:22', '2019-01-15 05:55:22'),
(88, 57, 'mcqueen-theme-3-oriscaketu4.jpeg', '2019-01-15 05:55:37', '2019-01-15 05:55:37'),
(89, 60, 'mcqueen-theme-4-oriscakevYV.jpeg', '2019-01-15 05:55:58', '2019-01-15 05:55:58'),
(90, 62, 'mcqueen-theme-5-oriscake3kJ.jpeg', '2019-01-15 05:56:05', '2019-01-15 05:56:05'),
(91, 64, 'mcqueen-theme-6-oriscakeCch.jpeg', '2019-01-15 05:56:31', '2019-01-15 05:56:31'),
(92, 66, 'mcqueen-theme-7-oriscakeoRI.jpeg', '2019-01-15 05:56:37', '2019-01-15 05:56:37'),
(93, 69, 'mcqueen-theme-8-oriscakeHk9.jpeg', '2019-01-15 05:56:58', '2019-01-15 05:56:58'),
(94, 71, 'mcqueen-theme-9-oriscakeS7X.jpeg', '2019-01-15 05:57:21', '2019-01-15 05:57:21'),
(95, 73, 'mcqueen-theme-10-oriscakeZFo.jpeg', '2019-01-15 05:57:27', '2019-01-15 05:57:27'),
(96, 76, 'mcqueen-theme-11-oriscakeOm2.jpeg', '2019-01-15 05:57:33', '2019-01-15 05:57:33'),
(98, 53, 'animal-theme-12-oriscakespp.jpeg', '2019-01-15 06:05:06', '2019-01-15 06:05:06'),
(99, 54, 'animal-theme-13-oriscakeMv5.jpeg', '2019-01-15 06:05:25', '2019-01-15 06:05:25'),
(100, 58, 'animal-theme-14-oriscakeQM3.jpeg', '2019-01-15 06:05:39', '2019-01-15 06:05:39'),
(101, 59, 'animal-theme-15-oriscakehji.jpeg', '2019-01-15 06:05:54', '2019-01-15 06:05:54'),
(102, 61, 'animal-theme-16-oriscakeEKE.jpeg', '2019-01-15 06:06:11', '2019-01-15 06:06:11'),
(103, 63, 'animal-theme-17-oriscakeofV.jpeg', '2019-01-15 06:06:36', '2019-01-15 06:06:36'),
(104, 65, 'animal-theme-18-oriscakemWu.jpeg', '2019-01-15 06:06:52', '2019-01-15 06:06:52'),
(105, 87, 'minnie-mouse-theme-1-oriscakeFRm.jpeg', '2019-01-15 06:07:02', '2019-01-15 06:07:02'),
(106, 67, 'animal-theme-19-oriscakeoTC.jpeg', '2019-01-15 06:07:06', '2019-01-15 06:07:06'),
(107, 89, 'minnie-mouse-theme-2-oriscakemQq.jpeg', '2019-01-15 06:07:08', '2019-01-15 06:07:08'),
(108, 94, 'minnie-mouse-theme-3-oriscakeD0J.jpeg', '2019-01-15 06:07:11', '2019-01-15 06:07:11'),
(109, 96, 'minnie-mouse-theme-4-oriscakebgF.jpeg', '2019-01-15 06:07:17', '2019-01-15 06:07:17'),
(110, 97, 'minnie-mouse-theme-5-oriscakefOA.jpeg', '2019-01-15 06:07:23', '2019-01-15 06:07:23'),
(111, 68, 'animal-theme-20-oriscakeCpd.jpeg', '2019-01-15 06:07:27', '2019-01-15 06:07:27'),
(113, 98, 'minnie-mouse-theme-6-oriscaker7l.jpeg', '2019-01-15 06:07:31', '2019-01-15 06:07:31'),
(114, 99, 'minnie-mouse-theme-7-oriscakeMJz.jpeg', '2019-01-15 06:07:37', '2019-01-15 06:07:37'),
(115, 100, 'minnie-mouse-theme-8-oriscakeEEr.jpeg', '2019-01-15 06:07:42', '2019-01-15 06:07:42'),
(116, 70, 'animal-theme-21-oriscakebNg.jpeg', '2019-01-15 06:07:44', '2019-01-15 06:07:44'),
(117, 101, 'minnie-mouse-theme-9-oriscakeC82.jpeg', '2019-01-15 06:07:48', '2019-01-15 06:07:48'),
(118, 102, 'minnie-mouse-theme-10-oriscakeM3b.jpeg', '2019-01-15 06:07:53', '2019-01-15 06:07:53'),
(119, 72, 'animal-theme-22-oriscakeREO.jpeg', '2019-01-15 06:08:00', '2019-01-15 06:08:00'),
(120, 74, 'animal-theme-23-oriscakesAC.jpeg', '2019-01-15 06:08:17', '2019-01-15 06:08:17'),
(121, 75, 'animal-theme-24-oriscakeoH5.jpeg', '2019-01-15 06:08:33', '2019-01-15 06:08:33'),
(122, 77, 'animal-theme-25-oriscake151.jpeg', '2019-01-15 06:08:49', '2019-01-15 06:08:49'),
(123, 78, 'animal-theme-26-oriscakejJY.jpeg', '2019-01-15 06:09:11', '2019-01-15 06:09:11'),
(124, 79, 'animal-theme-27-oriscakecAv.jpeg', '2019-01-15 06:09:31', '2019-01-15 06:09:31'),
(125, 80, 'animal-theme-28-oriscakeiZo.jpeg', '2019-01-15 06:09:50', '2019-01-15 06:09:50'),
(126, 81, 'animal-theme-29-oriscakenBm.jpeg', '2019-01-15 06:10:17', '2019-01-15 06:10:17'),
(127, 82, 'animal-theme-30-oriscakebZx.jpeg', '2019-01-15 06:10:36', '2019-01-15 06:10:36'),
(128, 103, 'mix-fruit-theme-1-oriscakes1o.jpeg', '2019-01-15 06:10:42', '2019-01-15 06:10:42'),
(129, 104, 'mix-fruit-theme-2-oriscakeWB2.jpeg', '2019-01-15 06:10:47', '2019-01-15 06:10:47'),
(130, 105, 'mix-fruit-theme-3-oriscake2sT.jpeg', '2019-01-15 06:10:51', '2019-01-15 06:10:51'),
(131, 83, 'animal-theme-31-oriscakeYuy.jpeg', '2019-01-15 06:10:55', '2019-01-15 06:10:55'),
(132, 84, 'animal-theme-32-oriscakevnE.jpeg', '2019-01-15 06:11:10', '2019-01-15 06:11:10'),
(133, 85, 'animal-theme-33-oriscakejRd.jpeg', '2019-01-15 06:11:24', '2019-01-15 06:11:24'),
(134, 86, 'animal-theme-34-oriscakeBj5.jpeg', '2019-01-15 06:11:39', '2019-01-15 06:11:39'),
(135, 88, 'animal-theme-35-oriscakeLqZ.jpeg', '2019-01-15 06:11:57', '2019-01-15 06:11:57'),
(136, 90, 'animal-theme-36-oriscakemjq.jpeg', '2019-01-15 06:12:20', '2019-01-15 06:12:20'),
(137, 91, 'animal-theme-37-oriscakebUK.jpeg', '2019-01-15 06:12:38', '2019-01-15 06:12:38'),
(138, 92, 'animal-theme-38-oriscake9s6.jpeg', '2019-01-15 06:12:55', '2019-01-15 06:12:55'),
(139, 93, 'animal-theme-39-oriscakegJR.jpeg', '2019-01-15 06:13:16', '2019-01-15 06:13:16'),
(140, 95, 'animal-theme-40-oriscakemfQ.jpeg', '2019-01-15 06:13:32', '2019-01-15 06:13:32'),
(141, 106, 'baby-shark-theme-1-oriscakep4e.jpeg', '2019-01-16 02:51:02', '2019-01-16 02:51:02'),
(142, 107, 'baby-shark-theme-2-oriscakevow.jpeg', '2019-01-16 02:51:17', '2019-01-16 02:51:17'),
(143, 108, 'baby-shark-theme-3-oriscakecMa.jpeg', '2019-01-16 02:51:28', '2019-01-16 02:51:28'),
(144, 109, 'baby-shark-theme-4-oriscakeAoJ.jpeg', '2019-01-16 02:51:44', '2019-01-16 02:51:44'),
(145, 110, 'baby-shark-theme-5-oriscake18Z.jpeg', '2019-01-16 02:51:57', '2019-01-16 02:51:57'),
(146, 111, 'baby-shark-theme-6-oriscake1UY.jpeg', '2019-01-16 02:52:46', '2019-01-16 02:52:46'),
(147, 112, 'baby-shark-theme-7-oriscakenVn.jpeg', '2019-01-16 02:53:08', '2019-01-16 02:53:08'),
(148, 113, 'baby-shark-theme-8-oriscakeoY2.jpeg', '2019-01-16 02:53:24', '2019-01-16 02:53:24'),
(149, 114, 'baby-shark-theme-9-oriscakeNuF.jpeg', '2019-01-16 02:53:35', '2019-01-16 02:53:35'),
(150, 115, 'baby-shark-theme-10-oriscakeC36.jpeg', '2019-01-16 02:53:53', '2019-01-16 02:53:53'),
(151, 116, 'baby-shark-theme-11-oriscakekdV.jpeg', '2019-01-16 02:54:34', '2019-01-16 02:54:34'),
(152, 117, 'baby-shark-theme-12-oriscaket0l.jpeg', '2019-01-16 02:54:56', '2019-01-16 02:54:56'),
(153, 118, 'baby-shark-theme-13-oriscakeZmB.jpeg', '2019-01-16 02:55:11', '2019-01-16 02:55:11'),
(154, 119, 'baby-shark-theme-14-oriscakeGsS.jpeg', '2019-01-16 02:55:28', '2019-01-16 02:55:28'),
(155, 120, 'barbie-theme-1-oriscakeapl.jpeg', '2019-01-16 02:57:57', '2019-01-16 02:57:57'),
(156, 121, 'barbie-theme-2-oriscakeUXC.jpeg', '2019-01-16 02:58:10', '2019-01-16 02:58:10'),
(157, 122, 'batman-theme-1-oriscakenxA.jpeg', '2019-01-16 03:04:49', '2019-01-16 03:04:49'),
(158, 123, 'batman-theme-2-oriscakecje.jpeg', '2019-01-16 03:04:59', '2019-01-16 03:04:59'),
(159, 124, 'batman-theme-3-oriscakeaDl.jpeg', '2019-01-16 03:05:37', '2019-01-16 03:05:37'),
(160, 125, 'batman-theme-4-oriscakeyW0.jpeg', '2019-01-16 03:05:49', '2019-01-16 03:05:49'),
(161, 126, 'batman-theme-5-oriscakepHk.jpeg', '2019-01-16 03:06:12', '2019-01-16 03:06:12'),
(162, 127, 'boobs-theme-1-oriscake1Dq.jpeg', '2019-01-16 03:09:44', '2019-01-16 03:09:44'),
(163, 128, 'boobs-theme-2-oriscakeY2E.jpeg', '2019-01-16 03:09:54', '2019-01-16 03:09:54'),
(164, 129, 'boobs-theme-3-oriscakeVDB.jpeg', '2019-01-16 03:10:11', '2019-01-16 03:10:11'),
(165, 131, 'boobs-theme-4-oriscakeuiQ.jpeg', '2019-01-16 03:10:25', '2019-01-16 03:10:25'),
(166, 132, 'boobs-theme-5-oriscakeKGU.jpeg', '2019-01-16 03:10:42', '2019-01-16 03:10:42'),
(167, 130, 'name-cake-theme-1-oriscakespO.jpeg', '2019-01-16 03:32:54', '2019-01-16 03:32:54'),
(168, 133, 'name-cake-theme-2-oriscakeuAJ.jpeg', '2019-01-16 03:33:00', '2019-01-16 03:33:00'),
(169, 156, 'name-cake-theme-3-oriscake4lh.jpeg', '2019-01-16 03:33:06', '2019-01-16 03:33:06'),
(170, 134, 'name-cake-theme-4-oriscakelJg.jpeg', '2019-01-16 03:33:13', '2019-01-16 03:33:13'),
(171, 135, 'name-cake-theme-5-oriscaker4c.jpeg', '2019-01-16 03:33:24', '2019-01-16 03:33:24'),
(172, 136, 'name-cake-theme-6-oriscakeSTk.jpeg', '2019-01-16 03:34:04', '2019-01-16 03:34:04'),
(173, 137, 'name-cake-theme-7-oriscakeRTv.jpeg', '2019-01-16 03:34:10', '2019-01-16 03:34:10'),
(174, 140, 'name-cake-theme-8-oriscakePRW.jpeg', '2019-01-16 03:34:15', '2019-01-16 03:34:15'),
(175, 139, 'name-cake-theme-9-oriscakeoMp.jpeg', '2019-01-16 03:34:23', '2019-01-16 03:34:23'),
(176, 138, 'name-cake-theme-10-oriscakeZpA.jpeg', '2019-01-16 03:34:27', '2019-01-16 03:34:27'),
(177, 141, 'name-cake-theme-11-oriscake2DH.jpeg', '2019-01-16 03:35:29', '2019-01-16 03:35:29'),
(178, 142, 'name-cake-theme-12-oriscakeKmZ.jpeg', '2019-01-16 03:35:36', '2019-01-16 03:35:36'),
(179, 143, 'name-cake-theme-13-oriscakezSi.jpeg', '2019-01-16 03:35:41', '2019-01-16 03:35:41'),
(180, 144, 'name-cake-theme-14-oriscakeICV.jpeg', '2019-01-16 03:35:47', '2019-01-16 03:35:47'),
(181, 145, 'name-cake-theme-15-oriscakeRRJ.jpeg', '2019-01-16 03:35:53', '2019-01-16 03:35:53'),
(182, 146, 'name-cake-theme-16-oriscakeXbl.jpeg', '2019-01-16 03:36:28', '2019-01-16 03:36:28'),
(183, 147, 'name-cake-theme-17-oriscakelYc.jpeg', '2019-01-16 03:36:31', '2019-01-16 03:36:31'),
(184, 148, 'name-cake-theme-18-oriscakee2m.jpeg', '2019-01-16 03:36:42', '2019-01-16 03:36:42'),
(185, 149, 'name-cake-theme-19-oriscakeiHC.jpeg', '2019-01-16 03:36:44', '2019-01-16 03:36:44'),
(186, 150, 'name-cake-theme-20-oriscakegwJ.jpeg', '2019-01-16 03:36:50', '2019-01-16 03:36:50'),
(187, 151, 'name-cake-theme-21-oriscakeecQ.jpeg', '2019-01-16 03:37:38', '2019-01-16 03:37:38'),
(188, 152, 'name-cake-theme-22-oriscakefBT.jpeg', '2019-01-16 03:37:46', '2019-01-16 03:37:46'),
(189, 153, 'name-cake-theme-23-oriscakeA9i.jpeg', '2019-01-16 03:37:54', '2019-01-16 03:37:54'),
(190, 154, 'name-cake-theme-24-oriscakeJne.jpeg', '2019-01-16 03:38:04', '2019-01-16 03:38:04'),
(191, 155, 'name-cake-theme-25-oriscakeUm1.jpeg', '2019-01-16 03:38:08', '2019-01-16 03:38:08'),
(192, 167, 'bus-tayo-theme-1-oriscakeyvu.jpeg', '2019-01-16 03:53:51', '2019-01-16 03:53:51'),
(193, 169, 'bus-tayo-theme-2-oriscakeYbQ.jpeg', '2019-01-16 03:54:05', '2019-01-16 03:54:05'),
(194, 171, 'bus-tayo-theme-3-oriscake25z.jpeg', '2019-01-16 03:54:15', '2019-01-16 03:54:15'),
(195, 173, 'bus-tayo-theme-4-oriscakeMSA.jpeg', '2019-01-16 03:54:26', '2019-01-16 03:54:26'),
(196, 176, 'bus-tayo-theme-5-oriscakesI4.jpeg', '2019-01-16 03:54:46', '2019-01-16 03:54:46'),
(197, 157, 'number-cake-1-oriscakep9e.jpeg', '2019-01-16 03:54:51', '2019-01-16 03:54:51'),
(198, 158, 'number-cake-2-oriscakeJNa.jpeg', '2019-01-16 03:54:57', '2019-01-16 03:54:57'),
(199, 159, 'number-cake-3-oriscakeD9K.jpeg', '2019-01-16 03:55:02', '2019-01-16 03:55:02'),
(200, 177, 'bus-tayo-theme-6-oriscaketOP.jpeg', '2019-01-16 03:55:04', '2019-01-16 03:55:04'),
(201, 160, 'number-cake-4-oriscake5md.jpeg', '2019-01-16 03:55:07', '2019-01-16 03:55:07'),
(202, 161, 'number-cake-5-oriscake0oe.jpeg', '2019-01-16 03:55:13', '2019-01-16 03:55:13'),
(203, 178, 'bus-tayo-theme-7-oriscake58w.jpeg', '2019-01-16 03:55:20', '2019-01-16 03:55:20'),
(204, 179, 'bus-tayo-theme-8-oriscakelYh.jpeg', '2019-01-16 03:55:33', '2019-01-16 03:55:33'),
(205, 182, 'bus-tayo-theme-9-oriscaket1q.jpeg', '2019-01-16 03:55:49', '2019-01-16 03:55:49'),
(206, 184, 'bus-tayo-theme-10-oriscakeoG8.jpeg', '2019-01-16 03:56:04', '2019-01-16 03:56:04'),
(207, 162, 'number-cake-6-oriscakekSh.jpeg', '2019-01-16 03:56:13', '2019-01-16 03:56:13'),
(208, 186, 'bus-tayo-theme-11-oriscakePHT.jpeg', '2019-01-16 03:56:17', '2019-01-16 03:56:17'),
(209, 163, 'number-cake-7-oriscakeqdC.jpeg', '2019-01-16 03:56:18', '2019-01-16 03:56:18'),
(210, 188, 'bus-tayo-theme-12-oriscaketxI.jpeg', '2019-01-16 03:56:30', '2019-01-16 03:56:30'),
(211, 164, 'number-cake-8-oriscakeItH.jpeg', '2019-01-16 03:56:35', '2019-01-16 03:56:35'),
(212, 165, 'number-cake-9-oriscakel41.jpeg', '2019-01-16 03:56:36', '2019-01-16 03:56:36'),
(213, 166, 'number-cake-10-oriscakeQhW.jpeg', '2019-01-16 03:56:40', '2019-01-16 03:56:40'),
(214, 189, 'bus-tayo-theme-13-oriscakeOui.jpeg', '2019-01-16 03:56:45', '2019-01-16 03:56:45'),
(215, 174, 'number-cake-14-oriscake1sk.jpeg', '2019-01-16 03:58:15', '2019-01-16 03:58:15'),
(216, 175, 'number-cake-15-oriscakepHf.jpeg', '2019-01-16 03:58:22', '2019-01-16 03:58:22'),
(217, 172, 'number-cake-13-oriscakemDX.jpeg', '2019-01-16 03:58:22', '2019-01-16 03:58:22'),
(218, 168, 'number-cake-11-oriscakeRvt.jpeg', '2019-01-16 03:58:22', '2019-01-16 03:58:22'),
(219, 170, 'number-cake-12-oriscakeXUR.jpeg', '2019-01-16 03:58:25', '2019-01-16 03:58:25'),
(220, 180, 'number-cake-16-oriscakea58.jpeg', '2019-01-16 04:00:25', '2019-01-16 04:00:25'),
(221, 181, 'number-cake-17-oriscake4Rl.jpeg', '2019-01-16 04:00:32', '2019-01-16 04:00:32'),
(222, 183, 'number-cake-18-oriscake7W9.jpeg', '2019-01-16 04:00:38', '2019-01-16 04:00:38'),
(223, 185, 'number-cake-19-oriscakeukS.jpeg', '2019-01-16 04:00:45', '2019-01-16 04:00:45'),
(224, 187, 'number-cake-20-oriscake0zx.jpeg', '2019-01-16 04:00:54', '2019-01-16 04:00:54'),
(225, 195, 'frozen-theme-1-oriscakeiyk.jpeg', '2019-01-16 04:01:40', '2019-01-16 04:01:40'),
(226, 196, 'frozen-theme-2-oriscaketEo.jpeg', '2019-01-16 04:01:59', '2019-01-16 04:01:59'),
(227, 197, 'frozen-theme-3-oriscakehw5.jpeg', '2019-01-16 04:02:12', '2019-01-16 04:02:12'),
(228, 190, 'number-cake-21-oriscaketb3.jpeg', '2019-01-16 04:02:17', '2019-01-16 04:02:17'),
(229, 193, 'number-cake-24-oriscakeLSy.jpeg', '2019-01-16 04:02:19', '2019-01-16 04:02:19'),
(230, 194, 'number-cake-25-oriscakeDHd.jpeg', '2019-01-16 04:02:25', '2019-01-16 04:02:25'),
(231, 192, 'number-cake-23-oriscakeJmw.jpeg', '2019-01-16 04:02:26', '2019-01-16 04:02:26'),
(232, 191, 'number-cake-22-oriscake2LK.jpeg', '2019-01-16 04:02:31', '2019-01-16 04:02:31'),
(233, 198, 'frozen-theme-4-oriscakeEgn.jpeg', '2019-01-16 04:02:35', '2019-01-16 04:02:35'),
(234, 199, 'frozen-theme-5-oriscakenYb.jpeg', '2019-01-16 04:02:46', '2019-01-16 04:02:46'),
(235, 200, 'frozen-theme-6-oriscakePWR.jpeg', '2019-01-16 04:02:56', '2019-01-16 04:02:56'),
(236, 201, 'princess-theme-1-oriscakeUVG.jpeg', '2019-01-16 07:01:39', '2019-01-16 07:01:39'),
(238, 202, 'princess-theme-4-oriscakeaSn.jpeg', '2019-01-16 07:02:04', '2019-01-16 07:02:04'),
(239, 203, 'princess-theme-2-oriscake2z2.jpeg', '2019-01-16 07:02:13', '2019-01-16 07:02:13'),
(240, 204, 'princess-theme-3-oriscakeHHw.jpeg', '2019-01-16 07:02:19', '2019-01-16 07:02:19'),
(241, 205, 'princess-theme-6-oriscakeONY.jpeg', '2019-01-16 07:02:28', '2019-01-16 07:02:28'),
(242, 206, 'princess-theme-7-oriscakeN7I.jpeg', '2019-01-16 07:02:37', '2019-01-16 07:02:37'),
(243, 208, 'princess-theme-5-oriscakelNg.jpeg', '2019-01-16 07:02:49', '2019-01-16 07:02:49'),
(244, 210, 'hello-kitty-theme-1-oriscakezMe.jpeg', '2019-01-16 07:10:47', '2019-01-16 07:10:47'),
(245, 212, 'hello-kitty-theme-2-oriscake0bK.jpeg', '2019-01-16 07:11:04', '2019-01-16 07:11:04'),
(246, 215, 'hello-kitty-theme-3-oriscakeO72.jpeg', '2019-01-16 07:11:18', '2019-01-16 07:11:18'),
(248, 217, 'hello-kitty-theme-4-oriscakeo34.jpeg', '2019-01-16 07:11:50', '2019-01-16 07:11:50'),
(249, 218, 'hello-kitty-theme-5-oriscakeGg0.jpeg', '2019-01-16 07:13:37', '2019-01-16 07:13:37'),
(250, 219, 'hello-kitty-theme-6-oriscake3l5.jpeg', '2019-01-16 07:13:51', '2019-01-16 07:13:51'),
(251, 209, 'request-design-1-oriscakeq9F.jpeg', '2019-01-16 07:44:06', '2019-01-16 07:44:06'),
(252, 211, 'request-design-2-oriscakeFKb.jpeg', '2019-01-16 07:44:11', '2019-01-16 07:44:11'),
(253, 213, 'request-design-3-oriscake92Q.jpeg', '2019-01-16 07:44:21', '2019-01-16 07:44:21'),
(254, 214, 'request-design-4-oriscakeZcx.jpeg', '2019-01-16 07:44:46', '2019-01-16 07:44:46'),
(255, 216, 'request-design-5-oriscakeJPz.jpeg', '2019-01-16 07:44:50', '2019-01-16 07:44:50'),
(256, 220, 'request-design-6-oriscakeYLc.jpeg', '2019-01-16 07:45:32', '2019-01-16 07:45:32'),
(257, 221, 'request-design-7-oriscake4ep.jpeg', '2019-01-16 07:45:37', '2019-01-16 07:45:37'),
(258, 222, 'request-design-8-oriscakeudg.jpeg', '2019-01-16 07:45:43', '2019-01-16 07:45:43'),
(259, 223, 'request-design-9-oriscakea9B.jpeg', '2019-01-16 07:45:51', '2019-01-16 07:45:51'),
(260, 224, 'request-design-10-oriscakerLg.jpeg', '2019-01-16 07:46:10', '2019-01-16 07:46:10'),
(261, 225, 'request-design-11-oriscakezA5.jpeg', '2019-01-16 07:47:00', '2019-01-16 07:47:00'),
(262, 226, 'request-design-12-oriscakexvT.jpeg', '2019-01-16 07:47:18', '2019-01-16 07:47:18'),
(263, 227, 'request-design-13-oriscakeDh2.jpeg', '2019-01-16 07:47:24', '2019-01-16 07:47:24'),
(264, 228, 'request-design-14-oriscakeBuK.jpeg', '2019-01-16 07:47:30', '2019-01-16 07:47:30'),
(265, 229, 'request-design-15-oriscakec3G.jpeg', '2019-01-16 07:47:38', '2019-01-16 07:47:38'),
(266, 230, 'request-design-16-oriscakeAjW.jpeg', '2019-01-16 07:49:01', '2019-01-16 07:49:01'),
(267, 231, 'request-design-17-oriscakefGH.jpeg', '2019-01-16 07:49:09', '2019-01-16 07:49:09'),
(268, 232, 'request-design-18-oriscakeKOQ.jpeg', '2019-01-16 07:49:12', '2019-01-16 07:49:12'),
(269, 233, 'request-design-19-oriscakePXv.jpeg', '2019-01-16 07:49:16', '2019-01-16 07:49:16'),
(270, 235, 'request-design-21-oriscakep92.jpeg', '2019-01-16 07:49:29', '2019-01-16 07:49:29'),
(271, 236, 'request-design-22-oriscakeuqq.jpeg', '2019-01-16 07:49:35', '2019-01-16 07:49:35'),
(272, 237, 'request-design-23-oriscakeVvf.jpeg', '2019-01-16 07:49:41', '2019-01-16 07:49:41'),
(273, 238, 'request-design-24-oriscake5Ai.jpeg', '2019-01-16 07:49:47', '2019-01-16 07:49:47'),
(274, 234, 'request-design-20-oriscakeZNS.jpeg', '2019-01-16 07:51:33', '2019-01-16 07:51:33'),
(275, 239, 'car-theme-1-oriscakeKyo.jpeg', '2019-01-17 02:51:11', '2019-01-17 02:51:11'),
(276, 240, 'car-theme-2-oriscakeygu.jpeg', '2019-01-17 02:51:48', '2019-01-17 02:51:48'),
(277, 241, 'car-theme-3-oriscaketpo.jpeg', '2019-01-17 02:52:00', '2019-01-17 02:52:00'),
(278, 242, 'car-theme-4-oriscake1DS.jpeg', '2019-01-17 02:53:42', '2019-01-17 02:53:42'),
(279, 243, 'car-theme-5-oriscakeqm0.jpeg', '2019-01-17 02:53:54', '2019-01-17 02:53:54'),
(280, 244, 'car-theme-6-oriscakemqR.jpeg', '2019-01-17 02:54:13', '2019-01-17 02:54:13'),
(281, 245, 'car-theme-7-oriscakee50.jpeg', '2019-01-17 02:54:24', '2019-01-17 02:54:24'),
(282, 246, 'car-theme-8-oriscakeckL.png', '2019-01-17 02:56:49', '2019-01-17 02:56:49'),
(283, 247, 'clothes-theme-1-oriscakeU1x.jpeg', '2019-01-17 03:18:44', '2019-01-17 03:18:44'),
(284, 248, 'clothes-theme-2-oriscakehwo.jpeg', '2019-01-17 03:18:58', '2019-01-17 03:18:58'),
(285, 249, 'clothes-theme-3-oriscakesNe.jpeg', '2019-01-17 03:19:11', '2019-01-17 03:19:11'),
(286, 250, 'clothes-theme-4-oriscakeKL7.jpeg', '2019-01-17 03:19:24', '2019-01-17 03:19:24'),
(287, 251, 'clothes-theme-5-oriscakeRS3.jpeg', '2019-01-17 03:19:40', '2019-01-17 03:19:40'),
(288, 252, 'clothes-theme-6-oriscakeY0Q.jpeg', '2019-01-17 03:19:52', '2019-01-17 03:19:52'),
(289, 253, 'clothes-theme-7-oriscakeXYb.jpeg', '2019-01-17 03:20:08', '2019-01-17 03:20:08'),
(290, 254, 'clothes-theme-8-oriscakeIeV.jpeg', '2019-01-17 03:20:23', '2019-01-17 03:20:23'),
(291, 255, 'clothes-theme-9-oriscakeWwk.jpeg', '2019-01-17 03:20:37', '2019-01-17 03:20:37'),
(292, 256, 'clothes-theme-10-oriscakeq1Z.jpeg', '2019-01-17 03:20:52', '2019-01-17 03:20:52'),
(293, 257, 'football-theme-1-oriscakeV9H.jpeg', '2019-01-17 03:34:34', '2019-01-17 03:34:34'),
(294, 258, 'football-theme-2-oriscake9oi.jpeg', '2019-01-17 03:34:46', '2019-01-17 03:34:46'),
(295, 259, 'football-theme-3-oriscakeXO8.jpeg', '2019-01-17 03:34:58', '2019-01-17 03:34:58'),
(296, 260, 'football-theme-4-oriscakeXDn.jpeg', '2019-01-17 03:35:08', '2019-01-17 03:35:08'),
(297, 261, 'football-theme-5-oriscakeehc.jpeg', '2019-01-17 03:35:20', '2019-01-17 03:35:20'),
(298, 262, 'football-theme-6-oriscakekMp.jpeg', '2019-01-17 03:35:34', '2019-01-17 03:35:34'),
(299, 264, 'football-theme-6-oriscakeb0i.jpeg', '2019-01-17 03:35:47', '2019-01-17 03:35:47'),
(300, 263, 'spiderman-theme-1-oriscakenMr.jpeg', '2019-01-17 03:36:05', '2019-01-17 03:36:05'),
(301, 266, 'football-theme-7-oriscakeO9s.jpeg', '2019-01-17 03:36:08', '2019-01-17 03:36:08'),
(302, 265, 'spiderman-theme-2-oriscakeKdc.jpeg', '2019-01-17 03:36:12', '2019-01-17 03:36:12'),
(303, 269, 'football-theme-8-oriscakemyU.jpeg', '2019-01-17 03:36:13', '2019-01-17 03:36:13'),
(304, 267, 'spiderman-theme-3-oriscakeG4K.jpeg', '2019-01-17 03:36:17', '2019-01-17 03:36:17'),
(305, 271, 'football-theme-9-oriscake917.jpeg', '2019-01-17 03:36:20', '2019-01-17 03:36:20'),
(306, 268, 'spiderman-theme-4-oriscake4pA.jpeg', '2019-01-17 03:36:23', '2019-01-17 03:36:23'),
(307, 274, 'football-theme-10-oriscake3Mg.jpeg', '2019-01-17 03:36:28', '2019-01-17 03:36:28'),
(308, 270, 'spiderman-theme-5-oriscakeQdl.jpeg', '2019-01-17 03:36:29', '2019-01-17 03:36:29'),
(309, 276, 'football-theme-11-oriscakerrk.jpeg', '2019-01-17 03:36:31', '2019-01-17 03:36:31'),
(310, 272, 'spiderman-theme-6-oriscakeD0q.jpeg', '2019-01-17 03:36:35', '2019-01-17 03:36:35'),
(311, 273, 'spiderman-theme-7-oriscakeYhr.jpeg', '2019-01-17 03:36:42', '2019-01-17 03:36:42'),
(312, 275, 'spiderman-theme-8-oriscakeAPx.jpeg', '2019-01-17 03:36:48', '2019-01-17 03:36:48'),
(313, 277, 'flower-theme-1-oriscake3qw.jpeg', '2019-01-17 03:44:18', '2019-01-17 03:44:18'),
(314, 280, 'flower-theme-2-oriscakeezw.jpeg', '2019-01-17 03:44:23', '2019-01-17 03:44:23'),
(315, 282, 'flower-theme-3-oriscake0E7.jpeg', '2019-01-17 03:44:28', '2019-01-17 03:44:28'),
(316, 283, 'flower-theme-4-oriscakes6C.jpeg', '2019-01-17 03:44:32', '2019-01-17 03:44:32'),
(317, 284, 'flower-theme-5-oriscake6bB.jpeg', '2019-01-17 03:44:37', '2019-01-17 03:44:37'),
(318, 285, 'flower-theme-6-oriscakevi9.jpeg', '2019-01-17 03:44:43', '2019-01-17 03:44:43'),
(319, 286, 'flower-theme-7-oriscakeR4D.jpeg', '2019-01-17 03:44:49', '2019-01-17 03:44:49'),
(320, 287, 'flower-theme-8-oriscakeYjg.jpeg', '2019-01-17 03:44:56', '2019-01-17 03:44:56'),
(321, 288, 'flower-theme-9-oriscakexZj.jpeg', '2019-01-17 03:45:01', '2019-01-17 03:45:01'),
(322, 289, 'flower-theme-10-oriscakeamo.jpeg', '2019-01-17 03:45:07', '2019-01-17 03:45:07'),
(323, 290, 'flower-theme-11-oriscakeazd.jpeg', '2019-01-17 03:45:22', '2019-01-17 03:45:22'),
(324, 292, 'flower-theme-12-oriscakesJp.jpeg', '2019-01-17 03:45:27', '2019-01-17 03:45:27'),
(325, 294, 'flower-theme-13-oriscake0cd.jpeg', '2019-01-17 03:45:30', '2019-01-17 03:45:30'),
(326, 296, 'flower-theme-14-oriscakeyyd.jpeg', '2019-01-17 03:45:35', '2019-01-17 03:45:35'),
(327, 298, 'flower-theme-15-oriscakedea.jpeg', '2019-01-17 03:45:43', '2019-01-17 03:45:43'),
(328, 278, 'superhero-theme-1-oriscakeB8Z.jpeg', '2019-01-17 03:46:04', '2019-01-17 03:46:04'),
(329, 279, 'superhero-theme-2-oriscakeAMP.jpeg', '2019-01-17 03:46:15', '2019-01-17 03:46:15'),
(330, 28, 'animal-theme-1-oriscake9gR.jpeg', '2019-01-18 03:44:17', '2019-01-18 03:44:17'),
(331, 29, 'animal-theme-2-oriscakemme.jpeg', '2019-01-18 03:51:03', '2019-01-18 03:51:03'),
(333, 30, 'animal-theme-3-oriscakeC8v.jpeg', '2019-01-18 03:54:51', '2019-01-18 03:54:51'),
(334, 31, 'animal-theme-4-oriscake37N.jpeg', '2019-01-18 03:55:24', '2019-01-18 03:55:24'),
(335, 32, 'animal-theme-5-oriscakeEDg.jpeg', '2019-01-18 03:56:52', '2019-01-18 03:56:52'),
(337, 33, 'animal-theme-6-oriscakeEId.jpeg', '2019-01-18 04:10:39', '2019-01-18 04:10:39'),
(338, 34, 'animal-theme-7-oriscake8oD.jpeg', '2019-01-18 04:11:09', '2019-01-18 04:11:09'),
(339, 35, 'animal-theme-8-oriscake2i6.jpeg', '2019-01-18 04:12:29', '2019-01-18 04:12:29'),
(341, 37, 'animal-theme-10-oriscakeos2.jpeg', '2019-01-18 04:13:42', '2019-01-18 04:13:42'),
(342, 51, 'animal-theme-11-oriscakePrS.jpeg', '2019-01-18 04:18:38', '2019-01-18 04:18:38'),
(343, 36, 'animal-theme-9-oriscakez6M.jpeg', '2019-01-19 06:01:00', '2019-01-19 06:01:00'),
(344, 40, 'little-pony-2-oriscakeCO1.jpeg', '2019-01-19 06:07:30', '2019-01-19 06:07:30'),
(345, 45, 'little-pony-8-oriscakes6s.jpeg', '2019-01-19 06:08:27', '2019-01-19 06:08:27'),
(346, 281, 'superhero-theme-3-oriscake9L9.jpeg', '2019-01-23 05:12:23', '2019-01-23 05:12:23'),
(347, 291, 'superhero-theme-4-oriscakeBK2.jpeg', '2019-01-23 05:12:36', '2019-01-23 05:12:36'),
(348, 293, 'superhero-theme-5-oriscakeSld.jpeg', '2019-01-23 05:12:42', '2019-01-23 05:12:42'),
(349, 295, 'superhero-theme-6-oriscakeSpM.jpeg', '2019-01-23 05:12:48', '2019-01-23 05:12:48'),
(350, 297, 'superhero-theme-7-oriscakejo2.jpeg', '2019-01-23 05:12:54', '2019-01-23 05:12:54'),
(351, 299, 'superhero-theme-8-oriscakeG11.jpeg', '2019-01-23 05:13:10', '2019-01-23 05:13:10'),
(352, 300, 'superhero-theme-9-oriscakeMoC.jpeg', '2019-01-23 05:13:12', '2019-01-23 05:13:12'),
(356, 302, 'tr001-oriscakebSh.jpeg', '2019-01-23 05:41:11', '2019-01-23 05:41:11'),
(357, 304, 'tr003-oriscakejip.jpeg', '2019-01-23 05:41:36', '2019-01-23 05:41:36'),
(358, 306, 'tr004-oriscakeIwM.jpeg', '2019-01-23 05:41:43', '2019-01-23 05:41:43'),
(359, 307, 'tr005-oriscakeO7i.jpeg', '2019-01-23 05:41:49', '2019-01-23 05:41:49'),
(360, 308, 'tr006-oriscakedqx.jpeg', '2019-01-23 05:41:53', '2019-01-23 05:41:53'),
(361, 303, 'tr002-oriscakeEOF.jpeg', '2019-01-23 05:42:19', '2019-01-23 05:42:19'),
(362, 311, 'un001-oriscake2DS.jpeg', '2019-01-23 05:57:59', '2019-01-23 05:57:59'),
(363, 312, 'un002-oriscakeOHA.jpeg', '2019-01-23 05:58:03', '2019-01-23 05:58:03'),
(364, 313, 'un003-oriscakeng2.jpeg', '2019-01-23 05:58:10', '2019-01-23 05:58:10'),
(365, 314, 'un004-oriscakezho.jpeg', '2019-01-23 05:58:15', '2019-01-23 05:58:15'),
(366, 315, 'ck001-oriscake1qm.jpeg', '2019-01-29 05:05:34', '2019-01-29 05:05:34'),
(367, 316, 'ck002-oriscake9XE.jpeg', '2019-01-29 05:05:40', '2019-01-29 05:05:40'),
(368, 317, 'ck003-oriscake2pP.jpeg', '2019-01-29 05:05:46', '2019-01-29 05:05:46'),
(369, 318, 'ck006-oriscakeK6D.jpeg', '2019-01-29 05:06:00', '2019-01-29 05:06:00'),
(370, 319, 'ck004-oriscakeZxf.jpeg', '2019-01-29 05:06:07', '2019-01-29 05:06:07'),
(371, 320, 'ck005-oriscakehfH.jpeg', '2019-01-29 05:06:16', '2019-01-29 05:06:16'),
(372, 321, 'ck007-oriscake0V4.jpeg', '2019-01-29 05:06:49', '2019-01-29 05:06:49'),
(373, 322, 'ck008-oriscakeP7q.jpeg', '2019-01-29 05:06:55', '2019-01-29 05:06:55'),
(374, 323, 'ck009-oriscakeyXk.jpeg', '2019-01-29 05:07:00', '2019-01-29 05:07:00'),
(375, 324, 'ck010-oriscake9E5.jpeg', '2019-01-29 05:07:05', '2019-01-29 05:07:05'),
(376, 325, 'ck011-oriscake7lL.jpeg', '2019-01-29 05:17:08', '2019-01-29 05:17:08'),
(377, 326, 'ck012-oriscakea0l.jpeg', '2019-01-29 05:17:14', '2019-01-29 05:17:14'),
(378, 327, 'ck013-oriscakezDw.jpeg', '2019-01-29 05:17:21', '2019-01-29 05:17:21'),
(379, 328, 'ck014-oriscakerD5.jpeg', '2019-01-29 05:17:29', '2019-01-29 05:17:29'),
(380, 329, 'ck015-oriscakeLhK.jpeg', '2019-01-29 05:17:40', '2019-01-29 05:17:40'),
(381, 330, 'ck016-oriscakeqEn.jpeg', '2019-01-29 05:17:48', '2019-01-29 05:17:48'),
(382, 331, 'ck017-oriscakeQz1.jpeg', '2019-01-29 05:18:00', '2019-01-29 05:18:00'),
(383, 332, 'ck018-oriscakeZf2.jpeg', '2019-01-29 05:18:08', '2019-01-29 05:18:08'),
(384, 333, 'ck019-oriscakeIIj.jpeg', '2019-01-29 05:18:16', '2019-01-29 05:18:16'),
(385, 334, 'ck020-oriscakeLL4.jpeg', '2019-01-29 05:18:22', '2019-01-29 05:18:22'),
(386, 335, 'ck021-oriscake9pw.jpeg', '2019-01-29 05:19:31', '2019-01-29 05:19:31'),
(387, 336, 'ck022-oriscake2q9.jpeg', '2019-01-29 05:19:36', '2019-01-29 05:19:36'),
(388, 337, 'ck023-oriscakeG5z.jpeg', '2019-01-29 05:19:42', '2019-01-29 05:19:42'),
(389, 338, 'ck024-oriscakeQXf.jpeg', '2019-01-29 05:19:51', '2019-01-29 05:19:51'),
(390, 339, 'ck025-oriscakeDRP.jpeg', '2019-01-29 05:19:57', '2019-01-29 05:19:57'),
(391, 340, 'ck026-oriscakeEGV.jpeg', '2019-01-29 05:20:08', '2019-01-29 05:20:08'),
(392, 341, 'ck027-oriscaket7j.jpeg', '2019-01-29 05:20:16', '2019-01-29 05:20:16'),
(393, 342, 'ck028-oriscakePTB.jpeg', '2019-01-29 05:20:23', '2019-01-29 05:20:23'),
(394, 343, 'ck029-oriscakefq0.jpeg', '2019-01-29 05:20:30', '2019-01-29 05:20:30'),
(395, 344, 'ck030-oriscake8Z1.jpeg', '2019-01-29 05:20:39', '2019-01-29 05:20:39'),
(396, 345, 'ck031-oriscakekdf.jpeg', '2019-01-29 05:22:19', '2019-01-29 05:22:19'),
(397, 346, 'ck032-oriscakeZT6.jpeg', '2019-01-29 05:22:30', '2019-01-29 05:22:30'),
(398, 347, 'ck033-oriscakeQrm.jpeg', '2019-01-29 05:22:36', '2019-01-29 05:22:36'),
(399, 348, 'ck034-oriscakeSOq.jpeg', '2019-01-29 05:22:46', '2019-01-29 05:22:46'),
(400, 349, 'ck035-oriscakeosr.jpeg', '2019-01-29 05:22:56', '2019-01-29 05:22:56'),
(401, 350, 'ck036-oriscakeOHE.jpeg', '2019-01-29 05:23:05', '2019-01-29 05:23:05'),
(402, 351, 'ck037-oriscakexen.jpeg', '2019-01-29 05:23:16', '2019-01-29 05:23:16'),
(403, 352, 'ck038-oriscakeN6x.jpeg', '2019-01-29 05:23:25', '2019-01-29 05:23:25'),
(404, 353, 'ck039-oriscakelAr.jpeg', '2019-01-29 05:23:32', '2019-01-29 05:23:32'),
(405, 354, 'ck040-oriscakesWH.jpeg', '2019-01-29 05:23:39', '2019-01-29 05:23:39'),
(406, 355, 'ck041-oriscakeKTd.jpeg', '2019-01-29 05:26:55', '2019-01-29 05:26:55'),
(407, 356, 'ck042-oriscakeniy.jpeg', '2019-01-29 05:27:05', '2019-01-29 05:27:05'),
(408, 357, 'ck043-oriscakeBKj.jpeg', '2019-01-29 05:27:13', '2019-01-29 05:27:13'),
(409, 358, 'cm001-oriscakeGUb.jpeg', '2019-01-30 02:43:10', '2019-01-30 02:43:10'),
(410, 359, 'cm002-oriscakeIFq.jpeg', '2019-01-30 02:43:15', '2019-01-30 02:43:15'),
(411, 360, 'cm003-oriscakelyk.jpeg', '2019-01-30 02:43:20', '2019-01-30 02:43:20'),
(412, 361, 'cm004-oriscakemKF.jpeg', '2019-01-30 02:43:26', '2019-01-30 02:43:26'),
(413, 362, 'cm005-oriscakelOh.jpeg', '2019-01-30 02:43:32', '2019-01-30 02:43:32'),
(414, 363, 'cm006-oriscakesvc.jpeg', '2019-01-30 02:43:42', '2019-01-30 02:43:42'),
(415, 364, 'cm007-oriscake1p4.jpeg', '2019-01-30 02:43:49', '2019-01-30 02:43:49'),
(416, 365, 'cm008-oriscakeXnJ.jpeg', '2019-01-30 02:43:57', '2019-01-30 02:43:57'),
(417, 366, 'cm009-oriscake9v7.jpeg', '2019-01-30 02:44:05', '2019-01-30 02:44:05'),
(418, 367, 'cm010-oriscakegHy.jpeg', '2019-01-30 02:44:11', '2019-01-30 02:44:11'),
(419, 368, 'cm011-oriscake0t6.jpeg', '2019-01-30 02:45:03', '2019-01-30 02:45:03'),
(420, 369, 'cm012-oriscakeziS.jpeg', '2019-01-30 02:45:10', '2019-01-30 02:45:10'),
(421, 370, 'cm013-oriscakeGYN.jpeg', '2019-01-30 02:45:13', '2019-01-30 02:45:13'),
(422, 371, 'cm014-oriscakeghH.jpeg', '2019-01-30 02:45:18', '2019-01-30 02:45:18'),
(423, 372, 'cm015-oriscakeVMn.jpeg', '2019-01-30 02:45:24', '2019-01-30 02:45:24'),
(424, 373, 'cm016-oriscake4NM.jpeg', '2019-01-30 02:45:30', '2019-01-30 02:45:30'),
(425, 374, 'cm017-oriscakeW8T.jpeg', '2019-01-30 02:45:39', '2019-01-30 02:45:39'),
(426, 375, 'cm018-oriscakeAYs.jpeg', '2019-01-30 02:45:47', '2019-01-30 02:45:47'),
(427, 376, 'cm019-oriscakewaT.jpeg', '2019-01-30 02:45:52', '2019-01-30 02:45:52'),
(428, 377, 'cm020-oriscakeP5m.jpeg', '2019-01-30 02:45:58', '2019-01-30 02:45:58'),
(429, 378, 'cm021-oriscakeO3S.jpeg', '2019-01-30 02:46:45', '2019-01-30 02:46:45'),
(430, 379, 'cm022-oriscakeh6B.jpeg', '2019-01-30 02:46:52', '2019-01-30 02:46:52'),
(431, 380, 'cm023-oriscakeyGv.jpeg', '2019-01-30 02:46:57', '2019-01-30 02:46:57'),
(432, 381, 'cm024-oriscakeH1J.jpeg', '2019-01-30 02:47:03', '2019-01-30 02:47:03'),
(433, 382, 'cm025-oriscakeFJk.jpeg', '2019-01-30 02:47:13', '2019-01-30 02:47:13'),
(434, 383, 'cm026-oriscakeYIu.jpeg', '2019-01-30 02:47:16', '2019-01-30 02:47:16'),
(435, 384, 'iu001-oriscakePp8.jpeg', '2019-01-30 03:03:25', '2019-01-30 03:03:25'),
(436, 385, 'iu002-oriscakeRDR.jpeg', '2019-01-30 03:03:30', '2019-01-30 03:03:30'),
(437, 386, 'iu003-oriscakefEn.jpeg', '2019-01-30 03:03:35', '2019-01-30 03:03:35'),
(438, 387, 'iu004-oriscake8V3.jpeg', '2019-01-30 03:03:41', '2019-01-30 03:03:41'),
(439, 388, 'iu005-oriscakelrb.jpeg', '2019-01-30 03:03:46', '2019-01-30 03:03:46'),
(440, 389, 'iu006-oriscakefDh.jpeg', '2019-01-30 03:03:59', '2019-01-30 03:03:59'),
(441, 390, 'iu007-oriscakeym3.jpeg', '2019-01-30 03:04:06', '2019-01-30 03:04:06'),
(442, 391, 'iu008-oriscakeOjN.jpeg', '2019-01-30 03:04:13', '2019-01-30 03:04:13'),
(443, 392, 'iu009-oriscakeLNM.jpeg', '2019-01-30 03:04:19', '2019-01-30 03:04:19'),
(444, 393, 'iu010-oriscakeb5z.jpeg', '2019-01-30 03:04:25', '2019-01-30 03:04:25'),
(445, 394, 'iu011-oriscakejec.jpeg', '2019-01-30 03:04:31', '2019-01-30 03:04:31'),
(446, 395, 'iu012-oriscakeCaF.jpeg', '2019-01-30 03:04:37', '2019-01-30 03:04:37'),
(447, 396, 'iu013-oriscakeFNx.jpeg', '2019-01-30 03:04:43', '2019-01-30 03:04:43'),
(448, 397, 'iu014-oriscakeQeD.jpeg', '2019-01-30 03:04:51', '2019-01-30 03:04:51'),
(449, 398, 'iu015-oriscakeaEn.jpeg', '2019-01-30 03:05:02', '2019-01-30 03:05:02'),
(450, 399, 'iu016-oriscakeQk2.jpeg', '2019-01-30 03:05:06', '2019-01-30 03:05:06'),
(451, 309, 'ul001-oriscakeAAK.jpeg', '2019-01-30 03:26:38', '2019-01-30 03:26:38'),
(452, 310, 'ul002-oriscake1Sw.jpeg', '2019-01-30 03:26:56', '2019-01-30 03:26:56'),
(453, 400, 'ul003-oriscakeqEc.jpeg', '2019-01-31 06:30:40', '2019-01-31 06:30:40');

-- --------------------------------------------------------

--
-- Table structure for table `tb_cake_rasa`
--

CREATE TABLE `tb_cake_rasa` (
  `id` int(10) NOT NULL,
  `id_rasa` int(10) UNSIGNED DEFAULT NULL,
  `id_kue` int(10) UNSIGNED DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_cake_tag`
--

CREATE TABLE `tb_cake_tag` (
  `id` int(11) NOT NULL,
  `id_tag` int(11) DEFAULT NULL,
  `id_kue` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_cake_tag`
--

INSERT INTO `tb_cake_tag` (`id`, `id_tag`, `id_kue`, `created_at`, `updated_at`) VALUES
(1, 2, 2, '2018-07-20 13:15:18', '2018-07-20 13:15:18'),
(3, 10, 2, '2018-07-20 15:59:27', '2018-07-20 15:59:27'),
(4, 11, 2, '2018-07-20 15:59:27', '2018-07-20 15:59:27'),
(5, 2, 14, '2018-08-30 16:54:49', '2018-08-30 16:54:49'),
(7, 5, 3, '2018-09-07 17:39:56', '2018-09-07 17:39:56'),
(8, 10, 9, '2018-09-07 17:48:33', '2018-09-07 17:48:33'),
(9, 5, 11, '2018-09-07 17:52:33', '2018-09-07 17:52:33'),
(10, 2, 13, '2018-10-02 19:56:31', '2018-10-02 19:56:31'),
(11, 10, 15, '2018-11-15 10:30:21', '2018-11-15 10:30:21'),
(12, 8, 16, '2018-11-23 17:38:27', '2018-11-23 17:38:27'),
(13, 10, 5, '2019-01-11 16:41:36', '2019-01-11 16:41:36'),
(14, 10, 13, '2019-01-11 16:44:37', '2019-01-11 16:44:37'),
(15, 2, 17, '2019-01-11 16:46:15', '2019-01-11 16:46:15'),
(16, 10, 17, '2019-01-11 16:46:15', '2019-01-11 16:46:15'),
(17, 1, 18, '2019-01-11 16:49:21', '2019-01-11 16:49:21'),
(18, 2, 18, '2019-01-11 16:49:21', '2019-01-11 16:49:21'),
(19, 10, 18, '2019-01-11 16:49:21', '2019-01-11 16:49:21'),
(20, 9, 19, '2019-01-11 16:55:13', '2019-01-11 16:55:13'),
(21, 10, 19, '2019-01-11 16:55:13', '2019-01-11 16:55:13'),
(22, 11, 19, '2019-01-11 16:55:13', '2019-01-11 16:55:13'),
(23, 2, 25, '2019-01-11 17:40:52', '2019-01-11 17:40:52'),
(24, 1, 26, '2019-01-11 17:43:23', '2019-01-11 17:43:23'),
(25, 6, 26, '2019-01-11 17:43:23', '2019-01-11 17:43:23'),
(26, 10, 26, '2019-01-11 17:43:23', '2019-01-11 17:43:23'),
(27, 11, 26, '2019-01-11 17:43:23', '2019-01-11 17:43:23'),
(28, 2, 27, '2019-01-14 11:01:55', '2019-01-14 11:01:55'),
(29, 10, 27, '2019-01-14 11:01:55', '2019-01-14 11:01:55'),
(30, 6, 9, '2019-01-14 14:44:17', '2019-01-14 14:44:17'),
(31, 2, 28, '2019-01-15 10:41:33', '2019-01-15 10:41:33'),
(32, 2, 29, '2019-01-15 11:14:26', '2019-01-15 11:14:26'),
(33, 2, 30, '2019-01-15 11:15:07', '2019-01-15 11:15:07'),
(34, 2, 31, '2019-01-15 11:16:13', '2019-01-15 11:16:13'),
(35, 2, 32, '2019-01-15 11:17:26', '2019-01-15 11:17:26'),
(36, 2, 33, '2019-01-15 11:18:20', '2019-01-15 11:18:20'),
(37, 2, 34, '2019-01-15 11:19:28', '2019-01-15 11:19:28'),
(38, 2, 35, '2019-01-15 11:20:32', '2019-01-15 11:20:32'),
(39, 2, 36, '2019-01-15 11:21:24', '2019-01-15 11:21:24'),
(40, 2, 37, '2019-01-15 11:22:24', '2019-01-15 11:22:24'),
(42, 10, 38, '2019-01-15 11:36:36', '2019-01-15 11:36:36'),
(44, 10, 39, '2019-01-15 11:37:56', '2019-01-15 11:37:56'),
(46, 10, 40, '2019-01-15 11:40:01', '2019-01-15 11:40:01'),
(48, 10, 41, '2019-01-15 11:40:32', '2019-01-15 11:40:32'),
(50, 10, 42, '2019-01-15 11:41:23', '2019-01-15 11:41:23'),
(51, 10, 43, '2019-01-15 11:42:50', '2019-01-15 11:42:50'),
(53, 10, 44, '2019-01-15 11:56:26', '2019-01-15 11:56:26'),
(55, 10, 45, '2019-01-15 11:58:34', '2019-01-15 11:58:34'),
(57, 10, 46, '2019-01-15 12:00:04', '2019-01-15 12:00:04'),
(59, 10, 47, '2019-01-15 12:00:28', '2019-01-15 12:00:28'),
(61, 10, 48, '2019-01-15 12:03:02', '2019-01-15 12:03:02'),
(63, 10, 49, '2019-01-15 12:03:29', '2019-01-15 12:03:29'),
(65, 2, 51, '2019-01-15 13:49:08', '2019-01-15 13:49:08'),
(67, 10, 52, '2019-01-15 13:49:23', '2019-01-15 13:49:23'),
(71, 10, 55, '2019-01-15 13:49:57', '2019-01-15 13:49:57'),
(72, 2, 56, '2019-01-15 13:50:16', '2019-01-15 13:50:16'),
(74, 10, 57, '2019-01-15 13:50:22', '2019-01-15 13:50:22'),
(78, 10, 60, '2019-01-15 13:51:05', '2019-01-15 13:51:05'),
(81, 10, 62, '2019-01-15 13:51:37', '2019-01-15 13:51:37'),
(84, 10, 64, '2019-01-15 13:52:04', '2019-01-15 13:52:04'),
(87, 10, 66, '2019-01-15 13:52:29', '2019-01-15 13:52:29'),
(91, 10, 69, '2019-01-15 13:53:04', '2019-01-15 13:53:04'),
(94, 10, 71, '2019-01-15 13:53:37', '2019-01-15 13:53:37'),
(97, 10, 73, '2019-01-15 13:54:08', '2019-01-15 13:54:08'),
(101, 10, 76, '2019-01-15 13:54:33', '2019-01-15 13:54:33'),
(113, 10, 87, '2019-01-15 13:59:11', '2019-01-15 13:59:11'),
(116, 10, 89, '2019-01-15 13:59:56', '2019-01-15 13:59:56'),
(122, 10, 94, '2019-01-15 14:01:29', '2019-01-15 14:01:29'),
(125, 10, 96, '2019-01-15 14:02:33', '2019-01-15 14:02:33'),
(127, 10, 97, '2019-01-15 14:02:55', '2019-01-15 14:02:55'),
(129, 10, 98, '2019-01-15 14:03:54', '2019-01-15 14:03:54'),
(131, 10, 99, '2019-01-15 14:04:31', '2019-01-15 14:04:31'),
(133, 10, 100, '2019-01-15 14:04:54', '2019-01-15 14:04:54'),
(135, 10, 101, '2019-01-15 14:05:19', '2019-01-15 14:05:19'),
(137, 10, 102, '2019-01-15 14:05:42', '2019-01-15 14:05:42'),
(139, 10, 103, '2019-01-15 14:08:46', '2019-01-15 14:08:46'),
(141, 10, 104, '2019-01-15 14:09:10', '2019-01-15 14:09:10'),
(143, 10, 105, '2019-01-15 14:09:56', '2019-01-15 14:09:56'),
(166, 2, 128, '2019-01-16 11:03:38', '2019-01-16 11:03:38'),
(169, 10, 130, '2019-01-16 11:06:41', '2019-01-16 11:06:41'),
(173, 10, 133, '2019-01-16 11:12:36', '2019-01-16 11:12:36'),
(175, 10, 134, '2019-01-16 11:19:33', '2019-01-16 11:19:33'),
(177, 10, 135, '2019-01-16 11:20:06', '2019-01-16 11:20:06'),
(179, 10, 136, '2019-01-16 11:21:24', '2019-01-16 11:21:24'),
(181, 10, 137, '2019-01-16 11:21:46', '2019-01-16 11:21:46'),
(183, 10, 138, '2019-01-16 11:22:43', '2019-01-16 11:22:43'),
(185, 10, 139, '2019-01-16 11:23:01', '2019-01-16 11:23:01'),
(187, 10, 140, '2019-01-16 11:23:18', '2019-01-16 11:23:18'),
(189, 10, 141, '2019-01-16 11:24:47', '2019-01-16 11:24:47'),
(191, 10, 142, '2019-01-16 11:25:08', '2019-01-16 11:25:08'),
(193, 10, 143, '2019-01-16 11:25:28', '2019-01-16 11:25:28'),
(195, 10, 144, '2019-01-16 11:25:43', '2019-01-16 11:25:43'),
(197, 10, 145, '2019-01-16 11:26:03', '2019-01-16 11:26:03'),
(199, 10, 146, '2019-01-16 11:27:19', '2019-01-16 11:27:19'),
(201, 10, 147, '2019-01-16 11:27:33', '2019-01-16 11:27:33'),
(203, 10, 148, '2019-01-16 11:27:48', '2019-01-16 11:27:48'),
(205, 10, 149, '2019-01-16 11:28:03', '2019-01-16 11:28:03'),
(207, 10, 150, '2019-01-16 11:28:22', '2019-01-16 11:28:22'),
(209, 10, 151, '2019-01-16 11:29:25', '2019-01-16 11:29:25'),
(211, 10, 152, '2019-01-16 11:29:38', '2019-01-16 11:29:38'),
(213, 10, 153, '2019-01-16 11:29:53', '2019-01-16 11:29:53'),
(215, 10, 154, '2019-01-16 11:30:08', '2019-01-16 11:30:08'),
(217, 10, 155, '2019-01-16 11:30:24', '2019-01-16 11:30:24'),
(219, 10, 156, '2019-01-16 11:31:52', '2019-01-16 11:31:52'),
(221, 10, 157, '2019-01-16 11:41:07', '2019-01-16 11:41:07'),
(223, 10, 158, '2019-01-16 11:42:04', '2019-01-16 11:42:04'),
(224, 9, 159, '2019-01-16 11:42:18', '2019-01-16 11:42:18'),
(225, 10, 159, '2019-01-16 11:42:18', '2019-01-16 11:42:18'),
(229, 2, 162, '2019-01-16 11:44:46', '2019-01-16 11:44:46'),
(230, 10, 162, '2019-01-16 11:44:46', '2019-01-16 11:44:46'),
(231, 9, 163, '2019-01-16 11:45:00', '2019-01-16 11:45:00'),
(232, 10, 163, '2019-01-16 11:45:00', '2019-01-16 11:45:00'),
(233, 10, 164, '2019-01-16 11:45:12', '2019-01-16 11:45:12'),
(234, 2, 165, '2019-01-16 11:45:27', '2019-01-16 11:45:27'),
(235, 10, 165, '2019-01-16 11:45:27', '2019-01-16 11:45:27'),
(236, 2, 166, '2019-01-16 11:45:41', '2019-01-16 11:45:41'),
(237, 10, 166, '2019-01-16 11:45:41', '2019-01-16 11:45:41'),
(238, 2, 167, '2019-01-16 11:47:04', '2019-01-16 11:47:04'),
(239, 2, 168, '2019-01-16 11:47:19', '2019-01-16 11:47:19'),
(240, 10, 168, '2019-01-16 11:47:19', '2019-01-16 11:47:19'),
(241, 2, 169, '2019-01-16 11:47:25', '2019-01-16 11:47:25'),
(242, 2, 170, '2019-01-16 11:47:36', '2019-01-16 11:47:36'),
(243, 10, 170, '2019-01-16 11:47:36', '2019-01-16 11:47:36'),
(244, 2, 171, '2019-01-16 11:47:46', '2019-01-16 11:47:46'),
(245, 2, 172, '2019-01-16 11:47:53', '2019-01-16 11:47:53'),
(246, 10, 172, '2019-01-16 11:47:53', '2019-01-16 11:47:53'),
(247, 2, 173, '2019-01-16 11:48:26', '2019-01-16 11:48:26'),
(248, 2, 174, '2019-01-16 11:48:29', '2019-01-16 11:48:29'),
(249, 10, 174, '2019-01-16 11:48:29', '2019-01-16 11:48:29'),
(250, 2, 175, '2019-01-16 11:48:41', '2019-01-16 11:48:41'),
(251, 10, 175, '2019-01-16 11:48:41', '2019-01-16 11:48:41'),
(252, 2, 176, '2019-01-16 11:48:51', '2019-01-16 11:48:51'),
(253, 2, 177, '2019-01-16 11:49:19', '2019-01-16 11:49:19'),
(254, 2, 178, '2019-01-16 11:49:44', '2019-01-16 11:49:44'),
(255, 2, 179, '2019-01-16 11:50:09', '2019-01-16 11:50:09'),
(256, 2, 180, '2019-01-16 11:50:28', '2019-01-16 11:50:28'),
(257, 10, 180, '2019-01-16 11:50:28', '2019-01-16 11:50:28'),
(258, 2, 181, '2019-01-16 11:50:43', '2019-01-16 11:50:43'),
(259, 10, 181, '2019-01-16 11:50:43', '2019-01-16 11:50:43'),
(260, 2, 182, '2019-01-16 11:50:49', '2019-01-16 11:50:49'),
(261, 2, 183, '2019-01-16 11:50:59', '2019-01-16 11:50:59'),
(262, 10, 183, '2019-01-16 11:50:59', '2019-01-16 11:50:59'),
(263, 2, 184, '2019-01-16 11:51:13', '2019-01-16 11:51:13'),
(264, 2, 185, '2019-01-16 11:51:13', '2019-01-16 11:51:13'),
(265, 10, 185, '2019-01-16 11:51:14', '2019-01-16 11:51:14'),
(266, 2, 186, '2019-01-16 11:51:34', '2019-01-16 11:51:34'),
(267, 9, 187, '2019-01-16 11:51:38', '2019-01-16 11:51:38'),
(268, 10, 187, '2019-01-16 11:51:38', '2019-01-16 11:51:38'),
(269, 2, 188, '2019-01-16 11:52:06', '2019-01-16 11:52:06'),
(270, 2, 189, '2019-01-16 11:52:31', '2019-01-16 11:52:31'),
(271, 2, 190, '2019-01-16 11:52:53', '2019-01-16 11:52:53'),
(272, 10, 190, '2019-01-16 11:52:53', '2019-01-16 11:52:53'),
(273, 2, 191, '2019-01-16 11:53:13', '2019-01-16 11:53:13'),
(274, 10, 191, '2019-01-16 11:53:13', '2019-01-16 11:53:13'),
(275, 2, 192, '2019-01-16 11:53:27', '2019-01-16 11:53:27'),
(276, 10, 192, '2019-01-16 11:53:27', '2019-01-16 11:53:27'),
(277, 2, 193, '2019-01-16 11:53:42', '2019-01-16 11:53:42'),
(278, 10, 193, '2019-01-16 11:53:42', '2019-01-16 11:53:42'),
(279, 2, 194, '2019-01-16 11:53:57', '2019-01-16 11:53:57'),
(280, 10, 194, '2019-01-16 11:53:57', '2019-01-16 11:53:57'),
(281, 2, 195, '2019-01-16 11:58:50', '2019-01-16 11:58:50'),
(282, 2, 196, '2019-01-16 11:59:12', '2019-01-16 11:59:12'),
(283, 2, 197, '2019-01-16 11:59:31', '2019-01-16 11:59:31'),
(284, 2, 198, '2019-01-16 11:59:51', '2019-01-16 11:59:51'),
(285, 2, 199, '2019-01-16 12:00:14', '2019-01-16 12:00:14'),
(286, 2, 200, '2019-01-16 12:00:37', '2019-01-16 12:00:37'),
(287, 2, 201, '2019-01-16 14:26:27', '2019-01-16 14:26:27'),
(288, 10, 201, '2019-01-16 14:26:27', '2019-01-16 14:26:27'),
(289, 2, 202, '2019-01-16 14:53:58', '2019-01-16 14:53:58'),
(290, 10, 202, '2019-01-16 14:53:58', '2019-01-16 14:53:58'),
(291, 2, 203, '2019-01-16 14:56:19', '2019-01-16 14:56:19'),
(292, 10, 203, '2019-01-16 14:56:19', '2019-01-16 14:56:19'),
(293, 2, 204, '2019-01-16 14:56:39', '2019-01-16 14:56:39'),
(294, 10, 204, '2019-01-16 14:56:39', '2019-01-16 14:56:39'),
(295, 2, 205, '2019-01-16 14:57:57', '2019-01-16 14:57:57'),
(296, 10, 205, '2019-01-16 14:57:57', '2019-01-16 14:57:57'),
(297, 2, 206, '2019-01-16 14:58:22', '2019-01-16 14:58:22'),
(298, 10, 206, '2019-01-16 14:58:22', '2019-01-16 14:58:22'),
(299, 2, 207, '2019-01-16 14:58:52', '2019-01-16 14:58:52'),
(300, 10, 207, '2019-01-16 14:58:52', '2019-01-16 14:58:52'),
(301, 2, 208, '2019-01-16 14:59:38', '2019-01-16 14:59:38'),
(302, 10, 208, '2019-01-16 14:59:38', '2019-01-16 14:59:38'),
(303, 2, 209, '2019-01-16 15:07:15', '2019-01-16 15:07:15'),
(304, 10, 209, '2019-01-16 15:07:15', '2019-01-16 15:07:15'),
(305, 2, 210, '2019-01-16 15:07:28', '2019-01-16 15:07:28'),
(306, 2, 211, '2019-01-16 15:07:36', '2019-01-16 15:07:36'),
(307, 10, 211, '2019-01-16 15:07:36', '2019-01-16 15:07:36'),
(308, 2, 212, '2019-01-16 15:07:51', '2019-01-16 15:07:51'),
(309, 2, 213, '2019-01-16 15:07:51', '2019-01-16 15:07:51'),
(310, 10, 213, '2019-01-16 15:07:51', '2019-01-16 15:07:51'),
(311, 2, 214, '2019-01-16 15:08:04', '2019-01-16 15:08:04'),
(312, 10, 214, '2019-01-16 15:08:04', '2019-01-16 15:08:04'),
(313, 2, 215, '2019-01-16 15:08:13', '2019-01-16 15:08:13'),
(314, 2, 216, '2019-01-16 15:08:21', '2019-01-16 15:08:21'),
(315, 10, 216, '2019-01-16 15:08:21', '2019-01-16 15:08:21'),
(316, 2, 217, '2019-01-16 15:08:36', '2019-01-16 15:08:36'),
(317, 2, 218, '2019-01-16 15:08:56', '2019-01-16 15:08:56'),
(318, 2, 219, '2019-01-16 15:09:24', '2019-01-16 15:09:24'),
(319, 2, 220, '2019-01-16 15:34:10', '2019-01-16 15:34:10'),
(320, 10, 220, '2019-01-16 15:34:10', '2019-01-16 15:34:10'),
(321, 2, 221, '2019-01-16 15:34:20', '2019-01-16 15:34:20'),
(322, 10, 221, '2019-01-16 15:34:20', '2019-01-16 15:34:20'),
(323, 2, 222, '2019-01-16 15:34:32', '2019-01-16 15:34:32'),
(324, 10, 222, '2019-01-16 15:34:32', '2019-01-16 15:34:32'),
(325, 2, 223, '2019-01-16 15:35:42', '2019-01-16 15:35:42'),
(326, 10, 223, '2019-01-16 15:35:42', '2019-01-16 15:35:42'),
(327, 2, 224, '2019-01-16 15:35:55', '2019-01-16 15:35:55'),
(328, 10, 224, '2019-01-16 15:35:55', '2019-01-16 15:35:55'),
(329, 2, 225, '2019-01-16 15:38:19', '2019-01-16 15:38:19'),
(330, 10, 225, '2019-01-16 15:38:19', '2019-01-16 15:38:19'),
(331, 2, 226, '2019-01-16 15:38:30', '2019-01-16 15:38:30'),
(332, 10, 226, '2019-01-16 15:38:30', '2019-01-16 15:38:30'),
(333, 2, 227, '2019-01-16 15:38:46', '2019-01-16 15:38:46'),
(334, 10, 227, '2019-01-16 15:38:46', '2019-01-16 15:38:46'),
(335, 2, 228, '2019-01-16 15:39:01', '2019-01-16 15:39:01'),
(336, 10, 228, '2019-01-16 15:39:01', '2019-01-16 15:39:01'),
(337, 2, 229, '2019-01-16 15:39:19', '2019-01-16 15:39:19'),
(338, 10, 229, '2019-01-16 15:39:19', '2019-01-16 15:39:19'),
(339, 2, 230, '2019-01-16 15:39:30', '2019-01-16 15:39:30'),
(340, 10, 230, '2019-01-16 15:39:30', '2019-01-16 15:39:30'),
(341, 2, 231, '2019-01-16 15:39:49', '2019-01-16 15:39:49'),
(342, 10, 231, '2019-01-16 15:39:49', '2019-01-16 15:39:49'),
(343, 2, 232, '2019-01-16 15:40:03', '2019-01-16 15:40:03'),
(344, 10, 232, '2019-01-16 15:40:03', '2019-01-16 15:40:03'),
(345, 2, 233, '2019-01-16 15:40:19', '2019-01-16 15:40:19'),
(346, 10, 233, '2019-01-16 15:40:19', '2019-01-16 15:40:19'),
(347, 2, 234, '2019-01-16 15:40:35', '2019-01-16 15:40:35'),
(348, 10, 234, '2019-01-16 15:40:35', '2019-01-16 15:40:35'),
(349, 2, 235, '2019-01-16 15:41:53', '2019-01-16 15:41:53'),
(350, 10, 235, '2019-01-16 15:41:53', '2019-01-16 15:41:53'),
(351, 2, 236, '2019-01-16 15:42:04', '2019-01-16 15:42:04'),
(352, 10, 236, '2019-01-16 15:42:04', '2019-01-16 15:42:04'),
(353, 2, 237, '2019-01-16 15:42:34', '2019-01-16 15:42:34'),
(354, 10, 237, '2019-01-16 15:42:34', '2019-01-16 15:42:34'),
(355, 2, 238, '2019-01-16 15:42:46', '2019-01-16 15:42:46'),
(356, 2, 239, '2019-01-17 10:18:13', '2019-01-17 10:18:13'),
(357, 2, 240, '2019-01-17 10:18:40', '2019-01-17 10:18:40'),
(358, 2, 241, '2019-01-17 10:20:09', '2019-01-17 10:20:09'),
(359, 2, 242, '2019-01-17 10:20:57', '2019-01-17 10:20:57'),
(360, 2, 243, '2019-01-17 10:21:56', '2019-01-17 10:21:56'),
(361, 2, 244, '2019-01-17 10:22:30', '2019-01-17 10:22:30'),
(362, 2, 245, '2019-01-17 10:23:29', '2019-01-17 10:23:29'),
(363, 2, 246, '2019-01-17 10:25:55', '2019-01-17 10:25:55'),
(364, 2, 247, '2019-01-17 11:09:23', '2019-01-17 11:09:23'),
(365, 2, 248, '2019-01-17 11:09:47', '2019-01-17 11:09:47'),
(366, 2, 249, '2019-01-17 11:10:09', '2019-01-17 11:10:09'),
(367, 2, 250, '2019-01-17 11:10:35', '2019-01-17 11:10:35'),
(368, 2, 251, '2019-01-17 11:13:16', '2019-01-17 11:13:16'),
(369, 2, 252, '2019-01-17 11:13:44', '2019-01-17 11:13:44'),
(370, 2, 253, '2019-01-17 11:14:26', '2019-01-17 11:14:26'),
(371, 2, 254, '2019-01-17 11:14:54', '2019-01-17 11:14:54'),
(372, 2, 255, '2019-01-17 11:15:28', '2019-01-17 11:15:28'),
(373, 2, 256, '2019-01-17 11:15:53', '2019-01-17 11:15:53'),
(374, 2, 257, '2019-01-17 11:28:14', '2019-01-17 11:28:14'),
(375, 2, 258, '2019-01-17 11:28:35', '2019-01-17 11:28:35'),
(376, 2, 259, '2019-01-17 11:28:54', '2019-01-17 11:28:54'),
(377, 2, 260, '2019-01-17 11:29:21', '2019-01-17 11:29:21'),
(378, 2, 261, '2019-01-17 11:29:42', '2019-01-17 11:29:42'),
(379, 2, 262, '2019-01-17 11:30:17', '2019-01-17 11:30:17'),
(380, 2, 263, '2019-01-17 11:30:32', '2019-01-17 11:30:32'),
(381, 10, 263, '2019-01-17 11:30:32', '2019-01-17 11:30:32'),
(382, 2, 264, '2019-01-17 11:30:54', '2019-01-17 11:30:54'),
(383, 2, 265, '2019-01-17 11:30:58', '2019-01-17 11:30:58'),
(384, 10, 265, '2019-01-17 11:30:58', '2019-01-17 11:30:58'),
(385, 2, 266, '2019-01-17 11:31:15', '2019-01-17 11:31:15'),
(386, 2, 267, '2019-01-17 11:31:15', '2019-01-17 11:31:15'),
(387, 10, 267, '2019-01-17 11:31:15', '2019-01-17 11:31:15'),
(388, 2, 268, '2019-01-17 11:31:31', '2019-01-17 11:31:31'),
(389, 10, 268, '2019-01-17 11:31:31', '2019-01-17 11:31:31'),
(390, 2, 269, '2019-01-17 11:31:41', '2019-01-17 11:31:41'),
(391, 2, 270, '2019-01-17 11:32:00', '2019-01-17 11:32:00'),
(392, 10, 270, '2019-01-17 11:32:00', '2019-01-17 11:32:00'),
(393, 2, 271, '2019-01-17 11:32:09', '2019-01-17 11:32:09'),
(394, 2, 272, '2019-01-17 11:32:15', '2019-01-17 11:32:15'),
(395, 10, 272, '2019-01-17 11:32:15', '2019-01-17 11:32:15'),
(396, 2, 273, '2019-01-17 11:32:41', '2019-01-17 11:32:41'),
(397, 10, 273, '2019-01-17 11:32:41', '2019-01-17 11:32:41'),
(398, 2, 274, '2019-01-17 11:32:42', '2019-01-17 11:32:42'),
(399, 2, 275, '2019-01-17 11:33:05', '2019-01-17 11:33:05'),
(400, 10, 275, '2019-01-17 11:33:05', '2019-01-17 11:33:05'),
(401, 2, 276, '2019-01-17 11:33:18', '2019-01-17 11:33:18'),
(402, 2, 277, '2019-01-17 11:37:33', '2019-01-17 11:37:33'),
(403, 2, 278, '2019-01-17 11:37:41', '2019-01-17 11:37:41'),
(404, 10, 278, '2019-01-17 11:37:41', '2019-01-17 11:37:41'),
(405, 2, 279, '2019-01-17 11:37:58', '2019-01-17 11:37:58'),
(406, 10, 279, '2019-01-17 11:37:58', '2019-01-17 11:37:58'),
(407, 2, 280, '2019-01-17 11:37:58', '2019-01-17 11:37:58'),
(408, 2, 281, '2019-01-17 11:38:18', '2019-01-17 11:38:18'),
(409, 10, 281, '2019-01-17 11:38:18', '2019-01-17 11:38:18'),
(410, 2, 282, '2019-01-17 11:38:20', '2019-01-17 11:38:20'),
(411, 2, 283, '2019-01-17 11:38:42', '2019-01-17 11:38:42'),
(412, 2, 284, '2019-01-17 11:39:05', '2019-01-17 11:39:05'),
(413, 2, 285, '2019-01-17 11:39:25', '2019-01-17 11:39:25'),
(414, 2, 286, '2019-01-17 11:39:50', '2019-01-17 11:39:50'),
(415, 2, 287, '2019-01-17 11:40:11', '2019-01-17 11:40:11'),
(416, 2, 288, '2019-01-17 11:40:36', '2019-01-17 11:40:36'),
(417, 2, 289, '2019-01-17 11:41:02', '2019-01-17 11:41:02'),
(418, 2, 290, '2019-01-17 11:41:22', '2019-01-17 11:41:22'),
(419, 2, 291, '2019-01-17 11:41:36', '2019-01-17 11:41:36'),
(420, 10, 291, '2019-01-17 11:41:36', '2019-01-17 11:41:36'),
(421, 2, 292, '2019-01-17 11:41:43', '2019-01-17 11:41:43'),
(422, 2, 293, '2019-01-17 11:41:56', '2019-01-17 11:41:56'),
(423, 10, 293, '2019-01-17 11:41:56', '2019-01-17 11:41:56'),
(424, 2, 294, '2019-01-17 11:42:04', '2019-01-17 11:42:04'),
(425, 2, 295, '2019-01-17 11:42:16', '2019-01-17 11:42:16'),
(426, 10, 295, '2019-01-17 11:42:16', '2019-01-17 11:42:16'),
(427, 2, 296, '2019-01-17 11:42:26', '2019-01-17 11:42:26'),
(428, 2, 297, '2019-01-17 11:42:37', '2019-01-17 11:42:37'),
(429, 10, 297, '2019-01-17 11:42:37', '2019-01-17 11:42:37'),
(430, 2, 298, '2019-01-17 11:42:52', '2019-01-17 11:42:52'),
(431, 2, 299, '2019-01-17 11:43:52', '2019-01-17 11:43:52'),
(432, 10, 299, '2019-01-17 11:43:52', '2019-01-17 11:43:52'),
(433, 2, 300, '2019-01-17 11:44:13', '2019-01-17 11:44:13'),
(434, 10, 300, '2019-01-17 11:44:13', '2019-01-17 11:44:13'),
(435, 2, 301, '2019-01-17 11:49:26', '2019-01-17 11:49:26'),
(436, 10, 302, '2019-01-23 13:15:28', '2019-01-23 13:15:28'),
(437, 10, 303, '2019-01-23 13:15:42', '2019-01-23 13:15:42'),
(438, 10, 304, '2019-01-23 13:15:55', '2019-01-23 13:15:55'),
(439, 10, 305, '2019-01-23 13:17:28', '2019-01-23 13:17:28'),
(440, 10, 306, '2019-01-23 13:35:09', '2019-01-23 13:35:09'),
(441, 10, 307, '2019-01-23 13:37:52', '2019-01-23 13:37:52'),
(442, 10, 308, '2019-01-23 13:38:21', '2019-01-23 13:38:21'),
(443, 10, 309, '2019-01-23 13:45:43', '2019-01-23 13:45:43'),
(444, 10, 310, '2019-01-23 13:46:06', '2019-01-23 13:46:06'),
(445, 10, 311, '2019-01-23 13:55:39', '2019-01-23 13:55:39'),
(446, 10, 312, '2019-01-23 13:56:12', '2019-01-23 13:56:12'),
(447, 10, 313, '2019-01-23 13:56:39', '2019-01-23 13:56:39'),
(448, 10, 314, '2019-01-23 13:57:17', '2019-01-23 13:57:17'),
(449, 10, 50, '2019-01-23 14:10:05', '2019-01-23 14:10:05'),
(450, 10, 53, '2019-01-23 14:11:33', '2019-01-23 14:11:33'),
(451, 10, 54, '2019-01-23 14:11:48', '2019-01-23 14:11:48'),
(452, 10, 58, '2019-01-23 14:12:49', '2019-01-23 14:12:49'),
(453, 10, 59, '2019-01-23 14:34:12', '2019-01-23 14:34:12'),
(454, 10, 61, '2019-01-23 14:36:05', '2019-01-23 14:36:05'),
(455, 10, 63, '2019-01-23 14:36:51', '2019-01-23 14:36:51'),
(456, 10, 65, '2019-01-23 14:37:27', '2019-01-23 14:37:27'),
(457, 10, 67, '2019-01-23 14:37:59', '2019-01-23 14:37:59'),
(458, 10, 68, '2019-01-23 14:39:53', '2019-01-23 14:39:53'),
(459, 10, 70, '2019-01-23 14:41:07', '2019-01-23 14:41:07'),
(460, 10, 72, '2019-01-23 14:41:47', '2019-01-23 14:41:47'),
(461, 10, 74, '2019-01-23 14:42:21', '2019-01-23 14:42:21'),
(462, 10, 75, '2019-01-23 14:42:32', '2019-01-23 14:42:32'),
(463, 10, 77, '2019-01-23 14:43:02', '2019-01-23 14:43:02'),
(464, 10, 78, '2019-01-23 14:43:14', '2019-01-23 14:43:14'),
(465, 10, 79, '2019-01-23 14:50:04', '2019-01-23 14:50:04'),
(466, 10, 80, '2019-01-23 14:50:15', '2019-01-23 14:50:15'),
(467, 10, 81, '2019-01-23 14:50:28', '2019-01-23 14:50:28'),
(468, 10, 82, '2019-01-23 14:50:53', '2019-01-23 14:50:53'),
(469, 10, 83, '2019-01-23 14:51:06', '2019-01-23 14:51:06'),
(470, 10, 84, '2019-01-23 14:51:18', '2019-01-23 14:51:18'),
(471, 10, 85, '2019-01-23 14:51:30', '2019-01-23 14:51:30'),
(472, 10, 86, '2019-01-23 14:52:14', '2019-01-23 14:52:14'),
(473, 10, 88, '2019-01-23 14:52:55', '2019-01-23 14:52:55'),
(474, 10, 315, '2019-01-29 10:56:03', '2019-01-29 10:56:03'),
(475, 10, 316, '2019-01-29 10:57:01', '2019-01-29 10:57:01'),
(476, 10, 317, '2019-01-29 11:01:42', '2019-01-29 11:01:42'),
(477, 10, 318, '2019-01-29 11:01:42', '2019-01-29 11:01:42'),
(478, 10, 319, '2019-01-29 11:02:35', '2019-01-29 11:02:35'),
(479, 10, 320, '2019-01-29 11:03:21', '2019-01-29 11:03:21'),
(480, 10, 321, '2019-01-29 11:08:48', '2019-01-29 11:08:48'),
(481, 10, 322, '2019-01-29 11:09:31', '2019-01-29 11:09:31'),
(482, 10, 323, '2019-01-29 11:10:11', '2019-01-29 11:10:11'),
(483, 10, 324, '2019-01-29 11:11:01', '2019-01-29 11:11:01'),
(484, 10, 325, '2019-01-29 11:11:41', '2019-01-29 11:11:41'),
(485, 10, 326, '2019-01-29 11:14:00', '2019-01-29 11:14:00'),
(486, 10, 327, '2019-01-29 11:14:40', '2019-01-29 11:14:40'),
(487, 10, 328, '2019-01-29 11:16:22', '2019-01-29 11:16:22'),
(488, 10, 329, '2019-01-29 11:17:09', '2019-01-29 11:17:09'),
(489, 10, 330, '2019-01-29 11:17:37', '2019-01-29 11:17:37'),
(490, 10, 331, '2019-01-29 11:18:48', '2019-01-29 11:18:48'),
(491, 10, 332, '2019-01-29 11:19:15', '2019-01-29 11:19:15'),
(492, 10, 333, '2019-01-29 11:20:11', '2019-01-29 11:20:11'),
(493, 10, 334, '2019-01-29 11:20:34', '2019-01-29 11:20:34'),
(494, 10, 335, '2019-01-29 11:22:08', '2019-01-29 11:22:08'),
(495, 10, 336, '2019-01-29 11:22:37', '2019-01-29 11:22:37'),
(496, 10, 337, '2019-01-29 11:23:05', '2019-01-29 11:23:05'),
(497, 10, 338, '2019-01-29 11:23:33', '2019-01-29 11:23:33'),
(498, 10, 339, '2019-01-29 11:23:55', '2019-01-29 11:23:55'),
(499, 10, 340, '2019-01-29 11:24:26', '2019-01-29 11:24:26'),
(500, 10, 341, '2019-01-29 11:24:50', '2019-01-29 11:24:50'),
(501, 10, 342, '2019-01-29 11:25:11', '2019-01-29 11:25:11'),
(502, 10, 343, '2019-01-29 11:25:41', '2019-01-29 11:25:41'),
(503, 10, 344, '2019-01-29 11:26:13', '2019-01-29 11:26:13'),
(504, 10, 345, '2019-01-29 11:28:01', '2019-01-29 11:28:01'),
(505, 10, 346, '2019-01-29 11:28:31', '2019-01-29 11:28:31'),
(506, 10, 347, '2019-01-29 11:28:56', '2019-01-29 11:28:56'),
(507, 10, 348, '2019-01-29 11:29:29', '2019-01-29 11:29:29'),
(508, 10, 349, '2019-01-29 11:29:55', '2019-01-29 11:29:55'),
(509, 10, 350, '2019-01-29 11:30:46', '2019-01-29 11:30:46'),
(510, 10, 351, '2019-01-29 11:31:34', '2019-01-29 11:31:34'),
(511, 10, 352, '2019-01-29 11:32:16', '2019-01-29 11:32:16'),
(512, 10, 353, '2019-01-29 11:32:36', '2019-01-29 11:32:36'),
(513, 10, 354, '2019-01-29 11:33:00', '2019-01-29 11:33:00'),
(514, 10, 355, '2019-01-29 11:41:46', '2019-01-29 11:41:46'),
(515, 10, 356, '2019-01-29 11:43:08', '2019-01-29 11:43:08'),
(516, 10, 357, '2019-01-29 11:43:57', '2019-01-29 11:43:57'),
(517, 12, 358, '2019-01-29 13:29:23', '2019-01-29 13:29:23'),
(518, 13, 359, '2019-01-29 13:29:58', '2019-01-29 13:29:58'),
(519, 14, 360, '2019-01-29 13:30:26', '2019-01-29 13:30:26'),
(520, 15, 361, '2019-01-29 13:30:58', '2019-01-29 13:30:58'),
(521, 16, 362, '2019-01-29 13:31:34', '2019-01-29 13:31:34'),
(522, 17, 363, '2019-01-29 13:31:58', '2019-01-29 13:31:58'),
(523, 18, 364, '2019-01-29 13:32:35', '2019-01-29 13:32:35'),
(524, 19, 365, '2019-01-29 13:33:04', '2019-01-29 13:33:04'),
(525, 20, 366, '2019-01-29 13:33:41', '2019-01-29 13:33:41'),
(526, 21, 367, '2019-01-29 13:34:08', '2019-01-29 13:34:08'),
(527, 16, 368, '2019-01-30 10:24:46', '2019-01-30 10:24:46'),
(528, 16, 369, '2019-01-30 10:25:09', '2019-01-30 10:25:09'),
(529, 16, 370, '2019-01-30 10:25:35', '2019-01-30 10:25:35'),
(530, 16, 371, '2019-01-30 10:26:22', '2019-01-30 10:26:22'),
(531, 16, 372, '2019-01-30 10:26:59', '2019-01-30 10:26:59'),
(532, 16, 373, '2019-01-30 10:27:40', '2019-01-30 10:27:40'),
(533, 16, 374, '2019-01-30 10:28:10', '2019-01-30 10:28:10'),
(534, 16, 375, '2019-01-30 10:28:36', '2019-01-30 10:28:36'),
(535, 16, 376, '2019-01-30 10:29:02', '2019-01-30 10:29:02'),
(536, 16, 377, '2019-01-30 10:29:29', '2019-01-30 10:29:29'),
(537, 10, 90, '2019-01-30 10:36:58', '2019-01-30 10:36:58'),
(538, 10, 91, '2019-01-30 10:37:20', '2019-01-30 10:37:20'),
(539, 10, 92, '2019-01-30 10:37:31', '2019-01-30 10:37:31'),
(540, 10, 93, '2019-01-30 10:37:42', '2019-01-30 10:37:42'),
(541, 10, 95, '2019-01-30 10:38:16', '2019-01-30 10:38:16'),
(542, 16, 378, '2019-01-30 10:38:29', '2019-01-30 10:38:29'),
(543, 16, 379, '2019-01-30 10:39:07', '2019-01-30 10:39:07'),
(544, 16, 380, '2019-01-30 10:39:31', '2019-01-30 10:39:31'),
(545, 16, 381, '2019-01-30 10:40:00', '2019-01-30 10:40:00'),
(546, 16, 382, '2019-01-30 10:40:32', '2019-01-30 10:40:32'),
(547, 16, 383, '2019-01-30 10:40:57', '2019-01-30 10:40:57'),
(548, 10, 106, '2019-01-30 10:46:46', '2019-01-30 10:46:46'),
(549, 10, 107, '2019-01-30 10:47:02', '2019-01-30 10:47:02'),
(550, 10, 108, '2019-01-30 10:47:17', '2019-01-30 10:47:17'),
(551, 10, 109, '2019-01-30 10:48:27', '2019-01-30 10:48:27'),
(552, 10, 110, '2019-01-30 10:48:50', '2019-01-30 10:48:50'),
(553, 22, 384, '2019-01-30 10:49:03', '2019-01-30 10:49:03'),
(554, 10, 111, '2019-01-30 10:49:09', '2019-01-30 10:49:09'),
(555, 10, 112, '2019-01-30 10:49:24', '2019-01-30 10:49:24'),
(556, 23, 385, '2019-01-30 10:49:46', '2019-01-30 10:49:46'),
(557, 10, 113, '2019-01-30 10:49:48', '2019-01-30 10:49:48'),
(558, 10, 114, '2019-01-30 10:50:05', '2019-01-30 10:50:05'),
(559, 24, 386, '2019-01-30 10:50:13', '2019-01-30 10:50:13'),
(560, 25, 387, '2019-01-30 10:50:50', '2019-01-30 10:50:50'),
(561, 10, 115, '2019-01-30 10:50:55', '2019-01-30 10:50:55'),
(562, 26, 388, '2019-01-30 10:51:19', '2019-01-30 10:51:19'),
(563, 10, 116, '2019-01-30 10:51:24', '2019-01-30 10:51:24'),
(564, 10, 117, '2019-01-30 10:51:44', '2019-01-30 10:51:44'),
(565, 27, 389, '2019-01-30 10:51:50', '2019-01-30 10:51:50'),
(566, 10, 118, '2019-01-30 10:52:02', '2019-01-30 10:52:02'),
(567, 28, 393, '2019-01-30 10:54:33', '2019-01-30 10:54:33'),
(568, 10, 119, '2019-01-30 10:55:02', '2019-01-30 10:55:02'),
(569, 22, 391, '2019-01-30 10:55:57', '2019-01-30 10:55:57'),
(570, 29, 394, '2019-01-30 10:58:03', '2019-01-30 10:58:03'),
(571, 10, 120, '2019-01-30 10:58:24', '2019-01-30 10:58:24'),
(572, 30, 395, '2019-01-30 10:58:32', '2019-01-30 10:58:32'),
(573, 10, 121, '2019-01-30 10:58:47', '2019-01-30 10:58:47'),
(574, 10, 122, '2019-01-30 10:59:27', '2019-01-30 10:59:27'),
(575, 31, 397, '2019-01-30 10:59:48', '2019-01-30 10:59:48'),
(576, 10, 123, '2019-01-30 11:00:09', '2019-01-30 11:00:09'),
(577, 10, 124, '2019-01-30 11:00:33', '2019-01-30 11:00:33'),
(578, 10, 125, '2019-01-30 11:00:49', '2019-01-30 11:00:49'),
(579, 10, 126, '2019-01-30 11:01:08', '2019-01-30 11:01:08'),
(580, 32, 399, '2019-01-30 11:01:43', '2019-01-30 11:01:43'),
(581, 10, 127, '2019-01-30 11:05:29', '2019-01-30 11:05:29'),
(582, 10, 129, '2019-01-30 11:07:20', '2019-01-30 11:07:20'),
(583, 10, 131, '2019-01-30 11:08:01', '2019-01-30 11:08:01'),
(584, 10, 132, '2019-01-30 11:08:22', '2019-01-30 11:08:22'),
(585, 10, 400, '2019-01-30 11:28:29', '2019-01-30 11:28:29'),
(586, 2, 160, '2019-01-31 11:02:30', '2019-01-31 11:02:30'),
(587, 2, 161, '2019-01-31 11:03:10', '2019-01-31 11:03:10');

-- --------------------------------------------------------

--
-- Table structure for table `tb_cart`
--

CREATE TABLE `tb_cart` (
  `id_cart` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `note` text,
  `biaya_delivery` bigint(20) DEFAULT NULL,
  `total` bigint(20) DEFAULT NULL,
  `grand_total` bigint(20) DEFAULT NULL,
  `opsi_pengiriman` tinyint(2) DEFAULT NULL,
  `wilayah` varchar(255) DEFAULT NULL,
  `alamat` text,
  `pick_up_time` time DEFAULT NULL,
  `pick_up_date` date DEFAULT NULL,
  `status_order` varchar(225) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_cart`
--

INSERT INTO `tb_cart` (`id_cart`, `id_user`, `tanggal`, `note`, `biaya_delivery`, `total`, `grand_total`, `opsi_pengiriman`, `wilayah`, `alamat`, `pick_up_time`, `pick_up_date`, `status_order`) VALUES
(6, 3, '2018-10-25', '', 32000, 125000, 157000, NULL, 'Denpasar Timur', 'alamat', '08:57:32', '2018-10-25', 'menunggu'),
(7, 3, '2018-10-25', '', 32000, 120000, 152000, NULL, 'Denpasar Timur', 'almaat', '08:59:22', '2018-10-25', 'menunggu');

-- --------------------------------------------------------

--
-- Table structure for table `tb_cart_detail`
--

CREATE TABLE `tb_cart_detail` (
  `id_detail_cart` int(10) UNSIGNED NOT NULL,
  `id_cart` int(10) UNSIGNED DEFAULT NULL,
  `kue` varchar(225) DEFAULT NULL,
  `deskripsi` varchar(225) DEFAULT NULL,
  `qty` int(2) DEFAULT NULL,
  `harga` bigint(20) DEFAULT NULL,
  `sub_total` bigint(20) DEFAULT NULL,
  `rasa` varchar(225) DEFAULT NULL,
  `huruf` char(10) DEFAULT NULL,
  `angka` int(11) DEFAULT NULL,
  `tulisan_kue` varchar(225) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_cart_detail`
--

INSERT INTO `tb_cart_detail` (`id_detail_cart`, `id_cart`, `kue`, `deskripsi`, `qty`, `harga`, `sub_total`, `rasa`, `huruf`, `angka`, `tulisan_kue`) VALUES
(5, 6, 'Mini Cake', '', 1, 125000, 125000, NULL, NULL, NULL, 'kue'),
(6, 7, 'Number Cake', '', 1, 120000, 120000, NULL, NULL, 123, 'kue');

-- --------------------------------------------------------

--
-- Table structure for table `tb_cart_edible`
--

CREATE TABLE `tb_cart_edible` (
  `id_cart_edible` int(10) UNSIGNED NOT NULL,
  `id_cart` int(10) UNSIGNED DEFAULT NULL,
  `nama_file` varchar(255) DEFAULT NULL,
  `keterangan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_cart_edible`
--

INSERT INTO `tb_cart_edible` (`id_cart_edible`, `id_cart`, `nama_file`, `keterangan`) VALUES
(4, 6, '/storage/edible/6-0-20181025085732.jpg', NULL),
(5, 7, '/storage/edible/7-0-20181025085922.jpg', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_category`
--

CREATE TABLE `tb_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `category` varchar(225) DEFAULT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `link` varchar(225) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `type_home` enum('0','1') DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_category`
--

INSERT INTO `tb_category` (`id`, `category`, `icon`, `link`, `created_at`, `updated_at`, `type_home`) VALUES
(1, 'Animal Theme', 'cutlery', 'tart-cake', NULL, '2019-01-23 03:08:50', '0'),
(2, 'Baby Shark Theme', 'anchor', 'jar-cake', NULL, '2019-01-23 03:10:13', '0'),
(3, 'Barbie Theme', 'coffee', 'slice-cake', NULL, '2019-01-23 03:10:51', '0'),
(5, 'Batman Theme', 'birthday-cake', 'cup-cake', '2018-10-03 10:20:32', '2019-01-23 03:17:24', '0'),
(7, 'Animal Theme', 'birthday-cake', 'animal-theme', '2018-11-06 09:56:23', '2019-01-23 03:43:11', '1'),
(9, 'Baby Shark', 'birthday-cake', 'baby-shark', '2018-11-15 05:36:50', '2019-01-23 03:48:12', '1'),
(11, 'Barbie Theme', 'birthday-cake', 'barbie-theme', '2019-01-11 08:41:13', '2019-01-23 03:43:34', '1'),
(12, 'Boobs Theme', NULL, 'number-cake', '2019-01-11 08:41:27', '2019-01-23 03:17:42', '0'),
(13, 'Batman Theme', 'birthday-cake', 'batman-theme', '2019-01-11 08:54:28', '2019-01-23 03:43:45', '1'),
(14, 'Bus Tayo Theme', NULL, 'mini-cake', '2019-01-11 08:54:41', '2019-01-23 03:17:50', '0'),
(15, 'Boobs Theme', 'birthday-cake', 'boobs-theme', '2019-01-11 09:25:31', '2019-01-23 03:43:52', '1'),
(16, 'Cake', NULL, 'name-cake', '2019-01-11 09:25:42', '2019-01-23 03:17:59', '0'),
(17, 'Car Theme', NULL, 'car-theme', '2019-01-23 03:19:22', '2019-01-23 03:19:22', '0'),
(18, 'Cartoon Mix', NULL, 'cartoon-mix', '2019-01-23 03:19:36', '2019-01-23 03:19:36', '0'),
(19, 'Clothes Theme', NULL, 'clothes-theme', '2019-01-23 03:19:42', '2019-01-23 03:19:42', '0'),
(20, 'Edible Pic', NULL, 'edible-pic', '2019-01-23 03:22:21', '2019-01-23 03:22:21', '0'),
(21, 'Flower Theme', NULL, 'flower-theme', '2019-01-23 03:22:31', '2019-01-23 03:22:31', '0'),
(22, 'Football Theme', NULL, 'football-theme', '2019-01-23 03:22:41', '2019-01-23 03:22:41', '0'),
(23, 'Frozen Theme', NULL, 'frozen-theme', '2019-01-23 03:23:32', '2019-01-23 03:23:32', '0'),
(24, 'Hello Kitty Theme', NULL, 'hello-kitty-theme', '2019-01-23 03:23:40', '2019-01-23 03:23:40', '0'),
(25, 'I Heart U Cake', NULL, 'i-heart-u-cake', '2019-01-23 03:24:02', '2019-01-23 03:24:02', '0'),
(26, 'Little Pony Theme', NULL, 'little-pony-theme', '2019-01-23 03:25:07', '2019-01-23 03:25:07', '0'),
(27, 'McQueen Theme', NULL, 'mcqueen-theme', '2019-01-23 03:25:13', '2019-01-23 03:25:13', '0'),
(28, 'Minnie Mouse Theme', NULL, 'minnie-mouse-theme', '2019-01-23 03:25:34', '2019-01-23 03:25:34', '0'),
(29, 'MixFruit Theme', NULL, 'mixfruit-theme', '2019-01-23 03:27:40', '2019-01-23 03:27:40', '0'),
(30, 'Name Cake', NULL, 'name-cake', '2019-01-23 03:27:52', '2019-01-23 03:27:52', '0'),
(31, 'Number Cake', NULL, 'number-cake', '2019-01-23 03:27:59', '2019-01-23 03:27:59', '0'),
(32, 'Princess Theme', NULL, 'princess-theme', '2019-01-23 03:28:38', '2019-01-23 03:28:38', '0'),
(33, 'Request Design', NULL, 'request-design', '2019-01-23 03:28:56', '2019-01-23 03:28:56', '0'),
(34, 'Spiderman Theme', NULL, 'spiderman-theme', '2019-01-23 03:29:09', '2019-01-23 03:29:09', '0'),
(35, 'Superhero', NULL, 'superhero', '2019-01-23 03:32:56', '2019-01-23 03:32:56', '0'),
(36, 'Transformer Theme', NULL, 'transformer-theme', '2019-01-23 03:33:09', '2019-01-23 03:33:09', '0'),
(37, 'Ultramen Theme', NULL, 'ultramen-theme', '2019-01-23 03:33:21', '2019-01-23 03:33:21', '0'),
(38, 'Unicorn Theme', NULL, 'unicorn-theme', '2019-01-23 03:33:27', '2019-01-23 03:33:27', '0'),
(40, 'Bus Tayo', 'birthday-cake', 'bus-tayo', '2019-01-23 03:49:05', '2019-01-23 03:49:05', '1'),
(41, 'Cake', 'birthday-cake', 'cake', '2019-01-23 03:49:22', '2019-01-23 03:49:22', '1'),
(42, 'Car Theme', 'birthday-cake', 'car-theme', '2019-01-23 03:49:25', '2019-01-23 03:49:25', '1'),
(43, 'Cartoon Mix', 'birthday-cake', 'cartoon-mix', '2019-01-23 03:49:40', '2019-01-23 03:49:40', '1'),
(44, 'Clothes Theme', 'birthday-cake', 'clothes-theme', '2019-01-23 03:49:57', '2019-01-23 03:49:57', '1'),
(45, 'Edible Pic', 'birthday-cake', 'edible-pic', '2019-01-23 03:51:20', '2019-01-23 03:51:20', '1'),
(46, 'Flower Theme', 'birthday-cake', 'flower-theme', '2019-01-23 03:51:30', '2019-01-23 03:51:30', '1'),
(47, 'Football Theme', 'birthday-cake', 'football-theme', '2019-01-23 03:51:49', '2019-01-23 03:51:49', '1'),
(48, 'Hello Kitty', 'birthday-cake', 'hello-kitty', '2019-01-23 03:52:06', '2019-01-23 03:52:06', '1'),
(49, 'Frozen Theme', 'birthday-cake', 'frozen-theme', '2019-01-23 03:52:16', '2019-01-23 03:52:16', '1'),
(50, 'I Heart U', 'birthday-cake', 'i-heart-u', '2019-01-23 03:53:08', '2019-01-23 03:53:08', '1'),
(51, 'Little Pony', 'birthday-cake', 'little-pony', '2019-01-23 03:53:26', '2019-01-23 03:55:03', '1'),
(52, 'McQueen Theme', 'birthday-cake', 'mcqueen-theme', '2019-01-23 03:53:38', '2019-01-23 03:53:38', '1'),
(53, 'Minnie Mouse', 'birthday-cake', 'minnie-mouse', '2019-01-23 03:53:41', '2019-01-23 03:53:41', '1'),
(54, 'Mix Fruit', 'birthday-cake', 'mix-fruit', '2019-01-23 03:54:01', '2019-01-23 03:54:01', '1'),
(55, 'Name Cake', 'birthday-cake', 'name-cake', '2019-01-23 03:56:31', '2019-01-23 03:56:31', '1'),
(56, 'Number Cake', 'birthday-cake', 'number-cake', '2019-01-23 03:56:33', '2019-01-23 03:56:33', '1'),
(57, 'Princess Theme', 'birthday-cake', 'princess-theme', '2019-01-23 03:56:43', '2019-01-23 03:56:43', '1'),
(58, 'Request Design', 'birthday-cake', 'request-design', '2019-01-23 03:56:57', '2019-01-23 03:56:57', '1'),
(59, 'Spiderman', 'birthday-cake', 'spiderman', '2019-01-23 03:57:06', '2019-01-23 04:03:29', '1'),
(60, 'Superhero', 'birthday-cake', 'superhero', '2019-01-23 03:59:02', '2019-01-23 03:59:02', '1'),
(61, 'Transformer', 'birthday-cake', 'transformer', '2019-01-23 03:59:17', '2019-01-23 03:59:17', '1'),
(62, 'Ultramen Theme', 'birthday-cake', 'ultramen-theme', '2019-01-23 03:59:28', '2019-01-23 03:59:28', '1'),
(63, 'Unicorn Theme', 'birthday-cake', 'unicorn-theme', '2019-01-23 03:59:34', '2019-01-23 03:59:34', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tb_detail_user`
--

CREATE TABLE `tb_detail_user` (
  `id_detail_user` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `nik` varchar(20) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `tlp` varchar(20) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_detail_user`
--

INSERT INTO `tb_detail_user` (`id_detail_user`, `id_user`, `nik`, `tgl_lahir`, `tlp`, `alamat`) VALUES
(1, 1, '0987654321', '2000-01-01', '089765456721', 'jalan gerogak'),
(3, 2, '5105020208920002', '2018-10-11', '085792078364', 'Jalan nuri 2 semarapura'),
(4, 3, '987654323456', '2000-02-10', '098765432', 'sukawati'),
(5, 4, '08368512587872653', '2018-11-08', '085792078364', 'Jalan nuri 2 semarapura'),
(6, 5, '098765432123', '2000-02-22', '086435676453', 'dlodtangluk'),
(7, 6, '13132132123132', '1986-07-07', '081333111445', 'Denpasar'),
(8, 7, '123456789', '2019-06-12', '087863210360', 'Bungkulan'),
(9, 8, '5171015002840011', '1984-02-10', '081999752970', 'Jl.kertadalem Sari I gang kertajaya perumahan kertajaya Residence Blok D3');

-- --------------------------------------------------------

--
-- Table structure for table `tb_history_order`
--

CREATE TABLE `tb_history_order` (
  `id_history_order` int(10) UNSIGNED NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `deskripsi_order` varchar(255) DEFAULT NULL,
  `total` bigint(20) DEFAULT NULL,
  `alamat_delivery` varchar(225) DEFAULT NULL,
  `status` varchar(225) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_history_order`
--

INSERT INTO `tb_history_order` (`id_history_order`, `id_user`, `tanggal`, `deskripsi_order`, `total`, `alamat_delivery`, `status`) VALUES
(1, 1, '2018-10-31', 'hbh', 120000, NULL, 'selesai'),
(2, 1, '2018-11-07', 'we', 45000, 'DEDE', 'selesai'),
(3, 2, '2018-11-05', NULL, 120000, NULL, 'selesai'),
(4, 2, '2018-11-07', NULL, 250000, NULL, 'gagal'),
(5, 2, '2018-11-07', NULL, 120000, NULL, 'gagal');

-- --------------------------------------------------------

--
-- Table structure for table `tb_konfirmasi_pembayaran`
--

CREATE TABLE `tb_konfirmasi_pembayaran` (
  `id_konfirmasi_pembayaran` int(10) UNSIGNED NOT NULL,
  `id_tagihan` int(11) UNSIGNED DEFAULT NULL,
  `tanggal_transfer` date DEFAULT NULL,
  `nama_bank` varchar(100) DEFAULT NULL,
  `atas_nama_bank` varchar(225) DEFAULT NULL,
  `jumlah_transfer` bigint(20) DEFAULT NULL,
  `note` text,
  `bukti_transfer` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_kue`
--

CREATE TABLE `tb_kue` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_category` int(10) UNSIGNED DEFAULT NULL,
  `nama_kue` varchar(225) DEFAULT NULL,
  `ukuran` int(10) UNSIGNED DEFAULT NULL,
  `link` varchar(225) DEFAULT NULL,
  `ukuran_id` int(11) DEFAULT NULL,
  `tinggi` int(11) DEFAULT NULL,
  `pax` int(11) DEFAULT NULL,
  `harga` bigint(20) DEFAULT NULL,
  `id_type` int(10) UNSIGNED DEFAULT NULL,
  `jumlah_rasa` int(11) UNSIGNED DEFAULT NULL,
  `jumlah_huruf` int(11) DEFAULT NULL,
  `jumlah_angka` int(11) DEFAULT NULL,
  `deskripsi_kue` text,
  `rating` int(11) DEFAULT NULL,
  `spesial_kue` enum('0','1') DEFAULT NULL,
  `homepage_kue` enum('0','1') DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `id_home_category` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kue`
--

INSERT INTO `tb_kue` (`id`, `id_category`, `nama_kue`, `ukuran`, `link`, `ukuran_id`, `tinggi`, `pax`, `harga`, `id_type`, `jumlah_rasa`, `jumlah_huruf`, `jumlah_angka`, `deskripsi_kue`, `rating`, `spesial_kue`, `homepage_kue`, `created_at`, `updated_at`, `id_home_category`) VALUES
(28, 1, 'AN001', NULL, 'an001', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-15 02:41:33', '2019-01-23 05:59:13', 7),
(29, 1, 'AN002', NULL, 'an002', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-15 03:14:26', '2019-01-23 05:59:21', 7),
(30, 1, 'AN003', NULL, 'an003', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-15 03:15:07', '2019-01-23 05:59:30', 7),
(31, 1, 'AN004', NULL, 'an004', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-15 03:16:13', '2019-01-23 05:59:41', 7),
(32, 1, 'AN005', NULL, 'an005', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-15 03:17:26', '2019-01-23 05:59:44', 7),
(33, 1, 'AN006', NULL, 'an006', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-15 03:18:20', '2019-01-23 05:59:53', 7),
(34, 1, 'AN007', NULL, 'an007', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-15 03:19:28', '2019-01-23 06:00:00', 7),
(35, 1, 'AN008', NULL, 'an008', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-15 03:20:32', '2019-01-23 06:00:06', 7),
(36, 1, 'AN009', NULL, 'an009', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-15 03:21:24', '2019-01-23 06:00:12', 7),
(37, 1, 'AN010', NULL, 'an010', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-15 03:22:24', '2019-01-23 06:00:18', 7),
(38, 26, 'LP001', NULL, 'lp001', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Loremipsum</p>', 5, '0', '0', '2019-01-15 03:36:36', '2019-01-23 06:01:25', 51),
(39, 26, 'LP003', NULL, 'lp003', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-15 03:37:56', '2019-01-23 06:01:42', 51),
(40, 26, 'LP002', NULL, 'lp002', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-15 03:40:01', '2019-01-23 06:01:58', 51),
(41, 26, 'LP004', NULL, 'lp004', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-15 03:40:32', '2019-01-23 06:02:28', 51),
(42, 26, 'LP005', NULL, 'lp005', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-15 03:41:23', '2019-01-23 06:06:02', 51),
(43, 26, 'LP006', NULL, 'lp006', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-15 03:42:50', '2019-01-23 06:05:44', 51),
(44, 26, 'LP007', NULL, 'lp007', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-15 03:56:26', '2019-01-23 06:05:22', 51),
(45, 26, 'LP008', NULL, 'lp008', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-15 03:58:34', '2019-01-23 06:04:32', 51),
(46, 26, 'LP009', NULL, 'lp009', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-15 04:00:04', '2019-01-23 06:04:14', 51),
(47, 26, 'LP010', NULL, 'lp010', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-15 04:00:28', '2019-01-23 06:03:56', 51),
(48, 26, 'LP011', NULL, 'lp011', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-15 04:03:02', '2019-01-23 06:09:32', 51),
(49, 26, 'LP012', NULL, 'lp012', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-15 04:03:29', '2019-01-23 06:09:48', 51),
(50, 26, 'LP013', NULL, 'lp013', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-15 04:05:03', '2019-01-23 06:10:05', 51),
(51, 1, 'AN011', NULL, 'an011', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<h1>lorem ipsum</h1>', 5, '0', '0', '2019-01-15 05:49:08', '2019-01-23 06:10:45', 7),
(52, 27, 'MQ001', NULL, 'mq001', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-15 05:49:23', '2019-01-23 06:11:09', 52),
(53, 1, 'AN012', NULL, 'an012', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-15 05:49:33', '2019-01-23 06:11:33', 7),
(54, 1, 'AN013', NULL, 'an013', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-15 05:49:54', '2019-01-23 06:11:48', 7),
(55, 27, 'MQ002', NULL, 'mq002', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-15 05:49:57', '2019-01-23 06:12:08', 52),
(57, 27, 'MQ003', NULL, 'mq003', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-15 05:50:22', '2019-01-23 06:12:31', 52),
(58, 1, 'AN014', NULL, 'an014', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-15 05:50:38', '2019-01-23 06:12:49', 7),
(59, 1, 'AN015', NULL, 'an015', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-15 05:51:03', '2019-01-23 06:34:12', 7),
(60, 27, 'MQ004', NULL, 'mq004', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-15 05:51:05', '2019-01-23 06:35:39', 52),
(61, 1, 'AN016', NULL, 'an016', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-15 05:51:26', '2019-01-23 06:36:05', 7),
(62, 27, 'MQ005', NULL, 'mq005', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-15 05:51:37', '2019-01-23 06:36:32', 52),
(63, 1, 'AN017', NULL, 'an017', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-15 05:51:46', '2019-01-23 06:36:51', 7),
(64, 27, 'MQ006', NULL, 'mq006', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-15 05:52:04', '2019-01-23 06:37:13', 52),
(65, 1, 'AN018', NULL, 'an018', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-15 05:52:18', '2019-01-23 06:37:27', 7),
(66, 27, 'MQ007', NULL, 'mq007', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-15 05:52:29', '2019-01-23 06:39:25', 52),
(67, 1, 'AN019', NULL, 'an019', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-15 05:52:41', '2019-01-23 06:39:36', 7),
(68, 1, 'AN020', NULL, 'an020', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-15 05:53:00', '2019-01-23 06:39:53', 7),
(69, 27, 'MQ008', NULL, 'mq008', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-15 05:53:04', '2019-01-23 06:40:54', 52),
(70, 1, 'AN021', NULL, 'an021', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-15 05:53:26', '2019-01-23 06:41:07', 7),
(71, 27, 'MQ009', NULL, 'mq009', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-15 05:53:37', '2019-01-23 06:41:28', 52),
(72, 1, 'AN022', NULL, 'an022', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-15 05:53:48', '2019-01-23 06:41:47', 7),
(73, 27, 'MQ010', NULL, 'mq010', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-15 05:54:07', '2019-01-23 06:42:07', 52),
(74, 1, 'AN023', NULL, 'an023', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-15 05:54:08', '2019-01-23 06:42:21', 7),
(75, 1, 'AN024', NULL, 'an024', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-15 05:54:30', '2019-01-23 06:42:32', 7),
(76, 27, 'MQ011', NULL, 'mq011', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-15 05:54:32', '2019-01-23 06:42:49', 52),
(77, 1, 'AN025', NULL, 'an025', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-15 05:54:56', '2019-01-23 06:43:02', 7),
(78, 1, 'AN026', NULL, 'an026', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-15 05:55:18', '2019-01-23 06:43:14', 7),
(79, 1, 'AN027', NULL, 'an027', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-15 05:55:56', '2019-01-23 06:50:04', 7),
(80, 1, 'AN028', NULL, 'an028', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-15 05:56:31', '2019-01-23 06:50:15', 7),
(81, 1, 'AN029', NULL, 'an029', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-15 05:56:53', '2019-01-23 06:50:28', 7),
(82, 1, 'AN030', NULL, 'an030', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-15 05:57:15', '2019-01-23 06:50:52', 7),
(83, 1, 'AN031', NULL, 'an031', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-15 05:57:40', '2019-01-23 06:51:06', 7),
(84, 1, 'AN032', NULL, 'an032', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-15 05:57:59', '2019-01-23 06:51:18', 7),
(85, 1, 'AN033', NULL, 'an033', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-15 05:58:44', '2019-01-23 06:51:30', 7),
(86, 1, 'AN034', NULL, 'an034', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-15 05:59:10', '2019-01-23 06:52:14', 7),
(87, 28, 'MM001', NULL, 'mm001', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-15 05:59:11', '2019-01-23 06:52:43', 53),
(88, 1, 'AN035', NULL, 'an035', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-15 05:59:41', '2019-01-23 06:52:54', 7),
(89, 28, 'MM002', NULL, 'mm002', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-15 05:59:56', '2019-01-31 06:28:44', 7),
(90, 1, 'AN036', NULL, 'an036', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-15 06:00:03', '2019-01-30 02:40:20', 7),
(91, 1, 'AN037', NULL, 'an037', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-15 06:00:27', '2019-01-30 02:37:20', 7),
(92, 1, 'AN038', NULL, 'an038', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-15 06:00:50', '2019-01-30 02:37:31', 7),
(93, 1, 'AN039', NULL, 'an039', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-15 06:01:11', '2019-01-30 02:37:42', 7),
(94, 28, 'MM003', NULL, 'mm003', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-15 06:01:29', '2019-01-30 02:38:02', 53),
(95, 1, 'AN040', NULL, 'an040', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-15 06:01:31', '2019-01-30 02:38:15', 7),
(96, 28, 'MM004', NULL, 'mm004', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-15 06:02:33', '2019-01-30 02:38:43', 53),
(97, 28, 'MM005', NULL, 'mm005', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-15 06:02:55', '2019-01-30 02:39:01', 53),
(98, 28, 'MM006', NULL, 'mm006', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-15 06:03:54', '2019-01-30 02:39:38', 53),
(99, 28, 'MM007', NULL, 'mm007', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-15 06:04:31', '2019-01-30 02:44:31', 53),
(100, 28, 'MM008', NULL, 'mm008', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-15 06:04:54', '2019-01-30 02:44:48', 53),
(101, 28, 'MM009', NULL, 'mm009', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-15 06:05:19', '2019-01-30 02:45:03', 53),
(102, 28, 'MM010', NULL, 'mm010', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-15 06:05:42', '2019-01-30 02:45:24', 53),
(103, 29, 'MF001', NULL, 'mf001', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-15 06:08:46', '2019-01-30 02:45:42', 54),
(104, 29, 'MF002', NULL, 'mf002', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-15 06:09:10', '2019-01-30 02:46:00', 54),
(105, 29, 'MF003', NULL, 'mf003', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-15 06:09:56', '2019-01-30 02:46:18', 54),
(106, 2, 'BS001', NULL, 'bs001', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 02:43:47', '2019-01-30 02:46:46', 9),
(107, 2, 'BS002', NULL, 'bs002', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 02:44:16', '2019-01-30 02:47:02', 9),
(108, 2, 'BS003', NULL, 'bs003', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 02:44:38', '2019-01-30 02:47:17', 9),
(109, 2, 'BS004', NULL, 'bs004', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 02:45:04', '2019-01-30 02:48:27', 9),
(110, 2, 'BS005', NULL, 'bs005', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 02:45:25', '2019-01-30 02:48:50', 9),
(111, 2, 'BS006', NULL, 'bs006', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 02:45:46', '2019-01-30 02:49:09', 9),
(112, 2, 'BS007', NULL, 'bs007', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 02:46:16', '2019-01-30 02:49:24', 9),
(113, 2, 'BS008', NULL, 'bs008', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 02:46:37', '2019-01-30 02:49:48', 9),
(114, 2, 'BS009', NULL, 'bs009', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 02:47:23', '2019-01-30 02:50:04', 9),
(115, 2, 'BS010', NULL, 'bs010', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 02:47:59', '2019-01-30 02:50:55', 9),
(116, 2, 'BS011', NULL, 'bs011', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 02:48:23', '2019-01-30 02:51:24', 9),
(117, 2, 'BS012', NULL, 'bs012', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 02:48:45', '2019-01-30 02:51:44', 9),
(118, 2, 'BS013', NULL, 'bs013', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 02:49:10', '2019-01-30 02:52:02', 9),
(119, 2, 'BS014', NULL, 'bs014', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 02:49:42', '2019-01-30 02:55:02', 9),
(120, 3, 'BA001', NULL, 'ba001', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 02:56:39', '2019-01-30 02:58:24', 11),
(121, 3, 'BA002', NULL, 'ba002', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 02:57:07', '2019-01-30 02:58:47', 11),
(122, 5, 'BM001', NULL, 'bm001', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:00:08', '2019-01-30 02:59:27', 13),
(123, 5, 'BM002', NULL, 'bm002', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:01:13', '2019-01-30 03:00:09', 13),
(124, 5, 'BA003', NULL, 'ba003', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:01:39', '2019-01-30 03:00:33', 13),
(125, 5, 'BA004', NULL, 'ba004', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:02:09', '2019-01-30 03:00:49', 13),
(126, 5, 'BA005', NULL, 'ba005', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:02:34', '2019-01-30 03:01:08', 13),
(127, 12, 'BO001', NULL, 'bo001', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:03:03', '2019-01-30 03:05:29', 15),
(128, 12, 'BO002', NULL, 'bo002', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:03:38', '2019-01-30 03:06:01', 15),
(129, 12, 'BO003', NULL, 'bo003', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:04:05', '2019-01-30 03:07:20', 15),
(130, 30, 'NC001', NULL, 'nc001', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:06:41', '2019-01-30 03:07:42', 55),
(131, 12, 'BO004', NULL, 'bo004', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:08:24', '2019-01-30 03:08:01', 15),
(132, 12, 'BO005', NULL, 'bo005', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:08:54', '2019-01-30 03:08:22', 15),
(133, 30, 'NC002', NULL, 'nc002', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:12:36', '2019-01-30 03:09:00', 55),
(134, 30, 'NC004', NULL, 'nc004', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:19:33', '2019-01-30 03:21:45', 55),
(135, 30, 'NC005', NULL, 'nc005', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:20:06', '2019-01-30 03:22:05', 55),
(136, 30, 'NC006', NULL, 'nc006', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:21:24', '2019-01-30 03:22:29', 55),
(137, 30, 'NC007', NULL, 'nc007', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:21:46', '2019-01-30 03:22:43', 55),
(138, 30, 'NC010', NULL, 'nc010', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:22:43', '2019-01-30 03:23:03', 55),
(139, 30, 'NC009', NULL, 'nc009', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:23:01', '2019-01-30 03:29:52', 55),
(140, 30, 'NC008', NULL, 'nc008', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:23:18', '2019-01-30 03:30:12', 55),
(141, 30, 'NC011', NULL, 'nc011', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:24:47', '2019-01-30 03:30:29', 55),
(142, 30, 'NC012', NULL, 'nc012', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:25:08', '2019-01-30 03:30:45', 55),
(143, 30, 'NC013', NULL, 'nc013', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:25:28', '2019-01-30 03:31:04', 55),
(144, 30, 'NC014', NULL, 'nc014', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:25:43', '2019-01-30 03:31:20', 55),
(145, 30, 'NC015', NULL, 'nc015', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:26:03', '2019-01-30 03:31:37', 55),
(146, 30, 'NC016', NULL, 'nc016', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:27:19', '2019-01-30 03:31:56', 55),
(147, 30, 'NC017', NULL, 'nc017', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:27:33', '2019-01-30 03:32:10', 55),
(148, 30, 'NC018', NULL, 'nc018', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:27:48', '2019-01-30 03:32:25', 55),
(149, 30, 'NC019', NULL, 'nc019', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:28:03', '2019-01-30 03:42:10', 55),
(150, 30, 'NC020', NULL, 'nc020', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:28:22', '2019-01-30 03:43:25', 55),
(151, 30, 'NC021', NULL, 'nc021', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:29:25', '2019-01-30 03:43:42', 55),
(152, 30, 'NC022', NULL, 'nc022', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:29:38', '2019-01-30 03:44:14', 55),
(153, 30, 'NC023', NULL, 'nc023', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:29:53', '2019-01-30 03:45:54', 55),
(154, 30, 'NC024', NULL, 'nc024', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:30:08', '2019-01-30 03:46:10', 55),
(155, 30, 'NC025', NULL, 'nc025', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:30:24', '2019-01-30 03:46:50', 55),
(156, 30, 'NC003', NULL, 'nc003', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:31:52', '2019-01-30 03:49:06', 7),
(157, 31, 'NU001', NULL, 'nu001', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:41:07', '2019-01-30 03:47:40', 56),
(158, 31, 'NU002', NULL, 'nu002', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:42:04', '2019-01-30 03:48:05', 56),
(159, 31, 'NU003', NULL, 'nu003', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:42:18', '2019-01-31 03:00:54', 56),
(160, 31, 'NU004', NULL, 'nu004', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:42:34', '2019-01-31 03:02:30', 56),
(161, 31, 'NU005', NULL, 'nu005', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:42:48', '2019-01-31 03:03:10', 56),
(162, 31, 'NU006', NULL, 'nu006', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:44:46', '2019-01-31 03:03:37', 56),
(163, 31, 'NU007', NULL, 'nu007', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:45:00', '2019-01-31 03:04:05', 56),
(164, 31, 'NU008', NULL, 'nu008', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:45:12', '2019-01-31 03:04:21', 56),
(165, 31, 'NU009', NULL, 'nu009', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:45:27', '2019-01-31 03:04:36', 56),
(166, 31, 'NU010', NULL, 'nu010', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:45:41', '2019-01-31 03:04:51', 56),
(167, 14, 'BT001', NULL, 'bt001', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:47:04', '2019-01-31 03:05:22', 40),
(168, 31, 'NU011', NULL, 'nu011', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:47:19', '2019-01-31 03:05:38', 56),
(169, 14, 'BT002', NULL, 'bt002', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:47:25', '2019-01-31 03:07:15', 40),
(170, 31, 'NU012', NULL, 'nu012', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:47:36', '2019-01-31 03:07:33', 56),
(171, 14, 'BT003', NULL, 'bt003', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:47:46', '2019-01-31 03:07:49', 40),
(172, 14, 'NU013', NULL, 'nu013', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:47:53', '2019-01-31 03:08:03', 40),
(173, 14, 'BT004', NULL, 'bt004', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:48:26', '2019-01-31 03:08:18', 40),
(174, 31, 'NU014', NULL, 'nu014', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:48:29', '2019-01-31 03:08:36', 56),
(175, 31, 'NU015', NULL, 'nu015', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:48:41', '2019-01-31 03:08:55', 56),
(176, 14, 'BT005', NULL, 'bt005', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:48:51', '2019-01-31 03:09:12', 40),
(177, 14, 'BT006', NULL, 'bt006', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:49:19', '2019-01-31 03:09:26', 40),
(178, 14, 'BT007', NULL, 'bt007', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:49:44', '2019-01-31 03:09:42', 40),
(179, 14, 'BT008', NULL, 'bt008', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:50:09', '2019-01-31 03:35:24', 40),
(180, 31, 'NU016', NULL, 'nu016', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:50:28', '2019-01-31 03:35:45', 56),
(181, 31, 'NU017', NULL, 'nu017', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:50:43', '2019-01-31 03:36:00', 56),
(182, 14, 'BT009', NULL, 'bt009', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:50:49', '2019-01-31 03:36:14', 40),
(183, 31, 'NU018', NULL, 'nu018', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:50:59', '2019-01-31 03:36:31', 56),
(184, 2, 'BT010', NULL, 'bt010', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:51:13', '2019-01-31 03:36:47', 9),
(185, 31, 'NU019', NULL, 'nu019', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:51:13', '2019-01-31 03:37:04', 56),
(186, 14, 'BT011', NULL, 'bt011', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:51:34', '2019-01-31 03:37:17', 40),
(187, 31, 'NU020', NULL, 'nu020', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:51:38', '2019-01-31 03:37:31', 56),
(188, 2, 'BT012', NULL, 'bt012', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:52:06', '2019-01-31 03:37:45', 9),
(189, 14, 'BT013', NULL, 'bt013', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:52:31', '2019-01-31 03:53:49', 40),
(190, 31, 'NU021', NULL, 'nu021', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:52:53', '2019-01-31 03:54:05', 56),
(191, 31, 'NU022', NULL, 'nu022', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:53:13', '2019-01-31 03:54:25', 56),
(192, 31, 'NU023', NULL, 'nu023', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:53:27', '2019-01-31 03:54:42', 56),
(193, 31, 'NU024', NULL, 'nu024', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:53:42', '2019-01-31 03:54:56', 56),
(194, 31, 'NU025', NULL, 'nu025', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:53:57', '2019-01-31 03:55:12', 56),
(195, 23, 'FZ001', NULL, 'fz001', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:58:50', '2019-01-31 03:55:34', 49),
(196, 23, 'FZ002', NULL, 'fz002', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:59:12', '2019-01-31 03:55:59', 49),
(197, 23, 'FZ003', NULL, 'fz003', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:59:31', '2019-01-31 03:56:18', 49),
(198, 23, 'FZ004', NULL, 'fz004', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 03:59:51', '2019-01-31 03:56:35', 49),
(199, 23, 'FZ005', NULL, 'fz005', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 04:00:14', '2019-01-31 03:59:47', 49),
(200, 23, 'FZ006', NULL, 'fz006', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 04:00:36', '2019-01-31 04:00:06', 49),
(201, 32, 'PR001', NULL, 'pr001', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 06:26:27', '2019-01-31 04:00:29', 57),
(202, 32, 'PR004', NULL, 'pr004', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 06:53:58', '2019-01-31 04:00:45', 57),
(203, 32, 'PR002', NULL, 'pr002', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 06:56:19', '2019-01-31 04:01:00', 57),
(204, 32, 'PR003', NULL, 'pr003', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 06:56:39', '2019-01-31 04:01:18', 57),
(205, 32, 'PR006', NULL, 'pr006', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 06:57:57', '2019-01-31 04:01:35', 57),
(206, 32, 'PR007', NULL, 'pr007', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 06:58:22', '2019-01-31 04:01:48', 57),
(208, 32, 'PR005', NULL, 'pr005', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 06:59:38', '2019-01-31 04:02:04', 57),
(209, 33, 'RQ001', NULL, 'rq001', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 07:07:15', '2019-01-31 04:02:21', 58),
(210, 24, 'HK001', NULL, 'hk001', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 07:07:28', '2019-01-31 04:06:19', 48),
(211, 33, 'RQ002', NULL, 'rq002', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 07:07:36', '2019-01-31 04:06:32', 58),
(212, 24, 'HK002', NULL, 'hk002', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 07:07:51', '2019-01-31 04:06:47', 48),
(213, 33, 'RQ003', NULL, 'rq003', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 07:07:51', '2019-01-31 04:07:02', 58),
(214, 33, 'RQ004', NULL, 'rq004', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 07:08:04', '2019-01-31 04:07:17', 58),
(215, 24, 'HK003', NULL, 'hk003', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 07:08:13', '2019-01-31 04:07:33', 48),
(216, 33, 'RQ005', NULL, 'rq005', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 07:08:21', '2019-01-31 04:07:46', 58),
(217, 24, 'HK004', NULL, 'hk004', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 07:08:36', '2019-01-31 04:08:00', 48),
(218, 24, 'HK005', NULL, 'hk005', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 07:08:56', '2019-01-31 04:08:19', 48),
(219, 24, 'HK006', NULL, 'hk006', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-16 07:09:24', '2019-01-31 04:08:35', 48),
(220, 33, 'RQ006', NULL, 'rq006', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 07:34:10', '2019-01-31 04:17:44', 58),
(221, 33, 'RQ007', NULL, 'rq007', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 07:34:20', '2019-01-31 04:18:02', 58),
(222, 33, 'RQ008', NULL, 'rq008', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 07:34:32', '2019-01-31 04:18:17', 58),
(223, 33, 'RQ009', NULL, 'rq009', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 07:35:42', '2019-01-31 04:18:32', 58),
(224, 33, 'RQ010', NULL, 'rq010', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 07:35:55', '2019-01-31 04:18:48', 58),
(225, 33, 'RQ011', NULL, 'rq011', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 07:38:19', '2019-01-31 04:19:03', 58),
(226, 33, 'RQ012', NULL, 'rq012', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 07:38:30', '2019-01-31 04:19:17', 58),
(227, 33, 'RQ013', NULL, 'rq013', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 07:38:46', '2019-01-31 04:19:32', 58),
(228, 33, 'RQ014', NULL, 'rq014', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 07:39:01', '2019-01-31 04:19:44', 58),
(229, 33, 'RQ015', NULL, 'rq015', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 07:39:19', '2019-01-31 04:19:59', 58),
(230, 33, 'RQ016', NULL, 'rq016', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 07:39:30', '2019-01-31 05:35:06', 58),
(231, 33, 'RQ017', NULL, 'rq017', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 07:39:49', '2019-01-31 05:35:57', 58),
(232, 33, 'RQ018', NULL, 'rq018', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 07:40:03', '2019-01-31 05:36:17', 58),
(233, 33, 'RQ019', NULL, 'rq019', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 07:40:19', '2019-01-31 05:36:32', 58),
(234, 33, 'RQ020', NULL, 'rq020', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 07:40:35', '2019-01-31 05:36:47', 58),
(235, 33, 'RQ021', NULL, 'rq021', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 07:41:53', '2019-01-31 05:37:01', 58),
(236, 33, 'RQ022', NULL, 'rq022', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 07:42:04', '2019-01-31 05:37:14', 58),
(237, 33, 'RQ023', NULL, 'rq023', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 07:42:34', '2019-01-31 05:37:28', 58),
(238, 33, 'RQ024', NULL, 'rq024', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-16 07:42:46', '2019-01-31 05:37:40', 58),
(239, 17, 'CR001', NULL, 'cr001', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-17 02:18:13', '2019-01-31 05:38:00', 42),
(240, 17, 'CR002', NULL, 'cr002', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-17 02:18:40', '2019-01-31 05:40:40', 42),
(241, 17, 'CR003', NULL, 'cr003', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-17 02:20:09', '2019-01-31 05:41:01', 42),
(242, 17, 'CR004', NULL, 'cr004', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-17 02:20:57', '2019-01-31 05:46:57', 42),
(243, 17, 'CR005', NULL, 'cr005', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-17 02:21:56', '2019-01-31 05:51:20', 42),
(244, 17, 'CR006', NULL, 'cr006', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-17 02:22:30', '2019-01-31 05:51:56', 42),
(245, 17, 'CR007', NULL, 'cr007', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-17 02:23:29', '2019-01-31 05:52:16', 42),
(246, 17, 'CR008', NULL, 'cr008', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-17 02:25:55', '2019-01-31 05:59:29', 42),
(247, 19, 'CL001', NULL, 'cl001', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:09:23', '2019-01-31 05:59:51', 44),
(248, 19, 'CL002', NULL, 'cl002', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:09:47', '2019-01-31 06:01:04', 44),
(249, 19, 'CL003', NULL, 'cl003', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:10:09', '2019-01-31 06:01:32', 44),
(250, 19, 'CL004', NULL, 'cl004', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:10:35', '2019-01-31 06:03:11', 44),
(251, 19, 'CL005', NULL, 'cl005', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:13:16', '2019-01-31 06:03:24', 44),
(252, 19, 'CL006', NULL, 'cl006', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:13:43', '2019-01-31 06:03:40', 44),
(253, 19, 'CL007', NULL, 'cl007', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:14:26', '2019-01-31 06:03:54', 44),
(254, 19, 'CL008', NULL, 'cl008', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:14:53', '2019-01-31 06:04:09', 44),
(255, 19, 'CL009', NULL, 'cl009', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:15:28', '2019-01-31 06:04:23', 44),
(256, 19, 'CL010', NULL, 'cl010', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:15:53', '2019-01-31 06:04:40', 44),
(257, 22, 'FB001', NULL, 'fb001', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:28:14', '2019-01-31 06:05:04', 47),
(258, 22, 'FB002', NULL, 'fb002', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:28:35', '2019-01-31 06:05:27', 47),
(259, 22, 'FB003', NULL, 'fb003', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:28:54', '2019-01-31 06:05:40', 47),
(260, 22, 'FB004', NULL, 'fb004', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:29:21', '2019-01-31 06:08:18', 47),
(261, 22, 'FB005', NULL, 'fb005', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:29:42', '2019-01-31 06:08:39', 47),
(262, 22, 'FB006', NULL, 'fb006', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:30:17', '2019-01-31 06:08:59', 47),
(263, 34, 'SM001', NULL, 'sm001', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem Ipsum</p>', 5, '0', '0', '2019-01-17 03:30:32', '2019-01-31 06:09:16', 59),
(264, 22, 'FB006', NULL, 'fb006', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:30:54', '2019-01-31 06:09:39', 47),
(265, 34, 'SM002', NULL, 'sm002', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:30:58', '2019-01-31 06:09:56', 59),
(266, 22, 'FB007', NULL, 'fb007', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:31:14', '2019-01-31 06:10:10', 47),
(267, 22, 'SM003', NULL, 'sm003', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:31:15', '2019-01-31 06:10:25', 47),
(268, 34, 'SM004', NULL, 'sm004', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:31:31', '2019-01-31 06:10:43', 59),
(269, 22, 'FB008', NULL, 'fb008', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:31:41', '2019-01-31 06:10:58', 47),
(270, 34, 'SM005', NULL, 'sm005', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:32:00', '2019-01-31 06:11:13', 59),
(271, 22, 'FB009', NULL, 'fb009', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:32:09', '2019-01-31 06:11:29', 47),
(272, 34, 'SM006', NULL, 'sm006', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:32:15', '2019-01-31 06:11:45', 59),
(273, 34, 'SM007', NULL, 'sm007', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:32:41', '2019-01-31 06:12:01', 59),
(274, 22, 'FB010', NULL, 'fb010', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:32:42', '2019-01-31 06:12:28', 47),
(275, 34, 'SM008', NULL, 'sm008', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:33:05', '2019-01-31 06:12:40', 59),
(276, 22, 'FB011', NULL, 'fb011', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:33:18', '2019-01-31 06:12:57', 47),
(277, 21, 'FL001', NULL, 'fl001', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:37:33', '2019-01-31 06:14:12', 46),
(278, 35, 'SH001', NULL, 'sh001', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:37:41', '2019-01-31 06:14:27', 60),
(279, 35, 'SH002', NULL, 'sh002', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:37:58', '2019-01-31 06:14:42', 60),
(280, 21, 'FL002', NULL, 'fl002', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:37:58', '2019-01-31 06:14:59', 46),
(281, 35, 'SH003', NULL, 'sh003', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:38:18', '2019-01-31 06:15:14', 60),
(282, 21, 'FL003', NULL, 'fl003', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:38:20', '2019-01-31 06:15:31', 46),
(283, 21, 'FL004', NULL, 'fl004', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:38:42', '2019-01-31 06:15:49', 46),
(284, 21, 'FL005', NULL, 'fl005', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:39:05', '2019-01-31 06:16:20', 46),
(285, 21, 'FL006', NULL, 'fl006', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:39:25', '2019-01-31 06:16:40', 46),
(286, 21, 'FL007', NULL, 'fl007', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:39:50', '2019-01-31 06:17:25', 46),
(287, 21, 'FL008', NULL, 'fl008', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:40:11', '2019-01-31 06:17:42', 46),
(288, 21, 'FL009', NULL, 'fl009', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:40:36', '2019-01-31 06:17:52', 46),
(289, 21, 'FL010', NULL, 'fl010', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:41:02', '2019-01-31 06:18:07', 46),
(290, 21, 'FL011', NULL, 'fl011', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:41:22', '2019-01-31 06:18:23', 46),
(291, 35, 'SH004', NULL, 'sh004', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:41:36', '2019-01-31 06:18:37', 60),
(292, 21, 'FL012', NULL, 'fl012', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:41:43', '2019-01-31 06:18:51', 46),
(293, 35, 'SH005', NULL, 'sh005', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:41:56', '2019-01-31 06:19:05', 60),
(294, 21, 'FL013', NULL, 'fl013', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:42:04', '2019-01-31 06:19:19', 46),
(295, 35, 'SH006', NULL, 'sh006', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:42:16', '2019-01-31 06:19:40', 60),
(296, 21, 'FL014', NULL, 'fl014', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:42:26', '2019-01-31 06:20:01', 46),
(297, 35, 'SH007', NULL, 'sh007', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:42:37', '2019-01-31 06:20:16', 60),
(298, 21, 'FL015', NULL, 'fl015', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:42:52', '2019-01-31 06:21:52', 46),
(299, 35, 'SH008', NULL, 'sh008', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:43:52', '2019-01-31 06:22:07', 60),
(300, 35, 'SH009', NULL, 'sh009', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-17 03:44:13', '2019-01-31 06:22:22', 60),
(302, 36, 'TR001', NULL, 'tr001', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem Ipsum</p>', 5, '0', '0', '2019-01-23 05:15:28', '2019-01-23 05:15:28', 61),
(303, 36, 'TR002', NULL, 'tr002', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem Ipsum</p>', 5, '0', '0', '2019-01-23 05:15:42', '2019-01-23 05:15:42', 61),
(304, 36, 'TR003', NULL, 'tr003', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem Ipsum</p>', 5, '0', '0', '2019-01-23 05:15:55', '2019-01-23 05:15:55', 61),
(306, 36, 'TR004', NULL, 'tr004', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem Ipsum</p>', 5, '0', '0', '2019-01-23 05:35:08', '2019-01-23 05:35:08', 61),
(307, 36, 'TR005', NULL, 'tr005', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem Ipsum</p>', 5, '0', '0', '2019-01-23 05:37:52', '2019-01-23 05:37:52', 61),
(308, 36, 'TR006', NULL, 'tr006', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem Ipsum</p>', 5, '0', '0', '2019-01-23 05:38:21', '2019-01-23 05:38:21', 61),
(309, 37, 'UL001', NULL, 'ul001', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem Ipsum</p>', 5, '0', '0', '2019-01-23 05:45:43', '2019-01-23 05:45:43', 62),
(310, 37, 'UL002', NULL, 'ul002', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem Ipsum</p>', 5, '0', '0', '2019-01-23 05:46:06', '2019-01-23 05:46:06', 62),
(311, 38, 'UN001', NULL, 'un001', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem Ipsum</p>', 5, '0', '0', '2019-01-23 05:55:38', '2019-01-23 05:55:38', 63),
(312, 38, 'UN002', NULL, 'un002', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem Ipsum</p>', 5, '0', '0', '2019-01-23 05:56:12', '2019-01-23 05:56:12', 63),
(313, 38, 'UN003', NULL, 'un003', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem Ipsum</p>', 5, '0', '0', '2019-01-23 05:56:39', '2019-01-23 05:56:39', 63),
(314, 38, 'UN004', NULL, 'un004', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem Ipsum</p>', 5, '0', '0', '2019-01-23 05:57:17', '2019-01-23 05:57:17', 63),
(315, 16, 'CK001', NULL, 'ck001', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 02:56:03', '2019-01-29 02:56:03', 41),
(316, 16, 'CK002', NULL, 'ck002', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 02:57:01', '2019-01-29 02:57:01', 41),
(317, 16, 'CK003', NULL, 'ck003', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 03:01:42', '2019-01-29 03:01:42', 41),
(318, 16, 'CK006', NULL, 'ck006', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 03:01:42', '2019-01-29 03:07:53', 7),
(319, 16, 'CK004', NULL, 'ck004', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 03:02:35', '2019-01-29 03:02:35', 41),
(320, 16, 'CK005', NULL, 'ck005', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 03:03:21', '2019-01-29 03:03:21', 41),
(321, 16, 'CK007', NULL, 'ck007', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 03:08:48', '2019-01-29 03:08:48', 41),
(322, 16, 'CK008', NULL, 'ck008', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 03:09:31', '2019-01-29 03:09:31', 41),
(323, 16, 'CK009', NULL, 'ck009', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 03:10:11', '2019-01-29 03:10:11', 41),
(324, 16, 'CK010', NULL, 'ck010', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 03:11:01', '2019-01-29 03:11:01', 41),
(325, 16, 'CK011', NULL, 'ck011', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 03:11:41', '2019-01-29 03:11:41', 41),
(326, 16, 'CK012', NULL, 'ck012', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 03:14:00', '2019-01-29 03:14:00', 41),
(327, 16, 'CK013', NULL, 'ck013', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 03:14:39', '2019-01-29 03:14:39', 41),
(328, 16, 'CK014', NULL, 'ck014', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 03:16:22', '2019-01-29 03:16:22', 41),
(329, 16, 'CK015', NULL, 'ck015', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 03:17:09', '2019-01-29 03:17:09', 41),
(330, 16, 'CK016', NULL, 'ck016', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 03:17:37', '2019-01-29 03:17:37', 41),
(331, 16, 'CK017', NULL, 'ck017', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 03:18:48', '2019-01-29 03:18:48', 41),
(332, 16, 'CK018', NULL, 'ck018', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 03:19:15', '2019-01-29 03:19:15', 41),
(333, 16, 'CK019', NULL, 'ck019', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 03:20:11', '2019-01-29 03:20:11', 41),
(334, 16, 'CK020', NULL, 'ck020', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 03:20:34', '2019-01-29 03:20:34', 41),
(335, 16, 'CK021', NULL, 'ck021', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 03:22:08', '2019-01-29 03:22:08', 41),
(336, 16, 'CK022', NULL, 'ck022', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 03:22:37', '2019-01-29 03:22:37', 41),
(337, 16, 'CK023', NULL, 'ck023', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 03:23:05', '2019-01-29 03:23:05', 41);
INSERT INTO `tb_kue` (`id`, `id_category`, `nama_kue`, `ukuran`, `link`, `ukuran_id`, `tinggi`, `pax`, `harga`, `id_type`, `jumlah_rasa`, `jumlah_huruf`, `jumlah_angka`, `deskripsi_kue`, `rating`, `spesial_kue`, `homepage_kue`, `created_at`, `updated_at`, `id_home_category`) VALUES
(338, 16, 'CK024', NULL, 'ck024', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 03:23:33', '2019-01-29 03:23:33', 41),
(339, 16, 'CK025', NULL, 'ck025', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 03:23:55', '2019-01-29 03:23:55', 41),
(340, 16, 'CK026', NULL, 'ck026', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 03:24:26', '2019-01-29 03:24:26', 41),
(341, 16, 'CK027', NULL, 'ck027', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 03:24:50', '2019-01-29 03:24:50', 41),
(342, 16, 'CK028', NULL, 'ck028', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 03:25:11', '2019-01-29 03:25:11', 41),
(343, 16, 'CK029', NULL, 'ck029', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 03:25:41', '2019-01-29 03:25:41', 41),
(344, 16, 'CK030', NULL, 'ck030', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 03:26:13', '2019-01-29 03:26:13', 41),
(345, 16, 'CK031', NULL, 'ck031', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 03:28:01', '2019-01-29 03:28:01', 41),
(346, 16, 'CK032', NULL, 'ck032', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 03:28:31', '2019-01-29 03:28:31', 41),
(347, 16, 'CK033', NULL, 'ck033', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 03:28:56', '2019-01-29 03:28:56', 41),
(348, 16, 'CK034', NULL, 'ck034', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 03:29:29', '2019-01-29 03:29:29', 41),
(349, 16, 'CK035', NULL, 'ck035', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 03:29:55', '2019-01-29 03:29:55', 41),
(350, 16, 'CK036', NULL, 'ck036', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 03:30:46', '2019-01-29 03:30:46', 41),
(351, 16, 'CK037', NULL, 'ck037', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 03:31:34', '2019-01-29 03:31:34', 41),
(352, 16, 'CK038', NULL, 'ck038', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 03:32:16', '2019-01-29 03:32:16', 41),
(353, 16, 'CK039', NULL, 'ck039', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 03:32:36', '2019-01-29 03:32:36', 41),
(354, 16, 'CK040', NULL, 'ck040', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 03:33:00', '2019-01-29 03:33:00', 41),
(355, 16, 'CK041', NULL, 'ck041', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 03:41:46', '2019-01-29 03:41:46', 41),
(356, 16, 'CK042', NULL, 'ck042', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 03:43:08', '2019-01-29 03:43:08', 41),
(357, 16, 'CK043', NULL, 'ck043', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 03:43:57', '2019-01-29 03:43:57', 41),
(358, 18, 'CM001', NULL, 'cm001', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 05:29:23', '2019-01-29 05:29:23', 43),
(359, 18, 'CM002', NULL, 'cm002', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 05:29:58', '2019-01-29 05:29:58', 43),
(360, 18, 'CM003', NULL, 'cm003', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 05:30:26', '2019-01-29 05:30:26', 43),
(361, 18, 'CM004', NULL, 'cm004', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 05:30:58', '2019-01-29 05:30:58', 43),
(362, 18, 'CM005', NULL, 'cm005', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 05:31:34', '2019-01-29 05:31:34', 43),
(363, 18, 'CM006', NULL, 'cm006', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 05:31:58', '2019-01-29 05:31:58', 43),
(364, 18, 'CM007', NULL, 'cm007', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 05:32:35', '2019-01-29 05:32:35', 43),
(365, 18, 'CM008', NULL, 'cm008', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 05:33:04', '2019-01-29 05:33:04', 43),
(366, 18, 'CM009', NULL, 'cm009', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 05:33:41', '2019-01-29 05:33:41', 43),
(367, 18, 'CM010', NULL, 'cm010', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-29 05:34:08', '2019-01-29 05:34:08', 43),
(368, 18, 'CM011', NULL, 'cm011', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-30 02:24:46', '2019-01-30 02:24:46', 43),
(369, 18, 'CM012', NULL, 'cm012', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-30 02:25:09', '2019-01-30 02:25:09', 43),
(370, 18, 'CM013', NULL, 'cm013', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-30 02:25:35', '2019-01-30 02:25:35', 42),
(371, 18, 'CM014', NULL, 'cm014', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-30 02:26:22', '2019-01-30 02:26:22', 43),
(372, 18, 'CM015', NULL, 'cm015', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-30 02:26:59', '2019-01-30 02:26:59', 43),
(373, 18, 'CM016', NULL, 'cm016', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-30 02:27:40', '2019-01-30 02:27:40', 43),
(374, 18, 'CM017', NULL, 'cm017', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-30 02:28:10', '2019-01-30 02:28:10', 43),
(375, 18, 'CM018', NULL, 'cm018', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-30 02:28:36', '2019-01-30 02:28:36', 43),
(376, 18, 'CM019', NULL, 'cm019', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-30 02:29:02', '2019-01-30 02:29:02', 43),
(377, 18, 'CM020', NULL, 'cm020', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-30 02:29:29', '2019-01-30 02:29:29', 43),
(378, 18, 'CM021', NULL, 'cm021', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-30 02:38:29', '2019-01-30 02:38:29', 43),
(379, 18, 'CM022', NULL, 'cm022', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-30 02:39:07', '2019-01-30 02:39:07', 43),
(380, 18, 'CM023', NULL, 'cm023', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-30 02:39:31', '2019-01-30 02:39:31', 43),
(381, 18, 'CM024', NULL, 'cm024', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-30 02:40:00', '2019-01-30 02:40:00', 43),
(382, 18, 'CM025', NULL, 'cm025', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-30 02:40:32', '2019-01-30 02:40:32', 43),
(383, 18, 'CM026', NULL, 'cm026', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-30 02:40:57', '2019-01-30 02:40:57', 43),
(384, 25, 'IU001', NULL, 'iu001', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-30 02:49:03', '2019-01-30 02:49:03', 50),
(385, 25, 'IU002', NULL, 'iu002', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-30 02:49:46', '2019-01-30 02:49:46', 50),
(386, 25, 'IU003', NULL, 'iu003', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-30 02:50:13', '2019-01-30 02:50:13', 50),
(387, 25, 'IU004', NULL, 'iu004', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-30 02:50:50', '2019-01-30 02:50:50', 50),
(388, 25, 'IU005', NULL, 'iu005', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-30 02:51:19', '2019-01-30 02:51:19', 50),
(389, 25, 'IU006', NULL, 'iu006', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-30 02:51:50', '2019-01-30 02:51:50', 50),
(390, 25, 'IU007', NULL, 'iu007', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-30 02:52:16', '2019-01-30 02:52:16', 50),
(391, 25, 'IU008', NULL, 'iu008', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-30 02:52:45', '2019-01-30 02:57:07', 50),
(392, 25, 'IU009', NULL, 'iu009', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-30 02:53:01', '2019-01-30 02:55:09', 7),
(393, 25, 'IU010', NULL, 'iu010', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-30 02:54:33', '2019-01-30 02:56:39', 50),
(394, 25, 'IU011', NULL, 'iu011', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-30 02:58:03', '2019-01-30 02:58:03', 50),
(395, 25, 'IU012', NULL, 'iu012', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-30 02:58:32', '2019-01-30 02:58:32', 50),
(396, 25, 'IU013', NULL, 'iu013', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-30 02:58:59', '2019-01-30 02:58:59', 50),
(397, 25, 'IU014', NULL, 'iu014', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-30 02:59:48', '2019-01-30 02:59:48', 50),
(398, 25, 'IU015', NULL, 'iu015', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-30 03:01:08', '2019-01-30 03:01:08', 50),
(399, 25, 'IU016', NULL, 'iu016', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>lorem ipsum</p>', 5, '0', '0', '2019-01-30 03:01:43', '2019-01-30 03:01:43', 50),
(400, 37, 'UL003', NULL, 'ul003', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '<p>Lorem ipsum</p>', 5, '0', '0', '2019-01-30 03:28:29', '2019-01-30 03:28:29', 62);

-- --------------------------------------------------------

--
-- Table structure for table `tb_master_delivery`
--

CREATE TABLE `tb_master_delivery` (
  `id_master_delivery` int(11) NOT NULL,
  `wilayah_delivery` varchar(255) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_master_delivery`
--

INSERT INTO `tb_master_delivery` (`id_master_delivery`, `wilayah_delivery`, `harga`) VALUES
(3, 'Denpasar Timur', 32000);

-- --------------------------------------------------------

--
-- Table structure for table `tb_nota`
--

CREATE TABLE `tb_nota` (
  `id_nota` int(10) UNSIGNED NOT NULL,
  `id_tagihan` int(11) UNSIGNED DEFAULT NULL,
  `jumlah_bayar` bigint(20) DEFAULT NULL,
  `bank` varchar(100) DEFAULT NULL,
  `atas_nama` varchar(100) DEFAULT NULL,
  `tanggal_konfirmasi` date DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_order`
--

CREATE TABLE `tb_order` (
  `id_order` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `note` text,
  `biaya_delivery` bigint(20) DEFAULT NULL,
  `total` bigint(20) DEFAULT NULL,
  `grand_total` bigint(20) DEFAULT NULL,
  `wilayah` varchar(255) DEFAULT NULL,
  `alamat` text,
  `pick_up_time` time DEFAULT NULL,
  `pick_up_date` date DEFAULT NULL,
  `status_order` varchar(225) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_order`
--

INSERT INTO `tb_order` (`id_order`, `id_user`, `tanggal`, `note`, `biaya_delivery`, `total`, `grand_total`, `wilayah`, `alamat`, `pick_up_time`, `pick_up_date`, `status_order`) VALUES
(1, 1, '2018-10-30', NULL, 0, 0, 0, '0', 'test', '07:53:16', '2018-10-30', 'menunggu'),
(2, 2, '2018-10-31', 'hbh', 0, 120000, 120000, '0', NULL, '06:12:34', '2018-10-31', 'terbayar'),
(3, 2, '2018-11-03', NULL, 0, 240000, 240000, '0', NULL, '06:37:52', '2018-11-03', 'terbayar'),
(4, 2, '2018-11-03', NULL, 0, 145000, 145000, '0', NULL, '07:28:06', '2018-11-03', 'terbayar'),
(5, 2, '2018-11-05', NULL, 0, 120000, 120000, '0', NULL, '02:52:50', '2018-11-05', 'terbayar'),
(6, 2, '2018-11-05', NULL, 0, 120000, 120000, '0', NULL, '03:10:51', '2018-11-05', 'terbayar'),
(7, 2, '2018-11-05', NULL, 0, 120000, 120000, '0', NULL, '03:13:47', '2018-11-05', 'selesai'),
(8, 2, '2018-11-05', NULL, 0, 125000, 125000, '0', NULL, '03:26:16', '2018-11-05', 'terbayar'),
(9, 2, '2018-11-07', NULL, 0, 250000, 250000, '0', NULL, '03:04:29', '2018-11-07', 'gagal'),
(10, 2, '2018-11-07', NULL, 0, 120000, 120000, '0', NULL, '05:24:37', '2018-11-07', 'gagal'),
(11, 2, '2018-11-07', NULL, 0, 120000, 120000, '0', NULL, '05:26:44', '2018-11-07', 'terbayar'),
(12, 1, '2018-11-07', 'rer', 32000, 120000, 152000, 'Denpasar Timur', '4rerere', '05:32:22', '2018-11-07', 'menunggu'),
(13, 1, '2018-11-07', NULL, 0, 120000, 120000, '0', NULL, '05:36:04', '2018-11-07', 'terbayar'),
(14, 1, '2018-11-07', 'we', 0, 45000, 220000, '0', 'DEDE', '12:04:18', '2018-11-07', 'ready'),
(15, 5, '2018-11-09', 'note', 32000, 2040000, 2122000, 'Denpasar Timur', 'denpasar', '12:29:17', '2018-11-09', 'menunggu'),
(16, 1, '2018-11-15', 'Pembayaran', 0, 150000, 450000, '0', NULL, '06:24:28', '2018-11-15', 'menunggu'),
(17, 1, '2018-11-15', 'Test', 0, 150000, 150000, '0', NULL, '06:26:49', '2018-11-15', 'menunggu'),
(18, 1, '2018-11-23', 'test order', 32000, 120000, 202000, 'Denpasar Timur', NULL, '05:23:49', '2018-11-23', 'menunggu'),
(19, 1, '2018-11-24', 'note', 32000, 145000, 227000, 'Denpasar Timur', 'jalan', '00:43:29', '2018-11-24', 'menunggu'),
(20, 2, '2018-12-07', NULL, 0, 90000, 90000, '0', NULL, '03:50:21', '2018-12-07', 'menunggu'),
(21, 2, '2018-12-07', NULL, 0, 45000, 45000, '0', NULL, '03:52:05', '2018-12-07', 'menunggu'),
(22, 2, '2018-12-11', NULL, 0, 145000, 145000, '0', NULL, '03:34:25', '2018-12-11', 'terbayar'),
(23, 2, '2019-01-16', NULL, 0, 340000, 340000, '0', NULL, '06:21:41', '2019-01-16', 'menunggu'),
(24, 2, '2019-01-16', NULL, 0, 340000, 340000, '0', NULL, '06:22:58', '2019-01-16', 'menunggu'),
(25, 2, '2019-01-16', NULL, 0, 340000, 340000, '0', NULL, '06:25:41', '2019-01-16', 'menunggu'),
(26, 2, '2019-01-16', NULL, 0, 340000, 340000, '0', NULL, '06:27:27', '2019-01-16', 'menunggu');

-- --------------------------------------------------------

--
-- Table structure for table `tb_order_detail`
--

CREATE TABLE `tb_order_detail` (
  `id_detail_order` int(10) UNSIGNED NOT NULL,
  `id_order` int(10) UNSIGNED DEFAULT NULL,
  `kue` varchar(225) DEFAULT NULL,
  `deskripsi` varchar(225) DEFAULT NULL,
  `qty` int(2) DEFAULT NULL,
  `harga` bigint(20) DEFAULT NULL,
  `sub_total` bigint(20) DEFAULT NULL,
  `rasa` varchar(225) DEFAULT NULL,
  `huruf` char(10) DEFAULT NULL,
  `angka` int(11) DEFAULT NULL,
  `tulisan_kue` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_order_detail`
--

INSERT INTO `tb_order_detail` (`id_detail_order`, `id_order`, `kue`, `deskripsi`, `qty`, `harga`, `sub_total`, `rasa`, `huruf`, `angka`, `tulisan_kue`, `created_at`, `updated_at`) VALUES
(1, 2, 'Number Cake', '', 1, 120000, 120000, NULL, NULL, 789, 'hjhnj', NULL, NULL),
(2, 3, 'Number Cake', '', 2, 120000, 240000, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 4, 'Jar Cake', '', 1, 145000, 145000, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 5, 'Number Cake', '', 1, 120000, 120000, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 6, 'Number Cake', '', 1, 120000, 120000, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 7, 'Number Cake', '', 1, 120000, 120000, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 8, 'Mini Cake', '', 1, 125000, 125000, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 9, 'Mini Cake', '', 2, 125000, 250000, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 10, 'Number Cake', '', 1, 120000, 120000, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 11, 'Number Cake', '', 1, 120000, 120000, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 12, 'Number Cake', '', 1, 120000, 120000, NULL, NULL, 324, '43434', NULL, NULL),
(12, 13, 'Number Cake', '', 1, 120000, 120000, NULL, NULL, 324, '43434', NULL, NULL),
(13, 14, 'Mini Cake', NULL, 1, 125000, 125000, NULL, NULL, NULL, 'dwdwd', NULL, NULL),
(14, 14, 'Tart Cake', '', 1, 45000, 45000, NULL, NULL, NULL, 'e', NULL, NULL),
(15, 15, 'Name Cake', '', 6, 340000, 2040000, NULL, 'gfrt', NULL, 'tulisan kue', NULL, NULL),
(16, 16, 'Cup Cake', NULL, 2, 150000, 300000, NULL, NULL, NULL, 'Test', NULL, NULL),
(17, 16, 'Cup Cake', '', 1, 150000, 150000, NULL, NULL, NULL, 'T', NULL, NULL),
(18, 17, 'Cup Cake', '', 1, 150000, 150000, NULL, NULL, NULL, 'T', NULL, NULL),
(19, 18, 'Number Cake', '', 1, 120000, 120000, NULL, NULL, 432, 'ds', NULL, NULL),
(20, 19, 'Jar Cake', '', 1, 145000, 145000, NULL, NULL, NULL, 'kue', NULL, NULL),
(21, 20, 'Tart Cake', '', 2, 45000, 90000, NULL, NULL, NULL, NULL, NULL, NULL),
(22, 21, 'Tart Cake', '', 1, 45000, 45000, NULL, NULL, NULL, NULL, NULL, NULL),
(23, 22, 'Jar Cake', '', 1, 145000, 145000, NULL, NULL, NULL, NULL, NULL, NULL),
(24, 23, 'Tart Cake', '', 1, 340000, 340000, NULL, NULL, NULL, NULL, NULL, NULL),
(25, 24, 'Tart Cake', '', 1, 340000, 340000, NULL, NULL, NULL, NULL, NULL, NULL),
(26, 25, 'Tart Cake', '', 1, 340000, 340000, NULL, NULL, NULL, NULL, NULL, NULL),
(27, 26, 'Tart Cake', '', 1, 340000, 340000, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_order_edible`
--

CREATE TABLE `tb_order_edible` (
  `id_order_edible` int(10) UNSIGNED NOT NULL,
  `id_order` int(10) UNSIGNED DEFAULT NULL,
  `nama_file` varchar(255) DEFAULT NULL,
  `keterangan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_order_edible`
--

INSERT INTO `tb_order_edible` (`id_order_edible`, `id_order`, `nama_file`, `keterangan`) VALUES
(1, 14, '/storage/edible/9-0-C-20181107074325.png', NULL),
(2, 15, '/storage/edible/15-0-O-20181109122917.jpg', NULL),
(3, 19, '/storage/edible/19-0-O-20181124004329.jpg', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_payment_channel`
--

CREATE TABLE `tb_payment_channel` (
  `id_chanel` int(11) NOT NULL,
  `code` varchar(2) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_payment_channel`
--

INSERT INTO `tb_payment_channel` (`id_chanel`, `code`, `description`) VALUES
(1, '02', 'Mandiri ClickPay '),
(2, '03', 'KlikBCA '),
(3, '04', 'DOKU Wallet '),
(4, '06', 'BRI e-Pay'),
(5, '13', 'BNI Debit Online (VCN) '),
(6, '15', 'Credit Card Visa/Master/JCB '),
(7, '16', 'Credit Card Tokenization '),
(8, '17', 'Recurring Payment '),
(9, '18', 'BCA KlikPay '),
(10, '19', 'CIMB Clicks '),
(11, '20', 'PT POS (National Post Office) '),
(12, '21', 'Sinarmas VA '),
(13, '22', 'Sinarmas VA Lite '),
(14, '23', 'MOTO '),
(15, '25', 'Muamalat Internet Banking '),
(16, '26', 'Danamon Internet Banking'),
(17, '28', 'Permata Internet Banking '),
(18, '29', 'BCA VA '),
(19, '31', 'Indomaret'),
(20, '33', 'Danamon VA '),
(21, '34', 'BRI VA '),
(22, '35', 'Alfagroup '),
(23, '36', 'Permata VA '),
(24, '37', 'Kredivo '),
(25, '41', 'Mandiri VA ');

-- --------------------------------------------------------

--
-- Table structure for table `tb_rasa`
--

CREATE TABLE `tb_rasa` (
  `id` int(10) UNSIGNED NOT NULL,
  `rasa` varchar(225) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_rasa`
--

INSERT INTO `tb_rasa` (`id`, `rasa`, `updated_at`, `created_at`) VALUES
(2, 'Coklat', '2019-01-11 08:07:50', NULL),
(6, 'Strawberry', '2018-11-15 02:53:54', '2018-11-15 02:53:54'),
(7, 'Vanilla', '2019-01-14 09:09:43', '2019-01-14 09:09:43'),
(8, 'Blueberry', '2019-01-14 09:09:56', '2019-01-14 09:09:56'),
(9, 'Fruit', '2019-01-14 09:10:16', '2019-01-14 09:10:16'),
(10, 'Milk', '2019-01-14 09:10:25', '2019-01-14 09:10:25'),
(11, 'Coffee', '2019-01-14 09:11:59', '2019-01-14 09:11:59');

-- --------------------------------------------------------

--
-- Table structure for table `tb_tag`
--

CREATE TABLE `tb_tag` (
  `id` int(10) UNSIGNED NOT NULL,
  `tag` varchar(225) DEFAULT NULL,
  `link` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_tag`
--

INSERT INTO `tb_tag` (`id`, `tag`, `link`, `created_at`, `updated_at`) VALUES
(1, 'simple tart', 'simple-tart', NULL, NULL),
(2, 'tart cake', 'tart-cake', NULL, NULL),
(5, 'jar cake', 'jar-cake', NULL, NULL),
(6, 'name cake', 'name-cake', NULL, NULL),
(8, 'slince cake', 'slince-cake', NULL, NULL),
(9, 'tart cake', 'tart-cake', NULL, NULL),
(10, 'cake', 'cake', '2018-07-20 08:59:27', '2018-07-20 08:59:27'),
(11, 'minicake', 'minicake', '2018-07-20 08:59:27', '2018-07-20 08:59:27'),
(12, 'cartoon mix', 'cartoon-mix', '2019-01-29 05:29:23', '2019-01-29 05:29:23'),
(13, 'cartoon mix', 'cartoon-mix', '2019-01-29 05:29:58', '2019-01-29 05:29:58'),
(14, 'cartoon mix', 'cartoon-mix', '2019-01-29 05:30:26', '2019-01-29 05:30:26'),
(15, 'cartoon mix', 'cartoon-mix', '2019-01-29 05:30:58', '2019-01-29 05:30:58'),
(16, 'Cartoon Mix', 'cartoon-mix', '2019-01-29 05:31:34', '2019-01-29 05:31:34'),
(17, 'Cartoon Mix', 'cartoon-mix', '2019-01-29 05:31:58', '2019-01-29 05:31:58'),
(18, 'Cartoon Mix', 'cartoon-mix', '2019-01-29 05:32:35', '2019-01-29 05:32:35'),
(19, 'cartoon mix', 'cartoon-mix', '2019-01-29 05:33:04', '2019-01-29 05:33:04'),
(20, 'Cartoon Mix', 'cartoon-mix', '2019-01-29 05:33:41', '2019-01-29 05:33:41'),
(21, 'Cartoon Mix', 'cartoon-mix', '2019-01-29 05:34:08', '2019-01-29 05:34:08'),
(22, 'I Heart U Cake', 'i-heart-u-cake', '2019-01-30 02:49:03', '2019-01-30 02:49:03'),
(23, 'I Heart U Cake', 'i-heart-u-cake', '2019-01-30 02:49:46', '2019-01-30 02:49:46'),
(24, 'I Heart U Cake', 'i-heart-u-cake', '2019-01-30 02:50:13', '2019-01-30 02:50:13'),
(25, 'I Heart U Cake', 'i-heart-u-cake', '2019-01-30 02:50:50', '2019-01-30 02:50:50'),
(26, 'I Heart U Cake', 'i-heart-u-cake', '2019-01-30 02:51:19', '2019-01-30 02:51:19'),
(27, 'I Heart U Cake', 'i-heart-u-cake', '2019-01-30 02:51:50', '2019-01-30 02:51:50'),
(28, 'I Heart U Cake', 'i-heart-u-cake', '2019-01-30 02:54:33', '2019-01-30 02:54:33'),
(29, 'I Heart U Cake', 'i-heart-u-cake', '2019-01-30 02:58:03', '2019-01-30 02:58:03'),
(30, 'I Heart U Cake', 'i-heart-u-cake', '2019-01-30 02:58:32', '2019-01-30 02:58:32'),
(31, 'I Heart U Cake', 'i-heart-u-cake', '2019-01-30 02:59:48', '2019-01-30 02:59:48'),
(32, 'I Heart U Cake', 'i-heart-u-cake', '2019-01-30 03:01:43', '2019-01-30 03:01:43');

-- --------------------------------------------------------

--
-- Table structure for table `tb_tagihan`
--

CREATE TABLE `tb_tagihan` (
  `id_tagihan` int(11) UNSIGNED NOT NULL,
  `id_order` int(10) UNSIGNED DEFAULT NULL,
  `tanggal_tagihan` date DEFAULT NULL,
  `jumlah_tagihan` bigint(20) DEFAULT NULL,
  `rekening_tujuan` varchar(25) DEFAULT NULL,
  `nama_bank` varchar(225) DEFAULT NULL,
  `kode_bank` char(25) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_transaction`
--

CREATE TABLE `tb_transaction` (
  `id_transaction` int(11) NOT NULL,
  `id_order` int(10) DEFAULT NULL,
  `PAYMENTDATETIME` varchar(15) DEFAULT NULL,
  `PURCHASECURRENCY` varchar(3) DEFAULT NULL,
  `LIABILITY` varchar(10) DEFAULT NULL,
  `PAYMENTCHANNEL` varchar(2) DEFAULT NULL,
  `AMOUNT` varchar(16) DEFAULT NULL,
  `PAYMENTCODE` varchar(8) DEFAULT NULL,
  `MCN` varchar(17) DEFAULT NULL,
  `WORDS` varchar(200) DEFAULT NULL,
  `RESULTMSG` varchar(20) DEFAULT NULL,
  `VERIFYID` varchar(30) DEFAULT NULL,
  `TRANSIDMERCHANT` varchar(20) DEFAULT NULL,
  `BANK` varchar(100) DEFAULT NULL,
  `STATUSTYPE` varchar(1) DEFAULT NULL,
  `APPROVALCODE` varchar(20) DEFAULT NULL,
  `EDUSTATUS` varchar(10) DEFAULT NULL,
  `THREEDSECURESTATUS` varchar(5) DEFAULT NULL,
  `VERIFYSCORE` varchar(3) DEFAULT NULL,
  `CURRENCY` varchar(3) DEFAULT NULL,
  `RESPONSECODE` varchar(4) DEFAULT NULL,
  `CHNAME` varchar(50) DEFAULT NULL,
  `BRAND` varchar(10) DEFAULT NULL,
  `VERIFYSTATUS` varchar(10) DEFAULT NULL,
  `SESSIONID` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_transaction`
--

INSERT INTO `tb_transaction` (`id_transaction`, `id_order`, `PAYMENTDATETIME`, `PURCHASECURRENCY`, `LIABILITY`, `PAYMENTCHANNEL`, `AMOUNT`, `PAYMENTCODE`, `MCN`, `WORDS`, `RESULTMSG`, `VERIFYID`, `TRANSIDMERCHANT`, `BANK`, `STATUSTYPE`, `APPROVALCODE`, `EDUSTATUS`, `THREEDSECURESTATUS`, `VERIFYSCORE`, `CURRENCY`, `RESPONSECODE`, `CHNAME`, `BRAND`, `VERIFYSTATUS`, `SESSIONID`) VALUES
(37, 6, '20181105101203', '360', 'CUSTOMER', '15', '120000.00', '', '542640******8754', '4e833d7a01784b82eba966901024e8c6ea0b3b80', 'SUCCESS', '', '6', 'Bank BNI', 'P', '095674', 'NA', 'TRUE', '-1', '360', '0000', 'astradanta', 'MASTERCARD', 'NA', '6'),
(38, 8, '20181105102843', '360', 'CUSTOMER', '15', '125000.00', '', '542640******8754', 'e12ca0832a2656fcc45af9f3ccc9b34b00bd5f7d', 'SUCCESS', '', '8', 'Bank BNI', 'P', '683921', 'NA', 'TRUE', '-1', '360', '0000', 'astradanta', 'MASTERCARD', 'NA', '8'),
(39, 11, '20181107122900', '360', 'CUSTOMER', '15', '120000.00', '', '542640******8754', 'f9d42c45d96716ed945894a7134f38a02efe6327', 'SUCCESS', '', '11', 'Bank BNI', 'P', '199653', 'NA', 'TRUE', '-1', '360', '0000', 'astradanta', 'MASTERCARD', 'NA', '11'),
(40, 13, '20181107123803', '360', 'CUSTOMER', '15', '120000.00', '', '542640******8754', 'd2abb161c47034de4d56335a7b8e3ea71010f2c7', 'SUCCESS', '', '13', 'Bank BNI', 'P', '495901', 'NA', 'TRUE', '-1', '360', '0000', 'Admin', 'MASTERCARD', 'NA', '13'),
(41, 0, '20181107190151', '360', NULL, '01', '10000.00', '', '', '368c72937ea9fe784a4c836c63160573139b5f4b', 'FAILED', '', 'INVOICE-TEST-1584100', '', 'P', '', NULL, NULL, '-1', '360', '5510', NULL, NULL, 'NA', 'verivo610bax5k6'),
(42, 0, '20181107190153', '360', NULL, '01', '10000.00', '', '', '368c72937ea9fe784a4c836c63160573139b5f4b', 'FAILED', '', 'INVOICE-TEST-1584100', '', 'P', '', NULL, NULL, '-1', '360', '5510', NULL, NULL, 'NA', 'verivo610bax5k6'),
(43, 22, '20181211103519', '360', 'CUSTOMER', '15', '145000.00', '', '542640******8754', 'b2e9d2eacf082601e99ea3ea1c6851639319ec91', 'SUCCESS', '', '22', 'Bank BNI', 'P', '966878', 'NA', 'TRUE', '-1', '360', '0000', 'astradanta', 'MASTERCARD', 'NA', '22');

-- --------------------------------------------------------

--
-- Table structure for table `tb_type`
--

CREATE TABLE `tb_type` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(225) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_type`
--

INSERT INTO `tb_type` (`id`, `type`, `created_at`, `updated_at`) VALUES
(1, 'Simple Tarts / Milecrepes', NULL, '2018-07-03 09:13:21'),
(2, 'Mini Tart', NULL, NULL),
(4, 'Name Cake', NULL, NULL),
(5, 'Number Cake', NULL, NULL),
(6, 'Jar Cake', NULL, NULL),
(7, 'Cup Cake / Slice Cake', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_ukuran_tart_cake`
--

CREATE TABLE `tb_ukuran_tart_cake` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_kue` int(11) UNSIGNED DEFAULT NULL,
  `ukuran` int(11) DEFAULT NULL,
  `harga` bigint(20) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(10) UNSIGNED NOT NULL,
  `nama_user` varchar(225) DEFAULT NULL,
  `jenis_user` varchar(225) DEFAULT NULL,
  `email` varchar(225) DEFAULT NULL,
  `password` varchar(225) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `no_hp` varchar(25) DEFAULT NULL,
  `jenis_kelamin` varchar(100) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `nama_user`, `jenis_user`, `email`, `password`, `tanggal_lahir`, `no_hp`, `jenis_kelamin`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'Admin', 'admin@gmail.com', '$2y$10$zWNLVStetOEWMp6qnx/0iO3vD/YI8unYZstKQPV7Odq0sDFNSO.CS', '2000-07-24', '08123456789', 'Laki-laki', '4A2u2FwsIRVaHc68vLkITpCkO7AQ6vGKZPRRbHDeMC6ThHOvRaeeOqplxXVp', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_wilayah`
--

CREATE TABLE `tb_wilayah` (
  `id_wilayah` int(10) UNSIGNED NOT NULL,
  `nama_wilayah` varchar(225) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `full_admin` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `role`, `full_admin`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@gmail.com', '$2y$10$zWNLVStetOEWMp6qnx/0iO3vD/YI8unYZstKQPV7Odq0sDFNSO.CS', 'admin', '1', 'E5zlxI2u1liNr7lp0t8dHSnoTf3XjAkpFAJ5VPat8FPZYw7FfQRwHdW8GVDy', '2017-08-14 20:21:39', '2017-08-14 20:21:39'),
(2, 'astradanta', 'astra.danta@gmail.com', '$2y$10$ZMIdwS1CVhBFTS8uNYis8OMEcXWC5IA84AMZ8bVh7eaONAWKULz3C', 'admin', '0', 'eMCz5OYbWb93Q69kAfuMWDw6eKtCCSzWkqJxxTmialQ3pn6AEHOHCKhaKyqW', '2018-10-11 15:25:56', '2018-10-11 15:25:56'),
(3, 'gung', 'nachahppc@gmail.com', '$2y$10$r.uYw8Nm5Xpb/6Sx/vSes.si61EoSukhzMzTjrBxaRv7CimXNJKra', 'customer', '0', 'LzMwdodh5Pl7d97WZvCf9O3GYEq1qUEcLc2vO8piFisjpJLRcSV45NE9segA', '2018-10-25 15:54:50', '2018-10-25 15:54:50'),
(4, 'Bang Joker', 'joker@gmail.com', '$2y$10$w3JVoBV8EVmS6eWY.WvrDOldkAWJ2NtemCcinszKMl8yv1Du0w0nG', NULL, '0', NULL, '2018-11-03 08:57:25', '2018-11-03 08:57:25'),
(5, 'asd', 'asd@gmail.com', '$2y$10$zWNLVStetOEWMp6qnx/0iO3vD/YI8unYZstKQPV7Odq0sDFNSO.CS', NULL, '0', NULL, '2018-11-09 20:25:10', '2018-11-09 20:25:10'),
(6, 'Yodie Aryantika', 'rajyayodie@gmail.com', '$2y$10$fOftwna2OAdZMiD0MbIki.KBt0MdoT/K8yaQQ.9zA8YrXterhb7WO', NULL, '0', NULL, '2019-01-23 16:01:55', '2019-01-23 16:01:55'),
(7, 'Bondan Noviada', 'noviadarkbondan@gmail.com', '$2y$10$V/AaA6sUPcDtHJG6uNP6Tul1qwnR0/DgAMOzGKBzTcXyZFj//9em6', NULL, '0', NULL, '2019-06-03 23:36:52', '2019-06-03 23:36:52'),
(8, 'Ni Made sukerti', 'sukerti_kadek@yahoo.com', '$2y$10$sAJUK4LR6qOzKhOLZAlNRuIoaO7weWGibK2eLaBmjFN5WY76TTluO', NULL, '0', NULL, '2019-06-08 07:44:40', '2019-06-08 07:44:40');

-- --------------------------------------------------------

--
-- Table structure for table `web_config`
--

CREATE TABLE `web_config` (
  `id` int(10) UNSIGNED NOT NULL,
  `config_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `web_config`
--

INSERT INTO `web_config` (`id`, `config_name`, `value`, `created_at`, `updated_at`) VALUES
(1, 'kue_pagination', '6', NULL, NULL),
(2, 'blog_pagination', '4', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `article_category`
--
ALTER TABLE `article_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_messages`
--
ALTER TABLE `contact_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `emails`
--
ALTER TABLE `emails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images_category`
--
ALTER TABLE `images_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sociallinks`
--
ALTER TABLE `sociallinks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_alamat`
--
ALTER TABLE `tb_alamat`
  ADD PRIMARY KEY (`id_alamat`),
  ADD KEY `oriscake_alamat_user` (`id_user`);

--
-- Indexes for table `tb_biaya_delivery`
--
ALTER TABLE `tb_biaya_delivery`
  ADD PRIMARY KEY (`id_biaya_delivery`);

--
-- Indexes for table `tb_cake_image`
--
ALTER TABLE `tb_cake_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oriscake_image_cake` (`id_cake`);

--
-- Indexes for table `tb_cake_rasa`
--
ALTER TABLE `tb_cake_rasa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_rasa` (`id_rasa`),
  ADD KEY `id_kue` (`id_kue`);

--
-- Indexes for table `tb_cake_tag`
--
ALTER TABLE `tb_cake_tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_cart`
--
ALTER TABLE `tb_cart`
  ADD PRIMARY KEY (`id_cart`) USING BTREE;

--
-- Indexes for table `tb_cart_detail`
--
ALTER TABLE `tb_cart_detail`
  ADD PRIMARY KEY (`id_detail_cart`) USING BTREE,
  ADD KEY `oriscake_order_detail` (`id_cart`);

--
-- Indexes for table `tb_cart_edible`
--
ALTER TABLE `tb_cart_edible`
  ADD PRIMARY KEY (`id_cart_edible`) USING BTREE,
  ADD KEY `oriscake_edible_order` (`id_cart`);

--
-- Indexes for table `tb_category`
--
ALTER TABLE `tb_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_detail_user`
--
ALTER TABLE `tb_detail_user`
  ADD PRIMARY KEY (`id_detail_user`);

--
-- Indexes for table `tb_history_order`
--
ALTER TABLE `tb_history_order`
  ADD PRIMARY KEY (`id_history_order`);

--
-- Indexes for table `tb_konfirmasi_pembayaran`
--
ALTER TABLE `tb_konfirmasi_pembayaran`
  ADD PRIMARY KEY (`id_konfirmasi_pembayaran`),
  ADD KEY `oriscake_konfirmasi_tagihan` (`id_tagihan`);

--
-- Indexes for table `tb_kue`
--
ALTER TABLE `tb_kue`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oriscake_kue_category` (`id_category`),
  ADD KEY `oriscake_kue_type` (`id_type`);

--
-- Indexes for table `tb_master_delivery`
--
ALTER TABLE `tb_master_delivery`
  ADD PRIMARY KEY (`id_master_delivery`) USING BTREE;

--
-- Indexes for table `tb_nota`
--
ALTER TABLE `tb_nota`
  ADD PRIMARY KEY (`id_nota`),
  ADD KEY `oriscake_nota_tagihan` (`id_tagihan`);

--
-- Indexes for table `tb_order`
--
ALTER TABLE `tb_order`
  ADD PRIMARY KEY (`id_order`);

--
-- Indexes for table `tb_order_detail`
--
ALTER TABLE `tb_order_detail`
  ADD PRIMARY KEY (`id_detail_order`),
  ADD KEY `oriscake_order_detail` (`id_order`);

--
-- Indexes for table `tb_order_edible`
--
ALTER TABLE `tb_order_edible`
  ADD PRIMARY KEY (`id_order_edible`),
  ADD KEY `oriscake_edible_order` (`id_order`);

--
-- Indexes for table `tb_payment_channel`
--
ALTER TABLE `tb_payment_channel`
  ADD PRIMARY KEY (`id_chanel`);

--
-- Indexes for table `tb_rasa`
--
ALTER TABLE `tb_rasa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_tag`
--
ALTER TABLE `tb_tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_tagihan`
--
ALTER TABLE `tb_tagihan`
  ADD PRIMARY KEY (`id_tagihan`),
  ADD KEY `oriscake_tagihan_order` (`id_order`);

--
-- Indexes for table `tb_transaction`
--
ALTER TABLE `tb_transaction`
  ADD PRIMARY KEY (`id_transaction`);

--
-- Indexes for table `tb_type`
--
ALTER TABLE `tb_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_ukuran_tart_cake`
--
ALTER TABLE `tb_ukuran_tart_cake`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_kue` (`id_kue`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `tb_wilayah`
--
ALTER TABLE `tb_wilayah`
  ADD PRIMARY KEY (`id_wilayah`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_config`
--
ALTER TABLE `web_config`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `article`
--
ALTER TABLE `article`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `article_category`
--
ALTER TABLE `article_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `contact_messages`
--
ALTER TABLE `contact_messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=283;

--
-- AUTO_INCREMENT for table `emails`
--
ALTER TABLE `emails`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `images_category`
--
ALTER TABLE `images_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sociallinks`
--
ALTER TABLE `sociallinks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tb_alamat`
--
ALTER TABLE `tb_alamat`
  MODIFY `id_alamat` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_biaya_delivery`
--
ALTER TABLE `tb_biaya_delivery`
  MODIFY `id_biaya_delivery` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_cake_image`
--
ALTER TABLE `tb_cake_image`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=454;

--
-- AUTO_INCREMENT for table `tb_cake_rasa`
--
ALTER TABLE `tb_cake_rasa`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_cake_tag`
--
ALTER TABLE `tb_cake_tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=588;

--
-- AUTO_INCREMENT for table `tb_cart`
--
ALTER TABLE `tb_cart`
  MODIFY `id_cart` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tb_cart_detail`
--
ALTER TABLE `tb_cart_detail`
  MODIFY `id_detail_cart` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tb_cart_edible`
--
ALTER TABLE `tb_cart_edible`
  MODIFY `id_cart_edible` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tb_category`
--
ALTER TABLE `tb_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `tb_detail_user`
--
ALTER TABLE `tb_detail_user`
  MODIFY `id_detail_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tb_history_order`
--
ALTER TABLE `tb_history_order`
  MODIFY `id_history_order` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tb_konfirmasi_pembayaran`
--
ALTER TABLE `tb_konfirmasi_pembayaran`
  MODIFY `id_konfirmasi_pembayaran` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_kue`
--
ALTER TABLE `tb_kue`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=401;

--
-- AUTO_INCREMENT for table `tb_master_delivery`
--
ALTER TABLE `tb_master_delivery`
  MODIFY `id_master_delivery` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_nota`
--
ALTER TABLE `tb_nota`
  MODIFY `id_nota` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_order`
--
ALTER TABLE `tb_order`
  MODIFY `id_order` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `tb_order_detail`
--
ALTER TABLE `tb_order_detail`
  MODIFY `id_detail_order` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `tb_order_edible`
--
ALTER TABLE `tb_order_edible`
  MODIFY `id_order_edible` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_payment_channel`
--
ALTER TABLE `tb_payment_channel`
  MODIFY `id_chanel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `tb_rasa`
--
ALTER TABLE `tb_rasa`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tb_tag`
--
ALTER TABLE `tb_tag`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `tb_tagihan`
--
ALTER TABLE `tb_tagihan`
  MODIFY `id_tagihan` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_transaction`
--
ALTER TABLE `tb_transaction`
  MODIFY `id_transaction` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `tb_type`
--
ALTER TABLE `tb_type`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tb_ukuran_tart_cake`
--
ALTER TABLE `tb_ukuran_tart_cake`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_wilayah`
--
ALTER TABLE `tb_wilayah`
  MODIFY `id_wilayah` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `web_config`
--
ALTER TABLE `web_config`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_alamat`
--
ALTER TABLE `tb_alamat`
  ADD CONSTRAINT `oriscake_alamat_user` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_cake_image`
--
ALTER TABLE `tb_cake_image`
  ADD CONSTRAINT `oriscake_image_cake` FOREIGN KEY (`id_cake`) REFERENCES `tb_kue` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_cake_rasa`
--
ALTER TABLE `tb_cake_rasa`
  ADD CONSTRAINT `id_kue` FOREIGN KEY (`id_kue`) REFERENCES `tb_kue` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `id_rasa` FOREIGN KEY (`id_rasa`) REFERENCES `tb_rasa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_konfirmasi_pembayaran`
--
ALTER TABLE `tb_konfirmasi_pembayaran`
  ADD CONSTRAINT `oriscake_konfirmasi_tagihan` FOREIGN KEY (`id_tagihan`) REFERENCES `tb_tagihan` (`id_tagihan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_kue`
--
ALTER TABLE `tb_kue`
  ADD CONSTRAINT `oriscake_kue_category` FOREIGN KEY (`id_category`) REFERENCES `tb_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `oriscake_kue_type` FOREIGN KEY (`id_type`) REFERENCES `tb_type` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Constraints for table `tb_nota`
--
ALTER TABLE `tb_nota`
  ADD CONSTRAINT `oriscake_nota_tagihan` FOREIGN KEY (`id_tagihan`) REFERENCES `tb_tagihan` (`id_tagihan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_order_detail`
--
ALTER TABLE `tb_order_detail`
  ADD CONSTRAINT `oriscake_order_detail` FOREIGN KEY (`id_order`) REFERENCES `tb_order` (`id_order`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_order_edible`
--
ALTER TABLE `tb_order_edible`
  ADD CONSTRAINT `oriscake_edible_order` FOREIGN KEY (`id_order`) REFERENCES `tb_order` (`id_order`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_tagihan`
--
ALTER TABLE `tb_tagihan`
  ADD CONSTRAINT `oriscake_tagihan_order` FOREIGN KEY (`id_order`) REFERENCES `tb_order` (`id_order`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_ukuran_tart_cake`
--
ALTER TABLE `tb_ukuran_tart_cake`
  ADD CONSTRAINT `oriscake_kue_tart_ukuran` FOREIGN KEY (`id_kue`) REFERENCES `tb_kue` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
